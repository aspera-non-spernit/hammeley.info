---
title: Grüzi und Hallå
layout: default
---
Nach 37 Monaten und 22 Tagen in Frankreich zieht **hammeley.info** ein weiteres Mal um. Diesmal zum schweizerischen Internetdienstanbieter
[hoststar.ch](https://www.hoststar.ch/de) und dem schwedischen Registrar [namesrs.com / nordreg.se](https://namesrs.com/nordreg).

Bewirtet wird **hammeley.info** nun auf gitlab Pages. Elektronische Post wird über die bekannte Adresse ab nun über [proton.me](https://proton.me) empfangen und versendet.
