---
title: "Die Schwurbel-Universität"
layout: default
---
# ipse se nihil scire id unum sciat

Willkommen an der Schwurbeluniversität. "Ich weiß das ich nichts weiß" ist das Motto der Schwurbeluniversität.

## (Exemplarische) Studiengänge

Im Aufbau:

- [Philosophie (englisch)](/schwurbelu/philosophy/philosophy-preparation)
- [Interdisziplinäre Studien](/schwurbelu/interdisciplinary/interdisziplinaere-studien)

## Vorteile und Nachteile eines Schwurbelstudiums

**Vorteile**

Menschen, die keine Möglichkeit haben, an einer herkömmlichen Universität zu studieren, weil ihnen die formalen Voraussetzungen fehlen, die sie nicht nachholen wollen oder können, finden an der Schwurbeluniversität ebenso die Möglichkeit, ihren Wissensdurst zu stillen, wie selbstdenkende Schwurbler, die aus politischen Gründen an einem Studium an einer herkömmlichen Universität gehindert und diskriminiert wurden oder werden.

Ein Schwurbelstudium bietet die Möglichkeit, ein Thema von Interesse flexibel und in beliebiger Tiefe zu studieren.

Es gibt keine unorganisierten Professoren.

Man muss weder das Kommunistische Manifest unterschreiben noch sich der Gendersprache aussetzen.

**Nachteile**

Ein Schwurbelstudium ist wesentlich anspruchsvoller als ein herkömmliches Studium. Der Studienerfolg hängt in hohem Maße von der eigenen Bereitschaft ab, sich mit den Themen auseinanderzusetzen.

Die gesellschaftliche Anerkennung eines Schwurstudiums ist derzeit nicht gegeben. Der Schwurbelabsolvent bleibt aus Sicht der Mehrheitsgesellschaft und der mit ihr assoziierten Minderheiten ein Schwurbelabsolvent.

Die Frage, ob ein Schwurbelstudium ein gleichwertiger Ersatz für ein traditionelles Studium ist, ist nicht geklärt und wird derzeit vom Institut für Schwurbelforschung intensiv erforscht.

## Hilfreiches

### [Institut für Schwurbelforschung](/schwurbelu/ifsf/institut-fuer-schwurbelforschung)

Das Institut für Schwurbelforschung führt exemplarisch verschiedene Studiengänge durch und stellt diese als Muster zur Verfügung, welche als Grundlage für das eigene Studium genutzt werden können (Tutor). Es kann eine erste Anlaufstelle für neue Schwurbelstudenten sein, wenn sie Probleme mit ihrem Studium haben. Aber, zu jeder Zeit ist der Schwurbelstuduent gleichzeitig selbst Mitglied des Forschungsteams und somit Forscher.

### Betrachtung aus verschiedenen Perspektiven

Es mag auf den ersten Blick so scheinen, als ob es der Schwurbelstudium an Tiefe mangelt. Der Versuch, mit immer gleichen Fragen vertiefende Informationen zu erhalten, wird wahrscheinlich erfolglos bleiben. Es empfiehlt sich daher, auch Teilbereiche des Studiums zu beleuchten, die zunächst nicht interessant erscheinen.

Mit fortschreitendem Studium fügen sich die einzelnen Wissensbereiche dann zu einem Gesamtwissen zusammen, das dem eines Absolventen einer traditionellen Universität entsprechen kann.

### Fachübergreifende Grundlagen

Unabhängig davon, welches Studium an der Schwurbel-Universität begonnen werden soll, wird empfohlen, den Kurs ["Epistemology (englisch)"](/schwurbelu/philosophy/introduction-to-epistemology) zu besuchen. Das Wissen darüber, was Wissen ist und was nicht, gilt als Grundvoraussetzung für jedes Studium. Das Wissen, das während des Studiums an der Schwurbeluniversität erworben wird, mag auf den ersten Blick objektiv, umfassend und neutral erscheinen. Es kann sich jedoch von Student zu Student durch persönliche Erfahrung, kulturelle Prägung, individuelle Wahrnehmung und Interpretation von Informationen und - für die Studenten der Schwurbeluniversität besonders wichtig - durch die Art der gestellten Fragen deutlich unterscheiden.

Menschen sind voreingenommen. Dies kann dazu führen, dass Suggestivfragen gestellt werden, die zwar vom GPT-3 richtig beantwortet werden, das Ergebnis aber vom Studenten individuell interpretiert wird, z.B. weil wichtige Informationen fehlen.

Der Kurs ["Epistemology"](/schwurbelu/philosophy/introduction-to-epistemology) kann helfen, ein aufgenommenes Studium möglichst objektiv und neutral zu erfahren. Es kann durchaus sinnvoll sein, in einer Diskussion mit GPT-3, eine gegenteilige Position einzunehmen oder Erkenntnisse aus anderen Bereichen in eine Frage einfließen zu lassen, um neue Einblicke zu erhalten.

Der Kurs ["Epistemology" (englisch)](/schwurbelu/philosophy/introduction-to-epistemology) kann dabei helfen, ein Studium so objektiv und neutral wie möglich zu erfahren. Es kann durchaus sinnvoll sein, in einer Diskussion mit GPT-3 eine entgegengesetzte Position einzunehmen oder Erkenntnisse aus anderen Bereichen in eine Fragestellung einfließen zu lassen, um neue Einsichten zu gewinnen.

Ebenso kann es sinnvoll sein, mathematische, statistische Grundlagen (z.B. Bayes-Theorem) zu behandeln, sobald diese in einem ChatGPT-Seminar erwähnt werden.