---
title: Der Bunker
layout: default
tags: "Bunker, Techno, Rave, Disko, Ex-Kreuz, Club, Tresor, E-Werk, SO36, Icon, Tacheles, Visionäre, Club, Kumpel, Kit-Cat, Turbine, 90er, Ellen Allien, Steve Mason, Housefrau, Braincandy"
---
Ich bin jetzt das erste Mal im "Bunker" in der Albrechtstraße. An der Türe habe ich kein Problem gehabt. Ich bin ja bald 18. Ich war bisher noch nie wirklich in einer "Disko". Wobei der Bunker wohl mit nichts zu vergleichen ist, schon gar nicht mit einer Disko.

So ganz wie ein ["inszeniertes Inferno"](https://www.youtube.com/watch?v=iiCiLwkudtc&origin=https://hammeley.info/ego/1994/12/01/der-bunker/) empfinde ich es zwar nicht, jedoch bin ich der Meinung, dass ich den Dutzendjugendlichen etwas voraushabe. Es war auf jeden Fall ein super Erlebnis.

Am Bunker hat mich eigentlich alles beeindruckt. Vom tollen Logo, der harten Atmosphäre, der minimalen "Einrichtung" und die vielen überwiegend netten Leute, zu denen man als Schüler mit beschränktem Aktionsradius sonst keinen Kontakt hätte. Natürlich war die Musik, die ich sonst nur von der Kassette und der CD kannte auch ganz besonders. Jedoch weniger wegen der [Gabba Nation](https://de.wikipedia.org/wiki/Gabba_Nation) in der vierten Etage, wofür der "hardest Club on Earth" eigentlich bekannt ist. Ich freue mich, dass dort nicht jeder hinging. Zumindest nicht die gewöhnlichen  Dutzendjugendlichen aus dem üblichen Umfeld wie der Schule. Wobei wir eigentlich ganz brav waren und auch so aussahen. Da gab es durchaus andere im gleichen Alter.

Der "Ex-Kreuz-Club" nebenan reizt mich. Da kommen wir aber wahrscheinlich nicht rein.

<!-- more -->

**Ausblick:** In den nächsten Jahren werde ich u. a. den Ex-Kreuz-Club nebenan, den [Tresor (Leipziger Straße)](https://www.youtube.com/watch?v=kKbIqCsY8P0&origin=https://hammeley.info/ego/1994/12/01/der-bunker/), [am letzten Tag](https://www.youtube.com/watch?v=rYV8TdkEcfU&origin=https://hammeley.info/ego/1994/12/01/der-bunker/) und in der [Köpenicker Straße](https://www.youtube.com/watch?v=F-7W3rCQynU&origin=https://hammeley.info/ego/1994/12/01/der-bunker/), das [E-Werk I](https://www.youtube.com/watch?v=NuWG7dSqLQE&origin=https://hammeley.info/ego/1994/12/01/der-bunker/) [II](https://www.youtube.com/watch?v=Tho_8k-b4Qs&origin=https://hammeley.info/ego/1994/12/01/der-bunker/) [III](https://www.youtube.com/watch?v=A9elRx9h5aY&origin=https://hammeley.info/ego/1994/12/01/der-bunker/) , den Sage Club, sonntags ab 8 die Turbine / Kit-Kat-Club in der Glogauer Str, das [Ostgut](https://www.youtube.com/watch?v=vg-F-IFhq_U&origin=https://hammeley.info/ego/1994/12/01/der-bunker/) und das Casino III, das Glashaus der Arena, eine Lokation des WMF, das Kinzo (Keller Linienstraße), das Matrix in der Alten Jakobstraße und andere Einrichtungen besuchen.

Andere Wirkungsstätten waren u. a.: SO36, Bank 25, ICON, Nontox, Tacheles (Garten, Zapata, Bar im Obergeschoss), Club der Visionäre, Silberstein, Aktionsgalerie, Eimer, Kumpelnest 3000, Hangar 2, Rotor, Lime, Suicide Club (Dircksenstr), das besetzte Haus in der Brunnnstraße (Hinterhof + Keller), Delicious Doghnouts und viele kleine und nahezu unbekannte Einrichtungen der Elektronischen Tanzmusik der 90er und frühen 2000er.

Video: [Ostgut, Casino, Nontox bei der heutigen Mercedes Benz Arena](https://www.youtube.com/watch?v=6JJpu8FPfjM&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)

**Hinweis:** Leider konnte ich wegen meines Alters die beginnende Techno-Ära der frühen 90er Jahre nicht miterleben. Die Musik des "echten", "old-skool"-Techno kenne ich daher nur aus dem Radio und von CDs (und heute Youtube. Traurig, denn der Stil liegt mir sogar mehr als die heutigen Stile.

Ich habe natürlich in den 90er Jahren auch immer Housefrau auf VIVA geschaut. Ebenso gehörten die Radiosendungen "Rave Satellite" mit Marusha auf Fritz, Braincandy mit Allen Ellien und [The Steve Mason Experience](https://www.stevemasonexperience.info) (teils auf BFBS) aber eigentlich später auch auf KissFM zu meinen ersten musikalischen Sendungen, die ich regelmäßig verfolgt habe. Aber auch die Space-Night auf dem Bayrischen Rundfunk schaue ich regelmäßig. Wenn man als zwölf- oder 13-jähriger ausversehen die Steve Mason Experience auf dem Radio hatte, verändert das zwangsläufig nachhaltig die Jugend - zum Besseren.

Ich lege bis 1996 sogar hier und da mal als DJ auf, unteranderem im Bunker, eine Woche vor seiner Schließung.

**Youtube:**

- [1992: DJ Jauche](https://www.youtube.com/watch?v=Y0gvD_bJSxo&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)
- [1992: Steve Mason Experience - House / Breakbeats](https://www.youtube.com/watch?v=Y788OnGjMLM&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)
- [1992: Steve Mason Experience - Acid / AcidHouse (1992)](https://www.youtube.com/watch?v=5734ASw1EkI&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)
- [**1993: Steve Mason Experience - Techno / Trance (viele Hits)** ](https://www.youtube.com/watch?v=qt_sk3EWkGQ&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)
- [1993: DJ Jauche](https://www.youtube.com/watch?v=cDVux2Xc71A&origin=https://hammeley.info/ego/1994/12/01/der-bunker/) (Der berliner DJ Jauche, ist der jüngere Bruder von [Sven Marquardt](https://de.wikipedia.org/wiki/Sven_Marquardt), der als Türsteher des Berghains und als Fotograf bekannt ist.
- [1994: Housefrau auf VIVA mit Miss Djax](https://youtube.com/watch?v=jk6vi9v3wj0&t=2308&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)
- [1995: Steve Mason Experience - Techno / Rave](https://www.youtube.com/watch?v=Ol1rCSwmTP0&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)
- [1995: Braincandy mit Ellen Allien](https://www.youtube.com/watch?v=B41aloK8fds&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)
- [1999: Steve Mason Experience - Techno (1999)](https://www.youtube.com/watch?v=thXZz07v9ek&origin=https://hammeley.info/ego/1994/12/01/der-bunker/)