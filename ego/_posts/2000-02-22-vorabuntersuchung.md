---
title: Vorabuntersuchung
layout: default
tags: "Bundeswehr, Verpflichtung, Pilot, Offizier"
---
Ich kann mir gut vorstellen, als Flusicherungskontrolloffizier zu arbeiten. Ich werde ja wohl weder Astronaut noch Pilot werden. Der Chef möchte eine ärztliche Voruntersuchung.

Es stellt sich heraus, dass mein Studium des Lebens in den Jahren nach der Schule und vor dem Wehrdienst nicht positiv aufgenommen wurde. Es wird mich an die Verwendungsausschlüsse erinnert und diese begründet.

Das war's also mit dem Kontrolloffizier.
