---
title: Deutschland siegrreich
layout: default
tags: "Corona, Schwurbler, Gesundheit, Pandemie, Virus, test, Tote, Freiheit, Demokratie, Politik, Impfpflicht, Masken, test, Verweiger, Bill Gates, Medien, Studium, Uni, menschenfeind, Luca"
---
Trotz aller Widrigkeiten und Schwurblern wie mir verkündet nach fast 3,5 Jahren der [Gesundheitsminister den Sieg über die Corona-Pandemie](https://www.twitter.com/Karl_Lauterbach/status/1643625424625229826?cxt=HHwWhICzqbGnqs8tAAAA). Während dieser schwierigen Zeit war ich als Ungetesteter, Ungenesener und Ungeimpfter allein verantwortlich für:

- [Grenzschließungen innerhalb Europas](https://www.google.com/search?q=corona+grenzschlie%C3%9Fungen)
- [Reiseverbote zwischen den Bundesländern](https://www.google.com/search?q=corona+mecklenburg+vorpommern+Einreise+Tagestouristen)
- Maskenpflicht in Räumen und im Freien
- Abstandsregelungen, drinnen und draußen
- 3G am Arbeitsplatz
- 2G Regelungen
- [Ausgangssperren](https://www.google.com/search?q=corona+Ausgangssperren)
- Lockdowns
- Maskenkontrollen durch Bundespolizei mit Maschinenpistolen
- Sperrung von Sport- und Spielplätzen
- [elektronische Bewegungsprofile dank Luca-App](https://www.google.com/search?q=Luca-App+Corona)
- [Demonstrationsverbote](https://www.google.com/search?q=Demonstrationsverbote+Corona)
- für [Besuchsverbote](https://www.google.com/search?q=besuchsverbote+Corona)
- Eingesperrte im Altenheim
- für die Isolationspflicht von Geimpften und Getesteten
- für die Freiheitseinschränkungen von Geimpften
- für die [gespaltene Gesellschaft](https://www.google.com/search?q=Corona+gespaltene+Gesellschaft)
- und die ganzen Querdenker, [Staatsfeinde](https://www.google.com/search?q=Corona+Staatsfeinde) und die ganzen Menschenfeinde
- für die vielen Toten in Deutschland, Europa und in der Welt (insbesondere die Geimpften tun mir leid)
- für [jahrelangem Impfschutz](https://www.google.com/search?q=Corona+Impfung+jahrelanger+Schutz) nach einer, zwei, drei, vier oder fünf [nebenwirkungsfreien](https://www.google.com/search?q=corona+Impfung+Nebenwirkungsfrei) Impfungen
- für die Impfpflicht in Gesundheitssektor
- und natürlich auch für den wirtschaftlichen Schaden
- für den [§28a im Infektionsschutzgesetz](https://www.gesetze-im-internet.de/ifsg/__28a.html)

Ich möchte mich von ganzem Herzen entschuldigen. Vor allem bei den Hinterbliebenen der Coronatoten. Ich distanziere mich hiermit ausdrücklich von meinem devianten, staatszersetzenden Verhalten. Ohne Menschen wie mich hätte es keine oder nur wenige Coronatoten gegeben.

Nach reiflicher Überlegung habe ich es endlich eingesehen. Ich bin ein Tyrann, ein [Terrorist](https://www.google.com/search?q=Corona-RAF), ein [Sozialschädling](https://www.google.com/search?q=Corona+Sozialsch%C3%A4dling) und [Staatsfeind](https://www.google.com/search?q=Corona+Staatsfeind). Ich bin der [Blinddarm der Gesellschaft](https://www.google.com/search?q=Corona+Blinddarm+der+Gesellschaft). Ich habe Millionen Tote auf dem Gewissen. Ich bin allein dafür verantwortlich, dass die Geimpften ihrer Freiheit beraubt wurden und sich regelmäßig isolieren mussten.

Nur wegen mir und meinem Einfluss auf einige verwirrte Parlamentarier ist es nicht zu einer allgemeinen Impfpflicht gekommen. Das sind, wie ich, [Menschenfeinde](https://www.google.com/search?q=Corona+Menschenfeinde). Aber ich hoffe inständig, dass Uschi Glas, Mai - Thi Leiendecker, der Ärztepräsident und die vielen, vielen Menschen aus Funk und Fernsehen, Politik und Wissenschaft und Kunst und Kultur bei der nächsten Pandemie endlich eine Impfpflicht durchsetzen. Nur so kann die Menschheit gerettet werden. Denn eine Impfpflicht führt dazu, dass sich die Menschen freiwillig impfen lassen.

<!-- more -->
Für mein Fehlverhalten wurde ich gerecht bestraft. Mein 2019 begonnenes Studium, das ich jetzt auch in Teilzeit beendet hätte, durfte ich zu Recht nicht beenden. Mein Gehalt wurde einbehalten und ich habe mehrere tausend Euro Schulden angehäuft. Als Masken-, Impf- und Testverweigerin habe ich jahrelang keinen Supermarkt, kein Geschäft und keinen anderen Laden betreten. Die Ächtung durch die Gesellschaft habe ich mir redlich verdient. Ich bin asozial. Ich möchte aber auf Vergleiche verzichten, weil ich erkannt habe, dass mit jedem Vergleich, den ich ziehe, ein rechtsradikaler Staats- und Demokratiefeind aus mir spricht.

Sicher ist bei dieser Pandemie nicht alles gut gelaufen. Sie kam so überraschend, dass die Politik einfach handeln musste. Das Virus war so neu, dass die Verunsicherung groß war. Ich verstehe das.
 
[Lauterbach hat Recht](https://mobile.twitter.com/ARD_BaB/status/1643999972420104193?cxt=HHwWgsDR7ffQ1NAtAAAA). Die Schließung der Kindergärten war eine Fehlentscheidung der Politik und könnte hier und da noch verbessert werden. Alle anderen Maßnahmen, so sehen es auch die Gerichte, waren richtig. Natürlich muss die Ausnahmesituation jetzt noch ein, zwei Wochen aufgearbeitet werden. Aber das geht auch ohne die Öffentlichkeit. Wir sollten die Pandemie jetzt einfach hinter uns lassen und wieder an die Arbeit gehen.

Zum Glück wurde die Zeit genutzt, um Gesetze auf den Weg zu bringen, damit Freiheitsbeschränkungen jetzt sofort ausgesprochen werden können. Nur durch Einschränkung der Freiheit, lässt sich die Freiheit sichern.

Auch die digitalen Zugangskontrollen sind etabliert. Technische Probleme wie bei der tollen Luca-App konnten behoben werden, so dass sie in Zukunft schnell einsatzbereit sein werden.

Ich will jetzt auch nicht um die [Deutungshoheit](https://www.zdf.de/comedy/bosetti-will-reden/bosetti-will-reden-vom-22-maerz-2023-104.html) kämpfen. Nein. Ich bin asozial und entschuldige mich von ganzem Herzen für das Leid, das ich über die Menschheit gebracht habe. Ich bin ein skrupelloser, egoistischer und liberal-hysterischer Mörder.

Ich möchte mich bei den Wissenschaftlern bedanken, die trotz der Hetzkampagne der Corona-Gegner standhaft geblieben sind und bis zum Schluss die einzige Wahrheit vertreten haben. Ich möchte mich bei Bill Gates bedanken, der sich die Zeit genommen hat, sich in den Nachrichten zu informieren und gerade noch rechtzeitig in Biontech investiert hat. Ich möchte mich bei den Betreibern der Impf- und Testzentren bedanken, die ohne Vorbereitung aus dem Nichts heraus überall im Land für Sicherheit gesorgt haben. Und natürlich danke ich den vielen Impfstoffherstellern für die nebenwirkungsfreien Impfstoffe, die auch bei Mehrfachimpfungen wirksam bleiben. Sie haben viele Leben gerettet, vor allem aber haben sie dafür gesorgt, dass die weniger Gefährdeten, die Jugendlichen, die Heranwachsenden, die Kinder und Kleinkinder ihren sozialen Beitrag zur Verhinderung der Pandemie leisten konnten, indem sie sich solidarisch zeigten und sich mehrfach impfen ließen.

Da ich mir eingebildet habe, in die Zukunft sehen zu können, gehe ich davon aus, dass ich diese Geisteskrankheit immer noch besitze und daher über die nächste Pandemie im Voraus informiert bin.

Für den Fall, dass ich es nicht vorher tue, biete ich euch an, mich am ersten Tag der nächsten Pandemie, sei es die tödliche Schweinegrippe oder ein neuartiges Coronavirus, umzubringen, damit ich für euch keine Gefahr mehr darstelle. Wie, weiß ich noch nicht, aber bis zur nächsten Pandemie oder Klimakrise habe ich sicher noch ein paar Tage oder Wochen Zeit zum Nachdenken. Der Teufel muss mit mir im Bunde sein, sonst hätte mich die Pest zu Recht längst dahingerafft.

Ihr sollt endlich in Freiheit leben können, in einer freien Gesellschaft ohne Ungeimpfte. Ich will euch die Angst nehmen, an eurem Glauben zu sterben, wenn ihr mir begegnet. Ihr seid das Rückgrat dieses Landes und der Menschheit.

Für mich ist das die richtige Entscheidung. Ich kann einfach nicht in so einer großartigen Demokratie und so einem solidarischen Land leben. Ich wünschte, ich könnte, aber ich bin ein dummer Aluhut und Covidiot. Außerdem stelle ich mir vor, dass die Freiheiten immer mehr eingeschränkt werden. Außerdem möchte ich nicht überall von Kameras beobachtet werden. Im Supermarkt, auf öffentlichen Plätzen, im Schwimmbad, in der U-Bahn. Ich habe auch ständig falsche und unreflektierte Meinungen, sei es zu Russland und der Ukraine, zur Umwelt oder zu den vielen Krisen, die unsere Politiker so gut meistern. Ich lebe einfach in der falschen Zeit, und das ist gut so.

Es hat sich in den letzten 20 Jahren so viel Schönes und Neues etabliert, aber einer wie ich von der alten Generation sollte endlich gehen, damit aus Deutschland, dem Land der Dichter und Denker, endlich etwas Neues und Besseres entstehen kann.

Mein vorzeitiges Ableben ist meine Wiedergutmachung für den Terror, den ich diesem Land angetan habe. Wenn jetzt jemand denkt: "Das klingt aber dramatisch." So ist es nicht gemeint. Als ich 1989 von West-Berlin aus gegen die Mauer gehämmert habe, habe ich nicht daran gedacht, wie rückständig ich 30 Jahre später mit dem Freiheitsbegriff umgehe, der sich in den letzten Jahrzehnten herausgebildet hat.
In den Wahlprogrammen taucht der Freiheitsbegriff zu Recht kaum noch auf. Ich als Artefakt einer alten, ewig gestrigen Welt finde keine Freude mehr an dieser neuen, absolut sicheren, gerechten, fairen und besseren Welt. Für mich ist der langsame Abschied also etwas, auf das ich mich freuen kann. Es ist eine Erlösung für beide Seiten. Jeder kennt das, wenn das fünfte Rad am Wagen endlich verschwindet und man keine Höflichkeit mehr vortäuschen muss.

Die notwendigen Überwachungsmaßnahmen und mehr Befugnisse für die staatlichen Organe werden wohl auch in Zukunft notwendig bleiben, denn es könnte ja sein, dass es außer mir noch andere Ungeimpfte gibt, die sich bei der nächsten Pandemie unter euch mischen wollen. Regelmäßige Impfbescheinigungen sind auch gut für die Partnerwahl. Ihr werdet Euch sicher bald daran gewöhnen.

Aber wenn irgendwann alle geimpften Menschen gestorben sind und die anderen in Sicherheit sind, dann könnt Ihr endlich in Freiheit und Frieden leben. Bleibt stark und arbeitet auf diesen Tag hin. aspera non spernit. Lasst euch nicht beirren, lasst euch nicht von den Hindernissen auf dem Weg zur völligen Sicherheit und Freiheit abschrecken, die euch von den zukünftigen Verrätern in den Weg gelegt werden. Folgt einfach den Führern. Sie werden Euch den Segen bringen.

Bitte sprecht mich bis zur nächsten Pandemie nicht an. Ich würde euer Leid, das ihr meinetwegen erdulden musstet und das bei einer Begegnung offensichtlich wird, nicht ertragen. Aber es ist nicht mehr lange hin bis zur nächsten Pandemie. Sie wird kommen. Hoffen wir gemeinsam darauf.

Wenn ihr mir verzeiht, dann gewährt mir diesen Wunsch.

Vielen Dank
