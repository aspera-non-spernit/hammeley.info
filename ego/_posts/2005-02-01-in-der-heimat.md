---
title: In der Heimat
layout: default
---
Meinem Antrag auf Versetzung nach Berlin wurde entsprochen. Ich leiste nun meine Restdienstzeit bei der 3. Lufttransportgruppe des Bundesministeriums der Verteidigung in Berlin-Tegel ab.

Hier habe ich noch weniger zu tun. Das liegt nicht nur daran, dass hier natürlich nur die Übergangsregelung gilt, von der hier noch niemand etwas gehört haben soll, sondern viel mehr daran, dass hier der Flugverkehr über die zivile Stelle des Flughafens Tegel koordiniert wird. Meine Tätigkeit ist hier eigentlich völlig überflüssig. Es gibt noch ein paar AH 7, die ein bisschen die VFR-Karte und die Flugregeln aktualisieren.

Ich habe hier einen unmöglichen Schichtdienst im wöchentlichen Wechsel. Für die Frühschicht stehe ich um 02:00 Uhr auf, um mit dem Nachtbus von Schöneberg nach Tegel zu fahren und um 04:00 Uhr mit der Arbeit zu beginnen. Wenn ich gegen 10.00 Uhr wieder zu Hause bin, muss ich erst einmal schlafen. Meistens so bis 16.00 Uhr. In neun Stunden muss ich dann wieder (unausgeschlafen) bei der Arbeit sein.

In meinem letzten Jahr bei der Bundeswehr werde ich im Dienst viel fernsehen. CSI ist gerade angesagt, aber auch Filme und Serien wie Band of Brothers werden auf DVD geschaut.

Zum Glück habe ich nur noch ein Jahr.

![Flughafen Berlin-Tegel (Nord)](){:data-src="/assets/images/tegel_grand.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }
