---
title: Erfassung von Wehrpflichtigen
layout: default
tags: "Bundeswehr, Wehrpflicht"
---
Ich erhielt heute Post vom Kreiswehrersatzamt Berlin. Als männlicher Deutscher bin ich nun als Wehrpflichtiger erfasst.

**Ausblick:** Ich werde auf die Ladungen zur Musterung mehrmals nicht reagieren und kann mich bis zur letzten Ladung zum 17.9.1997 vor einer Musterung drücken. Ich werde aufgrund meines hedonistischen Lebensstils und der labortechnischen Begleiterscheinungen als vorübergehend nicht wehrdienstfähig erklärt.

Ich habe jetzt erst einmal wieder bis zum 30.4.1999 Ruhe.
