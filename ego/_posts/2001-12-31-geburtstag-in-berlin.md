---
title: Geburtstag in Berlin
layout: default
tags: "Berlin, Geburtstag, Freundin"
---
Ich bin heute doch wieder in Berlin zu meinem Geburtstag, sogar mit Freundin aus Berlin, noch etwas jünger und hübscher als die zuvor, aber auch diese wird am nächsten Morgen wohl meine Ex sein.

Irgendwas scheint in meinem Leben nicht in Ordnung zu sein. Ich werd' ganz sicher nicht mehr meinen Geburtstag mit irgendwem feiern.

**Ausblick:** Mindestens bis in die 2020er bleibt dabei. Geburtstag wird nicht mehr gefeiert, zumindest nicht mit Personen, die ich kenne.
