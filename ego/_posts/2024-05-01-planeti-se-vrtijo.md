---
title: Planeti se vrtijo
layout: default
---
..in časi se spreminjajo.

<div id="carouselNL" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner ratio ratio-16x9">
        <div class="carousel-item active">
            <img data-src="/assets/images/nl/IMG_20240501_114700.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Snofitto">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240511_123136.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240516_103307.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240517_150730.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240518_122719.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240519_001605.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240520_130824.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240520_132223.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240520_144133.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240522_142227.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240522_143243.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240522_144005.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240523_202454.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240523_203220.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240523_203626.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240524_150444.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240524_151325.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240524_202526.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240524_202640.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240526_102729.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240526_104213.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240526_113939.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240531_123825.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240601_113613.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240601_121849.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240604_104455.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/nl/IMG_20240607_141249.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="">
            <div class="carousel-caption d-none d-md-block">
                <h5></h5>
                <p></p>
            </div>
        </div>
        <div class="carousel-item">
           <hmmly-player id="planeti-se-vrtijo" data-yt-vid="Pdl9wdxjzFw"></hmmly-player>
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselNL" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Vorheriges</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselNL" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Nächstes</span>
    </button>
</div>

Končno sem zapustil najboljšo državo na svetu z najboljšo demokracijo, najbolj izobraženimi in razsvetljenimi ljudmi ter najboljšimi politiki, najboljšimi strokovnjaki in po zaslugi najboljših znanstvenikov na svetu državo z najboljšimi akcijami "Covid" v vesolju. (Če želite preveriti ta dejstva, uporabite enega od programov za preverjanje dejstev, ki jih sponzorira vlada)

Vedno "boostiti" - Vaš prispevek k Nemčiji..

Od 1. maja živim v Republiki Sloveniji in od danes pa sem prijavljen v svoji novi domovini. Imam dovoljenje za prebivanje in EMŠO številko.

Zdaj potrebujem le še sanjsko ženo in stalno stanovanje.

- [Bled + Bohinj](https://www.youtube.com/watch?v=VJvmIrux7nY)
- [Izola](https://www.youtube.com/watch?v=koGkuiyJWnU)
- [Kotor, Montenegro](https://www.youtube.com/watch?v=a5TpFQx8D-M)
- [Ljubljana](https://www.youtube.com/watch?v=V41D7lsEruI)
- [Logarska Dolina](https://www.youtube.com/watch?v=LCUF1IshkHk)
- [Piran + Portorož](https://www.youtube.com/watch?v=7GuUXWWbUXY)
- [Postojna](https://www.youtube.com/watch?v=S5GufnjI4Rk)
- [Dolina Soče + Triglav](https://www.youtube.com/watch?v=9WZLouOK43U)
- [Škocjan](https://www.youtube.com/watch?v=9Xs60cMXuUY)
- [Tolmin Gorge](https:/www.youtube.com/watch?v=eXi_grgYWJM)
- [Triglav](https://www.youtube.com/watch?v=IFrLO4BhkK0)
- [Velenje](https://m.youtube.com/watch?v=LuyqgKJ44Cs)
- [Velika Planina](https://www.youtube.com/watch?v=ugDYWtiWHNM)
- [Vintgar Gorge](https://www.youtube.com/watch?v=1EXQdOzL5og)