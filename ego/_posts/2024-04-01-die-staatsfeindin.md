---
title: Die Staatsfeindin
layout: default
tags: politik, staatskritik, corona, gesellschaft
---
**Hinweis:** Dies ist kein Scherzbeitrag. Ich distanziere mich von allen Aussagen, welche die Gefühle von vernünftigen Impfspinn:enden und Maskenidiotinnen verletzen könnten.

Nachdem meine Existenz nach mehreren vorsätzlichen Versuchen endlich zerstört wurde und ich über die Fakten berichtete, wurde gegen mich Anzeige erstattet. Das wurde mir in einer elektronischen, aber nicht verifizierbaren Mitteilung der Abteilung Präv 4 (politisch motivierte Kriminalität) des LKA Berlin mitgeteilt. Ich habe ja keine Wohnung mehr, in der ich Post erhalten könnte. (hat die AG der möglichen Anzeig:enden selbst verschuldet)

Man bietet mir an, Beratungsstellen und die richtigen Träger zu finden, damit ich meine Schicksalsschläge bewältigen kann. Der beste und unfehlbare Links- und Asozialstaat lässt nichts unversucht. Wie damals. Ich glaube, ich lehne lieber ab.

Ich, der geschworen hat, die „Freiheit des Deutschen Volkes tapfer zu verteidigen“, bin nun eine beobachtungswürdige, aber zumindest aktenkundlich bekannte Staatsfeindin und eine politische Kriminelle. Ich werde mir ein rotes Dreieck an die Kleidung nähen, damit die vernünftigen und guten Deutschinnen gleich sehen, mit wem sie es zu tun haben.

Ob ich allerdings Nazi:in, Mitglied:in der linksextremistischen Corona-RAF oder aluhuttrag:ende Delegitimiererin des Staates bin, geht aus der Nachricht nicht hervor.

Das ist auch nicht so wichtig, denn neuerdings hängt das nicht mit den vermeintlichen Taten der beschuldigten Delinquentinnen zusammen, sondern vielmehr mit der aktuell amtierenden Regierung und ihrer politischen Ausrichtung. In Amerika, Polen oder Ungarn wäre das schlecht, hier ist das sehr, sehr gut.

Sehr gut ist auch meine Hoffnung, dass die Täterinnen, die meine Situation zu verantworten haben, beim guten Regierungswechsel gefeuert werden und Leistungen von Leistungsträgern verwehrt werden.

Nichts ist so konstant wie die Veränderung. Ich freue mich drauf.

Ebenso wenig geht aus der Mitteilung hervor, was denn der Straftatbestand sein soll. Irgendjemand fühlt sich wohl beleidigt und bedroht. Vielleicht, weil ich ungeimpft bin?

Ich erhalte ja keine Post und daher auch keine Anzeigen, in denen das stehen könnte (hat das JC und der Corona-Linksstaat selbst verschuldet).

Nach meinem Kenntnisstand wird das Verfahren also nach wenigen Monaten eingestellt.

Es gibt selten Momente, in denen ich auf mich stolz bin, aber als politische Kriminelle Post vom LKA eines in sich in Auflösung befindlichen ehemaligen Rechtsstaates zu bekommen, ist einer dieser Momente.

**Hinweis:** Ich (Nazi:in und Mitgliedin der Corona-RAF) zitiere nur frei den ehemaligen Verfassungsrichter (heute Demokratieschwurblerin) Papier und zahlreiche Andere mit ähnlicher, aber noch falscher Auffassung von Rechtsstaat und Demokratie.

**Rückblick:** Beim Schreiben dieser Zeilen habe ich das Deutsche Demokratische Reich bereits verlassen.

Ihr kennt das, die Dummen und Faulen, die Rechtsextremen verlassen aufstrebende Imperien immer als Erstes. Wie damals.

Ich habe auch an meinem derzeitigen Wohnsitz keine feste Adresse. Das haben die Mitarbeiterinnen des Corona-Linksstaats und des JC selbst verschuldet.

Man kann es auch positiver sehen als ich: Ich, die rechtsextreme Schwurblerin, räume meinen Platz für dringend gebrauchte und vor allem vernünftige, Fachkräft:ende aus dem Ausland. So ist das eine Win-Win-Win-Situation für alle.

Daher, aus dem bösen Ausland: viel Spaß beim Kriegspielen und viel Erfolg für Deutschland bei den kommenden Landtagswahlen.

**Achtung:** Das Lesen der folg:ende(n) Artikels durch Idiotinnen und Spinn:enden ist linksisch verboten:e. Maske aufsetzen und Webseite verlassen. Sofort, sonst rufe ich die Polizei.

- [Dr. Frank Greuel (pot. Delegitimierer) kritisiert die Präventionspraxis im besten Land der Welt](https://www.bpb.de/themen/infodienst/311923/zum-konzept-der-praevention/)
- [Hundertausende Rechtsextremisten verlassen endlich Deutschland](https://www.cicero.de/innenpolitik/auswandern-aus-deutschland-im-trend)
- [tagesschau.de verbreitet Lügen von Amnesty International zum Zustand der Demokratie und über den Umgang mit 'Andersde kern' im besten Land der Welt](https://www.tagesschau.de/ausland/europa/amnesty-meinungsfreiheit-europa-100.html)
- [Erfolgsmeldung der Agentur für Arbeit](/assets/images/Erfolgsmeldung_AA_20240710_135421.jpg)

**Sollte ich Kinder haben:**

Wenn ihr schon einmal mit dem Gedanken gespielt habt auszuwandern, es wäre jetzt der richtige Zeitpunkt. Es wird nicht viel besser. Das Leben ist zu kurz, um die Zeit mit der Hoffnung auf Verbesserung oder mit Zweifeln zu verschwenden.