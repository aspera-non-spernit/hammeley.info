---
title: Statistiken für die Stasi
layout: default
tags: "Amt, Statistik, Berlin, Brandenburg, programm, Repository, Version, Uni, Sonderzahlung, 77, AfS-BB, Coaching, Wahlen, Staat, Politik, Corona, Pandemie"

---
Ich bin beruflich im Amt für Statistik Berlin-Brandenburg gelandet. Das Gebäude und eigentlich alles hat einen DDR-Mief. Jede Tätigkeit ist zu schwer und wird erst einmal auf später verschoben. Der Tag beginnt bei den "Kollegen" mit Frühstück und Zeitungslektüre. Ich sauge mir Arbeit aus der Nase.

Hier wird angeblich programmiert. Es gibt aber weder ein zentrales "Repository" noch irgendeine Versionskontrolle oder sonst etwas, das auf eine Entwicklerumgebung hindeuten könnte.

Der "Kollege" M.S., dem ich sage, dass ich nicht glaube, dass das noch lange so weitergeht, nachdem in den letzten Jahren schon die Hälfte des Referats aufgelöst wurde, ist sauer und rennt sofort zum Referatsleiter O., wie ich später vom Referatsleiter erfahre. Ich solle mich mit solchen Äußerungen zurückhalten.

**Ausblick** Ich werde nach fünf Monaten noch vor Dezember, dem Monat in dem es eine Sonderzahlung gibt, entlassen. Ich bin nicht unglücklich. Dank meines sparsamen Lebenswandels kann ich meinen in Anspruch genommenen Dispositionskredit, mit dem ich teils die Gebühren für die Uni bezahlt habe, in kurzer Zeit ausgleichen.

<!-- more -->

Die "Leistungsträger" im AfS-BB bleiben natürlich, bis das Referat aus mir unerklärlichen Gründen ganz aufgelöst wird. Zu den Leistungsträgern  des Referats gehören unter anderem der Neffe G. des Leiters R.F. des AfS und sein Busenfreund M.S. Aber auch der Referatsleiter O., der nun ein anderes Referat übernommen hat und zwei andere Mitarbeiter gesetzten Alters u.a. H. Manchmal treffen sie sich mit anderen Leistungsträgern der Gesellschaft. Nicht konspirativ, sondern verschwiegen. So ist das in Deutschland. Leistung zahlt sich halt aus. Warum ich nach Abschluss meines Studiums nirgends eine Anstellung fand und ausgerechnet beim AfS gelandet bin und das alles weiß, gebe ich Euch als Rätsel auf. Warum ich seitdem keine Arbeit mehr finde. Ich vermute, weil ich ein dummer, fauler Schwurbler und Staatsfeind bin, der unsere tolle, korruptionsfreie Demokratie durcheinander bringt.

Das Beste Lacher zum Schluss: Ich war im Referat 77. Das Referat war u.a. für die Durchführung der Wahlen in den Bundesländern Berlin und Brandenburg zuständig. Als ich da war, gabs noch keinen Wahlskandal in Berlin. Man sieht ja was bei rauskommt, wenn alte Stasi-Kader und durch Vetternwirtschaft Bevorteilte [Wahlen durchführen](https://www.tagesschau.de/inland/innenpolitik/bundestagswahl-berlin-106.html).

Nun darf ich mich wieder den Fragen der Verwandten stellen: "Warum wurdest du entlassen?", "Warum arbeitest du nicht?"

Frau B., eine der Sachbearbeiterinnen mit befristeten Verträgen beim Jobcenter, die früher wahrscheinlich auch für die Stasi gearbeitet hat (zumindest inoffiziell) und meiner noch vor der "Plandemie" getroffenen Aussage "Wir bekommen einen faschistischen Überwachungsstaat" zustimmt, schickt mich zu Bewerbungstrainings und "Coaches", da ich offensichtlich mehrere Hindernisse habe, die einer Karriere in Superdeutschland im Wege stehen. Schließlich haben wir Fachkräftemangel. Die Coaches, allesamt selbst Berufsversager wollen scheinen geeignet, mich für den Arbeitsmarkt fit zu machen.

Coach ***Thomas K.*** von der ***inzeit Akademie*** erzähle ich, dass es in zwei Jahren eine "Pandemie" geben wird. Mehr als ein "ahja" bekomme ich nicht aus ihm heraus. Da ich schon mal bei inzeit war, und daher weiß, dass die Akten nicht wie vorgeschrieben, nach zwei Jahren vernichtet werden, gibt es dort vielleicht noch einen Hinweis. Wie er mir erzählt, hat er es als Schwuler in Berlin auch sehr schwer. Er soll in der Neuen Bahnhofstraße (Friedrichshain) wohnen und will mich wegen meines "Geschwurbels" gleich zu einer Psychologin schicken. Ich hätte wohl eine Anpassungsstörung. Diese Diagnose wird gene für alles mögliche gestellt, um Personen weiter zu degradieren. Besonders von Laien wie dem ehemaligen, erfolglosen Filmproduzenten Thomas K.

Die Psychologin F. L., die mit ***inzeit*** zusammenarbeitet, bei der ich aber nicht war, erinnert mich an die junge Frau, die ich damals im Sage Club kennengelernt hatte. Naja eigentlich war es ihr Freund, der später in Berlin ansässige DJ O. K. und mit diesem sie zumindest zeitweise verheiratet war. Der Typ war super nett. Die Frau scheint es ja auch gewesen zu sein. Wenn man einen Freund im Musikbiz hat, läufts vielleicht mit der eigenen Karriere besser. :)

Wenn ich mich nicht irre, hatte damals ein Tagebuch bei sich, das ich ihr abnahm, mit dem Versprechen, es ihr in 10 Jahren zurückzugeben. Das Tagebuch ist zwischenzeitlich in den Vereinigten Staaten von Amerika gelandet. Ich bin mir aber sicher, dass sie es zurückbekommen hat. Vorausgesetzt, es handelt sich um dieselbe Person und ich sage ab und zu die Wahrheit.
Super-Coach Thomas K., der mich wiederum an die Zeit in der Kneipe "Netzwerk" erinnert, kann das Coaching mit mir nicht erfolgreich abschließen, sodass das Jobcenter von ***inzeit*** Geld zurückfordert. Selbst schuld, bevor er mich für krank erklärte, meinte er bei einem anderen Coaching, ich würde stalken.

Für das dem Staat gesparte Geld müsste ich eigentlich Provision verlangen. Immerhin kosten solche Couchings zwischen 1.500€ - 6.000€.

**Hinweis:** Ich habe nie etwas aus dem Tagebuch gelesen.
