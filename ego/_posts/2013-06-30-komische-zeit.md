---
title: Komische Zeit
layout: default
tags: "Bar, Flugzeug, Geheimnisse, Terror, Berlin, Pandemie, Tiergarten, Medien, Netzwerk, Sonntagstraße, Corona, Virus, Lockdown, korrupt, Medizin, Charité, FDP, Kapitalist, neoliberal, Verächtlichmachung, Staatsfeind, Luca, Bundeswehr, Delegitimation, Terror, Gefährder, Krone, Schmalz, Russland, Fachkräftemangel, Politik, Chlor, Fake, News, Verschwörung"
---
Ich besuche die Bars der Gegend und treffe ständig auf Menschen, die mir "Geheimnisse" ins Ohr flüstern: Flugzeugabstürze in den Alpen und in Südostasien, geplante Terroranschläge in Berlin, eine Pandemie, ein erschossener "Bandit" im Tiergarten usw. usf.

Ich höre aber auch Geschichten über erkrankte Stars und über Mediennutten, die meinen, sich unentgeltlich inspirieren lassen zu müssen.

Was kann ich tun? Nichts. Im Nachhinein wird mir eh keiner glauben. Die, denen ich davon berichte, werden sich wahrscheinlich an gar nichts erinnern können.

Ich denke mir: Die ganze Welt ist so verkommen und korrupt. Alles ist so kaputt.

Ich erinnere mich an die junge Frau aus der Bar "Netzwerk" in der Sonntagstraße, die damals Ende der 90er-Jahre Medizin studiert hat und nun wohl als Oberärztin A. L. die Kinderurologie einer sehr bekannten Klinik in Berlin leitet. Ihre ca. 83-jährige Oma ist damals an irgend so einer Grippe gestorben, wie Alte halt an irgendwas sterben. Ich glaube, die Oma war sogar im Altenheim.

Ich erinnere mich, dass es wohl keine Grippe war, sondern ein Corona-Virus, und dass sie damals schon für einen "Lockdown" plädierte, also ganz auf Linie von Herrn Drosten (der übrigens in der gleichen Klinik arbeitet) und der WHO, dessen Chef sich übrigens nicht gegen die in paar Jahren auftretenden Corona-Viren impfen ließ.

Die gleichen Spinner, die damals übrigens in dem Laden verkehrten, sind heute wahrscheinlich irgendwelche Moderatoren auf twitter und reddit und sorgen dafür, dass nur linksfaschistisches Gedankengut veröffentlicht wird ("Fakten-Checker" und "Gesinnungsprüfer" für eine funktionierende Demokratie), während die Wahrheit zu einer rechtsextremistischen Verschwörung erklärt wird. Vielleicht sitzen sie auch irgendwo als alte Stasi-Kader in den Ämtern Berlins und des Bundes und besetzen dort als Referatsleiter oder Arbeitsbereichsleiter dank Vetternwirtschaft die Posten.

Vielleicht treffen sie sich auch alle bei den Rotariern oder sonstigen Übermenschen, und schachern sich gegenseitig die Posten zu.

Das wäre ja alles kein Problem, wenn diese korrupte Bagage kompetent wäre. Das ist aber meistens nicht der Fall.

<!-- more -->

**Rück- und Ausblick:** Nachdem ich in der unipolaren Epoche wegen meiner Tätigkeit bei der Bundeswehr mittelbar als Mörder bezeichnet wurde, bin ich nun aufgrund meiner Mitgliedschaft bei der FDP ein "Neoliberaler" und "Turbokapitalist".

Seitens der Regierungen der Welt wurde in den letzten Jahren nichts unversucht gelassen, eine allumfassende Überwachung der Bürger zu etablieren. Begriffe wie ***Gefährder***, und ***Terrorist*** und ca. zehn Jahre später ***Verächtlichmachung des Staates*** und ***Delegitimation*** werden jetzt auch für eindeutig nicht terroristische Taten gebraucht. Es könnte potenziell jeder ein ***Staatsfeind*** sein, in etwa so wie in der DDR und in China, mit dem Unterschied, dass dies in den Ländern gelogen ist und hier der Wahrheit entspricht. Ansders als in diesen Ländern gibt es hier keine Überwachung der Bevölkerung. Die Benutzung der Kontaktnachverfolgenden Luca-App und das Registrieren beim Betreten von Bars und Restaurants ist ja hier völlig freiwillig und dient dem Schutz der Bevölkerung.

In den letzten Jahren hat sich auch die Angewohnheit korrupter Politiker etabliert, im Amt bleiben zu wollen "um für ihre Verfehlungen Verantwortung zu übernehmen zu können". In ca. einem Jahr werden erstmals die Begriffe ***querdenken*** und ***verstehen*** negativ umgedeutet [(siehe Interview mit Gabriele Krone-Schmalz zu Russland)](https://www.hammeley.info/weltgeschehen/ukraine/) und Demonstranten werden als Wutbürger bezeichnet. Dabei liebt die Regierung doch alle Menschen und ihr handeln ist nun mal alternativlos. Die schleichende sprachliche Degradierung vom Subjekt zum substantivierten Verb wird anders als bei der Arbeiterbewegung der 1920 Jahre gerne angenommen werden.

Es werden vermehrt Personen in politische Ämter drängen, die keinerlei Ausbildung vorzuweisen haben. Das braucht man nun nicht mehr, solange die richtige Meinung und Einstellung vertreten wird. Gleichzeitig wird der Tenor der letzten Jahre "Wir haben Fachkräftemangel" weitergeführt.

Ein paar Legislaturperioden später können korrupte Politiker zum Bundeskanzler gewählt werden, die sich vor einem Untersuchungsausschuß an **nichts** erinnern können.

Jegliche Vergleiche mit Romanen sind "Verschwörungstheorien", auch wenn es nur Vergleiche bleiben. Bücher wie "Abstiegsgesellschaft", und "Bullshit Jobs" kommen in den nächsten Jahren auf den Markt. In den Sozialen Medien werden reihenweise Konten gesperrt, von Personen, die "Hass-Reden" verbreiten. Es sind diesmal nicht islamistische Terroristen, die zum Jihad aufrufen, die Videos sind weiterhin verfügbar, sondern Videos, die den Kurs der westlichen Regierungen kritisieren. Gegen Unrecht vorzugehen ist fast unmöglich. In Berlin "verbietet" [Innensenator Geisel Demonstrationen](https://www.berlin.de/sen/inneres/presse/pressemitteilungen/2020/pressemitteilung.980587.php), nicht "gegen die Versammlungsfreiheit, sondern für den Infektionsschutz".

Das Verbreiten von Wahrheiten auf den (a)sozialen Medienkanälen wird mit Beleidigungen und Kontosperrungen beantwortet. Einige Zeit später wird es Menschen mit falschem Haarschnitt treffen. Zum Vergleich: Der Sänger der Band [Job2Do](https://hammeley.info/ego/2009/12/05/sawadi-krap) wäre in Deutschland "gecancelt" worden, wie man nun sagt, aufgrund seiner Reggae-Musik, die ihm als Thailänder nicht zustehen würde. Die Meinungsführer nennen dies nun "kulturelle Aneignung". Wobei, als Thailänder gehört er zu den sogenannten "People of Color", ob er will oder nicht. Das entscheiden andere für ihn. Als POC darf man sich andere Kulturen aneignen, also vielleicht. Das ändert sich täglich, wie in dem Roman, den nun Verschwörungstheoretiker nennen.

Es ist auch die Zeit der Wortneuschöpfungen. "Chlor-Hühner", "Gen-Food", "Corona-Gegner" und "Klima-Leugner" werderm auch von Medien verwendet, die die Menschen für seriös halten. Alles muss nun noch ein wenig dramatischer klingen, wie der "zynische menschenverachtende (xyz)".

All das ist nun "linke"-Politik gegen "rechts". Überhaupt ist bald jeder rechtsradikal, wer nicht linientreu die Mehrheitsmeinung vertritt.

Ich treibe mich jetzt nur noch im Dark-Net herum, nicht so sehr, weil ich mich als Terrorist fühle, sondern weil alle Plattformen von Menschen wie mir gesäubert wurden.

Im Amerika wird von Bürgerkrieg gesprochen. Die Spaltung der Gesellschaft geht dort mit schnellem Tempo voran. Früher hat man gesagt, jeder Trend in Amerika kommt irgendwann zu uns.

Was für eine beschissene Dreckswelt. Aber es hat auch was Gutes. Man braucht nicht einmal mehr Terrorist zu werden, um dieses verlogene Land zugrunde zu richten, dass schaffen die selbst. Wer in Zukunft jedoch zu genau auf das Grundgesetz achtet, wird verbals zum Staatsfeind erklärt.

Es wird eine Zeit kommen, in der das Verfassungsgericht Grundrechtseinschränkungen durchwinken wird. Das Gesundheitsministerium wird wenige Tage bevor schwerwiegende Grundrechtseinschränkungen beschlossen werden, noch behaupten, es kursieren ["Fake-News / Falschinformationen"](https://twitter.com/bmg_bund/status/1238780849652465664?lang=de) die behaupten es würden Grundrechtseinschränkungen beschlossen. In der gleichen Zeit beantragt der Bundeswahlleiter Neuwahlen in Berlin zur Abgeordnetenhauswahl 2021, die wegen [mandatsrelevanter Wahlfehler](https://www.bundeswahlleiter.de/info/presse/mitteilungen/bundestagswahl-2021/54_21_einspruch_berlin.html) in zahlreichen Wahllokalen für nötig betrachtet werden. Es wird nicht neu gewählt. In Deutschland kann nun endlich auch ohne vernünftige Wahl durchregiert werden.

Ach ja, und das Vereinigte Königreich verlässt nach vierzig Jahren die Europäische Union. Andere EU Länder sollen nach dem Willen von "wahren Europäern" aus der Europäischen Union ausgeschlossen werden.
