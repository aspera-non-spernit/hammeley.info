---
title: 33 Jahre und sieben Monate
layout: default
hidden: true
---
Ich werde ständig gefragt, wie alt ich bin. Am 30. Juli 2011 bin ich genau 33 Jahre und sieben Monate alt.

![33 Jahre und sieben Monate alt 11.6.2011](){:data-src="/assets/images/33-7.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }
