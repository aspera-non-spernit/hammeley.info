---
title: Sich was Gutes tun
layout: default
tags: sport, Fahrrad, Fitness
---
In den letzten Jahren bin ich richtig speckig geworden. Nicht fett, auch nicht pummelig, aber dennoch sichtlich und sichtbar unathletisch er. In der Boxhagener Straße in deren Nähe ich wohne, esse ich regelmäßig in der Curry-Box, Cheeseburger und Pommes.
 
Da ich viel Zeit habe und eine große Reise plane habe ich den Sommer über etwas für den Körper getan. Ich fahre jetzt auch regelmäßif Fahrrad. Dafür habe ich mir extra ein neues Fahrrad gekauft - ein Felt Q500.

Das Ergebnis kann sich sehen lassen.

![Ich bin wieder fit](){:data-src="/assets/images/body.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

