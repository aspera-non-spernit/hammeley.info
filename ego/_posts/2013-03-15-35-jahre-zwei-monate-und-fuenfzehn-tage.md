---
title: 35-2-15
layout: default
---
Ich werde ständig gefragt, wie alt ich bin. Am 15. März 2013 bin ich genau 35 Jahre, zwei Monate und fünfzehn Tage alt.

![35 Jahre zwei Monate und 15 Tage alt, 15.3.2013](){:data-src="/assets/images/35-2-15.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" .class="rounded lazyload" }
