---
title: Vierundvierzig, Vier, Eins
layout: default
---
Ich werde ständig gefragt, wie alt ich bin. Am 1. Mai 2022 bin ich genau 44 Jahre, vier Monate und ein Tag alt.

![44 Jahre, vier Monate und ein Tag alt, 1.5.2022](){:data-src="/assets/images/44-4-1.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }
