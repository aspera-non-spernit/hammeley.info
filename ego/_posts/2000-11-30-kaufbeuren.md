---
title: Kaufbeuren
layout: default
tags: "Bundeswehr, Verpflichtung, Pilot, Klosterlechfeld, TSLw, Flugberatung, Ausbildung"
---
Ich bin jetzt bis zum 7.2.2001 bei der 5. / TSLw 1 in Kaufbeuren und belege dort den Lehrgang "Flugberatungsdienst Modul A".

Die Unterkunft ist recht angenehm und modern ausgestattet. Möglicherweise die Beste in der Bundeswehr.

![Kaufbeuren mit zwei Kameraden der Marine](){:data-src="/assets/images/kaufbeuren.png" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload"}
