---
title: 42 Jahre, neun Monate und 20 Tage
layout: default
---
Ich werde ständig gefragt, wie alt ich bin. Am 20. Oktober 2020 bin ich genau 42 Jahre und neun Monate und 20 Tage alt. (und sehe hier zehn Jahre jünger aus, als die Ehefrau (*1989) von Christian Lindner, FDP).

![42 Jahre, neun Monate und 20 Tage alt, 20.10.2020](){:data-src="/assets/images/42-9-20.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }
