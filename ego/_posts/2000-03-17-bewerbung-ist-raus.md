---
title: Bewerbung geschrieben
layout: default
tags: "Bundeswehr, Verpflichtung, Pilot, Offizier"
---
Ich habe die letzten Tage auf meiner Arbeitsstelle, der Flugberatung eine Stellenausschreibung für einen Flugdatenbearbeiter in Geilenkirchen (NRW) entdeckt. Es ist die Tätigkeit, die hier im Büro von ausgebildeten Soldaten ausgeübt wird, die ich als wehrpflichtiger Flugdatenbearbeitungsgehilfe unterstütze.

Ich habe mich drauf beworben. Mit den Frauen in Berlin ist das nichts geworden, hier hält mich nichts und ich weiß auch sonst nicht, was ich nach dem Wehrdienst machen soll.

Mal sehen, was daraus wird.

**Ausblick:** Am 27.3.2000 unterschreibe ich meine Verpflichtungserklärung (bei der Berufung in das Dienstverhältnis eines Soldaten auf Zeit).
