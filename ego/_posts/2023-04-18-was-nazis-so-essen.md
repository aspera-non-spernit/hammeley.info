---
title: Was Nazis so essen
layout: default
---
Für diejenigen, die in späteren Generationen sich mit der Geschichte des Landes beschäftigen und sich wundern, wie behinderte Schwurbler, Staatsfeinde, Sozialschädlinge
liberalhysterische Impfgegner usw. usf. lebten und welchen Beschäftigunen sie nachgingen und was sie aßen, präsentiere ich heute einen kleinen Einblick in das Leben eines Solchen: 
<!-- more -->
Garten- oder die "<a data-bs-toggle="collapse" href="#balkon" role="button" aria-expanded="false" aria-controls="#balkon">Balkon</a>"-Arbeit ist für den Covidioten ebenso Teil seines Lebens gewesen, wie die Aneignung von internationaler, aber noch viel mehr, von nationalistischer <a data-bs-toggle="collapse" href="#kueche" role="button" aria-expanded="false" aria-controls="#kueche">Küche</a>, um sich dem standigen Fressen von enorm großen Mengen von Fleisch aus umweltschädlicher Tierhaltung zu widmen. Nazis halt.

**Hinweis: Die Bildergalerien öffnen sich mit einem Druck auf die Verweise im Text oben**

<div class="row collapse" id="balkon"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
		  	<div id="carouselBalkonCtrls" class="carousel slide" data-bs-ride="carousel">
		  		<div class="carousel-inner" style="background-color: #333">
					<div class="carousel-item active">
						<div class="carousel-caption d-none d-md-block">
							<h5>Buntes Blumenbeet</h5>
							<p>Vieles nur gekauft</p>
					  	</div>
						<div class="d-flex justify-content-center">
					  		<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_blumenbeet.jpg" alt="Blumenbeet">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Buntes Blumenbeet</h5>
							<p>Vieles nur gekauft</p>
					  	</div>
						<div class="d-flex justify-content-center">
					  		<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_blumenbeet-2.jpg" alt="Blumenbeet">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Eine Sonnenblume</h5>
							<p>Aus russischen Sonnenblumenkernen aus dem Real (1kg blaue Packung)</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_sonnenblume.jpg" alt="Sonnenblume">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Eine Dalie</h5>
							<p>Selbstverständlich aus Samen gezogen.</p>
				  		</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_dalie.jpg" alt="Dalie">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Miniteich</h5>
							<p>Ganz frisch angelegt</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_teich-2.jpg" alt="Miniteich">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Miniteich</h5>
							<p>Die schwimmenden Wasserpflanzen sorgen für klares Wasser</p>
					  	</div>
					  	<div class="d-flex justify-content-center">
					  		<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_teich.jpg" alt="Miniteich">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Grashüpfer</h5>
							<p>Neben Vögeln kommen auch allerhand kleinere Tiere zu Besuch</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_grashuepfer.jpg" alt="Grashüpfer">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Vogel zu Besuch</h5>
							<p>Das Vogelbad wird gut angenommen</p>
					  	</div>
					  	<div class="d-flex justify-content-center">
					  		<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_vogelbad.jpg" alt="Vogelbad">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Vogel zu Besuch</h5>
							<p>Manchmal machen sie ganz schön Unordnung</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_vogelbad-2.jpg" alt="Vogelbad">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Ein Apfel</h5>
							<p>Aus eigenem Balkon-Anbau. Echt Bio.</p>
					  	</div>
		  				<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_apfel.jpg" alt="Apfel">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Kirschbaum</h5>
							<p>Nach der Blüte reifen die Kirschen.</p>
					  	</div>
						<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_kirschknospen.jpg" alt="Kirschknospen">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Kirschbaum</h5>
							<p>Nach der Blüte reifen die Kirschen.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/balkon_kirschknospe.jpg" alt="Kirschknospe">
			  			</div>
					</div>
					<button class="carousel-control-prev" type="button" data-bs-target="#carouselBalkonCtrls" data-bs-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="visually-hidden">Vorheriges</span>
					</button>
					<button class="carousel-control-next" type="button" data-bs-target="#carouselBalkonCtrls" data-bs-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="visually-hidden">Nächstes</span>
					</button>
				</div>
			</div>
		</div>
  	</div>
</div>

<div class="row collapse" id="kueche"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
		  	<div id="carouselKuecheCtrls" class="carousel slide" data-bs-ride="carousel">
		  		<div class="carousel-inner" style="background-color: #333">
		  			<div class="carousel-item active">
						<div class="carousel-caption d-none d-md-block">
							<h5>Kirschkuchen</h5>
							<p>Nicht vom eigenen Baum. Die eigenen waren auch sehr lecker, aber nicht ausreichend für einen Kuchen.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_kirschkuchen.jpg" alt="Kirschkuchen">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Apfelkuchen</h5>
							<p>Mit gekauften Äpfeln</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_apfelkuchen.jpg" alt="Apfelkuchen">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Frittierte Bananen</h5>
							<p></p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_fritierte-bananen.jpg" alt="Fritierte Bananen">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Amerikanische Pfannkuchen</h5>
							<p>Mit Blaubeeren</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_pancakes.jpg" alt="Pancakes">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Kokoseis</h5>
							<p>Selbstverständlich selbstgemacht</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_eis.jpg" alt="Eis">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Ziebelkuchen</h5>
							<p>Kuchen geht auch mal deftig</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_zwiebelkuchen.jpg" alt="Zwiebelkuchen">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Flammkuchen</h5>
							<p>aus dem Elsass (oder Baden)</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_flammkuchen.jpg" alt="Flammkuchen">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Bayrisch Kraut</h5>
							<p>Sehr leckere Beilage zu allem</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_bayrisch-kraut.jpg" alt="Bayrisch Kraut">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Obadzda</h5>
							<p>Gekauft kommt an selbstgemacht nicht heran.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_obadzda.jpg" alt="Obadzda">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Langos</h5>
							<p>Aus Ungarn</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_langos.jpg" alt="Langos">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Eintopf</h5>
							<p></p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_eintopf.jpg" alt="Eintopf">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Pfannenpizza</h5>
							<p>"Haiwai". Die einzig Wahre</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_pan-pizza.jpg" alt="Pfannenpizza">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Ente oder Gans?</h5>
							<p>Lange auf relativ niedriger Temperatur gegart.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_ente.jpg" alt="Ente">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Alu Gobi</h5>
							<p>Bin nicht sicher, ob das sogar meine eigenen Kartoffeln waren.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_alu-gobi.jpg" alt="Alu Gobi">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Cous Cous</h5>
							<p></p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_cous-cous.jpg" alt="Cous Cous">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Som Tam</h5>
							<p>Thailändisch.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_som-tam.jpg" alt="Som Tam">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Larb Gai</h5>
							<p>Thailändisch</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_larb-gai.jpg" alt="Larb Gai">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Naam Tok</h5>
							<p>Mein thailändischer Favorit</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_naam-tok.jpg" alt="Naam Tok">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Miso</h5>
							<p>Japanisch war auch mal "In"</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_miso-suppe.jpg" alt="Miso Suppe">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Fermentation</h5>
							<p>Muss jeder mal gemacht haben</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_fermentiert.jpg" alt="Fermentiert">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Fermentation</h5>
							<p>Durchaus schmackhaft</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_fermentiert-2.jpg" alt="Fermentiert">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Eigene Saftkreationen</h5>
							<p>Bevor es den "Smoothie"-Trend gab.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_saft.jpg" alt="Saft">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Eigene Saftkreationen</h5>
							<p>Bevor es den "Smoothie"-Trend gab.</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_saft-2.jpg" alt="Saft">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Sauerteigbrot</h5>
							<p>Geht auch ohne Brotbackmaschine</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_sauerteig-brot.jpg" alt="Sauerteigbrot">
			  			</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Soleier</h5>
							<p>Für zwischendurch</p>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/kueche-und-balkon/kueche_solei.jpg" alt="Solei">
			  			</div>
					</div>
		  		</div>
		  		<button class="carousel-control-prev" type="button" data-bs-target="#carouselKuecheCtrls" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Vorheriges</span>
			  	</button>
			  	<button class="carousel-control-next" type="button" data-bs-target="#carouselKuecheCtrls" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Nächstes</span>
			  	</button>
		  	</div>
	  	</div>
  	</div>
</div>
