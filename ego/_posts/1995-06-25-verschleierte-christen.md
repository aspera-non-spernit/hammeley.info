---
title: Verschleierte Christen
layout: default
tags: "Berlin, Reichstag, Verhüllung, Christo"
---

Heute wurde der Reichstag in Berlin vom englischen Verhüllungskünstler Christo verhüllt. Das bleibt bis zum 7. Juli so. An den Tagen darauf wird die Plane in kleine Stücke geschnitten und als <a data-bs-toggle="collapse" href="#andenken" role="button" aria-expanded="false" aria-controls="#andenken">Andenken</a>  verkauft. Ich kaufe eins.

<hmmly-player id="christo" data-yt-vid="ASiS_rhFgwI"></hmmly-player>

<div class="row collapse" id="andenken"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
            <img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="https://de.wikipedia.org/wiki/Verh%C3%BCllter_Reichstag#/media/Datei:MaterialReichstag.JPG" alt="Andenken">
        </div>
    </div>
</div>
