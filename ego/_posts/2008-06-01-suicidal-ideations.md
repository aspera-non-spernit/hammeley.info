---
title: Suicidal Ideations
layout: default
---
Irgendwann zwischen 2008 und 2010 lerne ich in der Sportsbar nebenan eine amerikanische Psychologin von der Johns Hopkins University kennen, die in Bayern Kriegsheimkehrer betreut. Ich erzähle ihr, dass das Leben eigentlich scheiße sei, dass es sich nicht lohne zu leben und dass ich manchmal hoffe, dass mich ein Auto überfährt oder mir sonst etwas zustößt.

Ich erfahre, dass man das "suicidal ideation" oder "passive death wishes" nennt, auf Deutsch "Selbstmordgedanken". Es scheint ein Symptom verschiedener psychischer Störungen zu sein, die zwar nicht direkt zum Suizid führen, aber einen Risikofaktor darstellen.

Solche psychischen Störungen müssen nicht mit bekannten Krankheiten wie bipolaren Störungen, Schizophrenie, Psychosen oder Depressionen verbunden sein. Auch posttraumatische Belastungsstörungen, Alkohol- und Drogenkonsum, Arbeitslosigkeit, chronische Schmerzen, (Cyber-)Mobbing oder das Leben in einem faschistischen Staat können die Ursache sein.

Vom "optimistischen Realismus" meiner Jugend ist nichts mehr übrig geblieben.