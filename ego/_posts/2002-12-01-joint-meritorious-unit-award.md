---
title: Joint Meritorious Unit Award
layout: default
tags: "Bundeswehr, Amerika, Operation, Eagle Assist, Verteidigung"
---
Der Präsident und der Verteidigungsminister der Vereinigten Staaten von Amerika sprechen im Namen des amerikanischen Volkes allen an der Operation Eagle Assist Beteiligten ihren Dank aus. In diesem Zusammenhang verleiht Donald Rumsfeld zum ersten Mal in der Geschichte den Joint Meritorious Unit Award an eine Organisation außerhalb des Verteidigungsministeriums der Vereinigten Staaten von Amerika.

![Joint Meritorious Unit Award](){:data-src="https://images-na.ssl-images-amazon.com/images/I/61nugQ780aL._AC_UL1200_.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Da ich noch nicht einmal das Sportabzeichen abgelegt habe (als Soldat bei NATO E3-A war ich davon befreit), wird dies mein erstes und letztes Abzeichen in meiner militärischen Laufbahn sein.
