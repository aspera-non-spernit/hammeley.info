---
title: Zwerge und Würmer
layout: default
---
In einem Leben trifft man auf allerhand Menschen. Viel mehr jedoch auf Zwerge und Würmer. Diese Zwerge sind nicht unbedingt körperlich klein, meistens sind, sie es gar nicht. Es gibt aber, auch kleine Zwerge, dann kläffen, sie aber häufig wie kleine Köter, anstatt einfach die Fresse zu halten, wie es Gartenzwerge eigentlich tun.

Alle haben aber gemeinsam, dass sie geistig unterbelichtet sind oder charakterlicher Abschaum sind. Manchmal vereinigen sie auch beides.

Man findet sie überall, sogar schon im Kindergarten. Dort erkennt man sie meistens daran, dass sie als "Haben-Will"-Kinder immer rumjammern. Viel schlimmer jedoch ist, dass sie schon in dem Alter beginnen, andere Kinder zu beschuldigen, etwas getan zu haben, indem sie mit dem Finger auf sie zeigen. Dabei ist es gar nicht mal so wichtig zu wissen, ob das beschuldigte Kind etwas getan hat oder nicht. Meistens ist es jedoch so, dass diese Würmer selbst schuld sind, die Schuld aber auf andere schieben, oder wenn sie an einer Sache beteiligt waren, jede Schuld mit dem Fingerzeig von sich weisen.

Für das Volkswohl müsste man solche Kinder eigentlich sofort schreddern. Die Eltern als Präventivmaßnahme gleich mit. Aber das tut man natürlich nicht. Stattdessen fördert man sie und gibt ihnen später in der Schule gute Noten, damit Chancengleichheit besteht. Jeder kennt diese Tiefflieger, keiner will mit ihnen zu tun haben. Nach der Schule fliegen sie jedoch alle paar Jahre weiter und ziehen immer wieder das gleiche Spiel durch.

Heute erkennt man diese geistigen Tiefflieger u. a. daran, dass sie auf den Sozialen Medien ihre Unkenntnis in die Welt hinausposaunen und dort von den anderen Gehirn-Vakuen begeistert beklatscht werden. Das nehmen sie dann zum Anlass, noch mehr Scheiße zu veröffentlichen.

<!-- more -->

Manche von ihnen tarnen sich im weiteren Leben als Arbeitskollegen, die Deine Arbeit für ihre Eigene ausgeben, über die Brückentage immer krank sind oder wegen der Kinder immer dann verhindert sind, wenn es Sonderdienste oder Überstunden gibt. Dabei ist heutzutage eine Abtreibung durchaus gesellschaftlich anerkannt. Aber nein, sie werden gefördert. Diesen Würmern mit Minderwertigkeitskomplexen wird dann Verantwortung übertragen, damit sie Arbeit und Familie miteinander vereinbaren können. Hätte man sie bloß schon im Kindesalter ausgerottet. Aber nein, im Home-Office mutieren sie dann frei von jeglicher Kompetenz zu Fürsten. Also, sie halten sich für welche. Das geht meistens so lange, bis sie an eine natürliche Grenze stoßen und das Peter- und Dilbert-Prinzip voll zuschlägt. Schließlich muss der Abstand zwischen Idioten und Elite gewahrt bleiben. Da oben wird keine Konkurrenz geduldet. Ihr dürft das Gefühl haben, dazu gehören zu dürfen, ihr werdet aber nie dazu gehören.

Bei solchen Situationen wundert sich die restliche Belegschaft über die Entscheidung, bis ein Gerücht umhergeht und es einige ahnen. Es ist der Neffe vom Onkel des zweiten Cousins, der Mitglied bei irgendwelchen Rotariern ist, die sich schon aus der Zeit bei der Stasi kennen, deren Großväter zusammen in Stalingrad gekämpft haben oder durch Zwangsarbeiter reich wurden. "Ja, das war natürlich nicht richtig, aber dafür haben wir ja auch 3,50 Reichsmark Entschädigung bezahlt, also an jeden. Nicht insgesamt."

Es gibt Tausende Beispiele, woran man diesen Abschaum erkennt.

Auf den Ämtern setzen sie regelmäßig eigene neue Maßstäbe, um ihre "Macht" zu demonstrieren. Wenn Mitarbeit:ende aber wenig Mit:denk:ende behaupten, sie müssten wissen, wohin Leistungsempfänger Geld überweisen: "Schließlich müssen wir wissen, ob sie irgendwo ein geheimes Konto haben." Du dummes Stück. Es reicht also nicht, Leuten zu unterstellen, sie würden Teile der großzügig erhaltenen Leistungen auf ein geheimes Konto überweisen, nein, man möchte auch noch den Anschein erwecken, der Leistungsempfänger sei so dumm und würde das Geld per Überweisung auf ein Schwarzgeldkonto übertragen. Für diese gedankliche Meisterleistung hat der Blockwart im mittleren Dienst sicher den ganzen Tag drauf verwendet.

Selbstverständlich gibts diesen Abschaum auch in der Politik und in den Medien. Während dort ein kleiner Teil die Fäden zieht, führen die Nutten der Politik und in den Medien ahnungslos deren Pläne um.

Im Parlament sind es die Abnicker, die nie einen einzigen Gesetzestext gelesen haben und mit freiem Mandat abstimmen, so wie man es ihnen sagt und von ihnen verlangt. Schließlich muss ja das Haus abbezahlt werden. Viele können sich an nichts erinnern. Nur der Kontostand, der ist bekannt.

Es können aber auch die abgehobenen Politikbetreib:enden sein, die schon lange erkannt haben, dass es nicht mehr darauf ankommt, irgendetwas Inhaltsvolles zu sagen. Dieser Dreck wiederholt sich jedes Jahr auf ähnliche Weise mit immer den gleichen Sprüchen die je nach Situation entweder positiv oder negativ belegt sind: "Lohn-Preis-Spirale", "Rekordsteuereinnahmen", "Solidarität", "Fortschritt", "Mehr Sicherheit", "Unsere Kinder", "Klimaaaa". Der Vollständigkeit halber: "Die Ausländer nehmen uns die (Arbeit, Frauen, Wohnungen) weg". Ich muss kotzen. Ich frag mich, warum es eigentlich noch keine Schattenarmee und Milizen gibt, um diesem Schmierentheater von Schein-Demokratie ein Ende zu setzen. Ich biete mich auch als Führer oder Kaiser an. Ich bin jung, dynamisch und flexibel. Meine erste Amtshandlung: "Messerpflicht" und "Abstechprämie".

In den Medien nennt man diese Zwerge auch Mediennutten. Die geilen sich auf, weil sie ganz nah dran sind an der Politik. Mal einen echten Machtmenschen von nahem sehen, toll. Manchmal machen sie sogar den Anschein, sie würden ihre eigene Meinung vertreten, bis zu dem Zeitpunkt, an dem es unangenehm für sie wird. Dann wird darauf hingewiesen, wie Medien zur Meinungsbildung beitragen: entweder durch Angstmacherei, Verdrehung der Tatsachen oder durch hochtrabende moralische Ansprachen zur Besten Fernsehzeit.

Meine Meinung nach 45 Jahren Propaganda, Lügen, Angst- und Panikmache im Staatsfunk:

- Angst vor Überbevölkerung
- Angst vor Kommunisten
- Angst vor Russen
- Angst vor Terrorismus
- Angst vor Viren
- Angst vor Feinstaub
- Finanzkrise, Schuldenkrise, Eurokrise
- Flüchtlingskrise
- Coronakrise
- Klimakrise
- Fachkräftemangelkrise
- Inflationskrise

Ich muss jeden Tag wegen Euch kotzen. Wenn ich Euch aus Versehen auf Youtube sehe oder ihr mir über den Weg läuft, wird mir schlecht. Ihr seid nicht mächtig. Ihr seid minderwertiges Material, das fallen gelassen und ausgetauscht wird, sobald die Entscheidung darüber getroffen wurde. Und nein, es macht keinen Spaß, Leute fallen zu sehen. Entweder ist es ein notwendiges Übel, wenn einer von euch ausschert ist oder die Machtverhältnisse haben sich oberflächlich verändert, sodass neue Spinner bestimmen wollen, wie die gleiche Propaganda übermittelt wird. Vielleicht hattet ihr Mal ganz am Anfang Eurer Karriere den Anspruch, alles besser zu machen und aufklären zu wollen, aber was solls, das Haus muss ja abbezahlt werden. 90% von Euch würden auch bei der Aktuellen Kamera oder der Wochenschau von 1935 anheuern. Würmer ohne Rückgrat. Ich melde mich, wenn es so weit ist.

Leider findet man solche Würmer auch in der eigenen Familie. Zum Beispiel dann, wenn man wiederholt als Arsch bezeichnet wird, der eine Show abzieht, während man selber eine nicht unbeträchtliche Summe an Geld erhalten hat, um einen Traum vom Eigenheim zu verwirklichen, mit einem Versprechen, das man natürlich nicht einhält, wenn es eingelöst werden soll. Für das Geld, das man erhalten hat, brauchte man auch sonst nichts zu tun, außer zu jammern. Anders als der Rest der Familie, der dafür jahrelang jeden Pfennig umdrehte und manche deshalb sogar ihre ganze Kindheit in Armut verbrachten.

Es gibt auch Missgeburten, die sich dazu berufen fühlen, einen anschreiben zu müssen, obwohl man sich gar nicht kennt. Nämlich dann, wenn sich die Familie ein Beispiel an der verkommenen Gesellschaft und deren neuen Sprachgebrauch nimmt. Unter Vermeidung wichtiger Fakten wird einem erklärt, was für ein Scheiß-Typ man sei, weil man unberechtigt 30 Jahre ein Problem damit, dass in der Familie zu viel gesoffen wird und man Teile der Familie eigentlich noch nie nüchtern erlebt hat. Ich Arsch.

Ich bin der Arsch, der dumme Nichtsnutz. Die Schlauen und Guten sitzen in der Kneipe und verdienen gut am Elend der Kundschaft. Wenigstens zahlt ihr Idioten meinen Unterhalt. Geht dafür gefälligst arbeiten und sterbt kurz nach Renteneintritt. Für Deutschland.

Andere soziopatische Mitglieder der Familie oder der Gesellschaft tischen einem immer neue Geschichten auf und finden immer gerade jetzt einen guten Grund, etwas tun oder lassen zu müssen, was ihnen nützt. Gleichzeitig lassen sie sich von jedem mit schlechten Intentionen irgendwas einreden, während die wenigen, die es eigentlich gut meinen, eine Gefahr darstellen. Das passiert nicht einmal zweimal. Nein, immer. Jedes Mal. Das ganze Leben lang, von der Geburt an bis zum Tod.

Manche Menschen sind einfach nur dumm. Sie zerstören Leben, ohne dass sie davon etwas mitbekommen. Sie sind sich der Tragweite ihres Handelns nicht bewusst. Man kann sie natürlich nicht darüber aufklären, dass ihr Verhalten mehr schadet als nutzt: "Ich meine es doch nur gut." So kommt es auch, dass immer wieder aufs Neue alles daran getan wird, Leben zu zerstören. Ja, wiederholt. Ja, mehrmals immer das Gleiche, trotz Bitte es sein zu lassen. Man selbst ist nämlich schlau.

Ihr seid das dümmste und verlogenste Dreckspack das auf dieser Erde rumläuft.

Es gibt eigentlich nur zwei Gründe, warum ihr noch existiert:

1) Gesellschaftssysteme wie das in Deutschland funktionieren, wenn es möglichst viele dumme Menschen gibt, die gerade ausreichend intelligent und gebildet sind, damit sie genau das machen, was sie machen und tun sollen, aber bloß nicht anfangen zu denken und schon gar nicht eigenständig. Für das Denken ist das Fernsehen zuständig.

Sie sind die willfährigen Idioten der breiten Mehrheit, die regelmäßig die Nachrichten verfolgen "um sich zu informieren", wer denn heute der Bösewicht ist. Am Tag drauf wiederholen sie dann wie in der Schule die gelernten Floskeln: "Angriffskrieg", "Klimakatastrophe", "Querdenker" usw. usf. Für die vielen, für die selbst die Nachrichten zu kompliziert sind, gibt es Leute wie Böhmermann und Schröder, die den Wertekanon des Systems in lustiger und einfacher Sprache in Sendungen wie dem Schwarzen Humor Kanal, dem ZDF magazin und Kennzeichen D an die Personen mit und ohne Titten vermitteln.

Sie schauen auch gerne Sendungen die einem erklären, warum die eine Zahnpasta besser ist als die andere oder in der aufgeklärt wird, welche Mogelpackung diesmal entdeckt wurde. Vielleicht kaufen sie auch Bücher in denen ihnen gesagt wird, was sie tun müssen, um dies und das zu erreichen. Nach Aerobic, Stepper und Nordic Walking sind sie nun bei Yoga angekommen. Das ist so toll.
Nach der 37. Staffel Reality-TV oder der tausendsten Bundesligasaison sind dann endlich die letzten Gehirnzellen abgestorben. Dann tut die eigene Dummheit auch nicht mehr so weh.

Ob die beste Demokratie der Welt, real existierender Sozialismus, Nationalsozialismus oder Monarchie, ihr findet immer einen guten Grund, überzeugt zu sein, das Richtige zu tun. Heute noch bei der Stasi: "Ja, wir müssen uns ja vor den Imperialisten aus dem Westen schützen." Und Morgen schon: "Ja der imperialistische Russe.".

Ihr seid so armselig, widerlich und verkommen.

Die reiche und einflussreiche Elite, die seit jeher das Schicksal der Welt lenkt, gibt Euch genau so viel Anerkennung und Einfluss, dass ihr weiter macht wie bisher. Früher gab es dafür wenigstens noch etwas Lametta, aber selbst das hat man Euch ausgeredet: "Toll, wie die Gewerkschaften jetzt streiken. Ein Zeichen wahrer Demokratie. Heil Deutschland! Was werd' ich jetzt mit meinem ganzen Geld bloß machen?". Vor allem erst einmal Steuern zahlen, damit die Politik verkünden kann, wie erfolgreich sie doch handelt.

"Ja, Bürgerräte sind so wichtig für die Demokratie. Ganz toll."

Es soll ja wissenschaftliche Studien geben, die belegen, dass die Wahlentscheidung der unteren 70 %-80 % überhaupt keinen Einfluss auf ihr Leben hat: "Aber der Klimawandel, die Russen, die Chinesen, Corona und der Feinstaub." Ich muss kotzen.

Überhaupt, wenn ihr nur einmal in Eurem armseligen Leben euer beschissenes Gehirn einschalten würdet. Aber nein: "Buddha und Lauterbach haben gesagt und das iPhone ist viel besser als das andere. Das hat jetzt runde Ecken und hat beim letzten Test 173 Punkte bekommen."

Haltet doch einfach eure Fresse.

Manchmal sind auch Zwerge ratlos: "18 % AfD, wo kommen auf einmal die ganzen Nazis her?"

Es ist eigentlich eine Beleidigung, Euch als Würmer und Zwerge zu bezeichnen

2) Die wenigen, die weder zu den Zwergen und Würmern noch zur Elite gehören und zu denen ich mich auch zähle haben einfach Mitleid mit Euch. Entweder, weil es offensichtlich ist, dass ihr minderbemittelt seid und es mühselig ist, in irgendeiner Art zu Euch durchzudringen oder weil es irgendeinen sozialen Zwang wie die Familie gibt die einen davon abhält, das zu tun was eigentlich nötig wäre, um die Welt zu einem besseren Ort zu machen.

Leider wird das gutmütige Verhalten immer ausgenutzt. Entweder denken die Zwerge man sei dumm und würde nichts mitbekommen oder sie haben nicht die Fähigkeit zu denken. Meistens, weil es ihnen abtrainiert wurde. Manche sind einfach nur blöd.

Aber vielleicht bin ich auch der Idiot oder gibt es sogar mehrere Realitäten? Der eine hat Corona vielleicht überlebt, weil er fünffach geimpft ist und seit drei Jahren mit Maske durch die Gegend läuft, andere haben es vielleicht überlebt, weil sie ungeimpft und unmaskiert sind. Das ist doch Wissenschaft.

Falls sich jetzt jemand fragt: Nein, es ist kein Neid auf Euren Erfolg. Es ist eine abgrundtiefe Verachtung, die in einem Leben von 45 Jahren gereift ist. Ihr seid so einfach gestrickt, dass es selbst mir als Schwurbler gelingt, Euch nach belieben zu steuern. Solange ihr einfach weiter macht und fein arbeiten geht, ist auch alles in Ordnung. Lasst aber das ständige Jammern und Meckern sein. Haltet bitte Eure dumme Fresse. Lasst die anderen ihr Leben leben und bleibt am besten unter Euch. Tragt am Besten immer Maske, sodass ich Euch gleich erkenne und Euch aus dem Weg gehen kann. Das ist mein Angebot.

Ich habe auch keine Krise oder Anpassungsstörungen. All das und meine Meinung dazu ist seit Jahren bekannt.
Ihr seid einfach zu dumm und hört nicht zu und versteht die einfachsten Dinge nicht. Weil ihr armselige Idioten seid.

Anstatt Euch zu "canceln" werden Eure Namen für die nächsten 100.000 Jahre digital archiviert, damit auch Eure Kinder, Enkel und sonstige Verwandte jeden Tag daran erinnert werden, was für menschenverachtende (Begriff kenn ich aus dem Statsfunk) Zwerge ihr seid.

"Dass der so was schreibt. Unerhört." Ja, tut mir leid. Ich informiere mich halt nicht bei der tagesschau und Böhmermann, sondern bei [Nazis mit Ausbildung](https://www.youtube.com/watch?v=-kLzmatet8w&origin=https://hammeley.info/ego/2023/04/02-zwerge-und-wuermer)

Außerdem bin ich unschuldig. [Dieser Türke](https://www.youtube.com/watch?v=KKNb2GlU0_E&origin=https://hammeley.info/ego/2023/04/02/zwerge-und-wuermer) hat mich angestachelt. Wirklich.

Dies ist die x-te Veröffentlichung dieses odr eines ähnlichen Beitrags auf dieser Seite. Es ändert sich ja nichts.
