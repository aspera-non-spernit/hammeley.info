---
title: Klosterlechfeld
layout: default
tags: "Bundeswehr, Verpflichtung, Pilot, Klosterlechfeld, USLw, Ausbildung"
---
Ich bin jetzt bis zum 24.8.2000 bei der 5. / USLw in Klosterlechfeld und belege dort den Laufbahnlehrgang ULLw (Unteroffizierslehgang der Luftwaffe). Neben der körperlichen Ertüchtigung auf der Hindernisbahn, der Schwimmhalle und beim Nacht-Marsch mit Gepäck (>30km) lerne ich dort auch etwas über Menschenführung im Einsatz, etwas über das Humanitäre Völkerrecht und die [Innere Führung](https://www.bmvg.de/de/themen/verteidigung/innere-fuehrung/das-konzept).
