---
title: ดูเขาทำ
layout: default
---
Ich fliege das erste Mal mit einer B747-800 im Oberdeck nach Bangkok und besuche erstmals den asiatischen Teil Eurasiens. Es ist dort gerade die kalte Jahreszeit (25-30°C) und überraschenderweise wird gerade der Geburtstag des Königs Bhumibol gefeiert. Ich bleibe vier Wochen und besuche [Ayutthaya [ext jpg]](https://homeiswhereyourbagis.com/wp-content/uploads/2018/11/Ayutthaya-Tempel-Wat-Mahathat-01.jpg), [Chiang Mai [ext jpg]](https://cdn1.i-scmp.com/sites/default/files/styles/1200x800/public/images/methode/2019/01/17/b0e3d7cc-14b8-11e9-bd68-61a0d0b9ce58_image_hires_092253.jpg?itok=HJHQcSUk&v=1547688186), [Pai [ext jpg]](https://paperwriter.ca/wp-content/uploads/2022/07/Top-10-Things-to-Do-in-Pai-Thailand.jpg) und [Koh Chang [ext yt]](https://www.youtube.com/watch?v=NYnKmrUOHRA&pp=ygUfa29oIGNoYW5nIHdoaXRlIHNhbmQgYmVhY2ggMjAxMA%3D%3D).

Es ist gerade Weltwirtschaftskrise. Man sagt mir, dass touristisch, trotz Hauptsaison weniger los ist als sonst. Es ist aber überall ausreichend was los. Mir wird nicht langweilig.

Manche Thailänderinnen behaupten sogar, ich sei wie ein Thailänder. Es scheint wohl ein Kompliment zu sein. Das ist aber auch nicht schwer, wenn man sich das Verhalten so mancher Touristen anschaut.

<div class="row" id="thailand"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
            <div id="carouselThailandCtrls" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/bangkok-flughafen.jpg" class="rounded lazyload d-block w-100" alt="Flughafen Bangkok">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/saufen-mit-thais.jpg" class="rounded lazyload d-block w-100" alt="Saufen mit Thais">
                    </div>
                    <div class="carousel-item">
                        <img class="lazyload" src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/thailaendische-raststaette.jpg" class="rounded lazyload d-block w-100" alt="Thailändische Raststätte">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/ayutthaya.jpg" class="rounded lazyload d-block w-100" alt="Kultur in Ayutthaya">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/fuers-karma.jpg" class="rounded lazyload d-block w-100" alt="Karma für mich">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/gold-und-noch-mehr-karma.jpg" class="rounded lazyload d-block w-100" alt="Karma für mich">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/mit-dem-fahrrad.jpg" class="rounded lazyload d-block w-100" alt="Mit dem Fahrrad durch MaeHongSon">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/komfort-huette.jpg" class="rounded lazyload d-block w-100" alt="Unterkunft mit Komfort">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/bambushuette.jpg" class="rounded lazyload d-block w-100" alt="Unterkunft mit ohne Komfort">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/koh-chang.jpg" class="rounded lazyload d-block w-100" alt="Entspannte Insel Koh Chang">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/koh-chang-2.jpg" class="rounded lazyload d-block w-100" alt="Entspannte Insel Koh Chang">
                    </div>
                    <div class="carousel-item">
                        <img src="/assets/images/placeholder-image.png" data-src="/assets/images/thailand/koh-chang-3.jpg" class="rounded lazyload d-block w-100" alt="Entspannte Insel Koh Chang">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselThailandCtrls" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Vorheriges</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselThailandCtrls" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Nächstes</span>
                </button>
            </div>
        </div>
    </div>
</div>

- [Nordwest Thailand, 2007 [ext yt]](https://youtu.be/H7oebMteQSY?t=1075&origin=https://hammeley.info/ego/2009/12/05/sawadi-krap)
- [Mae La - Flüchtlingslager in Nordwesten Thailands, 2011 [ext yt]](https://www.youtube.com/watch?v=e221rgmd9Ls&origin=https://hammeley.info/ego/2009/12/05/sawadi-krap)
- [บรรจบ พลอินท - ดูเธอทำ [ext yt]](https://www.youtube.com/watch?v=Dmhg_zFMvIYc&origin=https://hammeley.info/ego/2009/12/05/sawadi-krap)