---
title: Um die drei
layout: default
tags: "Zoo, berlin"
---
Ich werde ständig gefragt, wie alt ich bin. Ich bin mir nicht sicher. Ich schätze ca. drei.

![Ca. drei Jahre alt](){:data-src="/assets/images/um-die-vier.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }
