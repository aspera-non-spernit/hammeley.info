---
title: Neuer Lebensabschnitt
layout: default
tags: "Weltmeister, Oberschule, Eltern, Investoren, Haus, Trennung, Ungarn, Ägypten, Kindergarten, Grundschule, Spaß, Dienstag, Spencer, Fraggles, Marco, Alice, Wunderland, Bruder, Unfall, Wiedervereinigung"
---
Ich bin jetzt 12 Jahre alt. 1990 ist ein besonderes Jahr. Nicht nur, dass das geteilte Deutschland wiedervereint wird, der Kalte Krieg demnächst beendet scheint und Deutschland zum ersten Mal in meinem Leben Fußballweltmeister wird, ich gehe jetzt auch auf die Oberschule (Robert-Blum-Schule (Gymnasium)).

Außerdem trennen sich meine Eltern. Das erfahre ich eines Morgens.

**Ausblick:** In den nächsten Jahren wird das Mietshaus in der Eberstraße für 1,2 Millionen D-Mark an irgendwelche Münchner Investoren verkauft, die in Berlin das große Geld machen wollen. Ebenso wird das Haus in Ehingen verkauft. Das "Stuttgarter Haus" (Steinenbronn) ist ja schon seit Jahren verkauft.

Meine Mutter und ich erhalten jeweils einen Anteil. Ich kann über die 50.000 D-Mark ab meinem 18. Lebensjahr verfügen.

In der Scheidungsurkunde steht, dass meine Mutter das Sorgerecht bekommt, da sich mein Vater seit der Trennung nicht mehr gemeldet hat. Ich soll bestätigen, dass ich damit einverstanden bin. Ich willige ein.

Mein Vater zieht nach Ungarn. Meine Leistungen in der Schule werden die nächsten Jahre durchwachsen bleiben. Immerhin mache ich es bis zur 11.

In ein paar Jahren besuche in meinen Vater in Solymar, Ungarn. Er erzählt mir, dass er ein Kind mit der Frau vom Haus gegenüber hat. Sie sei wohl hübsch und arbeitet bei Mercedes. Er hat aber keinen Kontakt mehr zu ihr. Weitere Jahre später erfahre ich von ihm, dass ich ein ungewolltes Kind bin und dass ich möglicherweise Alzheimer bekomme. Das sei in der Familie.

Statt meines Vaters wohnt nun ein alter Freund meiner Mutter hier. Er kommt aus Ägypten. Er ist wegen eines einmaligen BTM-Delikts vorbestraft, nachdem er am Mierrendorffplatz hochgenommen wurde.
Über ihn wurde auch in den 80ern im SFB-Fernsehen wegen der damals gängigen Abschiebepraxis berichtet. Na, jetzt ist er wieder da. In ein paar Jahren heiratet meine Mutter zum dritten Mal.

<!-- more -->

**Rückblick:** Dieser erste oder zweite Lebensabschnitt war aufgrund meines Alters überwiegend fremdbestimmt. Ich war im Kindergarten und in der Grundschule, in der F-D-Jugend beim Fußballverein Kickers 1900. Der Urlaub mit den Eltern ging überwiegend zur Oma in der Nähe von Ulm. Aber auch nach Bayern und Spanien, aber auch mit einer Kindergruppe nach Österreich. 

Neben Schule und Sport und Spielen gabs natürlich auch Fernsehen. Ich konnte ich fünf Programme empfangen. ARD, ZDF und den SFB. Die ARD hatte abends [Sendeschluss](https://www.youtube.com/watch?v=cnXeMUUvAdk&pp=ygURYXJkIDE5ODcgVGVzdGJpbGQ%3D&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/) bis zum nächsten Tag um 14:00 Uhr. Ähnlich war es bei den anderen Programmen. Als privilegierter West-Berliner konnte ich auch noch DDR1 und DDR2 empfangen. Auf dem großen Fernseher sogar mit Ostfarbe, wie es damals hieß. Dort schaute ich das Sandmännchen und "Mach mit mach's nach mach's besser mit" Adi und zwei Mädchen. Gelegentlich, mit viel Glück und selbstgebauten Antennen konnte ich auch AFN und BFBS im Radio empfangen.

Das Kinderprogramm, das ich gesehen habe:

- ["Spaß am Dienstag"](https://www.youtube.com/watch?v=G_G8l1-lIOk&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/)
- ["Hallo Spencer"](https://www.youtube.com/watch?v=qMCGIkHdW00&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/) 
- ["Die Fraggles"](https://www.youtube.com/watch?v=qeEWcl2_uQM&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/)
- ["Heidi - Deine Welt sind die Berge"](https://www.youtube.com/watch?v=Pu7MjOd0o0Y&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/) - Das Waisenkind Heidi kommt zum Opa auf die Alm
- ["Marco"](https://www.youtube.com/watch?v=dv95nkq7baU&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/) - Mmarcos Mutter verlässt die Familie in Italien, um in Argentinien arbeiten zu gehen. Marco macht sich allen auf die Reise, um sie zu finden
- ["Alice im Wunderland](https://www.youtube.com/watch?v=M6Et2KXfyo8&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/) - Habe ich sogar auch noch ca. 2005 geschaut, als ich morgens von der Frühschicht in Tegel kam.

und natürlich auch die Klassiker wie "Die Sendung mit der Maus" und "Löwenzahn". Im DDR-Fernsehen habe ich 

- [Mach mit, machs nach, machs besser](https://www.youtube.com/watch?v=lCg8yX3aaZ8&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/) - Uffta, uffta, täterää.
- [Das Sandmännchen](https://www.youtube.com/watch?v=aJ0sFUivOHo&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/)
- [seltener auch den Brummkreisel](https://www.youtube.com/watch?v=tD3zn_ms5so&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/)

geschaut. Es gab aber noch viele andere Serien im Vorabendprogramm von Anwälten, Ärzten, Baletttänzerinnen mit [Silvia Seidel +2012](https://www.youtube.com/watch?v=vgUMgykjeMo&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/), Frauen im Imbiss, und auch noch Geschichten aus dem [Kaiserreich](https://www.youtube.com/watch?v=wBF9iq__M70&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/). Ich glaube in die Schauspielerin [Anja Bayer](https://de.wikipedia.org/wiki/Anja_Bayer) war ich damals verliebt.

Bei der Nachbarin habe ich häufiger das Vorabendprogramm (für Erwachsene) im ZDF geschaut: ["Ein Colt für alle Fälle"](https://www.youtube.com/watch?v=3VXd0uPt908&list=PLW1C3UdlK_Je7bfxK3cfZNCZ2HZe4A5M4&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/), ["Ein Trio mit vier Fäusten"](https://www.youtube.com/watch?v=miNbDrX3OYs&list=PLQpzKXtb6C1tzeOW_NndMQet-n36FG8Mn&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/), ["Hart aber herzlich"](https://www.youtube.com/watch?v=99RDVvbKzC4&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/) und andere Serien. Mitte der 1980er konnten wir zuerst Sat.1, Jahre später dann RTL empfangen. Als ich dann einen [eigenen Fernseher (ungefähr so einen)](http://www.sonnewald.de/kugelfernseher-jvc-swws-gehaeuse-ws-4390.html) hatte, konnte ich auch mehrteilige Miniserie "[V - Die außerirdischen Besucher kommen](https://www.youtube.com/watch?v=wyXjmcy6BgI&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/). Es ist bis heute einer meiner Lieblingsserien. Absolut empfehlenswert. Kurz vor Ende dieses Lebensabschnitt in den nächsten hinein auch noch ["Max Gyver"](https://www.youtube.com/watch?v=yOEe1uzurKo&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt) und ["Max Headroom"](https://www.youtube.com/watch?v=aZY-yQYVf38&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt).

Der erste Computer im Haus war ein [Commodore +4](https://www.youtube.com/watch?v=dgnZPfNpouo&origin=https://hammeley.info/ego/1990/07/01/neuer-lebensabschnitt/), der dann später für einen C64 ausgetauscht wurde.

Während dieses Lebensabschnitts stirbt ein Vater zweier Brüder und Mitschüler auf der Grundschule durch einen Stromschlag und die Freundin meines Halbbruders und ihr Bruder bei einem Motorradunfall. 