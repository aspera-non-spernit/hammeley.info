---
title: Der Silberschmied
layout: default
---
Irgendwann behauptete eine Sachbearbeiterin beim Jobcenter, ich müsste alle Leistungen zurückzahlen, wenn ich mal erben würde. Auf die Frage, von wem und wieviel ich denn mal erben würde, meinte sie 100 Millionen.

Naja. Gut. Auf jedenfall dachte ich mir, dass ich schonmal die Zeichen des Reichtums annehme. Vor allem Schmuck. Da dieses Erbe wahrscheinlich erst in einen meiner nächsten Leben kommt, und ich nahezu mittellos bin und wegen des Studiums sogar noch etwas verschuldet bin, bleibt natürlich kein Geld übrig für teuren Schmuck. 

Ich fand eine Alternative auf Youtube. In Videos wird gezeigt, wie man aus Münzen Ringe herstellen kann. Ich schaue mir zahlreiche Videos an, kaufe bei proaurum eine Libertad (1 Unze) und gehe damit zu einer Goldschmiedin in der Nähe, die Workshops anbietet. Ich frage sie, ob ich anstelle ihres Standardprogramms einfach ihr Werkzeug nutzen kann, um meine Münze zu einem Ring zu machen. Ich zahle dennoch den kompletten Kurspreis.

Ich bohre die Münze vor, und alles andere machte ich in zwei Stunden (mit etwas Übung sicherlich schneller möglich) meinen Ring.

<div class="row" id="silberschmied"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
		  	<div id="carouselSilberCtrls" class="carousel slide" data-bs-ride="carousel">
		  		<div class="carousel-inner" style="background-color: #333">
                    <div class="carousel-item active">
						<div class="d-flex justify-content-center">
                            <img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/libertad.jpg" alt="Libertad">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="d-flex justify-content-center">
                            <img class="rounded lazyload d-block w-100" src="/assets/images/placeholder-image.png" data-src="/assets/images/ring.jpg" alt="Silberring">
				  		</div>
					</div>
                     <button class="carousel-control-prev" type="button" data-bs-target="#carouselSilberCtrls" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Vorheriges</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselSilberCtrls" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Nächstes</span>
                    </button>
				</div>
			</div>
		</div>
  	</div>
</div>

Den Ring habe ich natürlich irgendwann verloren. Ich glaube in der Parkaue, als ich die Enten dort fütterte.