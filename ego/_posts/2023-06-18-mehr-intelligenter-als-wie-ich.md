---
title: Mehr intelligenterer als wie ich
layout: default
---
Als Schwurbler ist man bekanntlich nicht nur Verfassungs- und Menschenfeind, sondern auch sehr dumm und nativ. Ich hatte also die Idee heute mithilfe einer echten Intelligenz, nämlich ChatGPT 3.5 ein kleines neuronales Netzwerk in der Programmiersprache rust zu schreiben und zu beweisen, dass es nicht mehr lange dauert, bis die ganzen Computernerds arbeitslos werden.

Da ich weder programmieren kann, noch sonst von irgendwas Ahnung habe, was in der Welt vor sich geht, war die Hoffnung entsprechend groß.

Ich wurde nicht enttäuscht. Es brauchte nur zwei Versuche und ca. zwei Stunden, bis ChatGPT das kleine Programm **percept** soweit geschrieben hat, dass es fehlerfrei kompiliert und auch das tut, was es tun soll.

Das Programm sollte

- die Buchstaben X und O als einen Vektor mit der Länge 25 so generieren
- dass Pixelwerte eines 5x5 1-Kanal-Bildes so angeordnet werden, dass man daraus ein X oder ein O erkennt, also ungefähr so:

```bash
Der Buchstabe X

1.0 0.0 0.0 0.0 1.0
0.0 1.0 0.0 1.0 0.0
0.0 0.0 1.0 0.0 0.0
0.0 1.0 0.0 1.0 0.0
1.0 0.0 0.0 0.0 1.0

bzw.

[1.0 0.0 0.0 0.0 1.0 0.0 1.0 0.0 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 0.0 1.0 0.0 1.0 0.0 0.0 0.0 1.0]

```
und das Gleiche eben für den Buchstaben O. Anstelle der perfekten Pixelwerte sollten dann noch etwas zufällige Zahlen hineingemischt werden, um Overfitting zu vermeiden. Also ungefähr so:

```bash
Der Buchstabe X

[0.9552764064486101, 0.11456329301414327, 0.04756514107782396, 0.02301265893829037, 0.9460340085976147, 0.12053173833045698, 0.94357232171275, 0.1054230262919237, 0.9207849031696771, 0.05669603124657045, 0.06876873202461413, 0.006368815986481826, 0.9367473092355467, 0.04494740914294507, 0.08615529829880104, 0.15187395988691496, 0.7988392040966688, 0.1350482175817918, 0.741961067885922, 0.09202618276820836, 0.7007504978088681, 0.13535248200548766, 0.11778886932605825, 0.13713085335506264, 0.8253311484026673]
```

Aktive Pixel haben hier die Werte >0.7 und inaktive Werte von <0.2

Na ja, und dann soll das Programm halt anhand eines Trainingssets das neuronale Netzwerk trainieren. Mit einem Testset (entweder mit perfekten oder zufälligen Pixelwerten) soll das Netzwerk dann ausgeben, ob es ein X oder ein O erkennt.

Der Anfang mit ChatGPT war ganz gut, nur irgendwann ging es nicht mehr weiter, und die Fehler waren nicht mehr zu korrigieren, sodass ich ChatGPT aufgefordert habe alles zu vergessen, was bisher gesagt wurde. Ich habe den Code von fehlerhaftem Code befreit, den bereinigten Code ChatGPT zur Ansicht gegeben und gefragt, ob ChatGPT versteht, was damit gemeint ist. Danach ging es recht zügig weiter. Ein paar Fehler mussten dann hier und dann korrigiert werden, aber auch nicht mehr, als wenn ich selbst programmieren würde. 

Es lief zwar nicht alles perfekt mit ChatGPT 3.5, aber es ist der schnellere und der bessere Programmierer. Das ist super, dann hab ich mehr Zeit zum Schwurbeln.

Die ganze Konversation ist [hier nachzulesen](https://chat.openai.com/share/cc6657b4-8239-4145-90a0-cfcbe45814ae). Das [Projekt](https://gitlab.com/aspera-non-spernit/percept) ist auf gitlab verfügbar.

Wer direkt zum zweiten Versuch springen möchte, sucht nach 'starting over'. Dort sagte ich ChatGPT, dass wir mehr der weniger von vorne beginnen.
