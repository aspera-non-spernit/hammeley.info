---
title: Treu geleistete Dienste
layout: default
layout: default
---
Ich wurde heute nach acht Jahren an drei Standorten und zwei Disziplinarbußen mit Dank und Anerkennung für meine dem Deutschen Volk geleisteten Dienste aus dem Dienst bei der Bundeswehr entlassen.

Ich bin ziemlich froh, dass ich hier raus bin. Es gab eine Zeit, da war es in Ordnung, aber die fehlende Perspektive, Frauenquote, die Beteiligung an einem aus meiner Sicht völkerrechtswidrigen Krieg im Irak, der noch viele Jahre andauern soll, hat es für mich seit ca. 2004 unerträglich gemacht.

![Abzeichen](){:data-src="/assets/images/abzeichen.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }
<!-- more -->
Das höchste Netto-Gehalt, das ich während der acht Jahre erhalten habe, betrug für den Dezember 2002: 2.564,24 € (inkl. Zulagen und Sonderzahlung).

In den letzten 15 Monaten erhielt ich 100% meines Gehalts (ohne Zulagen) weitergezahlt, obwohl ich bereits aus dem aktiven Dienst ausgeschieden bin.

Zum Abschluss erhalte ich noch Übergangsbeihilfe in Höhe von 10.942,19 €. Ich erhalte für die nächsten 21 Monate 70% meines Gehalts weiterbezahlt.

Dank meines sparsamen Lebenswandels konnte ich in den letzten Jahren ca. 60.000 € ansparen.

Ich werde jetzt irgendwas mit Medien machen. Ansonsten mal schauen.

Ich darf mich jetzt den Fragen der Familie stellen: "Warum bist Du nicht da geblieben?", "Wollten die dich nicht?"

**Rückblick:** Ich erfahre im Laufe dieses Lebensabschnitts vom Suizid einer Mitschülerin der Grundschule.

**Ausblick:** Nach fünf Jahren in Bayern und Nordrheinwestfalen, bin ich seit 2005 wieder in Berlin. Mit dem Übergang in den BfD begann dann langsam der Übergang in einen neuen Lebensabschnitt. Mit dem Dienstzeitende ender auch der Lebensabschnitt der im November 1999 begann und durch die Zeit bei der Bundeswehr und den 11. September 2001 geprägt war.
