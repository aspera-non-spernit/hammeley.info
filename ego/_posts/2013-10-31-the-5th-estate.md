---
title: The Fifth Estate
layout: default
tags: "Ersparnisse, Bundeswehr, Open, University, Film, Fernseh, Werbe, Komparse, Geld, TV, Kino, Onlie, Catering, Kulisse, Maske, Drehbuch, Schauspieler, Regisseur, Hanks"
---
Da meine Ersparnisse aus der Zeit bei der Bundeswehr langsam zur Neige gehen, verdiene ich mir neben meinem Studium an der Open University unter anderem als Komparse in Film und Fernsehen etwas dazu. In den nächsten Jahren werde ich in über 50 deutschen und internationalen [Film-, Fernseh- und Werbeproduktionen](/assets/Komparserie.pdf){:rel=“nofollow”} mitwirken.

The Fith Estate mit der hübschen Alicia Vikander, Moritz Bleibtreu, Daniel Brühl, Benedeikt Cumberbatch und vielen anderen ist der erste Film, bei dem ich 2013 mitspiele.

Komparsen werden sehr schlecht bezahlt und die Drehtage dauern manchmal bis zu 14 Stunden, bei Wind und Wetter.

Die Motivation, mehr als nötig als Komparse zu arbeiten, ist in der Regel nicht das Geld. Während bei Film- und Fernsehproduktionen mit dem Mindestlohn und teilweise mit Verpflegung unterschiedlicher Qualität bezahlt wird, können bei Werbeproduktionen schon mal ein paar hundert Euro verdient werden. Also als Statist, nicht als Werbe-"Hauptdarsteller". Diese (nicht prominenten) Werbedarsteller erhalten meist mehrere tausend Euro, je nach Dauer und Kanal (TV, Kino, Online) der Werbekampagne. Dies verhindert dann aber oft weitere Werbedeals, da diese Personen nicht für mehrere Werbetreibende gleichzeitig eingesetzt werden können.

Als Komparse ist es auch immer Glückssache, ob man im fertigen Film zu sehen ist und ich schaue mir nicht jeden Film an, in dem ich mitgespielt habe. Denn als Komparse hat man im Gegensatz zu Schauspielern keinen wirklichen Einfluss auf die Auswahl. Man nimmt, was man kriegen kann. Es ist also auch viel Müll dabei. Manchmal weiß nur der Komparse selbst, dass er zu sehen ist. In dem Film "WhoAmI" zum Beispiel sieht man nur meinen schönen Zeigefinger, der "MRX" in die Tasten tippt.

![Aus dem Film "Die Bücherdiebin"](){:data-src="/assets/images/buecherdieb.png" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

<!-- more -->

Manche Produktionen punkten mit der Kulisse, den historischen Kostümen oder auch dem Catering. Manchmal ist einfach nur das Wetter toll und man hat viel Zeit, sich mit anderen Komparsen zu unterhalten. Studiodrehs wie in Babelsberg oder an echten Schauplätzen können ihren Reiz haben, aber auch langweilig sein. So war das Set für den Film "Monuments Men" im Gegensatz zum Film selbst ein Erlebnis (in einer Szene renne ich schnell vor einem Panzer über die Straße). Aber auch als Arzt in einem echten Krankenhaus zu spielen, wie in dem Sat.1-Film "Zwei Leben, eine Hoffnung", oder an historischen Orten in historischen Filmen mitzuspielen, ist manchmal die harte Arbeit wert.

Viel hängt auch von der Stimmung ab. Manchmal sorgt schon das Drehbuch für ein interessantes Umfeld. Die Filmcrews sind sehr unterschiedlich und auch die anderen Komparsen können interessanter oder eher langweilig sein. Insgesamt ist die Arbeitsatmosphäre aber entspannt und interessant.

Manchmal sind Komparsenjobs Massenjobs, z.B. wenn viele Demonstranten oder Soldaten benötigt werden. Bei solchen Produktionen können mehrere hundert Komparsen gleichzeitig anwesend sein. Meistens, aber nicht immer, ist bei diesen Produktionen die Qualität des Caterings und der Abfertigung (Check-in, Abrechnung) nicht so gut wie bei "Boutique"-Jobs, bei denen die Produktion nur fünf bis zehn Komparsen benötigt. Aber auch das ist keine Garantie dafür, dass man im fertigen Film zu sehen ist oder dass der Job interessant ist. Manchmal werden am Ende ganze Szenen gestrichen. Manchmal sind diese kleinen Jobs so kurz, dass es auch kein Catering gibt und man nach ein paar Stunden und einer halben Gage wieder nach Hause fahren kann.

Welche Schauspieler "mit mir arbeiten durften", interessiert mich dagegen weniger. Obwohl ich schon immer gerne Filme geschaut habe, kenne ich die meisten Filme nicht, in denen sie mitgespielt haben, und ich kenne auch nicht alle "Promi-News" wie andere Filmfans. Ich habe mir auch nie Filme wegen eines Regisseurs oder Schauspielers angeschaut oder bin zu einer Premiere gegangen, um ein Autogramm zu bekommen.

Manchmal lohnt es sich, mitzumachen, wenn hübsche Schauspielerinnen am Set sind. Zum Beispiel Alicia Vikander in The Fifth Estate, Tena Desae und Tuppence Middleton in Sense8 oder Charlize Theron in Atomic Blonde. Alle anderen sind natürlich auch sehr hübsch (ich kann mich nicht an alle erinnern).

Ansonsten hat man mit Schauspielern oder Regisseuren eigentlich nichts zu tun, weil bei Filmproduktionen schon sehr zwischen "Above The Line" und "Below The Line" unterschieden wird. Das heißt, die wichtige Filmcrew "Above The Line": Schauspieler, Regisseure, Kamera etc. ist faktisch getrennt von den eher unwichtigen Mitwirkenden "Below The Line" wie 2nd Unit, Komparserie, Production Runner oder Catering. Man spricht nicht miteinander, isst nicht zusammen, verbringt die Pausen getrennt, beginnt und beendet die Arbeit zu unterschiedlichen Zeiten.

