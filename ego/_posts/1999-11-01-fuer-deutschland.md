---
title: Für Deutschland
layout: default
tags: "Bundeswehr, Wehrpflicht, Luftwaffe, Goslar, LwAusbRgt, Wiedervereinigung"
---
Nachdem ich mich nun anderthalb Jahre drücken konnte, bin ich heute am Bahnhof Goslar angekommen. Dort warten schon die Feldjäger und nehmen uns mit. Zumindest konnte ich bei der letzten Musterung einen Einsatzwunsch bei der Luftwaffe äußern. Zivildienst, der noch einmal drei Monate länger dauern soll, kam für mich nicht infrage.

**Ausblick:** 

Mit dem Wehrdienst beginnt heute ein neuer Lebensabschnitt, der bis ca. 2006 gehen wird.

Morgen erhhalte ich meine Kampf- und Friedenszusatzausstattung.

Ich werde für die nächsten sechs Wochen in der 8. / LwAusbRgt. 1 mit mehreren Unbekannten in einer Stube verbringen und lernen wie man Zeit rumbringt. Ich werde häufig in der Kälte stehen und warten, im Kreis laufen und vielleicht ein oder zwei Mal ein G3 schießen. Ich bin der erste seit meinem Opa der "Dienst an der Waffe" ausübt. Tolle Wiedervereinigung.

Es ist halb Abenteuer, halb vergeudete Zeit. Ich hatte auch nichts besseres zu tun und das Studium des Lebens der letzten Jahre war teuer.

**Rückblick:**

Kurz vor unseren Schießübungen auf dem nahegelegenen Übungsplatz Bergen Belsen wird uns mitgeteilt, dass sich dort vor zwei Jahren ein Schießunfall ereignet hat. Bei einem Nachtschießen wurde ein Wehrpflichtiger der 7. Kompanie des LwAusbRgt. 1 versehentlich erschossen, ein anderer schwer verletzt. Es wird deshalb nur noch in einer Rotte geschossen.

[Christian Stöbe *19.08.1978 +22.10.1997](https://www.fam-stöbe.de/wordpress/)
