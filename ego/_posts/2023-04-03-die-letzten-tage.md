---
title: Die letzten Tage
layout: protected
cipher: cookieEncr
---
### Berlin, Deutschland, der Sozialstaat

- 2013/14: Erhalte Informationen über Corona im Jahre 2020 in der Bar Kptn. A.W. in der Simon-Dach-Straße
- Werde von verschiedenen Personen in der Bar "interviewt". Ziemlich ungewöhnlich, bin doch ein ganz normaler Typ. Gebe (Corona)-Informationen u.a. an ein großes Netzwerk weiter.
- Zeitschleife (Aufnahme Studium in Potsdam, Beschäftigung beim ITZ-Bund, Corona-Paralleluniversum in Friedrichshain)
- Ich wette mit einem Grünen-Politiker in der Simon-Dach-Straße um einen Euro, dass es 2020 eine Corona-Pandemie geben wird. Möglicherweise hat er nun ein Kiezbüro in der Nähe.
- Erzähle <b>T. Krauslach</b> vom Coaching "inzeit" darüber.
- Nach Beschäftigung bei Wahlfälschern und Vetternwirtschatfern des Amt für Statistik Berlin-Brandenburg ab Sommer 2017 Mitglied der Post-Industriellen Reservearmee.
- Sommer 2017: informiere Familie darüber.
- **27.11.2019**: Vorstellungsgepräch beim Auswärtigen Amt. Beim Assessment sitzt jemand, der mich an jemanden erinnert, der mir in den 1990er Jahren in der Sonntagstraße (Kneipe "Netzwerk") über den Weg lief. Ich erinnere mich an einen Mitschüler. Ich verabschiede mich mit prägnantem Handdruck. Er könnte heute in Treptow oder Köln arbeiten. Ich spreche dort "Corona" an. Eine Person lacht.
- In den nächsten Jahren begegnen mir Personen (auch öffentlich bekannte) aus den "Ferninterviews" aus der Bar Kptn. A. W.
- Oktober 2020: Erneute Aufnahme Masterstudium an der Uni-Potsdam
- Keine Arbeit zu Oktober gefunden
- Einstellung beim ITZ-Bund erst ab Dezember 2020. Arbeite zwei mal wöchentlich ab 06:00 Uhr, teils alleine im Gebäude (wegen Corona)
- Jobcenter fordert Leistungen zurück
- Familie kann sich an nichts erinnern, weder an Gespräche noch an überlassene Gegenstände oder Dokumente
- Schwägerin schreibt einen Brief, meint ich würde eine Show abziehen und wünscht mir viel Spaß mit meinem neuen Lebensabschnitt
- Beim ITZ-Bund muss nun gegendert werden
- 15.November 2021: Verbot ohne Verzicht auf Grundrechte zu arbeiten. Arbeite eine Woche im selbstverordneten Home-Office
- Bis 19. Dezember 2021: eMail-Verkehr mit Vorgesetzten
- Mir wird seitens der Personalabteilung in einem unverschämten Ton Arbeitsverweigerung unterstellt, Gehalt wird einbehalten und soll zurückgefordert werden
- Einen Monat keine Miete und Krankenversicherung bezahlt
- Faschistische Medienkampagne gegen Ungeimpfte
- Es werden zahlreiche Personen mit "falschem Wertekanon" entlassen: Professoren, Comedians, Musiker usw.
- 2.Februar 2022: Antrag auf Leistungen zum Lebensunterhalt abgelehnt (Status Student und beschäftigt)
- 22.Februar 2022: Widerspruch und Antrag auf Einstweilige Verfügung abgelehnt (S 104 AS 663/22 ER)
- Drei Monate keine Miete und Krankenversicherung bezahlt. GEZ will Geld
- Aufforderung zur Mitwirkung (Weiterbewilligungsantrag) für einen abgelehnten Antrag
- Antrag wird weitere acht Wochen hinausgezögert (<b>Herr Hahn 752</b> fordert nun z.B. Nachweise der Selbstständigkeit. Bin seit 10 Jahren nicht Selbstständig)
- Leistungen bewilligt aber nicht überwiesen
- Leistungen zum Lebensunterhalt werden rückwirkend überwiesen
- 4.Mai 2022: Jobcenter veranlasst Pfändung des Kontos
- Konto wird durch Jobcenter (über Zoll) und AOK gepfändet. Kein Zugriff auf Konto
- Kann wieder keine Miete zahlen (fünf Monate keine Miete bezahlt)
- Gericht wird während anhängiger Klage darüber informiert (AZ: S 120  AL 971/21)
- Gericht entscheidet wohl das Konto freizugeben (zumindest für den Teil den das JC gepfändet hat)
- 30.Mai 2022: Jobcenter veranlasst die Rücknahme der Pfändung (<b>Hr. Laß</b>)
- 3.Juni 2022 Zoll informiert: Die Pfändungs- und Einziehungsverfügungen werden aufgehoben
- Zwei Monate rückwirkend Miete bezahlt (jetzt drei Monatsmieten Rückstand)
- 9.Juni 2022: <b>Fr. Birthe Hoffmann</b> vom JC ruft die Polizei: <a data-bs-toggle="collapse" href="#tuer" role="button" aria-expanded="false" aria-controls="#tuer">Polizei tritt Wohnungstüre ein (220610-1130-348654)</a>
<div class="row collapse" id="tuer"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
		  	<div id="carouselTuerCtrls" class="carousel slide" data-bs-ride="carousel">
		  		<div class="carousel-inner" style="background-color: #333">
                    <div class="carousel-item active">
						<div class="carousel-caption d-none d-md-block">
							<h5>Zerstörte Türe</h5>
					  	</div>
						<div class="d-flex justify-content-center">
					  		<img src="/assets/images/tuer_2_20220609.jpg" alt="Zersttörte Türe">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Zerstörte Türe</h5>
					  	</div>
						<div class="d-flex justify-content-center">
					  		<img src="/assets/images/tuer_1_20220609.jpg" alt="Zersttörte Türe">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Zerstörte Türe</h5>
					  	</div>
				  		<div class="d-flex justify-content-center">
				  			<img src="/assets/images/tuer_3_20220609.jpg" alt="Zersttörte Türe">
			  			</div>
					</div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselTuerCtrls" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Vorheriges</span>
			  	</button>
			  	<button class="carousel-control-next" type="button" data-bs-target="#carouselTuerCtrls" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Nächstes</span>
			  	</button>
            </div>
        </div>
    </div>
</div>
- 30.Juni 2022: Kündigung der Wohnung (derzeit drei Monatsmieten im Rückstand)
- Räumungsklage (6 C 279/22)
- Ich werde zur Clearingstelle, zur Sozialen Wohnhilfe und zum Sozialpsychiatrischen Dienst weitergeleitet
- Ich soll trotz Klage unbedingt meine Schuld anerkennen, sonst kann mir nicht geholfen werden (<b>Fischer, Eberlein, Lütke-Lanfer, Wrede-Büntjen</b>)
- Sozialpsychiatrischer Dienst schaltet sich ein. Ich bin jetzt wohl behindert
- Suche mir Wohnungen. In ganz Berlin weniger als 50 verfügbar (im Rahmen des Budgets). Von niemanden Antwort erhalten.
- 3.Januar 2023: Räumungsurteil mit Räumungsdatum
- Meldung beim Gesundheitsamt durch Dritte
- 19.Januar 2023: Auskunft über gespeicherte Daten nach DGSVO angefragt. Antrag an Bezirksamt geschickt. Wird von <b>Hr. David (BA)</b> an <b>Herrn Dr. Graubner</b> beim Gesundheitsamt weitergeleitet. Keine Antwort erhalten
- 17.Februar 2023: Aufforderung zur Mitwirkung (Weiterbewilligungsantrag)
- 13.März 2023: Berechtigungsanachweis endlich erhalten. BVG-Kundenkarte beantragt. Antrag wird bestätigt. Keine Kundenkarte erhalten.
- 13.März 2023: Aufforderung zur Mitwirkung: Miete wird auf Grund Räumungsurteils und zukünftiger Wohnungslosigkeit nicht anerkannt (<b>Hr. Hahn</b>)
- 17.März 2023: Antwort per eMail
- 31.März 2023: Keine Leistungen erhalten. Keine Antwort erhalten
- 3.April 2023: Erneute Anfrage per eMail
- 3.April 2023: Weiterbewilligungsantrag kann nicht weiterbearbeitet werden, da ich in Zukunft postalisch nicht erreichbar bin (<b>Fr. Deutrich 732</b>). Ich soll mich täglich bei der Obdachlosenhilfe oder anderen Organisationen melden, um Leistungen erhalten zu können.
- Nutzungsentgelt für die bewohnte Wohnung kann nicht bezahlt werden. (wieder vier Monate Rückstand). Erhalte auch keine Regelleistungen. Erneute Mittellosigkeit. Informiere das Sozialgericht bei anhängiger Klage.
- <b>Frau Deutrich, Frau Mielke, Herr Hahn</b> werden sterben. Heil Hitler!
- Zukunft: Jobcenter wird Anzeige erstatten. Nicht zum ersten Mal. Denn all das hatten wir schoneinmal. Damals habe ich die Polizei gebeten in meine Akte zu schreiben, dass ich von Corona rumphantasiere (also vor der Pandemie. Bin nämlich Zeitreisender).
- <b>Frau van Aart</b> will mich sechs Monate zu einer besonderen Maßnahme schicken, die sich in die Maßnahmen wie "DOC-Center" und" "Lichtblick" einreiht und schwurbelt was von viel Erreichen und Energie und entspanntem Leben.
- 11.April 2023: Das Amtsgericht teilt mit statt 182,00€ an Gerichtsgebühren werden nun zusätzlich 882,39€ fällig. Das ist der Betrag, den die Anwältin der Käger an fünf Tagen an mir verdienen möchte.
- 21.April 2023: Schreibe dem Amtsgericht, dass die Gläubiger die Frist für eine sofortige Beschwerde nicht eingehalten haben (fast 3 Monate nach KFB).
- 25.April 2023: Amtsgericht antwortet, schildert deren Auffasung. Frage mich wozu es Fristen gibt. Ich soll jetzt meine Einwände zurücknehmen. Ganz sicher nicht.
- 29.April 2023: Jobcenter überweist Regelbedarf für April und Mai aber keine Miete. Berechtigungsnachweis auch nicht erhalten. JC behauptet ich hätte Leistungen (für die Miete) nicht zweckentsprechend verwendet. Welche Leistungen? Für was denn verwendet? Was für eine Lüge. Ich muss jeden Tag kotzen. Dieser verlogene Drecksstaat.
- 17.Mai 2023: Amtsgericht erlässt neuen Kostenfestsetzungsbescheid 
- 20.Mai 2023 Meldung an die Sozialversicherungen für 01/23 - 03/23 wird storniert. (Mir fehlen also drei Rentenmonate)
- Lege innerhalb der Frist Beschwerde gegen KFB des Amtsgerichts ein
- 9.Juni 2023: Räumungstermin erhalten. Wenigstens die AfD steht nun bei den ersten Demoskopen bei 20 % und ist damit vor SPD und hinter CDU, zweitstärkste Partei.
- 12.Juni 2023: Meiner Beschwerde gegen neuen KFB wird nicht abgeholfen. Meine Sofortige Beschwerde (Blatt 62,63) liegt nun beim Landgericht Berlin
- Mein Leben endet vorzeitig. Schicksal.
- 21.Juli 2023: Ich wurde heute wieder aus der Wohnung gesetzt. Schlösser und Namensschilder (Haustüre noch nicht) wurden getauscht und entfernt. Ich bin bereits um 6:00 aus der Wohnung. Ich reise viel mit dem Zug. Hier ist es nachts warm und ich kann ruhen, Zähne putzen, mich frisch machen und umziehen. Aber halt alles unregelmäßig. Bisher geht es ganz gut. Noch setzen sich Leute neben mich oder bieten mir einen Platz an. Mich fragen sogar die anderen Obdachlosen nach Geld. Was für eine Komik. Trotz riesiger Augenringe und Kleinstpupillen sehe ich wohl noch ganz pasabel aus und gehöre hier mit Tablet zur Obdachlosenoberschicht. So sehen die nun mal aus im Jahr 2023. Geschlafen habe ich seit Verlassen der Wohnung nicht. Mein Magen macht aber schon Probleme, ich muss häufig aufs Klo. Alkohol habe ich noch keinen getrunken. Ich übernächtige in Bahnhöfen, im Zug oder im Park. Bis Ende des Monats, dann kann ich auch kein Deutschland-Ticket mehr zahlen. Mit Gepäck ist alles mühsam, es ist nicht nur schwer, es darf aber auch nicht geklaut werden. Ein Trekking-Rucksack mit Inhalt ist alles was ich derzeit besitze. Die Unterbringung fürs Gepäck ist teuer. 5-6€. Ich muss auch immer planen, wo gibt es etwas zu essen, wo eine (kostenlose) Toilette. Wo bleibe ich die Nacht. Die Polizei wollte noch nichts von mir. Im Erfurter Stadtpark hat sie mich von der Ferne angeleuchtet, ich habe zurückgeleuchtet. Das wars. Es wurde sehr kalt, bin gegen 4 dann in den Bahnhof und nach Berlin. Heute (23.) zum Frischmachen schnell nach Postdam gefahren. Was halt gerade fährt.
Es gibt noch kein Verfahren wegen der Pfändung zur Befriedigung der Gläubiger. Die Möbel dürften noch mir gehören. Wenn jemand etwas will: hochwertiges Holzbett (Zack Design), hochwertiges Sofa (Gepade), Holztische, Stühle, Vitrine, Schrank, E-Gitarre (Squier), E-Piano (Casio, recht ok), neuer Laptop (Lenovo), neuer Monitor (HP). Ich habe mein Testament auf dem Schreibtisch belassen und <b>Frau Obergerichtsvollzieherin Dusatko</b> gebeten, es beim Gericht abzugeben. Ein wortgleiches trage ich mit mir mit. Man weiß ja nie. Nein, ich habe nichts zu vererben, es gibt dennoch Dinge zu regeln.
Für die Verwahrung bei Gericht braucht es aber einen extra Antrag und die Zahlung einer Gebühr. Da ich keine Post erhalte und Öffentliche Bekanntmachungen nicht mitbekomme, kann es gut sein, dass es irgendwann einen Haftbefehl zur Abgabe der Eidesstattlichen Versicherung gibt. Solange plane ich aber nicht. 
Das wievielte mal ist es diesmal? Ich habe den Überblick verloren.
- Gehe zur Sozialen Wohnhilfe. SB mit osteuropäischem Akzent meint, ich sähe gar nicht aus wie ein Obdachloser. Habe keinen Bewilligungsbescheid dabei, der ist in der geräumten Wohnung. Man kann mir hier nicht helfen. Man würde mich eh nur einem 8-Bett-Zimmer zuweisen. Sie bestätigt, dass ich vor einem Jahr schonmal hier war. Ich wäre ein Fall fürs Betreute Wohnen. SB macht auf mich den Eindruck sie reif für die Klapse ist. Fake-Kundin vom letzten Mal ist auch wieder da und interessiert sich für meine Geschichte und empfiehlt mir auch das Betreute Wohnen. 
- 14.Juli 2023: Termin beim Jobcenter. Kooperationsplan erhalten. Ich soll mir eine Wohnung suchen. ah ja. SB <b>van Aart</b> bietet mir wieder ein sechs monatiges Coaching an. "Wohnen in Sicht". Kostet dem Staat mind.
6000€. Mietschulden weniger als 3000€. Gebe danach im Bezirksamt Frankfurter Allee einen WBS - Antrag ab.
- 30.Juli 2023: Erhalte Post vom Wohnamt. Der WBS-Antrag ist nun (mit Eingangsdatum 30.Juli) eingegangen. Bearbeitung kann einige Zeit in Anspruch nehmen. Man soll bloß nicht nachfragen.
- 15.August 2023: Bemühe mich um die Einlagerung meiner Möbel. Das günstigste Angebot kostet alleine für die Abholung aus dem Erdgeschoss 529,95€, Miete für vier Wochen 175,95€. Jobcenter genehmigt ca. 200€ + ca. 25€ für die Einlagerung pro Monat. Ich schreibe dem Jobcenter, dass das bisherige Angebot nicht ausreicht. Keine Antwort (bzw. die Fakten werden ignoriert) und erneut mitgeteilt, dass doch die Kosten für den Umzug genehmigt wurden.
- September: Fahre nach Hamburg, Verprasse fast mein letztes Geld in St. Pauli /  Reeperbahn / Große Freiheit, inkl. Titten-Bar. Treffe auf Leute, wie einen jungen Polizisten und einen  Mexikaner. Verliere Portemonnaie mit allem. Ausweis, Deutschland-Ticket, Giro-Karte usw.
- 13.September 2023: Das JC fordert nun wieder Geld. Diesmal 2.722,99€. Für drei Monate (10-12/2020) ALG2, die krumme Zahl von 1123,20€. Für drei Monate Miete, die krumme Zahl von 1348,49€. Krankenversicherung für 11/20 und 12/20 in Höhe von 192,18€, Pflegev. 44,02€ und dann für 11/20 und 12/20 einen "dsch Zusatzb" in Höhe von 15,10€. Das war der Ursprungsbeitrag. Irgendwie scheint der Ursprungsbeitrag dann noch um 48,14€ reduziert worden. So das ein Restbetrag von 2.674,85€ zu zahlen bis 27.9.2023 gefordert wird. Na dann viel Glück beim erneuten pfänden.
- 21.September 2023: Mein Rucksack mit Kleidung, Tablet, Lesebrillen, Testament und anderen Dokumenten (Jobcenter, Gericht, Pin-Brief, Photo-Tan-Gerät) wird nachts gestohlen. Möchte sterben. Ich habe nur noch das, was ich an mir trage. Teils seit zwei Tagen. Muss mir neues Messer/Rasierklingen kaufen.
- 29.September 2023: Die 703,80€, die seit über einem Jahr wegen der AOK auf meinen Konto eingefroren waren wurden nun über die Drittschulderregelung vom Konto geholt.
- 2.Oktober 2023: <b>Stefanie Bauer</b> von Heimstaden teilt nach mehrmaligem Nachfragen mit, dass meine Sachen aus der Wohnung entsorgt wurden. Das betrifft alle Möbel wie Sofa, Bett, Nachttisch, Schreib- und Esstisch, samt Stühlen, Vitrine und Sideboard, alle Bücher, meine Pflanzen, Grill und Gartenmöbel, Spül- und Waschmasine, Kühlschrank, Geschirr und Kochutensilien aus der Küche, Musikinstrumente wie meine E-Gitarre, Verstärker, E-Piano, meine Kleidung und andere Textilien aus Bad und Schlafzimmer, also Handtücher, mehrere Parfüms, Bettwäsche, (offizielle) Dokumente der letzten Jahre (Bank, Gericht, Sonstiges), Zeugnisse seit der Schule aber auch meine Bachelorurkunde und deren KMK-Anerkennung und Arbeitszeugnisse, (Liebes)-Briefe der letzten 20 Jahre, persönliches wie Fotoalben aus der Kindheit, oder über 100 Jahre alte Fotos, Erinnerungsstücke und Geschenke, die man im Leben so gesammelt hat, Auszeichnungen (z. B. aus der Bundeswehr), meine einzige Armbanduhr, technische Geräte wie Laptop, iMac, Monitor, Datenträger, Saugroboter, viele Kabel, Stecker- Adapter und Werkzeug. Also alles. Heil Hitler.
- 13.Oktober 2023: Seit 12 Wochen obdachlos auf der Straße. Vom Sozialgericht habe ich seit Juni nichts mehr gehört. WBS-Antrag bis heute auch nicht bearbeitet.
- 23.Oktober 2023: Habe einen am 14. Juli 2023 beantragten WBS-100 (bis 50qm) nach 69 Tagen (2 Monate 8 Tage, 0,19 Jahre) erhalten. Bei Immoscout24 gibt es eine handvoll Wohnungen mit WBS. Einige davon WBS-140 oder Abstellkammern mit 12qm. Für das Neubauprojekt der HOWOGE auf / an der Halbinsel Stralau eh viel zu spät. Haben sich wohl auch [43.000 Menschen auf 288 Wohnungen](https://www.bz-berlin.de/berlin/friedrichshain-kreuzberg/43-000-bewerber-in-30-minuten-fuer-nur-288-wohnungen) beworben. HOWOGE und co. vermieten Wohnungen mit 50,1 qm nicht an Leute mit meinen Schein (obwohl bis zu 10% drüber gehen würde). Für die Bahfeldtstraße wirds nicht anders aussehen. Ab Januar für Singles eh nur noch 1-Zimmer-Wohnung. Da ich wohl Single bleiben werde und als ungeimpfter Obdachloser Staatsfeind auch keinen Job mehr erhalten werde, der mir es ermöglicht in meiner Heimat (Schöneberg, Friedrichshain, .. paar andere Gegenden) zwei Zimmer mit einem akzeptablen Standard zu erhalten, brauche ich wohl nicht weiter suchen. Irgendwo müssen die neuen Fachkräfte mit Familienzuzug ja auch wohnen. 
- 3.November 2023: <b>Stefanie Bauer</b> von Heimstaden teilt nun (erneut auf Anfrage) und anders als zuvor mitgeteilt mit, eine Auswahl von Gegenständen sei eingelagert worden: 1 Fahrrad, 1 Gitarre mit Gitarrenständer, 6 Festplatten, 1 Videokamera, 1 Kiste mit Parfum, 1 Mikrofon, 1 Verstärker, 1 Münzsammlung, 1 iMac, 1 iBook Lenovo, 1 HP Bildschirm, 1 Digital Piano, 1 Saugroboter, 2 Laptops. Außerdem: 1 Karton mit LeitzOrdnern, 1 Kiste mit persönlichen Erinnerungsstücken (Postkarten, Briefe). Ich könnte die Gegenstände jedoch für 150,00€ freikaufen. Die Auswahl ist beliebig und die technischen Geräte, wie z.B. die Kamera teils 15 Jahre alt. Meine mehrere tausend Euro teuren Möbel u.a. ein neuwertiges Sofa aber auch gut erhaltene (und auseinandergebaute) günstigere Möbel, Spülmaschine, Kühlschrank und Waschmaschine wurden angeblich als wertlos "entsorgt", weil die Einlagerung den Wert übersteigt. Ebenso fehlen andere beliebige Gegenstände, die noch einen Wert haben: wie z.B. ein neuwertiger Chapeau-Claque (mind. 300,00€). Aber auch Dokumente wie meine Bachelor-Urkunde und deren Gleichwertigkeitsbescheiniging. <b>"Kautionsmanager" Fabian Andreas Gericke</b> von Heimstaden beweist kreative Rechnungsstellung. Die Forderungen aus Mietrückständen betrugen 2168,00€ (4x542,00€), Mietmindernde Umstände nicht abgezogen. Nach Abzug der hinterlegten Kaution von 1297,47€ und Zinsgutschriften von 2,52€, abzüglich einer Steuer von -0,63€ und Solidaritätszuschlag von 0,03€, werden von der Summe in Höhe von 1.299,33€, wegen der Mietrückstände 1.299,33€ einbehalten. Ausgezahlt werden also 0,00€, nun gefordert werden jedoch nun 3.369,37€. Der Grund dafür wird natürlich nicht genannt. Nachdem ich mehrmals nach der Abrechnung gefragt habe soll ich jetzt innerhalb von 10 Tagen den Betrag überweisen. Was für ein Amateurverein. Heil Hitler! 45 ist ein gutes Alter zum sterben. Für den Fall, dass ich meine Möbel in einer der zahlreichen möblierten Wohnungen im Haus sehe, biete ich Frau Bauer an persönlich bei Ihr zu Hause vorbeizuschauen, um die Sache zu klären. Ich erhalte drei Tage später von einem Herrn **Delor** die Antwort, dass meine Mitteilung bearbeitet wird und außerdem eine neue, "korrigierte" Abrechnung mit neuen Phantasiezahlen. Ursprünglich gefordert wurden 4*542€ Miete ( Forderung -2168,00€), 1297,47€ habe ich wohl im Mai 2008 als Kaution bezahlt. Ich erhalte für 15 Jahre 2,52€ Zinsen und zahle darauf 25% Steuern (-0,63€) und -0,03€ Soli. Also habe ich ein Guthaben von 1299,33€. Zieht man von der Ursprungsförderung von 2168,00€ mein Guthaben von 1299,33, wäre die Forderungssumme bei 868,76€. Gefordert wird nun die Phantasiezahl von 1117,30€. Eine Differenz von 248,54€ zu meinen Lasten. Die Gerichtskosten betrugen 182,00. Die Anwältin wollte um die 800€. Kosten für die Einlagerung oder andere Kosten werden nicht aufgeführt. Scheint so als könne man in Deutschland inzwischen x-beliebige Wunschwerte fordern.
<!-- more -->

###  Schwurbelnde Sachenbearbeit:ende vom 6. April 2023

<figure>
    <blockquote>
        <p>Sehr geehrter Herr Hammeley,</p>
        <p>da unser letztes Gespräch schon eine Weile her ist, wollte ich mal nachfragen wie es bei Ihnen aussieht? Ist der schlimmste Fall nun eingetreten und Sie sind obdachos?</p>
        <p>Aktuell bekommen Sie auch keine Leistungen von uns. **Haben Sie es verpasst einen Antrag zustellen** oder gibt es andere Gründe?</p>
        <p>Sie können mich auch anrufen, wenn sie mögen.</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Schwurbelnde Sachenbearbeit:ende vom 6. April 2023</cite>
    </figcaption>
</figure>

### 14. April 2023

<figure>
    <blockquote>
        <p>Guten Tag Herr Hammeley,</p>
        <p>danke für ihre Rückmeldung. Gut das es noch keinen Termin gibt. Haben Sie einen Plan B?</p>
        <p>Die neuen Standards ergeben sich in diesem Jahr durch die Umstellung auf das Bürgergeld.</p>
        <p>Ich habe mal nachgeschaut und gesehen, das bei Ihnen nur die Kto Auszüge fehlen und der **Nachweis das die Räumung noch nicht umgesetzt ist**. Mehr wird nicht angefordert. Den Auszug aus dem Schreiben der Leistungsabteilung füge ich hinzu.</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Schwurbelnde Sachenbearbeit:ende vom 14. April 2023</cite>
    </figcaption>
</figure>

Ich informiere das Personer erneut, dass die Kontoauszüge bereits am 17. März übermittelt wurden. Ich soll einen Nachweis erbringen, dass ich die Wohnung noch nutze. Wie?

### Schwurbelnde Sachenbearbeit:ende vom 17. April 2023

<figure>
    <blockquote>
        <p>Sehr geehrter Herr Hammeley,</p>
        <p>wenn Sie mir die Erlaubnis geben zu tun was ich möchte, dann weise ich Sie zu der Maßnahme Wohnen in Sicht zu. Ich würde sie zum 24.4.2023 um 10 Uhr zuweisen.</p>
        <p>Ich bitte Sie diese Chance zu nutzen, um das ihnen drohende Schicksal der Wohnungslosigkeit noch abzuwenden.</p>
        <p>Sie könnten noch viel erreichen, sie sind ein cleverer Mann und könnten ein entspanntes Leben führen wenn sie ihre Energie auf sich und ihren Fokus legen.</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">[Schwurbelnde Sachenbearbeit:ende](https://www.bmbf.de/bmbf/de/home/_documents/bof-bringt-zugewanderte-in-die-betriebliche-ausbildung.html) vom 17. April 2023</cite>
    </figcaption>
</figure>

Danke. Ich verzichte.

### Ich musste antworten

und fragte ob sie mich veraschen wolle. Schließlich sei der Staat für meine Situation verantwortlich.

### Antwort kam prompt

<figure>
    <blockquote>
        <p>Ich möchte niemanden verarschen.</p>
        <p>Ich bin Optimistin durch und durch.</p>
        <p>Ich glaube daran, dass wenn etwas noch nicht gut ist, ist es auch noch nicht zu Ende.</p>
        <p>**Sie könnten einfach arbeiten gehen**, sie sind ein gesunder Mann. Sie müssten nicht hier bei uns im Bezug sein.</p>
        <p>Wie soll denn der Staat für den Schaden aufkommen?</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Schwurbelnde Sachenbearbeit:ende vom 17. April 2023</cite>
    </figcaption>
</figure>

Das ist Realitätsverweigerung in Reinformm, wie man es in Fascho-Staaten kennt. Zur Erinnerung: Ich war beim ITZBund beschäftigt und Student an der Uni Potsdam, bis mir der Fasch-Staats jeglichen Zutritt verbot, weil ich gesund, ungeimpfz und ungetestet war. Gehalt wurde einbehalten. Ich wünsche ihnen einen qualvollen Krebstod. 

### eMail an Jobcenter vom 3. April 2023

<figure>
    <blockquote>
        <p>Mitarbeit:ende des Jobcenters,</p>
        <p>Hahn, Mielke,</p>
        <p>Ich habe keine Leistungen zum Unterhalt erhalten.</p>
        <p>Ich gebe ihnen bis Morgen Zeit, dann ist das Geld auf meinem Konto. Es gibt keine weitere Warnung mehr.</p>
        <p></p>
        <p>HEIL DEUTSCHLAND!</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Erinnerungsnachricht an das JC 3. April 2023</cite>
    </figcaption>
</figure>

Wiedereinmal werden irgendwelche Gründe vorgeschoben, um Zahlungen einstellen zu können. WBA und Dokumente wurden schon am 17. März 2003 übermittelt.

### Antwort des Jobcenters

<figure>
    <blockquote>
        <p>Sehr geehrter Herr Hammeley,</p>
        <p></p>
        <p>leider ist mir eine Bearbeitung Ihres Weiterbewilligungsantrages ab 01.04.2023 noch nicht möglich.</p>
        <p>Auch erwerbsfähige Wohnungslose müssen für das Jobcenter erreichbar sein, damit ggfs. eine Eingliederung erfolgen kann. Es bestehen keine Bedenken, die Erreichbarkeit zu bejahen, wenn eine tägliche Vorsprache bei einer Betreuungs- oder Beratungsstelle für Wohnungslose oder einer ähnlichen Stelle erfolgt.</p>
        <p></p>
        <p>Bitte bemühen Sie sich um eine Postanschrift und teilen mir diese mit.</p>
        <p>Michaela Deutrich</p>
        <p>Team 752 Leistungsabteilung</p>
        <p>Tel.    +49 (30) 5555 44 5203</p>  
        <p>Fax.    +49 (30) 5555 44 5299</p> 
        <p>mailto:Michaela.Deutrich@jobcenter-ge.de</p>
        <p></p>
        <p>Jobcenter-Berlin-Friedrichshain-Kreuzberg</p>
        <p>Landsberger Allee 50/52</p>
        <p></p>
        <p>10249 Berlin</p>
        <p>mailto:Jobcenter-Friedrichshain-Kreuzberg.Team-752@jobcenter-ge.de</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Ich bin nicht wohnnungslos. Zahlungen werden trotzdem erstmal nicht überwiesen.</cite>
    </figcaption>
</figure>

### Antwort auf Aufforderung zur Mitwirkung vom 17. März 2023

<figure>
    <blockquote>
        <p>Guten Tag Sachenbearbeit:ende,</p>
        <p></p>
        <p>zu dem Schreibem vom 13.3.2023, erhalten am 16.3.2023 teile ich ihnen mit:</p>
        <p></p>
        <p> - Der Räumungstermin wird durch den zuständigen Gerichtsvollzieher anberaumt. Dieser ist per Gesetz frühestens drei Wochen nach dem im Urteil genannten Räumungstermin zu erwarten. Für die Nutzung der Wohnung entstehen nach der rechtmäßigen Kündigung Nutzungsentgelte, mindestens in Höhe der vorher vereinbarten Miete. Trotz Kündigung und Urteil des Amtsgerichts entstehen somit Aufwendungen in Form von Nutzungsentgelten für meine Unterbringung in der bisher von mir zur Miete bewohnten Wohnung. Diese Kosten sind im WBA unter Mietkosten angegeben. Über die Räumung werden sie wahrscheinlich über das Gericht oder den Gerichtsvollzieher informiert.</p>
        <p></p>
        <p>- Die Kontoauszüge für die Monate Dezember 2022, Januar 2023 und Februar 2023 finden Sie im Anhang dieser Nachricht</p>
        <p>- Mit der Räumung aus meiner Wohnung bin ich postalisch nicht mehr erreichbar.</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Ich bin nicht wohnnungslos. </cite>
    </figcaption>
</figure>

### eMail von Tatjana Franz vom 3. Dezember 2021

<figure>
    <blockquote>
        <p>Sehr geehrter Herr Hammeley,</p>
        <p></p>
        <p>in Ihrer u.s. E-Mail zeigen Sie eine ernsthafte und endgültige Verweigerung Ihrer Arbeitsleistung an. Aus diesem Grund habe ich die Entgeltfortzahlung ab dem 01.01.2021 eingestellt. Hinsichtlich Ihrer Homeoffice-Tätigkeit am 24.11.2021 sowie am 25.11.2021 werde ich im Rahmen der Beendigung Ihres Beschäftigungsverhältnisses das BVA beauftragen, insgesamt 15 Stunden und 27 Minuten zurückzufordern.</p>
        <p></p>
        <p>Mit freundlichen Grüßen</p>
        <p>Im Auftrag</p>
        <p>Tatjana Franz</p>
        <p>Z 23 8</p>
        </p>ITZBund Bonn</p>
        </p>Tel: 022899 680 17757</p>
        </p>Erreichbar:</p>
        </p>Mi und Fr: 07:30 - 15:00</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Stecken sie ihre freundlichen Grüße sonst wohin. Verlogenes Stück.</cite>
    </figcaption>
</figure>

Man setzt auf Eskalation und wird dem Schwurbler nach 11 Monaten Zugehörigkeit Arbeitsverweigerung vor.Das Gehalt wird nicht zurückgefordert. Auch der Laptop (Lenovo T490 ca. 3000€ neu), die Smart-Card mit Sicherheitsschlüssel, meine Zutrittskarte zum Gebäude, und Schlüssel fürs Büro werden nicht zurückgefordert. Habe alles noch.

### eMail an Vorgesetzte vom 19. November 2021

<figure>
    <blockquote>
        <p>Sehr geehrte Frau Weise,</p>
        <p>hallo Christian Radke,</p>
        <p></p>
        <p>ich schreibe Ihnen mit der Bitte um Klärung eines möglichen weiteren Vorgehens im Falle verschiedener zukünftiger Corona-Maßnahmen.</p>
        <p></p>
        <p>Da ich gegenüber meinem Arbeitgeber oder anderen Dritten (z. B. im ÖPNV) keine Informationen bezüglich meines Impf- oder Sero-Status (Test) preisgeben werde, könnten insbesondere zwei Maßnahmen für meine Ausübung meiner Tätigkeit in der PP28 hinderlich sein:</p>
        <p></p>
        <p>- 3G am Arbeitsplatz</p>
        <p>- 3G im ÖPNV</p>
        </p></p>
        </p>Je nachdem, wie seitens des Senats in Berlin entschieden wird, besteht also die Gefahr, dass ich in Zukunft möglicherweise:</p>
        </p></p>
        </p>- keinen Zutritt zur Prenzlauer Promenade 28 erhalten werde und</p>
        </p>- die Öffentlichen Verkehrsmittel nicht mehr benutzen kann.</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Versuch eine Lösung zu finden.</cite>
    </figcaption>
</figure>

... eMails hin und her folgen.

### Einladung zum Auswahlverfahren beim AA

<figure>
    <blockquote>
        <p>Sehr geehrter Herr Hammeley,</p>
        <p></p>
        <p>ich danke Ihnen für Ihre Bewerbung und freue mich, Sie im Namen des Auswahlausschusses zum mündlichen Auswahlverfahren Nr. 51-2019 "IT-Spezialistinnen und IT-Spezialisten im vergleichbaren gehobenen Dienst" einladen zu dürfen, verbunden mit dem Hinweis, dass dem Auswärtigen Amt für Ihren ausländischen Studienabschluss bis spätestens einer eventuellen Einstellung eine Gleichwertigkeitsbescheinigung der Zentralstelle für ausländisches Bildungswesen (ZAB) im Sekretariat der Kultusministerkonferenz vorliegen muss. Sie können diese Bescheinigung auch jetzt schon vorlegen.</p>
        <p></p>
        <p>Bitte kommen Sie am Donnerstag, den 27.11.2019 um 13:30 Uhr</p>
        <p>in das Auswärtige Amt</p>
        <p>Kurstraße 36</p>
        <p>10117 Berlin-Mitte.</p>
        </p></p>
        </p>Das Ende ist laut Zeitplan für spätestens. 19:30 Uhr vorgesehen.  </p>
        </p></p>
        </p>.. weitere Hinweise zum Ablauf ..</p>
        </p>Mit freundlichen Grüßen</p>
        <p></p>
        <p>Elke Huber</p>
        <p>Dieses Schreiben wurde maschinell erstellt und ist auch ohne Unterschrift gültig.</p>
        <p></p>
        <p>Fachbereich 1-AK-8</p>
        <p>Im Auftrag</p>
        <p>Auswahlverfahren im Tarifbereich</p>
        <p></p>
        <p>Auswärtiges Amt</p>
        <p>Kurstr. 36</p>
        <p>10117 Berlin</p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Wikipedia">Einladung zum Auswahlverfahren</cite>
    </figcaption>
</figure>
