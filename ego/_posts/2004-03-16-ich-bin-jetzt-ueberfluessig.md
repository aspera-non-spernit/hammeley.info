---
title: Ich bin jetzt überflüssig
layout: default
---
Ca. ein Jahr nach meiner Weiterverpflichtung hat das Bundesministerium der Verteidigung am 04.08.2003 eine Änderung der Fähigkeitsstruktur im Verwendungsbereich 33 DB Flugberatungsdienst beschlossen. Im Wesentlichen werden nun Dienstposten meiner Ausbildungsstufe gestrichen und mit höher dotierten Dienstposten besetzt.

Es gelten jedoch Übergangsregelungen:

> Grundsätzlich sind Tätigkeiten auf dem Arbeitsplatz "Koordinator Flugdaten (Arbeitsplatz C)" durch Personal mit zuerkannter AH 6 (FlBerFw) durchzuführen.
> Ausgebildete Unteroffiziere im Flugberatungsdienst mit zuerkannter AH 7 können vorübergehend auf dem Arbeitsplatz "Koordinator Flugdaten (Arbeitsplatz C)" nachfolgend festgelegte Teilaufgaben wahrnehmen, die ihrer fachlichen Qualifikation und ihrem bisher vorgesehenen Aufgabenspektrumm entsprechen.
> [es folgt eine Liste mit meinen bisherigen Aufgaben]

Vielen Dank. Meine Motivation hält sich in Grenzen. Ebenso hält sich die Motivation bei den AH 6 in Grenzen, die nun die Arbeit verrichten sollen, die sie aufgrund ihrer Ausbildung eigentlich nicht mehr verrichten wollten.

**Ausblick:** Dafür kommen demnächst frisch ausgebildete AH 6 auf die Dienststellen, die weder auf AH 7 noch auf AH 6 Erfahrung haben und somit meine Arbeit übernehmen, damit sie in allen Bereichen Praxiserfahrung sammeln können.
