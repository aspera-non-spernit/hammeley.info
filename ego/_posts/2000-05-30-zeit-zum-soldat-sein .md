---
title: Zeit zum Soldat sein
layout: default
tags: "Bundeswehr, Verpflichtung, Pilot, Offizier, Sicherheit"
---
Meine Bewerbung wurde angenommen. Ich wurde heute vereidigt und bin damit seit heute offiziell Zeitsoldat der Bundesrepublik Deutschland.
Das Gehalt wird bei der Bundeswehr (und wahrscheinlich auch bei Beamten) im Voraus bezahlt. Ich erhalte also schon Morgen oder spätestens übermorgen mein erstes "richtiges" Gehalt. Ich habe es auch dringend nötig. Mein Studium der Menschheit im Nachtleben war recht teuer.

Es geht alles recht schnell. Schon vor der Vereidigung und der abgeschlossenen [Sicherheitsüberprüfung (Ü2)](https://de.wikipedia.org/wiki/Sicherheits%C3%BCberpr%C3%BCfung), erhalte ich die Lehrgangsplanung für die nächsten Monate.
