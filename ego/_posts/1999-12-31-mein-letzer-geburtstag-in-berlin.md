---
title: Mein letzter Geburtstag in Berlin
layout: default
tags: "Berlin, Geburtstag, Bayern, Bundeswehr, Freundin"
---
Über Weihnachten und Neujahr können die Wehrpflichtigen in den Urlaub. Ich bin in Berlin. Die Freundin meint ich sei fremd gegangen. Ich werde nie erfahren mit wem.

Ich kehre also als Single wieder zurück zum Dienst nach Bayern, spare mir aber immerhin ab sofort die zwei mal neun Stunden Bahnfahrt am Wochenende.

In den letzten sechs Monaten war ich einmal mit der nun Ex in einer Mensa der FU. Dort traf ich auf eine recht hübsche Studentin. Allerdings war sie sehr eingebildet und arrogant. Sie machte auf mich auch nicht den intelligentesten Eindruck. Sie nahm wohl an einer Veranstaltung der SPD teil. Auf jeden Fall meinte sie, sie würde Bürgermeisterin in Berlin, weil sie Migrantin sei.

**Ausblick:** Ich schwöre mir, dass diese Frau nicht Bürgermeisterin meiner Heimatstadt wird. In ca. zwanzig Jahren wird sich diese Frau zur Bürgermeisterkandidatin wählen lassen wollen und scheitert. "Alles Nazis". Sie ist nun bekannt dafür, dass sie Rolex trägt und auf dem Kurznachrichtendienst "twitter" alle als Nazis bezeichnet, die irgendwas Negatives äußern. Ihre Anzeigen und Klagen vor Gericht werden scheitern. Statt Bürgermeisterin wird sie Pressesprech: und Staatssekretier:ende. Bei ihren "Mitarbeit:enden", wie man um 2020 im Neusprech sagt, scheint sie äußerst beliebt zu sein und hohe Anerkennung wegen ihrer Kompetenz zu genießen.
