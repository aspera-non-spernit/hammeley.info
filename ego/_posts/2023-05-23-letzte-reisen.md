---
title: Letzte Reisen
layout: default
tags: "Reise, Pandemie, Corona, Virus, 49, Euro, Ticket, Covid, Ostdeutschland, Bodetal"

---
Im Mai, Juni und Juli 2023 nutze ich noch einmal die Chance mit dem 49€-Ticket durch die Ostgaue des sich in Auflösung befindlichen Super-Deutschlands zu reisen. 

In den Jahren 2022 und 2021 konnte ich aufgrund meiner religösen Einstellung zum Thema Maskentragen leider nur in die südöstlichen Ostgaue in Sachsen reisen, in denen die Durchsetzung der faschistischen Gesetzgebung am Widerstand der freien Sachsen größtenteils scheiterte. Für die Millionen Toten, die ich verursacht habe möchte ich mich an dieser Stelle noch einmal entschuldigen. Ich distanziere mich hiermit von meinem Verhalten.

Nachdem Covid-19, Covid-20, Covid-21, Covid-22 und Covid-23 durch die heroische Durchsetzung der Schutzhaft für ungeimpfte Staatsfeinde besiegt werden konnte, die letzten Bundesgaue auf Maskenpflichten verzichteten und die Reichsregierung schon vor der WGO die Pandemie für besiegt erklären konnte, nahm ich mir vor, die Chance zu nutzen vermehrt Orte in Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt, Thüringen und Sachsen zu besuchen, bevor das 49€-Ticket wieder einkassiert wird. Schließlich ist dafür in der viert reichsten Volkswirtschaft des Universums kein Geld übrig.

![Letzte Reisen](){:data-src="/assets/images/reisen.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

<!-- more -->

**Hinweis:** Die Auflistung der Erstbesuche Orte ist ohne genaue Datumsangabe chronologisch.

#### Thale + Bodetal (1)

Anfang Mai besuchte ich bereits zum zweiten Mal das wunderschöne [Bodetal [yt]](https://www.youtube.com/watch?v=o4tcjPXnmLQ&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/). Ich war eigentlich zur Nazizählung in den Ostharz gefahren. Der Nazi-Zensus war jedoch ein totaler Reinfall. Ich konnte keinen einzigen Nazi finden. Nur ein paar Assis, links-propagandistische Graffiti und Aufkleber, die ich schon aus Berlin kenne, ein paar Rehe, Hasen, Kühe und Pferde konnte ich auf meinem Weg dorthin entdecken. Über die politische Einstellung der dortigen Fauna kann ich natürlich keine Aussage treffen. Ein paar schwar-rot-gelbe-Fahnen (statt bunte oder blau-gelbe) habe ich gesehen. Ich Sachsen-Anhalt scheint wohl schon Heimatkunde im Kindergarten unterrichtet zu werden. Ein kleiner Junge ist mir aufgefallen, der sich schon gut auskennt und irgendwo im Zug sagte: "Hier ist Deutschland."

Die Reise dorthin verlief problemlos. Ich bin mit dem RE1 um 5:47 vom Bahnhof Ostkreuz gestartet. Bis nach Magdeburg wird die Strecke von der ODEG betrieben, die ich bereits seit meinen Reisen ins Tal der Ahnungslosen (über Görlitz) kenne und von der ich seitdemm ein großer Fan bin.

Ich nahm einen Zug vor dem sogenannten HEX-Express, dem Touri-Zug von und nach Berlin, der nur existiert damit die Berlin-Touris bloß keinen Kontakt zu anderen Menschen haben, sondern in Berlin einsteigen, die Touri-Strecke ablaufen und mit dem HEX-Express wieder zurück fahren können. Ich hingegen bin mit den echten Landmenschen in Kontakt gekommen.

Nach Magdeburg war ich mir nicht mehr ganz sicher, ob ich noch Nachwirkungen meiner [anstrengenden Fahrradtour](https://hammeley.info/ego/2023/04/19/fahrradtag) hatte oder ob ich in den falschen Zug gestiegen bin und irgendwo im Ausland gelandet bin. Auf jeden Fall hatte ich große Probleme die Menschen, die sich im Zug unterhielten, zu verstehen. Ich konnte keine mir bekannte Sprache identifizieren ([ähnliche Beispielkonversation](https://youtu.be/Z2z8dQO1udM?t=203&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/)). Als ich dann einen älteren Mann entdeckte, der mit seinem Kuscheltier sprach, war ich mir jedoch sicher, dass ich soweit weg von Berlin noch nicht sein kann. Kurz schießt mir Dr. Mengele durch den Kopf und ich frage mich, ob hier damals vielleicht Genexperimente durchgeführt wurden. Eine Auffrischung oder eine gute Durchmischung des Genpools würde der Gegend sicherlich gut tun.

Nach fast vier Stunden, so gegen 10, bin ich in Thale angekommen. Ich schlenderte kurz über den Kurpark. Dort war gerade eine Mischung zwischen einem volkstümlichen Mittelalter-Jahrmarkt und Flohmarkt im Aufbau. Viele schöne Holzschnitzereien, Metallbearbeitungen, deftiges Essen, aber auch allerhand Trödel konnte man kaufen. Ein komisches Mischmasch. Nicht so meins gewesen. Ich tue den Leute da bestimmt unrecht. Schließlich ist die Gegend ja bekannt für Hexen, die Walpurgisnacht und dem etwas westlich gelegenen Blocksberg auf dem schon Bibi Netanjahu sein Unheil getrieben haben soll.

Gleich hinter dem Kurpark geht es dann in das [wunderschöne Bodetal [yt]](https://www.youtube.com/watch?v=tCduPdEGDXQ&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/). Ich laufe eine Kombi-Tour aus verschiedenen Wanderwegen. Blauer Punkt, roter Punkt und blaues Dreieck, glaube ich. Die erste Etappe war auch die Anstrengendste. Ich brauchte fast 1,5 Stunden für die 2,5 km und musste schon nach ca. einer Stunde bei einem atemberaubenden Blick Pause machen und frühstücken (bei ca. 3:17 des Videos).

![Pause mit Ausblick](){:data-src="/assets/images/bodetal/pause.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Beim Aufstieg zum Hexentanzplatz war ich fast allein. Überholt hat mich niemand, aber ab und an sind mir Leute entgegengekommen. Sogar zwei sichtlich Alkoholisierte mit Bierflasche in der Hand. Ich bin mir nicht sicher, ob die aus einer nahegelegenen Entzugsklinik ausgebrochen sind, oder sich hier nur sehr gut auskennen. Nüchtern würde ich den Abstieg der ersten Etappe nicht antreten.

An der La Viershöhe ist man dann endlich oben angekommen. Ab hier fühlt man sich leider wie bei den zahlreichen Touristenfallen, die es überall gibt. Mit der Seilbahn werden hier Scharen von Städtern, Hipstern, Familien mit Hunden, Schulklassen usw. zum Obersalzberg nahe dem Hexentanzplatz hochgefahren. Man sieht allerhand Typen von Leuten. Mit Sandalen, Turnschuhen, Hunde, die mit meterlanger Leine auf dem Boden herumlaufen und sich verheddern, Leute, die auf ihr Handy schauen und stolpern und Tiere hinter Gittern. Man schießt mit dem Handy zwei, drei Fotos, trinkt ein Bier und fährt wieder herunter, wo der HEX-Express schon wartet. "Wir waren wandern - War das schön". Das übliche halt. Fast wie bei mir in Groß-Wokistan. Der Blick von dort oben ins Tal ist jedoch wirklich atemberaubend.

![Blick ins Tal](){:data-src="/assets/images/bodetal/hexentanz.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Ich laufe wieder ein Stück zurück und gehe zwischen Tierpark und La Viershöhe auf den roten Punkt. Hier sieht man zahlreiche gefällte und abgestorbene Bäume, es ist aber nicht apokalyptisch häßlich oder in der Art, jedoch deutlich wärmer und trockener als noch wenige Meter vorher. Als ich Autos von der Landstraße (L240) höre biege ich rechts ab. [Ab hier geht es erst einmal wieder bergab](https://youtu.be/-41KhLqq5x4?t=250&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/) und wieder in einen wunderschönen klimatisch sehr angenehmen, kühlen und feuchten Wald.

Nach der ca. 2,2 km langen und recht einfach zu bewältigenden Strecke beginnt die recht kurze (ca. 1,1 km) und anspruchsvollste, aber nicht die anstrengendste Etappe am Kästenbach entlang. Der Wanderweg wird zu einem Trampelpfad und ist teilweise kaum zu erkennen. Für Turnschuhwanderer, Kleinkinder und Hunde sind die Tour ungeeignet und kaum zu bewältigen. Wie auch der Erzähler im Video stellte ich mir die Frage, ob ich hier noch herausfinde. Nachdem ich eine Gruppe mit Hunden hinter mir gelassen habe, bin ich hier für mich allein. Bleibt man jedoch immer nah am Bach, findet man früher oder später auch wieder heraus. Irgendwann höre ich Menschen, die ca. 1 m über mir in die andere Richtung laufen. Ich wundere mich, denn ich hätte den Weg über mir nicht bemerkt, wenn dort nicht gerade jemand langgelaufen wäre. Ich erklimme den Meter nach oben und befinde mich auf einem steinigen Pfad. Nach einer Weile komme ich an, hinter einem Hinweisschild auf den einfacheren Wanderweg "Blaues Dreieck".

![Der Kästenbach](){:data-src="/assets/images/bodetal/kaestenbach.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Die weniger anspruchsvolle Etappe von schätzungsweise 5-6 km ist stark frequentiert. Hier geht es teilweise auf und ab, ist aber für jeden zu bewältigen, für Jung und Alt, Hunde und auch für Turnschuh-Hipster. Sogar zwei schöne junge Frauen sind mir entgegengekommen. Ständig grüßen mich die entgegenkommenden Leute. Für mich als Stadtkind ungewohnt. Dank meines Alters muss ich nur zurückgrüßen.

Die Bode ist teilweise recht wild, ich würde mich aber trotzdem gerne in das kalte Wasser legen.

Nach ca. vier Stunden erreiche ich wieder die Jugendherberge, an dem die Tour begonnen hat. Ich gehe noch einmal über den Kurpark und kaufe mir eine Zitronenlimonade (Duponia aus Calbe). Es ist wirklich schwer, eine gute, altmodische Limonade in Berlin zu finden. Chabeso findet man manchmal. Die ist auch lecker.

![Die Bode](){:data-src="/assets/images/bodetal/blaues_dreieck.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Auf dem Weg zum Bahnhof frage ich mich, warum bei den vielen Touristen und Kurgästen noch so viele unsanierte Häuser herumstehen. Wo ist nur das ganze Geld hin? 

Ich habe Glück und der Zug nach Berlin steht schon bereit. In ca. vier Stunden bin ich wieder zu Hause. Es soll hier eine große Demonstration von Rechtsextremisten in Kreuzberg und Neukölln gegeben haben. Ich hab davon nichts mitbekommen.

#### Über den Wolken (2 + 32)

Am nächsten Tag, den 2. Mai 2023 wollte ich nicht gleich wieder eine riesen Tour machen. Deshalb habe ich den Tag im Garten meiner zukünftigen Residenz, dem Schloss Sanssouci verbracht. Ich trinke einen Capuucino (1,70€) in der [Mensa meiner ehemaligen Uni Potsdam am Neuen Palais](https://www.uni-potsdam.de/fileadmin/projects/up-entdecken/migrated_contents/mensa-neues-palais_02.png). Am Nachmittag bin ich dann noch zum BER gefahren und habe mir die Flugzeuge vom Besucherbalkon angeschaut. Von hier sieht man nicht nur Flugzeuge, sondern auch Berlin. Ich erkenne sogar so etwas wie eine "Skyline" und kann dank Treptowers sogar meinen Kiez sehen.

![Flughafen BER](){:data-src="/assets/images/bodetal/ber.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Ich muss sagen, auch wenn der Flughafen äußerlich nicht zu den architektonischen Meisterwerken der Menschheitsgeschichte gehört, ist das Interieur das [eleganteste, geschmackvollste und Beste [yt]](https://www.youtube.com/watch?v=bJg024Ouj0M&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/), was ich in meinem Leben so in Flughäfen gesehen habe. Allein die recht filigranen Handläufe an den Treppen, die schönen Bodenfliesen, das Holz in verschiedenen Farbtönen und die Schrift und die Farbgestaltung der Informationsschilder begeistern mich. Schon deswegen lohnt ein Besuch. Beruflich war ich bereits zweimal noch vor Eröffnung dort und war sowohl in der Gepäckabfertigung, als auch auf den Laufbrücken und auf dem Flugfeld (als Komparse in "Homeland" und einmal als Stagehand) und kenne daher auch andere Bereiche des Flughafens. Auch der Übergang vom Bahnhof im Untergeschoss ist problemlos und kurz und die Einkaufsmöglichkeiten vor dem Sicherheitsbereich sind super.

Für die nächsten Wochen plane ich auf jeden Fall noch einige Ziele in Brandenburg und Sachsen zu besuchen. Auch möchte ich ein oder zwei Abstecher an die Ostsee und nach Polen machen. Für Ziele außerhalb des Ostzonenbereichs sind die Ziele mit den Regionalzügen einfach zu weit weg. Vielleicht schaffe ich es ja dieses Jahr mal ins [polnische Tatragebirge [yt]](https://www.youtube.com/watch?v=vARat-8xI54&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/).

### Die restlichen Reisen

Im Laufe des Monats besuchte ich dann noch Cottbus, Stralsund, Bad Belzig, Berlin-Wannsee, Wittenberg, Nauen, Bad Wilsnack, Burg (bei Magdeburg), Oranienburg, Templin, Schwedt, Krajnik Dolny, Angermünde, Ludwigsfelde und weitere Orte.

Keine der besuchten Städte kann irgendwelche einzigartigen Attraktionen vorweisen. Die meisten Städte ähneln sich sogar sehr: Bahnhof mit penny, netto, lidl oder Aldi, dann ein Weg in die Altstadt, wahlweise mit oder ohne Stadtmauer(-resten). In der Altstadt ein, zwei oder drei Parks, und je nach Asifizierungsgrad eine unterschiedliche Auswahl an Geschäften (z. B. AG (hoch): kik, Pfennigland, Tafel E.V. BarberShop, Nagelstudios, Asia Imbiss, internationale Restaurants, eventl. eine angehipsterte Bar und Lounge) und irgendwo ein öffentlich bestellter Vermessungsingenieur. Eine Brücke führt über einen künstlichen Teich oder einen See, irgendwo ein rechteckiger Marktplatz mit Rathaus und wenig Sitzgelegenheiten. Etwas außerhalb findet man das Krankenhaus, leichte Industrie und eine Neubausiedlung mit Bauten zwischen 1950 und 1980er und eine Einfamilienhaussiedlung aus den 1990ern und 2000ern. Befindet sich die Stadt an einem See oder Fluss, ist die Uferpromenade obligatorisch.

![Karte](){:data-src="/assets/images/aufschwung_ost/karte.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Dennoch unterscheiden sich die Städte im Wohlfühlfaktor und dem Asifizierungsgrad sehr deutlich. In einigen Städten fühle ich mich auf Anhieb viel wohler als in anderen Städten, die regelrecht eine kalte Unwirklichkeit ausstrahlen. In manchen Städten herrscht städteplanerische Einfalls- und Lieblosigkeit, möglicherweise das Ergebnis aus Korruption und des Zuzug von wenig solventen Personen. Während ich mir vorstellen könnte, in einigen Städten zumindest ein Wochenende zu verbringen, glaube ich, dass andere Städte nur existieren, um die Menschen, die dort wohnen, irgendwie unterzubringen. Ob der Asifizierungsgrad der Grund ist, oder der Zustand dieser Städte für den Asifizierungsgrad kann ich nicht beurteilen.

### Cottbus (3)

In [Cottbus [yt]](https://www.youtube.com/watch?v=giqSp1KDBIo&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/) treffe auf die ersten Mandys des Jahres, als ich mir im Cottbuser Konsumtempel bei penny etwas zum Frühstücken hole. Der Brot- und Backwaren-Auftau- und Erwärmungsapparat verströmt einen angenehmen Duft von frisch gebackenem. Möglicherweise ist es der beste eingestellte Automat des Landes.

Es gibt in der Stadt zwar wirklich alles, was da Mandy-Herz begehrt. Dessous-Geschäfte, Nagelstudios, Euro-Shops usw. Aber insgesamt ist die Stadt etwas trostlos. Die Stadt kommt mir vor wie eine schlechte Simulation der DDR.
Ganz schlimm sind auch die historischen Architekturstilen nachempfundenen Plattenbauten, auch mitten in der Altstadt. In meinen Augen ist das ein Verbrechen an der Menschheit. Es gibt zwar zwei interessante Gebäude im Art-Deco-Stil und ein oder zwei schöne Straßen mit Gründerzeitwohnhäusern, diese machen das städteplanerische Chaos aber auch nicht wieder gut. Der Asifizierungsgrad der Stadt ist mit "fortgeschritten" zu beschreiben. Es ist eigentlich eine Stadt, in der sich die berliner Selbstverwirklicher wohlfühlen könnten. Nicht zu schön, nicht zu teuer. Vielleicht mache ich einen Spendenaufruf, um eine Umvolkung und ein Begrüßungsgeld bei freiwilligem Umzug zu finanzieren?

![Cottbus](){:data-src="/assets/images/aufschwung_ost/cottbus.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Stralsund (4)

Es folgt eine etwas längere Reise nach Stralsund. Mir war nach Meeresbrise und plante eigentlich nach Schwerin und Wismar zu fahren. Die Fahrpläne der DR hinderten mich jedoch erst einmal daran. Stralsund ist super sauber. Ich war richtig überrascht, wie gepflegt und gesittet selbst die größten Asis in der Stadt aussehen. Wirklich. Ob Sternburg trinkender Lederjackenträger im Frankenpark oder die mehrfach übergewichtigen tätowierten Muttis mit drei Kleinkindern. Alle machten hier einen recht angenehmen Eindruck. Die Stadt ist so sauber, dass mir eigentlich zwei Orte auffallen, wo sich die wenigen Asis der Stadt wohl treffen. Das ist eine einzige Bank am Frankenteich und vor dem Eingang eines städtischen Verwaltungsgebäudes. Dort sind vermehrt Zigarettenstummel zu entdecken. Der Rest der Stadt ist sehr sauber. An der Hafenstraße gibt es ein paar Touristenfallen und sogar einen angehispterte Restaurants und Bars und vor allem Fisch. Fisch in allen Variationen. Ich esse leider keinen Fisch, aber riechen tut es gut. Für eine Stadt dieser Größe ist das Angebot eigentlich ganz gut und der Asifizierungsgrad gering.

Ich gehe auf den Dänholm und schieß ein zweites Alibifoto und genieße die fischig-salzige Meeresbrise.

![Stralsund](){:data-src="/assets/images/aufschwung_ost/stralsund.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Bad Belzig (5)

Ich besuche [Bad Belzig [yt]](https://www.youtube.com/watch?v=jaTT5YZ58IU&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/). Ein kleiner "Kurort" im Südwesten Berlins. Der Ort ist recht nett, sogar etwas hügelig, aber auch tot und merkwürdig. Es kommt mir hier vor, als würden sich hier Kranke nicht erholen, sondern ihre letzten Tage verbringen. Obwohl der Assifizierungsgrad eher gering ist, und die meisten Häuser ganz in Ordnung aussehen, macht die Stadt macht einen morbiden Eindruck. [Am Bahnhof](https://www.komoot.de/highlight/419818) kaufe ich mir ein Salami-Käse-Baguette. Es ist das Beste, das ich seit langem für einen akzeptablen Preis gegessen habe (mit Butter, Salat, Gurke, Radieschen).

### Berlin-Wannsee (34)

Am Wannsee wollte ich eigentlich eine Dampferfahrt machen. Da ich mich jedoch ausschließlich mit der Regionalbahnen in der Ersten Klasse fortbewege, um nicht mit dem asozialen Gutmenschen in Kontakt kommen zu müssen, müsste ich zu lange auf die Fähre warten und entscheide mich deshalb für eine Ortsbegehung um den Pohl- und Wannsee. Ich laufe die Bismarckstraße entlang, bis ich endlich etwas an das Wasser komme. Nördlich des Kleinen Wannsees laufe ich durch die Wohnsiedlung. Ich entdecke eine Parkbank mit bestimmt 20 leeren Flaschen Heinecken Bier. Ich bin überrascht, wie heruntergekommen teilweise die schmucken Häuser aussehen und wie klein doch die Autos hier sind. Ich sehe vielleicht einen Porsche. Sonst nichts wirklich Beeindruckendes. In einer Bäckerei kaufe ich mir einen kleinen Kaffee und einen Buttercroissant. 5 €. Am Loretta am Wannsee komme ich dann wieder mit Menschen in Kontakt und fahre nach Hause. Der Asifizierungsgrad ist zwar gering, jedoch deutlich höher als ich erwartet hätte - Typ: Jogginghose und Goldschmuck. 

### Wittenberg (6)

Ich entscheide mich etwas Erinnerungskultur zu betreiben und fahre nach Wittenberg. Überall in der Stadt gibt es Schilder, die auf Personen hinweisen, die einmal in der Stadt gewohnt haben, sie besucht haben oder sonst irgendwas mit der Stadt zu tun haben. Beeindruckend wie viele intelligente Menschen dort einmal ihre Zeit verbracht haben, wie z. B. [Johann Gottfried Galle](https://de.wikipedia.org/wiki/Datei:Gedenktafel_Kirchplatz_(Wittenberg)_Johann_Gottfried_Galle.jpg), dem Entdecker des Neptun, denn von dem einzigen Glanz dieser Zeit ist hier nichts mehr zu spüren. Die recht kleine Altstadt selbst ist zwar architektonisch ganz interessant und hat drei zusammenhängende Parks, in denen ich kurz pausiere und einen Schwanenteich mit Gänsen, aber die Stadt ist definitiv auch Asifizierungs-Risikogebiet. Die Dichte an Billigläden, wie Tedi, Sozialhilfeeinrichtungen, Nagel- und Sonnenstudios, Döner, Asia und anderen Imbissen und dem Shoppingcenter Arsenal ist deutlich zu viel für die zwei Straßenzügen. Nicht schlimm, aber etwas enttäuschend.

![Wittenberg](){:data-src="/assets/images/aufschwung_ost/wittenberg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Nauen (7)

Ich mache einen Abstecher nach Nauen und Bad Wilsnack. Wie bei fast allen Städten befindet sich gleich am Bahnhof ein Penny, Aldi, Netto, gelegentlich ein Rewe. Ich hole mir etwas zu frühstücken und laufe die Straße Richtung Altstadt entlang. Ich entdecke eine Tafel, Nagelstudio, Beautysalons, Barbershops und ein paar andere Läden mit total kreativen Namen. Insbesondere die Restaurants scheinen hier den Namen nach auf Weltniveau mit unendlich vielen Michellinsternen zu sein. Ich setze mich auf eine Bank auf dem Martin-Luther-Platz und frühstücke. Ich bin überhaupt nicht von der Stadt beeindruckt. Moderater Asifizierungsgrad.

### Bad Wilsnack (8)

Nach dem kurzen Abstecher nach Nauen fahre ich schnell weiter nach Bad Wilsnack. Der kleine "Kurort" hat eine Therme, ein interessantes Gradierwerk, ein paar kleinere Parks und ein paar Teiche. Zur Kur würde ich hier nicht hin, aber das [Gradierwerk](https://bad-wilsnack.de/images/stories/gradierwerk.jpg), ein Aussichtsturm in einem der Parks, die Pferde und der kurze Wanderweg sind immerhin eine Abwechslung zu Nauen. Überall in der Stadt höre ich Rasenmäher. Hier wird jeder noch so kleine Garten nicht mit einem normalen Rasenmäher gemäht, sondern mit so einem Traktor, mit Günther und 8000 PS. Der einzige Grund, warum hier keine Gartenzwerge in den Gärten stehen, dürfte das negative Stigma sein. 
Das kleine Kneipp-Becken ist immer noch "in Sanierung". Macht nichts, hier verirrt sich außer mir eh niemand hin. Auf dem Rückweg trinke ich am Bahnhof noch ein Radler. Asifizierunngsgrad: negativ.

### Burg (bei Magdeburg) (9)

Ich besuche ein weiteres Mal Sachsen-Anhalt. Burg bei Magdeburg überrascht positiv. Die Stadt ist zwar nicht mein absoluter Favorit, jedoch gibt es zahlreiche schöne alte Gebäude. Viele Fachwerkhäuser, teils modernisiert, aber auch zahlreiche Häuser aus anderen Epochen. Anders als z. B. in Cottbus fügt sich hier alles zu einem attraktiven und farbenfrohen Ensemble zusammen, in dem sogar die unsanierten und grauen Fassaden glänzen. Es ist die erste Stadt, die ich besuche, mit einer (teils stark) sanierten Stadtmauer, mit einigen Türmen und interessanten Informationstafeln. Beim Besuch fällt mir deutlich auf, dass ich den berliner Asigürtel verlassen habe. In Burg grüßen die Menschen und auch die Kinder haben keine Angst vorm alten weißen Mann. Die Parks sind sauber, könnten aber etwas interessanter gestaltet werden. Auf dem Rückweg zum Bahnhof entdecke ich noch ein Zeichen der kleinstädtischen Erinnerungskultur. An der Gedenkstätte am Panzer ist ein Friedhof mit fast 200 Gräbern. Es fällt auf, dass nahezu alle in den letzten Tagen und auch noch nach Ende des Zweiten Weltkrieges starben. Viele sehr junge Menschen darunter. Gleich gegenüber steht ein Panzer aus russischer Produktion, der mit ein paar Blumen verziert ist. Dahinter eine Gedenktafel für die gefallenen sowjetischen Soldaten. Asifizierungsgrad gering. Ein paar Graffiti sonst sehr sauber. Aber nicht übermäßig spießig und aufgesetzt sauber. Hier leben noch echte Menschen. Zudem hat die Stadt eine freundliche Bevölkerung. In meiner bisherigen Favoritenliste kommt Burg auf einen guten dritten oder zweiten Platz.

![Burg](){:data-src="/assets/images/aufschwung_ost/burg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Oranienburg (10)

Die Stadt ist eine herbe Enttäuschung. Ich merke deutlich, dass ich wieder im Asigürtel Berlins gelandet bin. Architektonisch eine Katastrophe. Viele Verbotsschilder, ein wenig attraktiver Uferweg am Schloss vorbei und ein recht hoher Asifizierungsgrad überzeugen mich überhaupt nicht. Oranienburg dürfte auf meiner Liste der beliebtesten und attraktivsten Städte vor Cottbus, aber hinter Nauen liegen.

### Templin (11)

Templin unterscheidet sich nicht großartig von vielen anderen Städten unterscheidet. Die Stadt begrüßt einen, wie fast alle anderen Städte am Bahnhof, mit einem penny und einem Asia-Imbiss. Ähnlich wie Burg ist die Stadt sehr ordentlich. Ich entdecke nur an einer Stelle Graffitis und die sind an einer hässlichen Metalltür. Ähnlich wie Burg leben hier noch nette, freundliche Menschen. Die Stadt ist zwar sauber, aber nicht Disneyland rein. Es liegt hier und da Laub auf der Straße und auch Gras wächst zwischen den Pflastersteinen. Der Häuserbestand ist nicht ganz so schön wie Burg. Dafür ist die Stadtmauer mit ihren größeren Natursteinen sehr hübsch. Außerdem ist hier schön kühl. Die Menschen sind hier freundlich, wie in Burg.  In einer unprätentiösen Bäckerei kaufe ich mir einen Kaffee. Der kommt aus der Thermoskanne. 

![Templin](){:data-src="/assets/images/aufschwung_ost/templin.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Schwedt + Krajnik Dolny (12)

Ich habe soviel Hoffnung gehabt, dass mich Schwedt überrascht. Leider war das nicht der Fall. Die Stadt ist zwar keine Katastrophe, wie man es vielleicht aufgrund der fast gleich großen PCK-Raffinerie nördlich der Stadt vermuten könnte, aber es ist ganz und gar nicht eine attraktive Stadt. Kommt man am Bahnhof Schwedt (Mitte) an, der sich am nördlichen Rand der Stadt befindet, wird man von einem Leichtindustriegebiet mit zahlreichen Einkaufmöglichkeiten konfrontiert. Hier gibt es nicht nur Aldi, sondern auch ein Konsumtempel mit dem üblichen Zeugs. Nagelstudios, Mediamarkt, C&A, McDonalds. Sonst halt so Sachen wie eine Autovermietung und anderen KFZ, nahen Einrichtungen, Möbelhaus usw. Der tolle Bahnhof hat nicht einmal ein Bahnhofsgebäude und nur ein Gleis, mit schmaler Plattform, mit kaputten Werbetafeln. In die Innenstadt mit sogenannter Altstadt läuft man ca. 20 Minuten oder fährt eine Station weiter nach Schwedt (Oder). Die Lindenallee ist die hässliche Hauptstraße der Stadt. Sie ähnelt der Frankfurter oder der Karl-Marx-Allee in Berlin. Links und rechts Plattenbauten, dann breiter Streifen grün und Gehwegplatten und eine sehr breite Straße in der Mitte. Etwa auf der Hälfte befindet sich noch eine Konsummöglichkeit. Am Platz der Befreiung kann man zur Post und Sparkasse. Am Ende der Straße befindet sich ein Gebäude, dass ich zuerst für Ceauşescu's Palast in Bukarest gehalten habe. Es scheint aber ein Theater zu sein. Architektonisch eine komische Mischung aus Brutalismus und sozialistischer Moderne (ähnlich dem SEZ in Berlin), mit gold-getönten Fenstern, wie in [Erichs Lampenladen](https://www.berlin.de/binaries/asset/image_assets/1846532/source/1329154030/624x468/). An der trostlosesten Uferpromenade des ganzen Universums kostet der Becher beschissenes Erdbeereis 6,50 €. Die Stadt ist zwar wenig asifiziert und auch nicht schmutzig, aber reiht sich eine hässliche Wohnsiedlung an die nächste. Zur Auswahl stehen Nachkriegsbauten aus den 50ern, Plattenbauten aus den 70ern, eine komische Mischung aus modernen Stadthäusern und Wochenendhäusern.

Zum Glück grenzt die Stadt an das [Odertal](https://www.nationalpark-unteres-odertal.eu/wp-content/uploads/2016/10/moore_und_suempfe.jpg), einem wirklich interessanten Naturpark mit teils überfluteten Wiesen und Feldern, Seen, Mooren und Sümpfen. Als ich Ende Mai dort bin, ist zwar nicht das ganze Gebiet überflutet, jedoch große Teile. Wiesen sind erkennbar, aber überwiegend unter Wasser.

Ich überquere einen Fluss. Als dummer Städter habe ich vermutet, es sei die Oder. Es war aber nur eine recht breite Stelle eines Kanals. Als ich das Odertal überquere, bemerke ich gleich ein anderes feuchteres, schwüles Klima. Dabei bin ich nur paar hundert Meter über die Brücke gelaufen. Nach vier weiteren Brücken bin ich in Polen angekommen. Der kleine Ort Krajnik Dolny hat außer einem Schaschlick-Imbiss, Geldwechsel, Zigaretten und einem Biedronka nicht viel zu bieten, aber hinter dem Odertal auf polnischer Seite hier wird es aber [hügelig](https://www.berlin.de/binaries/asset/image_assets/771491/source/1456496826/624x468/). Landschaftlich ist es hier nett. Ich habe leider keine Zloty dabei und auch nicht ausreichend Euros um zu tauschen. Trotzdem ist der kleine Ausflug in die früheren Reichsgebiete eine nette Abwechslung.

### Angermünde (13)

Da ich bei meinen Touren nach Templin, aber auch Schwedt und Stralsund bereits in der Uckermark war und ich die Landschaft gleich nach Chorin sehr schön fand, bin ich erneut in die Uckermark gereist. Diesmal nach [Angermünde [yt]](https://www.youtube.com/watch?v=I7HKNYbeXGk&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/). 

Vom Stadtbild her ist die Stadt auf dem Niveau von Burg (bei Magdeburg) und Templin. Fachwerke und gründerzeitliche Gebäude prägen das Stadtbild. Die einigen neueren Gebäude wirken nicht störend. Angermünde ist nicht nur sauber, sondern auch recht idyllisch. Überall in der Stadt riecht es blütenfrisch. Ebenso wie in Templin, Stralsund und Burg machen hier die Menschen einen ordentlichen Eindruck. Auch die Kinder, die vom Jobcenter sprechen, machen den Eindruck, als würden sie nicht nur vor der Glotze sitzen und sich von Dosenravioli ernähren. Die obligatorischen Einkaufsmöglichkeiten bei Netto, Kik und Penny finde ich erst auf dem Rückweg, als ich den Zug verpasse und bis zum nächste Zug noch die andere Stadtseite vom Bahnhof besuche. Dennoch ist der Asifizierungsgrad gering. Das Ufer zum Mündesee, das hier mit einem etwas hochtrabenden "Terraza panoramico" bestückt ist, führt auf den Mündesee-Wanderweg. Dieser führt zuerst am Ufer entlang, an Gänsen vorbei, bis man an einer Gartensiedlung vorbei, zwischen Getreidefeldern und der Landstraße entlang auf die Nordseite des Sees gelangt. Etwas erhöht hat man an der Steinskulptur Leuchtturm einen guten Ausblick. Etwas weiter geht es durch eine kleine Siedlung und den Ort oder Ortsteil Dobberzin immer am See entlang. Hier am östlichen Ufer sieht man recht große Einfamilienhäuser und Vorgärten, die geschmacklich grenzwertig sind. In Amerika würde man sowas eine McManson nennen. Es soll einen herrschaftlichen Eindruck vermitteln, wirkt aber eher neureich und geschmacklos. Es folgt erneut eine Kleingartensiedlung. Überraschenderweise gibt es hier einige leere Parzellen. Nach ca. 10 km ist man wieder in Angermünde angekommen. Hier riecht es nach geräuchertem Fisch.

Auf dem Weg zum Bahnhof finde ich hier einen Eisladen, der wirklich sehr gutes Eis für 1,50 € die Kugel verkauft.

Landschaftlich ist die [Uckermark [yt]](https://www.youtube.com/watch?v=shleRjHZ9pY&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/) mit der Märkischen Schweiz und dem Odertal die attraktivste Gegend. Gerade im Mai, wenn die Felder blühen und das Getreidefelder durch den Wind hin und her wehen.

Wegen des Wanderweges und der Landschaft vergebe ich Angermünde erst einmal den ersten Platz. Platz zwei und drei teilen sich Templin und Burg (bei Magdeburg).

### Ludwigsfelde (14)

Ludwigsfelde ist wie Nauen und Oranienburg eine Stadt in der Nähe Berlins, direkt am südlichen Autobahnring und damit innerhalb des Asifizierungsbelts. Das Allerschlimmste. Die Autobahn führt direkt durch die Stadt. Vom Bahnhof mit anschließendem Lidl und ostseitigem Leichtindustriegebiet führt die Potsdamer Straße in das, was man mit viel Fantasie Innenstadt nennen könnte. An dieser Straße befinden sich alle Läden der Stadt. Der Norma, sowas wie eine Mall, Sparkasse, Takko, noch eine Mall mit einem Edeka, und noch eine Mall mit einem Kaufland. Es folgen Subway, TEDi, Pokébowl, Döner und Kik, Penny, ein kleiner Konsumtempel mit MäcGeiz und ein Nagelstudio. Etwas weiter gibt es diese Discounter Bungalows mit Netto und Getränke Hoffmann, Aldi, Plattenbauten und andere 50er-Wohnsiedlungen bis man an das Ende der Stadt mit einer Einfamilienhaussiedlung kommt. Die Stadt kommt mir vor wie eine amerikanische Stadt, die mit zahlreichen Strip-Malls angeben kann. Auf Google Maps erkennt man im Westen eine weitere [Mehrfamilien-Neubausiedlung](https://goo.gl/maps/myw33Y6cptSP2Ji68), die mich an das tolle Amerika erinnert. Alle Häuser sehen gleich aus, selbst die Straßenführung erinnert an die Suburbs. Wer sich hier ein Haus kauft, hat es echt nicht besser verdient.

Der Assifizierungsgrad der Stadt ist moderat bis hoch. Nicht nur die Dichte der Asiläden, auch Graffiti und die Hässlichkeit der Stadt und na ja, auch die Menschen. Die einzigen Lichtblicke sind interessanterweise die kleinen grünen Oasen, die zwischen den 50er-Wohnblöcken liegen.

### Frankfurt (Oder) + Słubice (15)

Ich möchte nach Frankfurt (Oder). Wegen Gleisbauarbeiten hält der Zug nicht wie sonst am Ostkreuz. Ich muss mit dem gewöhnlichen Volk mit der S-Bahn zwei Stationen zum Ostbahnhof fahren. Dort wartet schon die ODEG. In weniger als einer Stunde ist man in Frankfurt. Schon vor der Einfahrt in den funktionellen Bahnhof entdecke ich einen Netto. Ich entscheide mich diesen nicht aufzusuchen, denn ich habe was Größeres vor. 

Der Bahnhof wird gerade in verschiedenen Hautfarbtönen gestrichen. Von käsig gelbweiß bis Schweinchen rosa ist alles dabei. Obwohl die Malerarbeiten fast abgeschlossen sind, ist noch kein Graffiti zu sehen. Ich bin beeindruckt. Ich gehe an zwei roten Häusern vorbei und dann ins Tal und in die Innenstadt. Mit jedem Meter, den ich mich der Innenstadt nähere, steigt der Asifizierungsgrad. Vor allem Graffiti ist überall zu sehen. Die Straßen ähneln, was den Grasbewuchs angeht, Berlin. Es ist deutlich zu merken, dass hier eine Brutstätte der zukünftigen Leistungsträger unserer großartigen Demokratie ausgebildet werden. Die Gegend um die Viadrina ist recht trostlos. Alles wirkt halbfertig und schon etwas ramponiert. Das Oderufer ist wahrscheinlich das langweiligste Ufer Deutschlands. Es gibt hier überhaupt nichts zu sehen, außer Carlas, mit lila, grün/weiß gefärbten Haaren, die hier auf Beton, Pflasterstein und Schotterweg umherlaufen.

In Frankfurt wollte ich eh nicht länger verweilen. Mich zieht es mich nämlich auf die östliche Oderseite. Direkt hinter der hässlichsten Oderbrücke in ganz Europa befindet sich die Stadt Słubice. Der Name richtig ausgesprochen, könnte suggerieren, dass es hier viele Männer gibt, die Männer toll finden. Das ist aber nicht der Fall. Die Stadt ist deutlich kleiner, der Asifizierungsgrad deutlich niedriger. 

Polnische Grenzstädte ähneln sich. Man findet zahlreiche Kantor, Fryzjer, Zigaretten, Netto, Aldi und Biedronka. Da ich letztens keine Złoty dabei hatte, um in Krajnik Dolny einen Schaschlikspieß zu probieren, hatte ich diesmal meine in Złoty angelegten Millionen mitgenommen.

Da ich noch nichts gefrühstückt habe, gehe ich in einen Biedronka und hole mir einen dieser kalten Kaffees, zwei von glasierten polnischen Brötchen. In Zgorzelec habe ich diese Brötchen das erste Mal gegessen und sie waren super. Hier, etwas nördlicher, waren sie schon nicht mehr so gut. In Kostrzyn nad Odrą, noch weiter nördlich gab es die schon gar nicht mehr.

Mein Polnisch beschränkt sich auf ein paar wenige Sätze. "Ładna kobieta. Dzień dobry. Poproszę dwa piwa / Poproszę piwo / kawe." Bei der weniger förmlichen Begrüßung "Cześć" (tschechs-schtsch) habe ich bereits Probleme. Zur Not bringe ich auch noch nach "Szukam Dworzec Główny" heraus, allerdings ohne vollständige Frage formulieren zu können. 

Das polnische ł (ua, teils auch stumm) und y (eh), habe ich verinnerlicht. Meine Aussprache ist immerhin so gut, dass man mich versteht und die Kassiererin im Biedronka und Julia beim Netto, bei dem ich später auch noch einkaufen gehe, auf Polnisch mit mir sprechen, nachdem ich sie begrüße. Wahrscheinlich hat sie nur gefragt, ob ich Kleingeld habe. Als Ausländer zahle ich natürlich immer nur in Scheinen (mind. 10 Zloty), weil ich die Münzen nicht kenne. Ich bilde mir ein, sie macht mir einen Heiratsantrag und antworte immer mit dem gleichen. "Przepraszam. Nie mówię po polsku."

Ich gehe auf einen Platz mit Bänken und Fontänen. Hier spielen Muttis, Omas und Opas mit ihren Kindern und Enkeln. In der Nähe muss es eine Berufsschule oder so geben. Als ich nach meinem Frühstück etwas durch den Ort laufe, treffe ich auf einige Anjas, Milenas, Annas und andere ładna kobiety und ein paar bitch. Die Stadt hat ansonsten nicht so viel zu bieten. Ich habe jedoch noch deutlich mehr Złoty dabei und finde einen Netto. 

Ich kaufe ich "Schab tradycyjnej wedzarni", "Czeska musztarda", ein Trybunał und ein glasiertes Hörnchen. Die geräucherte Schweinelende (Schinken) ist für 47,90 Złoty / kg, due Beste, den ich seit langem gegessen habe. Auch der tschechische Senf, der wie eine Mischung aus süßen Senf und Moutarde à l'ancienne aussieht, den es aber in verschiedenen Schärfegraden gibt, ist hervorragend. Ich nehme einen "słodko ostra". Zusammen im Brötchen ein "smaku"-fatzku, oder "Smakuje dobrze", wie die Polen sagen. Wem danach ist, findet hier aber auch kuźnia turecka, kuźnia orientalna und andere kuźnia smaku.

![Geräucherte Schweinelende](){:data-src="/assets/images/aufschwung_ost/schab.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Auf dem Rückweg komme ich in Frankfurt noch durch den Lenné-Park. Hier sitzen die hypermobilen Frankfurter mit den großen, quadratischen Einkaufstaschen auf den Bänken und trinken Bier. 
Obwohl Frankfurt (Oder) einen schon deutlich moderat-hohen Asifizierungsgrad aufweist, und für mich nicht attraktiv ist, lohnt sich der Abstecher. Jedoch eher wegen meines Einkaufs in Słubice.

Das Wetter hier muss ein anderes sein. Auf der Rückfahrt werde ich richtig müde. Zum Glück fahre in ja nur Erste Klasse und habe das Abteil für mich allein. In Fangschleuse steigen zahlreiche Fabrikarbeiter ein, und ich werde wieder rechtzeitig wach.

### Guben + Gubin (16)

Ich fahre in eine Stadt, von der die meisten Menschen im Land noch nie etwas gehört haben. Der Grund: Google Maps zeigt auf polnischer Seite zivilisatorische Aktivitäten an. Ich fahre also nach [Guben [yt]](https://www.youtube.com/watch?v=SeDCuv0DsQwu&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/). Guben ist eine der wenigen Städte, einfach mit der Bahn zu erreichen sind und auch noch einen polnischen Ortsteil haben. Davon gibt es von der Ostsee bis nach Tschechien nicht viel: Schwedt + Krajnik Dolny (sehr klein), Küstrin Kiez + Kostrzyn nad Odrą, Frankfurt (Oder) + Słubice, Görlitz + Zgorzelec, Zittau und zwei Orte und eben Guben.

Als ich ankomme, dachte ich, meine Entscheidung wäre falsch gewesen. Der Bahnhof, insbesondere die Unterführung ist hässlich, aber nicht dreckig. Aus einer Ecke nahe einer Treppe kommt mir südbrandenburgischer Pissegestank entgegen. 

Nur paar Meter weiter ändert sich meine Meinung jedoch. Ich gehe durch einen Park, nichts Besonderes, aber sauber und nett und komme dann am Plastinarium vorbei. Vor einigen Jahren hat mal ein verrückter Professor für Aufsehen gesorgt, als bekannt wurde, dass er Leichen, Mensch und Tier plastiniert und die Kunstwerke dann ausstellt. Dabei werden die Stücke so fixiert, wie sie entweder zu Lebzeiten häufig anzufinden waren, z. B. beim Sitzen, beim Karten oder Fußball spielen, beim Stabhochsprung oder wie sie zum Zeitpunkt des Todes vorgefunden wurden, z. B. bei einem Motorradunfall, beim Sex und wahrscheinlich auch beim Joggen im Wald mit Feinstaubmaske.

Ich gehe weiter und komme in eine recht angenehme Kleinstadt mit geringem Asifizierungsgrad. Die Straßen sind alle mit Blumen und Bäumen bestückt. Die Gehwege sind breit, die Häuser zeugen von einer vergangenen Blütezeit. Es gibt zahlreiche Stadtvillen, aber auch "normale" Häuser. Alles sehr nett. Die Stadt muss wie Cottbus durch die Textilindustrie reich geworden sein. Ein Tuchmacherweg und die Alte Färberei deuten darauf hin.

Mir fällt auf, für die kleine Stadt gibt es hier sehr viele Sitz- und Verweilmöglichkeiten und sehr viele Parks und Grünflächen. Selbst die Mittelinseln an den Kreisverkehren sind bepflanzt. 

Über eine von zwei Brücken geht es nach Gubin. Es fällt kaum auf, dass man nun in Polen ist. Ich erkunde die Stadt für die zukünftige Landnahme nach der polnischen Ostverschiebung, bis ich merke, dass die kapitalistischen Fronttruppen unter General Rossmann, der Kik-Brigaden, und dem CCC-Sturm bereits die Landnahme an der Konsumfront durchgeführt haben. Jetzt wird mir auch klar, warum mir am Bahnhof Guben kaum Asifizierungshinweise aufgefallen sind. Diese wurden gänzlich in das Ostgebiet verlagert.

Anders als in Frankfurt (Oder) und Słubice ist hier Gubin etwas stärker assifiziert als das deutsche Pendant, jedoch nur gering (etwas Graffiti). Auch hier gibt es mehrere Parks und Sitzgelegenheiten. "Kurwa"-schreiende Alkis treffe ich hier aber keine. Die wurden wahrscheinlich aufgrund der Landnahme nach Berlin umgesiedelt.

Ich laufe den Berg an der Ulica Grunwaldska entlang und entdecke, dass hier wohl einmal [Ludwig Mies van der Rohe](https://de.wikipedia.org/wiki/Ludwig_Mies_van_der_Rohe) eine Villa gebaut hat. Nicht mein Stil, aber was solls.

Gubin hat noch deutlich mehr Potential als Guben. Wer gedenkt nach Guben oder in die Lithium- und Batterie- und Energiezukunftsgegend zu ziehen, die ständigen Erfolgsmeldungen deutscher Exzellenzpolitik nicht mehr ertragen kann, kann einfach 20 m über die Brücke nach Gubin ziehen. Dort spart man auf 30 Jahre, die man ein Haus abzahlt, sogar mindestens 6660,00 € an freiwilligen Beiträgen für den Staatsfunk. 

Ich komme an einer Villa vorbei, wo Kinder auf einem sehr gesunden, grünen Rasen spielen. Ob das eine Privatschule, Kindergarten oder ein Indoktrinationszentrum für zukünftige polnische Leistungsträger ist, weiß ich nicht. Alle scheinen glücklich. In Deutschland würde wahrscheinlich ein Schild am Rasen stehen: "Betreten verboten".

Auf dem Rückweg gehe ich noch bei Horex einkaufen. Ich verstehe, dass die Frau mich fragt, ob ich noch 11 Groszy habe und antworte im fließenden Polnisch "nie". Schaue aber trotzdem noch mal nach, damit die Frau verstanden hat, dass ich sie verstanden habe. Die Groszy ließ ich aber zu Huase.

Gubin hat etwas vom Polen der 1990er. Hier sind die jungen Frauen so angezogen, als müssten sie sich noch anstrengen, um einen geeigneten Mann abzukriegen. Aber auch hier macht sich langsam die europäische Asifizierung bemerkbar.

Auf dem Weg zum Bahnhof sehe ich noch das übliche Grenzpolen. Hier parken überwiegend Autos mit deutschen Kennzeichen und zwischen Kantor, Apteka und Zigaretten gibt es ca. 1000 Salony fryzjerskie und "beauty".

Ich freue mich, dass ich die Entscheidung getroffen habe. Sowohl Guben und Gubin waren eine positive Überraschung. Beide Städte sind zwar trotzdem recht langweilig, aber nett und mit großem Potenzial. Ach, und Nazis habe ich hier auch keine gesehen.

### Magdeburg (17)

![Magdeburg](){:data-src="/assets/images/aufschwung_ost/magdeburg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Ziemlich hässliches Zentrum, aber der Bahnhof hat einen schönen Torbogen.


### Neustadt (Dosse) (18)

Nur eine Ansammlung von Wohnhäusern. Es gibt am Ortsrand noch eine Touri-Attraktion, irgendein Schloss oder einen Park und einen asiatischen Imbiss, bei dem ich etwas für die Heimfahrt hole.

### Dessau (19)

Auf Google Maps schaue ich nach Städten, schaue dann wie lange ich dahin brauche und wenn es passt, kommt der Ort auf eine Liste und dann reise ich die Liste ab. Meistens informiere ich mich ein wenig vorher und sortiere noch ein wenig nach Priorität, meistens nach aktuellem Befinden (Stadt, Natur, usw.).

Ich hatte mir vorgenommen, entweder noch einmal eine längere Strecke nach Wismar oder wenigstens Schwerin zu unternehmen, oder es noch einmal mit Kyritz zu probieren. Da ich meist nicht genau plane und einfach zum Bahnhof laufe, kann es sein, dass ich länger auf den nächsten Zug warten muss und ad-hoc ein anderes Ziel auswähle. Der erste Zug auf dem gleichen Gleis war der RE1 nach Dessau. Ich dachte, warum nicht. Da es schon fast 10 Uhr war, war es eigentlich schon recht spät für Wismar. Der Aufenthalt wäre für fast Stunden Fahrt zu kurz.

Dessau, obwohl auf der Liste, war recht weit hinten auf der Liste. Wikipedia schreibt, dass 80 % der Stadt im Zweiten Befreiungskrieg durch amerikanische Imperialisten zerstört wurde. Zudem ist Dessau seit dem im Jahr 1926 gebaute Bauhausgebäude mit der Bauhaus-Stilrichtung verbunden. Zusammen mit 40 Jahre real existierenden Sozialismus befürchtete ich das Schlimmste.

Ich fuhr die gleiche Strecke, die ich bereits nach Bad Belzig gefahren bin. Bis dahin ist die Strecke nicht sonderlich schön. Ab Bad Belzig wird es ein wenig hügeliger, aber bei weitem nicht so interessant wie es Richtung Uckermark geht.

Die erste Überraschung war Roßlau. Der Ort, der seit ein paar Jahren zur Doppelstadt Dessau-Roßlau gehört, hat statt einem Netto, Lidl oder Kik einen Rewe in der Nähe des Bahnhofs. Hier müssen reiche Bonzen wohnen. Ein paar Minuten später erreicht man den Hauptbahnhof Dessau. Der Bahnhof ist zwar nicht besonders schön, mir fällt aber eine [tolle Sitzbank](https://www.bahnbilder.de/bilder/bauhaus-am-dessauer-hauptbahnhofdie-bauhaus-stil-1235077.jpg) auf dem Bahnsteig auf. In Berlin wäre sie wahrscheinlich wegen der Liegebarrieren, die hier Armlehnen heißen, von grünen Obdachlosenhelf:enden verboten worden.

Die Unterführung ist auch nicht schön, aber irgendwie interessant, da die Decke verschieden hoch, mit Lichteinlässen, aber insgesamt sehr niedrig ist. Am Boden an den Wänden sieht man eine Rinne, die in Berlin mit den Fliesen an der Wand als Pissoir genutzt würde. Als ich vor meiner Reise noch einen Kaffee holte und durch die hippe Simon-Dach-Straße lief, sah ich einen Menschen, der äußeren Merkmale, hätte man sie früher als Mann bezeichnet, wie er auf einem dieser Donnerbalken rund um die Baumscheibe saß, mit heruntergelassener Hose und mit Kackehaufen auf der Baumscheibe. In jeder anderen Stadt würde man glauben, das sei gestellt und hier würde ein Film gedreht. Es ist aber in der kack grünen Hauptstadt durchaus tägliche Realität. Kritik unerwünscht. Nazigefahr! Den Touristen, die ein paar Meter weiter frühstücken, scheint das auch nicht zu stören.

Der Bahnhofsplatz hat nichts Besonderes. Ich sehe meine erste Amira, das arabische Pendant zur geilen Mandy aus Ostgermanien. Amiras tragen meistens enge Hosen, ein weißes Oberteil und haben sehr runde und apfelsinengroße Brüste. Meistens so um die Anfang 20 und recht hübsch. Ab 30 dann meistens mehrfach geschwängert, gekidnappt oder geköpft.

Insgesamt sehen die Menschen hier recht in Ordnung aus, nicht ganz so wie in Stralsund, aber bei weitem besser als in Cottbus oder Magdeburg. Es gibt ein paar Johannes und Malte-Sörens. Also Almans in ihren 20ern, meistens ein wenig untersetzt, mit T-Shirt und Rockbandaufdruck. Sie haben meistens zottelige Haare und im Gesicht sowas wie ein Bart. Sie spielen gerne Rollenbrettspiele, programmieren und stellen im Internet gerne ihre Unkenntnis über eigentlich alles zur Schau. 

Meine Route führt vom Bahnhof irgendwie zum Rathaus-Center. Hier merke ich, dialektisch leicht ange-öh-t. Beim Rewe-Imbiss wird "Fleischkääähs öhne alles" (leicht angeöht) bestellt. Ich kaufe mir ein Brötchen mit Boulette und gehe Richtung Stadtpark. 
Mein geschultes Auge entdeckt am Anfang meines länger als erwarteten Aufenthalts ein paar Personen, die man in Nazihochburgen "Assis" nennen würde. Nazis habe ich hier übrigens keine gesehen. In der Nähe des Bahnhofs ist ein griechisches Restaurant. Dort hängt eine schwarz, rot, gelbe Fahne. Vielleicht ist der Nazi?

Selbst die "Asis" mit Bierflasche und Tüten mit Flaschen sind hier nicht sehr laut und machen irgendwie einen zivilisierten Eindruck. Es liegt nicht übermäßig viel Dreck und Zigaretten auf dem Boden. Im [historischen Arbeitsamt](https://www.bauhaus-dessau.de/im/840x0/20536b86457b772b2d04a448fc529aaa.jpg) ist das "Amt für Zucht und Ordnung" untergebracht. Hier sehen die Angestellten auch assiger aus, als die Assis.

Nach meinem Frühstück, es ist ca. 12:30 gehe ich vom Stadtpark über die Askanische Straße. Irgendwo hier in der Nähe sehe ich [sanierte Häuser](https://www.bundesbaublatt.de/imgs/100536157_0a7c1d580c.jpg) und bin überrascht über das gute Ergebnis. Der Baumform und Höhe nach ist es einer dieser 50er-Bauten. Die Aneinanderreihung sieht eher nach 70er-Platte aus. Das Indernetz sagt Baujahr 1969.
Ich sehe auch einige unsanierte Wohnhäuser. Aber selbst die sehen frei stehend und in ihrem Umfeld nicht aus, als würden hier nur Asis wohnen, sondern strahlen sogar etwas Würde aus.

Ich laufe weiter und entdecke eine rot gefärbte Straße (im Asphalt, kein Fahrradweg). Ich folge der Straße und entdecke eine recht idyllische Industriebrache. Die wird teilweise noch genutzt. Aber es stehen hier auch leere Backsteinbauten mit kaputten Fenstern herum. Zwischendurch immer wieder Grünflächen. Die waren wahrscheinlich mal bebaut. Ich erinnere mich an meine Kindheit und das Bahngelände, das heute als durchgehipstertes "Südgelände Schöneberg" bekannt ist [I](https://www.ytti.de/wp-content/gallery/s/naturpark-schoeneberger-suedgelaende-berlin-drehscheibe.jpg?x87014) und [II](https://sbahn.berlin/fileadmin/_processed_/0/7/csm_Bildhighlight_Schienen_IMG_8047_Copyright_via_reise_verlag_Janna_Menke_b1599276db.jpg). Ich habe dort des Öfteren gespielt. 

Am noch in Nutzung befindlichen Kraftwerk "An der Fine" finde ich Zeugnisse Dessauer Kreativität in Form eines der amerikanischen Ausdrucksform nachempfundenen gesprühten Schriftzugs: "Sie ist krea ich bin tiv". Warum fallen mir solche Sachen nicht ein? Eine Karriere im Privatfernsehen ist dem Künstler sicher.

Beim Lidl in der Nähe merke ich deutlich, hier gehen die Uhren anders. Der Kaffeeautomat braucht mehrere Minuten. Ein Dessauer versichert mir, das sei normal.

Ich laufe irgendwo an und um die Askanische Straße zurück. Die Stadt macht hier den Eindruck, als sei sie irgendwann in den 70ern stehen geblieben. Viel Beton -platten, -brunnen und -blumentöpfe. Vom Brutalismus der 70er ist man hier aber weit entfernt. Es sieht hier auch nicht so schlimm, heruntergekommen und aus der Zeit gefallen aus, wie in anderen Städten mit 70er Bau-boomse. 

Ich komme an einer [hippen Brücke](https://files.structurae.net/files/photos/5256/2022-07-02/dsc02292.jpg) vorbei und überquere sie. Hier steht ein Schild und erzählt etwas von einem Unesco-Weltdingsbums und Eichen mit 2 m Durchmesser. Ok. Neben Eichen wachsen hier vor allem so tolle Pflanzen wie Brennnesseln, Wegerich und die mit den kleinen weißen Blüten, die zusammen ein Halbrund bilden. 

Der Park ist irgendwas zwischen Kleinstwald, Park und Heide. Nirgends gibt es Verweilmöglichkeiten und abseits der wenigen Wege ist der Park nicht begehbar. Immerhin ist er recht sauber, aber nicht Müll-befreit.

Ich dünge einen dieser Unseco-geschützten Eichen mit meinem Stadturin, da es nirgends öffentliche Toiletten in der Stadt gibt. Irgendwo in dem Park überquere ich eine zweite Brücke und komme dann nach einiger Zeit an einer größeren Brücke vorbei, die von Autos befahren wird und von mir unterquert wird. Obwohl die Brücke an sich nichts besonders hat, finde ich die Natursteinfassade recht ansprechend. Ich bin verwundert. Die Brücke wäre eigentlich ein Obdachlosenparadies. Man ist im Grünen, aber schnell im Zentrum und es gibt hier wenig Laufpublikum. Aber zu meiner Überraschung ist es hier absolut sauber. Es liegt nichts herum, auch keine alte Couch, Einkaufswagen oder alte Sprühdosen.  Dabei ist der Boden nicht einmal mit Obdachlosen-hinderungs-beton-spitzen bestückt, sondern mit einem ansprechenden und recht flachen Steinboden.

![Mulde](){:data-src="/assets/images/aufschwung_ost/dessau.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Irgendwann komme ich aus dem Park und sehe das Ortsschild Dessau und einen Hinweis, dass ich in der Wasserstadt gelandet bin. Ich entdecke ein Wehr, der über die Straße führt. Wahrscheinlich schützen sich die Dessauer hiermit vor der Gefahr einer woken Berliner:Innen-Schwemme.

Für Dessauer Verhältnisse ist dies hier die reinste Bonzengegend. Nach einer Weile komme ich wieder in das urbanere Dessau und an einem Kreisverkehr vorbei. Der scheint entweder sehr neu, oder generalüberholt zu sein. Alles nigelnagelneu. An einer weiteren Mulde-Brücke gibt es sogar einen kleinen Strand, der nur von zwei Personen besucht ist. Es gibt hier wieder zahlreiche der nicht allzu hässlichen Wohngebäude. Ich komme in eine Gegend, die sogar ganz nett aussieht, mit altem und neuen "Stadtvillen", später an einem top-sanierten Gebäude vorbei und durch eine Gegend mit teils älteren Gebäuden und kleinen, kurvigen Straßen. Gar nicht mal so schlecht. Es gibt ein paar Graffiti wie "acab" und "antifa", aber sonst nicht viel Asihinweise.

Überhaupt fällt mir die totale Abwesenheit von Identitärem Scheiß auf. In der ganzen Stadt sehe ich keine Tuntenflaggen, keine Autos mit Rammstein oder Böhse Onkelz Aufkleber auf der Rückscheibe. Keine Hipster-Idioten, keine Woke-Spinner, keine Norbertinen (Feministinnen mit Minipli, hässlichen Strümpfen und  bedruckten T-Shirts: "Fuck off", "Femi..blah").

Isch höre auch kein "isch ficke disch" und sehe nur sehr wenige Jogginghosen, Bierbäuche oder Vollgekotzte.

Junge Frauen gibt es hier auch viele. Muss am Bauhaus liegen. Viele etwas Bieder im Stil "Bibeltreue Christen". Vater Architekt, Mutter Notarin. Haut überwiegend bedeckt, dezent und vor allem fachfrauisch geschminkt. Die Strickjacke mit diesen großen Löchern ist immer dabei. Brille, liest viel, trinkt nicht und nach einem Glas Prosecco vergewaltigt sie ihren besten Freund.

Ich sehe in der ganzen Stadt nur einen Beauty-Salon, und sogar eine geile Mandy, diesmal mit pampelmusengroßen Brüsten und sehr kurzer Hose, mit strammen Waden und nicht zu dicken Oberschenkeln, dafür anders als in Magdeburg ohne Türsteheranhang. Sie ist wahrscheinlich Sekretärin.

Dessau hat viel mehr Zugezogene aus fernen Gefilden als man vielleicht erst einmal annimmt. Ich sehe viel Braune und dunkel braunen Menschen. Die Deutsche Leitkultur zeigt sich hier von der besten Seite.

Man merkt der Stadt an, dass sie stark zerstört wurde. Der Bestand an Vorkriegsbauten ist sehr gering und beschränkt sich auf ein paar Kirchen, Verwaltungsgebäude und ein paar Stadtvillen oder das [Hauptpostamt (1901](https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Dessau_asv2022-08_img06_Hauptpost.jpg/1200px-Dessau_asv2022-08_img06_Hauptpost.jpg). 

Die Befürchtung, dass Dessau eine hässliche und (deswegen) eine hochasifizierte Stadt sein könnte, bestätigt sich jedoch nicht. Ganz im Gegenteil. Architektonisch ist Dessau durchaus interessant und sogar teilweise "nett". Die 50er-Bauten, die sozialistischen 70er-Plattenbauten und die modernen Gebäude, wie z. B. [das Bundesumweltamt](https://www.umweltbundesamt.de/sites/default/files/styles/800w550h/public/medien/376/bilder/busse_03.jpg?itok=s2M4CvxO), fügen sich zu einem stimmigen Stadtbild zusammen.

Selbst [zwei kackbraune Plattenhochbauten](https://tag-wohnen.de/fileadmin/_processed_/a/5/csm_dessau_y-haeuser_tag_wohnen_9832d46150.jpg) sind hier fast hübsch. Die Proportionen aller Häuser sind stimmig. Zahlreiche 50er und Plattenbauten wurden saniert und sehen teils sogar richtig gut aus. Zum Beispiel eins der wenigen etwas [höheren Platten am Bahnhof](https://1.bp.blogspot.com/-S3vG6gc_exg/XWkzkafoo2I/AAAAAAAABQM/EulqTDn6PAQ0h7b4MU3OaCssrvqB4GJ-QCEwYBhgL/s1600/Dessau%2Bverschoenerte%2BPlatte.JPG). Oder dieses, dass ich am Ende meines Aufenthalts entdeckte: [vorher (Bj. 1977)](https://bmg-images.forward-publishing.io/2021/4/5/0902ce36-89e1-4ada-94f3-e42c6bdd1684.jpeg?auto=format) und nachher [I](https://www.wg-dessau.de/files/themes/bootstrap-contao/content/images/gebaeude/Marienstrasse.jpg) und [II](https://bmg-images.forward-publishing.io/2021/4/5/936fa193-a4ba-4559-ad6d-2eae0b272a77.jpeg?auto=format).

Solche Projekte würden in Berlin, dank militanter Vorfeldgruppen der Grünen und Linken als Luxussanierung abgelehnt und auf Jahre verhindert werden. 

Als ich wieder am Bahnhof ankomme, möchte ich eigentlich noch gar nicht zurück. Ich drehe noch eine Runde, gehe zum Kaufland und hole mir die Verpflegung für die Rückfahrt.

Ich wundere mich zwar, was die Dessauer so in ihrer Freizeit machen. Ich sehe wenig Unterhaltung, Restaurants usw. Die Stadt ist aber weit interessanter als ich gedacht habe. Weit entfernt von den Asihochburgen Magdeburg und Cottbus.

Dessau hat ca. 77.000 Einwohner und liegt damit zwischen Frankfurt (Oder) und Cottbus. An der Größe kann der Asifizierungsgrad nicht liegen.

### Torgau (20) + Leipzig (21)

[Torgau [yt]](https://www.youtube.com/watch?v=veSkSxv8Ix4&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/) ist eine wirkliche nette kleine Stadt. Hier laufen auch Menschen auf der Straße herum, es gibt viele Cafés und Restaurants mit echten Gästen.

![Torgau](){:data-src="/assets/images/aufschwung_ost/torgau.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Nach meiner Touri-Tour in Torgau fahre ich weiter nach Leipzig. Ich nehme dort die Tramlinie 88, die mich irgendwo nach Connewitz bringt. Connewitz ist aus berliner Perspektive ein Bonzenviertel in dem sanierte Altbauten stehen, in denen sich Linke einquartieren, die zu Demos nach Berlin reisen und hier was von Luxussanierungen labern. Connewitz gleicht etwa dem Prenzlauer Berg der frühen 2000er Jahre, als alles saniert war und die ersten Asigrafiti zu sehen waren.

Bina Bengel wurde heute zu fünf Jahren Muschilecken verurteilt.

Ihr pumpender Freund ist zwar seit zwei Jahren untergetaucht, die Staatsanwaltschaft geht aber davon aus, dass es keine Fluchtgefahr gibt. 
In Leipzig ist mir die heute freigelassene Terroristin Bina Bengel aber nicht über den Weg gelaufen. In Connewitz war auch alles ruhig. Die zwei schuhlosen Hobby-Linken, die ich im Bonzenviertel Connewitz gesehen habe, haben sich auch nicht für die Bina interessiert.

Was mir in Leipzig aber aufgefallen ist, ist die Präsenz von dicken Titten und diese sogar bei hübschen Frauen. Da Bina Bengel ja ein, zwei Jahre lang durchgepökeltes Kassler ist, muss das mit den dicken Titten in Leipzig am Bier liegen. Aber ihre blonden Haare gefallen mir gar nicht. Ist auch egal. Aber geile Titten hat sie, auch wenn die etwas tief hängen.

Mich würde es nicht wundern, wenn Bina Bengel schon seit mindestens 10 Jahren ein Verdachtsfall ist und seit 2013/14 beobachtet wurde. Vielleicht ist die Arme auch nur ein Opfer linker Linker. Linke tun halt linke Dinge.
Fünf Jahre und drei Monate, von denen sie nach ca. 3,5 Jahren freikommt, wäre für das kleine Dummchen doch die Chance für den Ausstieg. Sie soll ja bei Freilassung mit Auflagen sogar eine Träne vergossen haben.

Stellt Euch mal vor, die beste deutsche Demokratie aller Zeiten sorgt dafür, dass ich auf der Straße lande, sodass ich Humana-Kleidung tragen muss. Es ist nur ein Lonsdale Pullover, eine Bomberjacke und New Balance Schuhe in der Kiste und ich werde dann von Bina Bengel entstellt. Bina Bengek kommt nach paar Jahren frei und geht als Vorbestrafte in die Politik und ich bin für den Rest meines Lebens entstellt. Ich würde sie töten, wenn sie mir über den Weg liefe. Also rein hypothetisch.

### Stendal (22) + Tangermünde (23)

Das Bahnhofsgebäude in Stendal ist ganz nett, aber nicht zugänglich. Der Vorplatz ist nicht sehr hübsch. Ich sehe auch keinen Lidl, Netto oder sonst einen Assiladen in der Nähe, gehe also einfach mal gerade aus.

Mir fallen sofort die Straßen auf. Hier wechseln sich verschiedenste Straßenbeläge in einer komischen, chaotischen Art ab. Eine Straße kann 20 m mit Betonplatten, dann die halbe Breite des Gehweges mit Pflastersteinen, die andere Seite mit Verbundsteinen (mindestens zwei Sorten), eine Einfahrt halb mit Asphalt und halb mit Schotter, wahlweise auch mit rundem Kiesel bestückt sein. Es ist niemals so, dass Schotter an der Seite zum Haus oder Baumscheibe den Gehweg einfassen würde, wie man es sonst kennt. Es ist auch nicht gänzlich zufällig, sondern es macht den Eindruck als sind Zeugnisse der DDR-Mangelwirtschaft. Wenn es gerade fünf Betonplatten gab, dann wurden die dort verlegt, wo sie gerade passten. Für den Rest hat man dann halt mit Schotter aufgefüllt.

Das zieht sich durch die ganze Stadt. Selbst Einfahrten und Gärten von privaten Häusern schaffen es nicht einmal einen einheitlichen oder wenigstens ansprechenden Straßenbelag zu haben. An einer x-beliebigen Kreuzung kann es sein, dass 7-8 verschiedene Straßenbeläge vorzufinden sind.

Eine Ausnahme sind eine, bzw. zwei "Haupt"-Straßen, hier wurde die Fahrbahn und der Gehweg neu gestaltet und sieht recht gut aus. Hier wird gerade für ein Roland-Fest aufgebaut. Den Roland. So eine Steinfigur, mit komischen Proportionen und einem Schwert in der Hand, habe ich schonmal in einer anderen Stadt, die ich im Mai besucht habe, gesehen.

![Stendal](){:data-src="/assets/images/aufschwung_ost/stendal.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Die Stadt sieht nicht schlecht aus, ist aber auch nichts Besonderes. Gibt aber deutliche Grafitti-Assifizierung. Ich gehe in eine Kirche. Da wird gerade georgelt. Ich fühle mich als würde mir hier der Teufel ausgetrieben ([Beispiel [yt]](https://www.youtube.com/watch?v=PEHGxpRoZQM&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/)). Auch hier sieht der Boden typisch stendalisch aus. Die Orgelei ist aber interessant und die Orgel imposant. Ich trage mich brav ins Gästebuch ein. Ich gehe noch in eine zweite Kirche. Vielleicht war es auch der Dom. Ich finde einen Edeka in einem dieser Hauptstraßen und hole mir was zu frühstücken und erhalten Probe-Kaffee aus einem dieser Tab-Maschinen für zu Hause. Der Kaffee ist aber nicht in Tabs, sondern in Algen eingebettet. Schmeckt ganz gut. Die billige Milch auf dem Probentisch trübt das Empfinden. Die Dame war nett. Es scheint, als sei hier alles mit Käse überbacken. Wirklich alles. Ich kaufe mir ein Brötchen, mit Speck und Käse überbacken. 

Eigentlich habe ich genug gesehen. Ich finde noch einen kleinen Park und deutliche Zeichen der Assifizierung. "Stendal nazifrei" und irgendwas mit "BRD Sklavenstaat, wir haben dich zum kotzen satt". Ich erinnere mich an den Straßenbelag und denke mir: "Für Stendaler Verhältnisse ganz schön poetisch."

Ich komme an den Stadtrand und nehme mir vor, nach [Tangermünde [yt]](https://www.youtube.com/watch?v=bTPzfYO5_X4&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/) zu laufen. Eigentlich hätte ich etwas südlicher aus der Stadt raus, aber egal. Hier riecht es ländlich. Irgendwann komme ich an einem Klärwerk vorbei. Die Offline-HERE-Maps sind zeigen kaum kleine Wege an. Ich laufe weiter über eine Bundesstraße. Ab dem kleinen Wald, durch den ich komme, wird es etwas angenehmer. Komme durch Staffelde, überquere zwei Bahngleise. Hier ist es eigentlich ganz nett. Die Wiesen blühen mit Korn- und Mohnblumen. Die Grillen fangen an zu zirpen. Nach Hämerten laufe ich mehrere km auf einem Damm. Interessant. Die Elbe ist bestimmt noch 3-400 m östlich. Tangermünde hat sogar ein Industriegebiet. Die Sturm-Handels-GmbH hat ein interessantes Gebäude. Beim Lidl am Ortseingang gehe ich noch einmal etwas einkaufen. 

Google Maps sagt, es waren 22,5 km.

Hier werden von der Telekom kostenlos Glasfaserleitungen ins Haus gelegt. Ich sehe eine Straße, in denen mittig die Kabel gelegt und dann in die Häuser verlegt werden.

Tangermünde ist eine echt hübsche Kleinstadt. Interessant ist, dass die Stadt von der Elbe-Seite aussieht, als wäre sie eine riesengroße [Stendal](https://berliner-zeitung.imgix.net/2021/4/28/144c0033-014d-40bc-9423-2de4108f0ce2.jpeg?auto=format&fit=max&w=1880&auto=compress&rect=0,314,6000,3375). Um in die Stadt zu kommen, muss man entweder durch das Roßtor, die Treppen hoch oder durch eine der Zolltore.

Die Stadt sieht nicht zu durchgehipstert aus, obwohl alles recht saniert aussieht. Es gibt ein wenig Tourismus in der Stadt und natürlich auch Läden und Restaurants dafür. Ich sehe eine holde Maid auf einer Parkbank sitzen (Typ: jung, blond, langes Kleid und beim Fest ein Blümchenkranz). Für die aktuelle noch recht gute Wirtschaftssituation im Lande ist sie jedoch mind. 20 Jahre zu jung für mich.

![Tangermünde](){:data-src="/assets/images/aufschwung_ost/tangermuende.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Es gibt zwar Anzeichen leichter Assifizierung, aber es hält sich im Rahmen. Ich esse mein "Mittagessen" vom Lidl und trinke einen kalten Kaffee, gehe in die Kirche gleich neben mir. Hier steht eine Scherer-Orgel. Obwohl es entfernte Verwandte mit dem Namen Scherer gibt, haben wir mit dem Hamburger Orgelbauer bestimmt nichts zu tun.

Ich sehe noch eine leere Kneipe mit großen 90° Tresen und denke mir, sicherlich ist die für das Publikum hier nicht geeignet. Ich entdecke eine echte Brauerei und kaufe hier ein 4er-Pack Schulzens Ziegelrot.

Tangermünde hat zahlreiche Restaurants. Wenn ich mal reich bin, besuche ich die Stadt noch einmal.

Tangermünde teilt sich mit Torgau, Templin, Angermünde und Burg in der Klasse dieser Stadtgröße die vorderen Plätze. 

Ich habe Glück. Als ich am Bahnhof ankomme, kommt auch bald der Zug nach Stendal. Dort steht bereits der Zug nach Berlin.

### Zehdenick (24)

Ich fand ja die Uckermark ganz nett und dachte ich fahre einfach mal in die Richtung und steige am zweiten Bahnhof aus, nachdem die Landschaft hübscher wird. Von Berlin aus ist es ab Löwenberg und Bergsdorf schon ganz nett. Zehdenick habe ich mir vorher auf der Karte angeschaut und gesehen, dass es dort viel Seen gibt. Statt in Bergsdorf bin ich in Zehdenick ausgestiegen und wollte dann nach Bergsdorf zurücklaufen. Das wären ca. 7,9 km gewesen. Eigentlich viel zu wenig. Ich habe mir vorgenommen auf der nordöstlichen Seite nah an der Bahn, irgendwo auf Feld, Wiesen und Waldwegen zu laufen. 

Das stellte sich aber als nicht so einfach heraus, da ich keinen tragbaren Fernsprecher mit Kartenfunktion dabei hatte und habe vorher auch nicht geschaut, wie ich denn laufen könnte. Ich bin zwar kurz auf der östlichen Seite der Bahntrasse durch Felder gelaufen, hätte dann aber wieder ein ganzes Stück auf der Landstraße laufen müssen, um dann wieder Richtung Süden laufen zu können. Ich bin dann an einem kleinen Industriegebiet nach Norden. Bei einem kleinen Bahnübergang wird es dann sogar idyllisch. Hier beginnt die Seenlandschaft, die hier "-Stiche" heißen. Man hört viele Frösche, Grillen und Kuckucks:ende. Vor einer Bahnbrücke gibt es einen kleinen Pfad an der Havel entlang zurück. Ich sonne mich an einem der wenigen Stellen, wo man es kann und komme irgendwann in Zehdenick an. Am Schloss Zehdenick gibt es ein wenig was für Touristen. Restaurants und eine interessante Schleuse und ein paar nette Brücken über die Havel. Etwas nördlich von wo ich gerade herkomme auch einen Ziegeleipark und Camping. Es gibt viele Fahrradfahrer. Ich laufe durch die Stadt, sehe noch mehr Cafés, Restaurants, Döner und Asia-Imbiss in kulinarischer Union. Verkauft wird hier auch Pizza. Es ist Sonntag. Viele Läden sind geschlossen. Aber ein echter Tante-Emma-Laden ist geöffnet. Da steht auch eine echte Emma an der Kasse.

Die Stadt ist jetzt nicht so besonders, aber auch nicht stark assifiziert. Für Berlin-Touristen sicherlich mal einen Ausflug wert. Dann aber eher wegen der Natur, der Havel, den Seen und den Charter-Booten, die es hier gibt. In den ganzen Cafés sitzen Menschen ab 50-60, teils mit den Kindern oder Enkelkindern. Irgendwann finde ich wieder zum Bahnhof. Wie durch ein Wunder kommt der Zug nach Berlin in wenigen Minuten.

Mit Stadterkundung ca. 10 km. Kann man mal machen.

### Brandenburg an der Havel (25)

In Brandenburg war ich auch schon mehrmals. Es ist immer eine Reise wert. Obwohl der Bahnhof und der Bahnhofsvorplatz, die Straße davor und der Neubau dahinter Zumutungen für die Augen sind, wird das Stadterlebnis immer besser, je weiter man in die Stadt läuft. Meine Tour geht vom Bahnhof am südlichen Ende der Stadt durch die Straße, die halt ins Stadtzentrum führt. Ich gehe aber nicht durch über die Brücke, sondern die Kirchhofstraße entlang. Hier stehen alte Industrie-Backsteinbauten, die teils in Hipsterwohnungen umgewandelt wurden, teils von Verwaltungsfaschisten, wie der Agentur mit dem Runen-A, genutzt werden. In Annes Konsumtempel hole ich mir etwas zu frühstücken. Der Platz auf der einen Seite des Konsumtempels und die Straßenecke erinnert mich an Frankfurt (Oder), allerdings sieht es hier viel besser aus. Die Hauptstraße ist leicht anassifiziert. Irgendwo gibt es hier mehrere Baba-Shops, eine Spielhalle, einen Wollwort und die üblichen Assimarker wie Nagelstudios. Die Assi-Dichte fällt auf, aber es ist noch nicht Risikogebiet, aber definitiv ein Beobachtungsfall. Hinter der Brücke wird es ganz nett, mit Ausnahme des Havelufers. Hier sitzen viele Giselas, ab 55 Jahren. Die Haut im Gesicht schon ein wenig vom Rauchen gegerbt, die Stimme etwas rauchig. Giselas fallen meistens dadurch aus, dass sie auf einer Bank sitzen und viel rauchen. Die Zigarette legen sie dann unter den Schuh und drücken sie so aus, dass sie zu Feinstaub gemahlen wird und der Boden eine schwarze Tabakspur aufweist.

Brandenburg ist nicht super idyllisch, hat keine riesigen tollen Denkmäler oder eine besonders malerische Altstadt, die alle anderen überragen würde. Brandenburg ist wie viele Außenbezirke Berlins, mit teils fast schon dörflichen Charakter. Es gibt zahlreiche Parks, viel Wasser, Brücken, Kirchen. Die Wallpromenade, der Rathenaupark und die St. Gotthardt-Kirche und die Straßen drum rum, und die Insel mit dem Dom sind meine Lieblingsorte in der Stadt. Ich laufe am Dom vorbei und denke mir, die Grundschulkinder vom Domstift sehen alle ganz schön gepflegt und gut erzogen aus.

Ich laufe auch durch kleine Neubausiedlung (Mehr- und Einfamilienhäuser) und denke mir, man gibt sich hier Mühe, alles einigermaßen hübsch zu gestalten. Architektonisch habe ich in der Preisklasse deutlich Schlimmeres gesehen.

Eine Schleuse wird gerade saniert. Es ist ganz interessant zu sehen, wie eine Schleuse ganz ohne Wasser aussieht. Auf dem Rückweg komme ich an einer Straße mit großen Stadt-Villen vorbei. Auf einem Grundstück laufen Männer herum, die in ihrer Kindheit wohl häufiger mit "Mein Sportsfreund" angesprochen werden mussten.

Ich mag die kleinen Gassen und Straßen in der Stadt. Aber auch die Kirchen. In einer, vielleicht war es der Dom oder St. Gotthardt gibt es einen netten Andachtsraum und man kann eine enge Treppe zur Bibliothek hochlaufen. Die Orgel ist zwar nicht so schön, wie die Scherer-Orgel in Tangermünde, und sie orgelt auch nicht, ich spende trotzdem 0,30 €. Nachdem mir bereits der Teufel ausgetrieben wurde, erwarte ich dafür aber auch meine von Gott gewollte Krönung zum Pater Nemetaniens, dem neu zu schaffenden Reiches, das aus den fünf freien Gebieten der östlichen Bundesländer der Bananen Republik Deutschland, dem Protektorat West-Berlin, dem Assiauffanglager und Protektorat Ost-Berlin und aus den Naherholungsgebieten Westpommern, Pommern, Lebus, Niederschlesien, Oppeln und Schlesien besteht. Als Begründer der Hammeleyschen Dynastie mache ich dann Brandenburg A. d. H. zu meiner Wochenendresidenz und zum Gauzentrum des Ostreiches.

Ja und an Zypressen komme ich auch vorbei.

### Eberswalde (26)

Von mittelmäßig bis ohje. 

### Fürstenwalde (27)

Wenn ich nach Bad Saarow will und den Anschluss verpasse, dann laufe ich durch Fürstenwalde. Die Stadt ist nicht so interessant. In der Nähe des Bahnhofs gibt es aber Einkaufsmöglichkeiten. Dort hole ich mir etwas zu essen.

### Bad Saarow (28)

Nach [Bad Saarow [yt]](https://youtu.be/u2vrU4dFTDY?t=125&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/) fahre ich ab und an. Im Sommer meistens zum Baden in der Therme oder zum Verweilen am Scharmützelsee. Der SpBahnhofsvorplatz mit Springbrunnen ist im Jahr 2023 erneuert worden. Bin ganz gerne in Bad Saarow. Gehe dann meistens zum Edeka und dann zum Scharmützelsee zum frühstücken. Manchmal laufe ich auch Richtung Kliniken. Westlich bin ich bisher immer am Ufer entlang bis zum Golfplatz gelaufen. Hier gibt es überhaupt nichts zu sehen und gegenüber in Pieskow noch mehr Nichts. Aber irgendwie gefällt es mir und komme deshalb immer wieder hier her.

Manchmal muss ich bis zu 45 Minuten in Fürstenwalde (27) auf den Zug nach Bad Saarow warten, so dass ich einmal durch die Stadt gelaufen bin und auch einmal direkt nur nach Fürstenwalde gefahren bin. Die Stadt ist jetzt nicht so besonders, aber auch nicht besonders schlimmm. Für die kleine Stadt gibt es einen recht großen Stadtpark mit Wald und das übliche an Kleinstadtzeugs. Aldi, Penny, REWE. Mehrfamilien- und Einfamilienhaussiedlungen, leichte Industrie ohne irgendwelche Besonderheiten. 

### Usedom (Ahlbeck + Świnoujście) (29)

Ich habe auf YouTube so ein Video von einem Engländer gesehen, der irgendwo von Freiburg mit dem Deutschland-Ticket nach Ahlbeck Grenze, dem letzten Bahnhof vor Świnoujście gefahren ist. Er hat dafür zwei Tage gebraucht. Da ich ja schon länger wieder ans Meer wollte, ich Probleme mit dem Internet hatte und nicht schlafen konnte, Usedom (Ahlbeck) einen großen Strand hat, konnte ich die lange Fahrt auf mich nehmen. Man fährt ca. vier Stunden, 20 Minuten (+ das Gleiche zurück). In Ahlbeck Grenze geht es dann direkt am Eingang 2 an den Strand. Nach ca. 7,7 km, am Leuchtturm vorbei und an der Swina entlang, komme ich in Świnoujście an. 
Das Wetter ist ideal für einen Ostseeurlaub, bewölkt, nicht zu warm, sehr windig und der Strand war zwar besucht, aber dennoch recht frei von Menschen.

In Świnoujście laufe ich etwas durch die Stadt, gehe beim Polo Supermarkt einkaufen, hole mir bei einem "Späti" zwei Bier. Da ich letztens ja schon Eins mit dem tollen Namen "Trybunal" hatte, wollte ich im ersten Moment ein "Imperator" mit 12 % kaufen. Das hätte aber nur 2,99 Złoty gekostet. Der Wirtschaftshilfe zuliebe entschied ich mich dann doch für ein 4,99 Złoty (1,22 €) Bier, auch 12 %. Dann muss ich auch schon wieder zurück, 15:20 ist der letzte Zug ohne Schienenersatzverkehr, der die Rückfahrt noch einmal um 1,5 Stunden verlängern würde. Ich laufe ca. 6,6 km bis zum Bahnhof Ahlbeck Seebad, zuerst am Strand bis zum Eingang 3. ab da über die Promenade und Ahlbeck Grenze, wo der Platz um dieses "Grenzkunstwerk" inzwischen mit Touris überlaufen ist.

Świnoujście ist durch den Tourismus belebt. Es gibt eine kleine Innenstadt, und auf ein Fort und einige Restaurants, Cafés, Touri-Touren und zwei hässliche Hotelbunker amerikanischer Investoren. Ahlbeck ist ein langgezogener Strand mit angehängter Ein- und Mehrfamilienhaussiedlung, von denen viele als Ferienwohnungen vermietet werden.

![Ahlbeck und Świnoujście](){:data-src="/assets/images/aufschwung_ost/usedom.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Vor Usedom und im Hafen Świnoujście sieht man lange und dicke Dinger. So dicke Dinger habe ich noch nie gesehen. Verkehrt bestimmt zwischen Swinemünde und Schweden.

Aus Świnoujście bringe ich zwei verschiedene Pierogi, Kiełbasa Piwna, Krakus Turystyczna und zwei Książęce Złote Pszeniczne mit, von denen ich eins schon am Bahnhof Ahlbeck trinke.

### Görlitz (30), Hagenwerder, Ostritz, Bratków und Posada (31)

In einer Online-Zeitung lese etwas davon, dass es in Polen erlaubt ist, im Wald zu zelten. Ich lade mir die Datenbank "Bank Danych o Lasach" herunter und suche nach einem Waldstück, dass nicht mehr als ein Tagesmarsch von einem Bahnhof entfernt ist. Ich entscheide mich für Posada direkt an der östlichen Neiße-Seite. 

![Zgorzelec](){:data-src="/assets/images/hammeleyla/hammeleyla-1.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Zuerst fahre ich nach [Görlitz [yt]](https://youtu.be/HUw0jUMF8rs?feature=shared&t=77&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/), eine nette Stadt. Ich war schon mehrmals dort. Auch im polnischen Teil [Zgorzelec [yt]](https://www.youtube.com/watch?v=92U1pznabfc&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/). Hier habe ich die glasierten Brötchen gekauft. Ich frühstücke dort und fahre dann weiter nach Hagenwerder. Von dort aus laufe ich ca. 11 km (wahrscheinlich mehr) nach Ostritz. Ab Hagenwerder gibt es einen guten Wander- und Fahrradweg. In Ostritz mache ich eine Pause und esse ein Eis beim Penny. Hier geht es dann nach Polen. Dort nehmen mich polnische Rennfahrer ein paar km mit. Ich gehe dann direkt in den Wald und immer an der Lausitzer Neiße entlang, am Kloster St. Marienthal (auf polnischer Seite) vorbei. Das Geländer ist sehr hügelig und teils steinig. Die ganze Tour ist mit meinem ganzen Gepäck anstrengender als erwartet. Erst gegen 19 Uhr baue ich das Zelt auf. Gegen 10 Uhr morgens frühstücke ich, mach mich frisch und laufe ca. 7-8 km über Posada und Bratków nach Ostritz zurück. Dort nehme ich einen Bus nach Hagenwerder, wo ich wieder in einen Zug nach Görlitz und Berlin nehme.

![Bratkow](){:data-src="/assets/images/aufschwung_ost/bratkow.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Im Jahr 2023 besuche ich in Görlitz und Zgorzelec ein Stadtfest. Habe ein beschränltes Budget, kann mich trotzdem betrinken und essen.
Auf der polnischen Seite. Bestelle auf polnisch. Geilste Frau war die eine, die Pieroggi verkauft hat. Görlitz ist einer meiner Top-Favoriten im Osten. Wenn
gerade kein Stadtfest stattfindet, allerdings etwas zu ruhig. Die Bahnhofstoilette ist zwar kostenlos, aber eine Zumutung. Fast überall in Sachsen
(und Mecklenburg-Vorpommern) sind die öffentlichen Toiletten eine Katastrophe.

Nachdem ich Görlitz mehrmals besucht habe und auch abseits der touristischen Altstadt erkundet habe, behaupte ich, dass auch die nicht touristischen Gegenden sehr hübsch sind. Auch wenn es in manchen Wohngebieten weniger Geschäfte gibt, sind sie allesamt attraktiv.

### Küstrin Kiez + Kostrzyn nad Odrą (33)

### Frankfurt am Main (34)

Innenstadt, Alt-Sachsenhausen

### Halle an der Saale (35)

![Halle](){:data-src="/assets/images/aufschwung_ost/halle-suedpark.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Erfurt (36)

[Erfurt](https://youtu.be/yUOFC-L0uI4?feature=shared&origin=https://hammeley.info/ego/2023/05/23/letzte-reisen/) ist vielleicht die schönste Stadt in Deutschland. Der Stadtpark und die schöne, saubere (Alt)-Stadt. Anders als zum Beispiel auch das sehr schöne Quedlinburg oder viele andere kleinere Städte, z.B. in Brandenburg, ist Erfurt deutlich urbarner. Die Stadt hat ein paar tolle Sehenswürdigkeiten. Wenn ich mal meine Heimat verlassen muss, dann ist Erfurt absolut eine mögliche Alternative, also abgesehen von Jobmöglichkeiten und den vielen anderen Gründen, warum ich als Stadtmensch einer Metropole Probleme in einer Stadt wie Erfurt sehe, bei einem Umzug sehe.

![Erfurt](){:data-src="/assets/images/aufschwung_ost/erfurt.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Während meiner Obdachlosigkeit verbringe Nächte im Stadtpark, aber auch im "Boutique"-Opera-Hostel. Einmal im Einzelzimmer und einmal im 8-Bett-Zimmer mit zwei jungen Frauen und mir. Der Späti nebenan ist genauso gut sortiert wie die in Berlin. Die Öffnungszeiten sind natürlich nicht wie in Berlin. Hier macht nicht nur der Späti erst mittags auf, und schließt um 22:00 oder 23:00 Uhr. Mir scheint als würde die Stadt bis mittwochs eh im Ruhemodus sein.

Seit es das Deutschland-Ticket gibt, war ich schon mehrmals, in Erfurt, einfach um mich mir schönen Dingen zu umgeben.

### Wismar (37)

Ganz ok. Nichts besonderes. Paar nette Häuser (z. B. Alter Schwede) Etwas langweilig. Touristisch. Fü0e in einen der Teiche gehalten.

![Wismar](){:data-src="/assets/images/aufschwung_ost/wismar.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Sassnitz + Kreidefelsen (Rügen) (38)

Geht. Touristisch. Wasser ins Meer gehalten. An einem Kreidefelsen ins Meer gegangen und durch einen Wald gelaufen.

![Rügen](){:data-src="/assets/images/aufschwung_ost/ruegen.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Rostock (39)

Döner heißt hier Drehspieß. Komische Vögel gesehen. Dreckigste Bahnhofstoilette der Welt. Eher langweilig.

### Chorin (40)

Kloster Chorin besucht. Ort sehr klein ohne echten Ortskern. Sehr schöner Weg vom Bahnhof zum Kloster durch den Wald.

![Chorin](){:data-src="/assets/images/aufschwung_ost/chorin-kloster.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Zinnowitz (41)

Nicht weit vom Bahnhof zum Strand. Eigentlich Karlshagen geplant, aber wollte schneller zum Strand und ist in Zinnowitz näher vom Bahnhof.
Auch für Assis gut ausgestattet, z.B. Waschsalon.

![Zinnowitz](){:data-src="/assets/images/aufschwung_ost/zinnowitz.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Zittau (42)

Positiv überrascht. Die Stadt ist recht nett. Urlaub in der Toskana. Beim Bäcker Zwiebelkuchen und Federweisser. Drehe ein paar Bahnen im Stadtbad Zittau. Die alte Frau und der Mann im Rentenalter sind alle schneller und fitter als ich.

![Zittau](){:data-src="/assets/images/aufschwung_ost/zittau-rathaus.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Bautzen (43)

Namen der Stadt vergessen. War aber eine Stadt, die nach meinem Eindruck fast ausschließlich aus 

![Bautzen](){data-src="/assets/images/aufschwung_ost/bautzen.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Bad Freienwalde (44)

Geht so. Die Umgebung ist bestimmt interessanter. Esse ein Stück Torte und muss die Sahne extra bezahlen. Glaube es waren sogar 0,70 - 0,90 €.

![Bad Freienwalde](){:data-src="/assets/images/aufschwung_ost/bad-freienwalde.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Chemnitz (45)

Altstadt und Zentrum leider verhunzt. Riesengroße Ansammlung von Konsumtempeln. Es ist gerade Markt. Der sieht gut aus. Die Verkäufer geben sich Mühe und es sieht
alles sehr nett aus.

![Chemnitz](){:data-src="/assets/images/aufschwung_ost/chemnitz-rathaus.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Zwickau (46)

Hat meiner Meinung nach Potential. Hab die Stadt nur nachts besucht und bin morgens wieder weg.

![Zwickau](){:data-src="/assets/images/placeholder-image.png" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Dresden (47)

Nicht mein Favorit, aber in Ordnung. In Dresden Neustadt gibts einen Edeka und einen Waschsalon. 

![Dresden](){:data-src="/assets/images/aufschwung_ost/dresden.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Jena (48)

Städtisch in Ordnung. Nicht top. Gibt sogar ein, zwei Hipster Restaurants mit geilen Frauen. Das Beste, fast überall in der Stadt ist WLAN verfügbar. Verbringe die Zeit im Paradies. Schöner Blick auf die 'Berge'. Die paar Sehenswürdigkeiten holen nicht viel raus. Viele Straßen, zerstückeln das "Touristen"-Zentrum.

![Jena](){:data-src="/assets/images/aufschwung_ost/jena-paradies.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Quedlinburg (49)

Tolle Altstadt, teils bereits wieder renovierungsbedürftig. Stadt ist super touristisch. Überall Städteführungen und Touri-Gruppen. Sehr voll. Trotzdem ganz nett (wenn auch nicht so wie auf den Bildern).

![Quedlinburg](){:data-src="/assets/images/aufschwung_ost/quedlinburg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Eisenach (50)

Lohnt nur wenn man zur Wartburg möchte. Die Stadt selbst hätte zwar Potential, aber das nutzt sie nicht. Ich zahle hier glaube ich 1000€ für ein belegtes Brötchen und einen Kaffee. In der Bäckerei wird das Brötchen, das belegt ist sogar einzeln abgerechnet. Schlimmer gehts nur in den "Sternenbäck"-Läden, die es oft in Brandenburg, aber teilweise auch im Westen gibt. Hier zahlt man einen Aufpreis, wenn man den Kaffee oder das Brötchen im Laden essen möchte. Erinnert mich etwas an DDR. In anderen Läden, ich glaube es ist Frankfurt (Oder) zahlt man einen Aufpreis, wenn man außerhalb einen Kaffee trinken möchte. Bestes Deutschland.

![Eisenach](){:data-src="/assets/images/aufschwung_ost/eisenach.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Nürnberg (51)

Herbe Enttäuschung. Der Bahnhof ist in Ordnung, die Altstadt, selbst aus meiner berliner Sicht betrachtet ziemlich verdreckt und unattraktiv. Treffe auf Junkies
die sich gerade einen Schuss setzen. Die grüne Kloake, die durch die Stadt fließt stinkt, wie eigentlich große Teile der Stadt. Werde von irgendwas gebissen. 
Sieht aus wie ein Zeckenbiss, der aber nicht wie einer abheilt.

![Nürnberg](){:data-src="/assets/images/go_west/nuernberg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Hamburg (52)

Als erstes als ich aus dem Bahnhof raus bin, bin ich in Hamburg-Mitte. Heilige Scheiße. Soviel Obdachlose, Betrunkene, vollgekotzte und verwarloste Menschen gibts sonst nur in Amerika und in Frankfurt am Main. Die andere Seite vom Bahnhof, St. Georg, geht. Ich gehe durch die Mönckebergstraße und besuche auch St. Pauli, die Speicherstadt, die Hafencity, die Landungsbrücken, Reeperbahn, die Große Freiheit und die Herbertstraße. Hamburg hat eine tolle Bibliothek ganz in der Nähe des Hauptbahnhofs und das Bürgeramt ist auch top (Stichwort: Terminbuchung). Insgesamt sind die Hamburger recht freundlich. Öffentliche Toiletten kosten hier überall nut 0,50€ statt wie sonst 1,00€. Besser geht es nur in der Schweiz oder in manche südwestlichen Regionen, wo öffentliche Toiletten kostenlos sind, aber auch nicht immer sauber. MobyKlick bringt in Hamburg das Internet an viele Orte der Stadt. Eigentlich kann ich in jeder Straße, zumindest habe ich als Touri in der Innenstadt fast überall Internetempfang. In der Europa-Passage esse ich einen Döner-Kebap zu einem Eröffnungsangebot bei Ali für nur 4,50€ (September 2023). Der war sogar ganz gut.

![Hamburg](){:data-src="/assets/images/go_west/hamburg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Goslar (53)

Leider etwas im Zug liegen gelassen. Konnte Statt nicht anschauen. 

### Braunschweig (54)

Nicht sehr attraktiv. Die Stadt macht auf mich einen unheimlichen Eindruck. Alles ziemlich dunkel, auch wenn dort die Sonne scheint. Es ist die perfekte Kulisse für einen Zombie-Film. Es gibt ein paar chöne Tram-Züge, deren Innendecke statt weiß/grau/beige/vergilbt eine roteFarbe hat. Die Sitzbezüge haben ein schönes Muster, dass sich zu leichten Farbverläufen zusammenfügt. Ich kaufe bei New Yorker ein. Als Assi kaufe ich dort T-Shirts für 4,99€. Es ist der best-sortierteste New Yorker mit dem größten Angebot bisher.

### Senftenberg (55)

Eine ganz normale, nette, aber etwas langweilige Kleinstadt in der Lausitz, im Süden Brandenburgs. Im Sommer kann man hier sicherlich an die zahlreichen Seen zum Baden gehen.

### Heidelberg (56)

Eine Enttäuschung.

![Heidelberg](){:data-src="/assets/images/go_west/heidelberg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Mannheim (57)

Habe nicht viel gesehen. In Ordnung. Nichts besonderes. Bin nach M2 wegen Familienforschung. Das Haus, in dem mein Uropa eine Schlosserei hatte und wo mein Opa gelernt hat, existiert nicht mehr. Ich hatte die Hausnummer nicht mehr im Kopf. Aber ich glaube dort steht jetzt aus Haus in schwarz/wei0 mit Hipster-Mietern. In der Nähe eine Kirche mit netter Orgelmusik.

![Kirche Nahe M2 in Mannheim](){:data-src="/assets/images/go_west/mannheim.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Würzburg (58)

Von den Städten im Süden bisher die Beste. Weit attraktiver als z. B. Nürnberg. Gehört aber nicht zu meinen Favoriten. Am interessantesten waren hier eindeutig die Halsband-Sittiche, die zu hunderten, wenn nicht tausenden am Bahnhof leben und morgens ein lauten Vogelzwitschern von sich geben.

![Würzburg](){:data-src="/assets/images/go_west/wuerzburg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Basel (59)

Nicht sehr attraktiv, aber sauber. Die Schweizer sind netter, als man von das von den Schweizern vielleicht erwartet. Sowohl der Kaffee-Verkäufer in so einer Art "Späti" der mir ein Schoki zum Kaffee gibt, wie auch der Mann von der Stadtreinigung, der hier Zigarettenstummel aufsammelt und sich eine Weile mit mir unterhält bleiben im Gedächtnis. Die Schweiz ist teils sehr teuer. Man kan jedoch auch ab und an ein gutes Angebot finden. So kann eine Capri-Sonne an einem der Bahnhofsautomaten schonmal 4,00 Franken kosten (derzeit 4,40€). Andererseits habe ich beim "Denner" Supermarkt auch Clementinen für 2 Schweizer Franken das Kilo gekauft und in Neuhausen sogar ein sehr gutes halbes Hähnchen, verpackt, mit Flügel, Schenkel und Brust, aber ohne das Zeugs, dass man bei uns dazu bekommt, aber nicht mitisst, für 3,80 CHF. Man muss halt schauen. Kauft man ausschließlich Eigenmarken von z.B. Coop, die es nach meiner Erfahrung weit weniger gibt, als bei uns, kommt man ungefähr auf das gleiche Preisniveau. Ganz einfache Dinge wie z.B. ein Brötchen kosten hier mindestens 0,80 CHF, statt vielleicht 0,19€ im Supermarkt. Selbst nach den starken Preissteigerungen in Deutschland, kann man sich kaum vorstellen, wie teuer die Schweiz teilweise ist. Ein normaler Döner (Pide) z. B. kann zwischen 12CHF und 20 CHF kosten. Eine Döner-Box mind. 12 CHF Günstig im Vergleich hingegen ist billiges Essen bei McDonalds und Co. Am Centralbahnhof in Basel hole ich mir einen BigMac für unter 8,00 CHF (ca. 8,80€).

Das günstigste Frühstück, das etwa meinem berliner Frühstück entspricht kostete mich 4,10 CHF. Mit einem Kaffee (aus einem Automaten, ohne Barista und schnickschnack) statt einem Prego (ähnlich Almdudler) sicherlich 3,00 CHF mehr. 

![Günstiges Frühstück](){:data-src="/assets/images/go_west/guenstiges-fruehstueck.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Schaffhausen (60)

Ganz nette Kleinstadt am Rhein, östlich von Basel. Mit dem Deutschland-Ticket kann man von Weil am Rhein nach Basel, über Schaffhausen, Singen (Hohentwiel), Radolfzell, Konstanz (und Kreuzlingen, CH) fahren (RE, IRE und schweizer S-Bahn des RVL) ohne sich Gedanken machen zu müssen, ob man gerade in Deutschland oder in der Schweiz ist. Ich verweile ein wenig auf die zentralen Marktplatz, die man auch bei uns kennt, bis ich dann ca. 2,2km weiter am Rhein nach Neuhausen laufe.

Das Wasser des Rheins ist hier nahezu kristall klar. Ich kann das Grünzeug, Steine und Fische im Wasser sehr gut erkennen.

### Neuhausen (61)

Ein totaler Rhein-Fall. Hier ist nichts außer ein (ehemaliges) Fabrikgelände von Sig-Sauer (peng, peng) und der Rhein.
Nein, der Rhein-Fall, wenn auch nicht so spektalulär wir andere Wasserfälle ist schon ganz nett. Hier fahren die Touris mit einem Boot, teils gegen eine starke Strömung auf einen schmalen, aber hohen Felsen, der als Aussichtsplattform dient.

![Der Rhein-Fall in Neuhausen, Schweiz](){:data-src="/assets/images/go_west/rheinfall.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Nach meinem Abstecher in das Land meiner Vorfahren entscheide ich mich dafür, dieses Jahr noch einmal in die Schweiz, nämlich nach Zürich und insbesondere nach [Luzern [video]](https://piped.video/watch?v=7t2bTESyVeY) zu fahren. Vielleicht stelle ich dort auf einfach einen Antrag auf Asyl und bleibe dort.

### Konstanz (62)

Eine nette Stadt am Bodensee. Hat mir gefallen. Natürlich sehr touristisch. In Konstanz hatte mein Opa väterlicherseits eine Art Disko in den 1960er Jahren. Kenne den Namen aber nicht mehr. Es war aber wohl eine gut laufende Einrichtung zu der Zeit, in der auch mein Vater mitgearbeitet hat. In Konstanz war mein Vater dann u.a. auch in der Spielbank beschäftigt, bevor er 1977/70 nach Berlin zog.

### Freiburg (63)

Hat mir auch gefallen. Reiht sich in die top-Städte ein, aber nicht so wie top Erfurt. 

### Karlsruhe (64)

Eher häßlich. U-Bahnhöfe sind ganz interessant. Ich möchte eigentlich zurück nach Berlin. Soll über Frankfurt fahren. Verpasse mal wieder einen Anschluss, was bedeuten würde, dass ich irgendwo nachts 4-5 Stunden warten müsste und nicht mehr morgens vor 12 Uhr zum Post abholen in Berlin ankommen würde. Ich entscheide mich einen Schlenker ins Saarland und die Rheingegend zu machen. Homburg, Trier, Koblenz.

### Homburg (65)

Naja.

### Trier (66)

Netter als erwartet. Am Bahnhof gibt es einen Bäcker. Hier gibt es den besten Leberkäs-Pizza weit und breit. Sogar weit besser als den ich in Nürnberg bekam. Ich laufe sowohl durch die Judengasse, als auch über den Platz der Menschenwürde. lol.

![Trier](){:data-src="/assets/images/go_west/trier.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Koblenz (67)


### Kassel (68)

Nach meiner Süd-Tour in die Schweiz und dem West-Schlenker, mein zweiter Versuch nach Berlin zu kommen. Ich verpasse in Hannover erneut einen Anschluss. In Kassel habe ich eigentlich nur die Nacht auf dem Bahnhof verbracht und bin nachts durch die Stadt gelaufen. Ich entscheide mich spontan, morgens in den Norden, statt zurück nach Berlin zu fahren.

### Flensburg (69)

Nette Stadt im Norden. Viele Häuser, die an den Norden erinnern. Sofort am Bahnhof kommt mir eine angenehme nordische Meeresbrise entgegen. Abseits der Breiten Straße (Einkauf, -Touri), findet man viele nette Gassen und Hinterhöfe, die noch in Besitz der Einheimischen sind und nicht durchgehipstert wurden. Also statt Aperol Spritz und Fritten für 9€, sind die Läden hier mit Werkstätten, Lagern, Fahrradreparatur usw. belegt. Die Stadt hat sogar Hügel, die ganz schön steil hinaufgehen. Auf dem Rückweg komme ich bei der Flensburger Brauerei vorbei. Die Stadt ist der Küstenfavorit und "schöner" als Wismar, Stralsund, Rostock und wahrscheinlich auch Schwerin.

![Flensburg](){:data-src="/assets/images/go_west/flensburg-1.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Durch die Nähe zu Dänemark sieht man hier auch einige hübsche Blondinen.

### Wolfsburg (70)

Auf der Rückfahrt von Flensburg mache ich einen Halt in Wolfsburg. Hier steht direkt am Bahnhof das "Phaeno", ein Brutalismus 2.0 Bau. Der ist sogar ganz attraktiv. Es bietet Platz für Veranstaltungen und Austellungsfläche und ist vergleichbar mit dem Futurium in Berlin. Dreht man sich 180° (bzw. 360° nach Baerbockscher-Rechnung), sieht man teile des VW-Werks. Sehe sonst nicht viel von der Stadt, könnte mir aber vorstellen, dass sie ganz nett ist. 

![Phaeno in Wolfsburg](){:data-src="/assets/images/go_west/wolfsburg-1.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Bamberg (71)

Von den wenigen Städten, die ich in Bayern besucht habe, ist Bamberg mit Abstand die attraktivste. Sie reicht aber bei weitem nicht an Erfurt, Görlitz oder Leipzig heran. Ich besuche die Altstadt, laufe auch ein wenig Abseits der Touri-Wege durch Wohngebiete. Alles ganz in Ordnung, aber auch nicht aufregend. Zuletzt laufe ich so einen Berg hoch. Da ist ein Schloss oder öhnliches. Auf jeden Fall war mein Opa da mal. Das Gebäude wurde damals als Kaserne oder Lazarett genutzt.

![Bamberg](){:data-src="/assets/images/go_west/bamberg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Salzwedel

Reise wegen schlechter Bahnverbindung verschoben.

In der Bahn während meiner Nord-Tour unterhalte ich mich. Es fallen die Ortsnamen Salzwedel und Uelzen. In Uelzen hatte ich auf dem Hinweg auch einen kurzen Aufenthalt.
Ich schaue im Internet und entscheide die beiden Städte zu besuchen.

### Uelzen

Reise wegen schlechter Bahnverbindung verschoben.

Die Entscheidung für Uelzen habe ich auf der Hinfahrt zu meiner Nord-Tour gemacht. Ich hatte einen Aufenthalt und habe dort den Hundertwasser-Bahnhof gesehen. Zusammen mit Salzwedel im Internet recherhiert und bin dann noch einmal hingefahren.

### Salzburg (72)

![Salzburg](){:data-src="/assets/images/hammeleyla/hammeleyla-3.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Strasbourg (73)

![Strasbourg](){:data-src="/assets/images/hammeleyla/hammeleyla-4.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Luxembourg (74)

![Luxembourg](){:data-src="/assets/images/hammeleyla/hammeleyla-5.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Bremen (75)

![Bremen](){:data-src="/assets/images/placeholder-image.png" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

### Zossen (78)

Ständige Streckensperrungen und Fahrplanänderungen brachten mich nach Zossen. Die beste Kleinstadt im südlichen Speckgürtel Berlins.