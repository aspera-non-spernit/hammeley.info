---
title: Bayern
layout: default
tags: "Bundeswehr, Wehrpflicht, bayern, Penzing, LTG, 61, Allgäu"
---
Nachdem ich viele Stunden im Zug verbracht habe, bin ich im Allgäu angekommen (für den Stadtmensch ist es das Allgäu. Die Gebietsgrenze liegt jedoch ein paar km entfernt). Ich werde nun bis zum Ende der Wehrpflicht in der F-Staffel des Lufttransportgeschwaders 61 in Penzing (Landsberg am Lech) verbringen.

Ich komme nach dem üblichen Tagesdienst in Penzing an. Es ist keiner mehr da. Der Unteroffizier vom Dienst zeigt mir meine Stube und sagt mir, ich solle mich morgen früh beim Chef melden.

Ich erhalte eine recht große Stube für mich allein. Es sind zwar zwei Einzelbetten drin, aber es ist nur eins durch mich belegt.

Die Alpen sind zwar noch ca. 60 km entfernt, trotzdem kann ich sie vom Fenster meines neuen Arbeitsplatzes aus über die [Landebahn](https://www.youtube.com/watch?v=z-NbbxZjJ5I) sehr gut sehen.

**Hinweis:** Mein Arbeitsplatz befand sich im Gebäude der Flugsicherung bei 15, 17 und 23 Sekunden des Videos.

**Rückblick:** Am Ende der verkürzten Grundausbildung konnten sich die Wehrpflichtigen, sofern möglich, die Stammeinheit, in der sie die restliche Zeit der Wehrpflicht verbringen wollen, aussuchen. Dafür hingen Listen mit Dienststellen und offenen Posten aus. Viele haben nur den Wunsch nach heimatnahem Einsatz geäußert. Die sind dann meistens in Laage oder Holzdorf bei FlaRak oder der Luftwaffensicherungsstaffel gelandet.

Ich habe mir eine Stelle als Flugdatenbearbeitungsgehilfe ausgesucht. Die einzig angebotene Stelle war in Penzing.
