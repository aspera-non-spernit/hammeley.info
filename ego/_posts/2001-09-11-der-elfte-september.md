---
title: Der 11. September
layout: default
tags: "Terror, 9/11, NYC, New York, Flugzeug"
---
Heute ist der 182. Tag auf meiner neuen Dienststelle. Einige Zeit, nachdem ich meine sechsstündige Schicht beginne, kommt ein ziviler Mitarbeiter des Geoinformationsdienstes aus dem Wetterdienstbüro nebenan bei uns vorbei. Er meinte, wir sollten einmal den Fernseher einschalten, es sei ein Flugzeug in das World-Trade-Center in New York City geflogen.

Einige Zeit später sehen wir live im Fernseher eine [zweite Boeing 767 [ext yt]](https://www.youtube.com/watch?v=LsGcIkevyHM) in den zweiten Turm der Zwillingstürme fliegen. Von einem Unfall gehen nun auch die Zweifler nicht mehr aus.

Im Laufe des Tages fliegt ein drittes Flugzeug in das Pentagon, ein viertes soll von Passagieren in Pennsylvania zum Absturz gebracht worden sein, bevor es in das Weiße Haus fliegen konnte.

Zahlreiche Menschen springen aus den 400 m hohen Bürotürmen, bis die beiden Türme schließlich nacheinander und das Gebäude 7 [einstürzen [ext piped video]](https://piped.video/watch?v=UdfqEYITTm8&t=327) und Tausende Menschen unter sich begraben.

**Weiteres Video:**

- [https://youtu.be/UdfqEYITTm8?t=327](https://youtu.be/UdfqEYITTm8?t=327)
