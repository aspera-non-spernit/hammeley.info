---
title: Ärztliche Untersuchung
layout: default
tags: "Bundeswehr, Wehrpflicht"
---
Ich bin heute gemustert worden. Ergebnis: Wehrdienstfähig mit Einschränkungen (T2).

Ausgeschlossen sind Verwendungen:

- im Pionierdienst,
- in der Gebirgstruppe,
- im Fallschirmsprungdienst,
- im Flugsicherungsdienst,
- im Mil. Kraftfahrdienst,
- im Protokollarischen Dienst und
- im Sanitätsdienst.
