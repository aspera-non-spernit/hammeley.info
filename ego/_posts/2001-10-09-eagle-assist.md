---
title: Eagle Assist
layout: default
tags: "NATO, Bundeswehr, Artikel 5, Amerika"
---
Ich arbeite nun unter Ausrufung des Artikel V des Nordatlantikvertrages:

> "Die Parteien vereinbaren, daß ein bewaffneter Angriff gegen eine oder mehrere von ihnen in Europa oder Nordamerika als ein Angriff gegen sie alle angesehen werden wird; sie vereinbaren daher, daß im Falle eines solchen bewaffneten Angriffs jede von ihnen in Ausübung des in Artikel 51 der Satzung der Vereinten Nationen anerkannten Rechts der individuellen oder kollektiven Selbstverteidigung der Partei oder den Parteien, die angegriffen werden, Beistand leistet, indem jede von ihnen unverzüglich für sich und im Zusammenwirken mit den anderen Parteien die Maßnahmen, einschließlich der Anwendung von Waffengewalt, trifft, die sie für erforderlich erachtet, um die Sicherheit des nordatlantischen Gebiets wiederherzustellen und zu erhalten.
>
> Vor jedem bewaffneten Angriff und allen daraufhin getroffenen Gegenmaßnahmen ist unverzüglich dem Sicherheitsrat Mitteilung zu machen. Die Maßnahmen sind einzustellen, sobald der Sicherheitsrat diejenigen Schritte unternommen hat, die notwendig sind, um den internationalen Frieden und die internationale Sicherheit wiederherzustellen und zu erhalten."

Der NATO E3-A Verband verlegt einige seiner Flugzeuge in die Vereinigten Staaten, damit diese ihre ihre AWACS für die Luftüberwachung und den Krieg in Afghanistan nutzen können.
