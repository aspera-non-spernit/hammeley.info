---
title: Mauerfall
layout: default
tags: "DDR, BRD, Mauer, Fall, Wiedervereinigung, Deutschland"
---
Nach monatelangen Demonstrationen und zahlreichen Flüchtlingen, die u.a. auch in unserer Sporthalle untergebracht wurden, können die Bürger der DDR nun ohne weitere Probleme ausreisen.

<hmmly-player id="mauerfall" data-yt-vid="kTt4678cmTk"></hmmly-player>

**Rückblick:** Während der Demonstrationen im Osten habe ich mit Bekannten auch and er Mauer rumgehämmert und sogar ein paar Stücke mitnehmen können.

**Ausblick:** Mit Bekannten werden wir in den nächsten Monaten und Jahren häufig nach Ost-Berlin und ins berliner Umland fahren. Dort ist alles ziemlich kaputt, es riecht überall nach Kohle. Jahre später sind ehemalige Kasernen der Russen Orte zum entdecken. Die kommenden Wendejahre sind wahrscheinlich die besten Jahre, die man in einem Leben erleben kann.
