# Schöne Töne (8.2.23)

- Prefuse 73 - Pentagram
- (Max Richter - Untitled (Figures))
- Abul Mogard - Over My Head
- Mario Batkovic - Somnium
- This Will Destroy - You Mother Opiate
- (New Puritans Organ Eternal)
- (Joseph Shabason - Forest Run)
- Modeselektor - Don't Panic
- Felix Mendelssohn - String Quartet No. 6 in F Minor, Op. 80: I. Allegro vivace assai
- Ian William Craig - Innermost
- (Gyda Valtysdottir & Skúli Sverrisson - Unfold)
- Bill Callahan - As I Wander
- Miles Davis - In a Silent Way (New Mix)
- Daniel Aged - Bh
- Jonny Greenwood - They Were Mine
- Bohren & Der Club of Gore - Verloren (Alles)
- (Skee Mask Session - Add)
- Jordi Savall - Les Pleurs (Mr. de Sainte Colombe, Version Viole Seule de Jordi Savall)
- Sigur Rós - Avalon
- John Tavener - Eternal Memory For Cello And String Orchestra: Eternal Memory For Cello And String Orchestra
- Tönu Kaljuste - Neenia
- Alexander Scriabin - 3 Pieces for Piano, Op. 2: I. Etude in C-Sharp Minor
- Alexander Scriabin - 12 Etudes for Piano, Op. 8, No. 12 in D-Sharp Minor
- Steve Hauschildt - Cloudloss
- The Notwist - Red Room

# Schöne Töne (15.2.23)

- [Kidkanevil & Daisuke Tanabe - Nanotrees (Out In the Woods)](https://www.youtube.com/watch?v=-LBeO0hZusU)
- [Floex & Tom Hodge - Wednesday (Is the New Friday) [feat. Prague Radio Symphony Orchestra]](https://www.youtube.com/watch?v=FOP8bkliwUM)
- [Mario Batkovic - Ineunte Finis](https://www.youtube.com/watch?v=7Y9wUGv8VkQ)
- [Mario Batkovic - Quatere](https://www.youtube.com/watch?v=ggGJrTpaPnE)
- [Anton Bruckner - Scherzo - Sinfonie Nummer 9](https://www.youtube.com/watch?v=6wQP5TRnt4U)
- [Tiffany Poon - Kinderszenen, Op. 15: No. 10, Fast zu ernst](https://www.youtube.com/watch?v=0QlvTN2BL7c)
- [Sven Helbig - Zorn](https://www.youtube.com/watch?v=k5hwj1aCMkE)
- [Clemens Christian Poetzsch & Reentko Dirks - Reflexion](https://www.youtube.com/watch?v=-dnPyvvvL0k)
- [Fuensanta - Noche](https://www.youtube.com/watch?v=rB-brZKEL10)
- [James Ellis Ford - Pillow Village](https://www.youtube.com/watch?v=kMadw0SZhl4)
- [Sebastian Studnitzky & Odesa Symphonic Orchestra - Margolina](https://www.youtube.com/watch?v=Txfz7Rv4044)
- [Sebastian Studnitzky & Odesa Symphonic Orchestra - Organic](https://www.youtube.com/watch?v=OnsaBYJT23s)
- [Sebastian Studnitzky & Odesa Symphonic Orchestra - Memento](https://www.youtube.com/watch?v=k0MXV5W0jew)
- [Oneohtrix Point Never](https://www.youtube.com/watch?v=FuHWa-TRZTs&list=PLS4jAfE9d3aIQYs8V_AuO96t57vZBoALH)

# Elektronische Entspannungsmusik

- [Carbon Based Lifeforms - Best of (1996 - heute)](https://www.youtube.com/watch?v=XWNsWZ5VtWc)
- [Space Time Continuum - Fluresence E.P. (1993/2022RP)](https://www.youtube.com/watch?v=3hvTrNqcJHM)

# Klassik

- [Verdi - Nabucco - Gefangenenchor](https://www.youtube.com/watch?v=XttF0vg0MGo)
- [Chopin - Fantaisie-Impromptu (Op. 66)](https://www.youtube.com/watch?v=Gus4dnQuiGk)
- [Piazolla - Libertango](https://www.youtube.com/watch?v=pNJTJrnpbjE)
- [Debussy - Claire de Lune](https://www.youtube.com/watch?v=WNcsUNKlAKw)
- [Debussy - Prélude à l’après-midi d’un faune](https://www.youtube.com/watch?v=Y9iDOt2WbjY)
- [Rachmaninoff - 2ndPiano Concert](https://www.youtube.com/watch?v=rEGOihjqO9w)
- [Liszt - Serenade](https://www.youtube.com/watch?v=lv5xPlm6etI)
- [Prokofiev - Dance of the Knights](https://www.youtube.com/watch?v=Z_hOR50u7ek)