---
title: Nedefiniran
layout: default
tags: kokosy
---

<hmmly-player id="kokosy" data-playlist-id="kokosy"></hmmly-player>

> Zadnjič mi je ena teta rekla, da se v življenju ni prav zdravo prepogosto definirat. Da definicije ubijajo človeka in v resnici sam spoznavam iz dneva v dan pravo resničnost tega reka, nisem nek konzervativec, ki zapira svoje meje, ob nedeljah hodi k maši, v pondelk sežiga geje, niti ne zagret levičar, ki zahteva le pravice, druže tito mu je bog in slovenija so vice. Nisem roker, ne metalec, ne hiphoper, jazz plesalec, pišem pač glede na čase, tisto kar mi najbolj paše, ljubim lepe in neumne, ljubim grde in razumne in sovražim, a ne sovražim po sistemu, včasih levo, včasih desno, včasih v hecu, včasih resno, hodim po neznani poti, spremljajo me idioti, včasih lažem folku v faco, drugič sem pristaš resnice, v sredo plavam med delfini, v petek kličejo me ptice. 

> Danes hočem nekaj novega, 
danes hočem še živet.
Danes rabim nekaj močnega,
danes nočem še umret.

> Ker dandanes hoče met že vsak drug idiot mnenje o vsem, kar se dogaja na tem prizadetem svetu. Sej ne rabiš rdeče niti in konkretnih argumentov, da ni klele čez vse kar je po forumih na internetu, vsi se derejo čez druge in utopično slavijo, eni čase stare juge, drugi pač demokracijo. Večina sploh ne ve, zakaj sem vedno sprovuciran, eno stran moraš pač izbrat, brez mnenja si nedefiniran. Nedefiniran, neseciran, nezminiran, brez okuženih možganov in v solzo destiliran, brez predsodka sprogramiran in na novo skonstruiran, v človeka ki pač noče več ostati zamaskiran, ker ukleščenost, ki nas zatira, nas konstantno bode, že Demokrit in Dekart sta jokala, da ni svobode, a določena svoboda že obstaja in ne redko, vsaj do neke mere sem resnično prost pred lastno kletko. 

> Danes hočem nekaj novega, 
danes hočem še živet.
Skupaj z vsemi ki razumejo, 
v neznano poletet.

![Najbolše iz Slovenije in v Sloveniji](){:data-src="/assets/images/nl/IMG_20240519_001605.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }
