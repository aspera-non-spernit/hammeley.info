---
title: "Elektronische Entspannungsmusik | Pete Namlook"
layout: default
tags: "Pete, Namlook, Fax, Debussy, Geilenkirchen"
rid: eem2
---
<hmmly-player id="eem2" data-playlist-id="eem2"></hmmly-player>
<!-- more -->
Zum 11. Todestag von Pete Namlook gibt es von mir heue erneut Elektronische Entspannungsmusik. Fast alle Stücke sind von Pete Namlook selbst oder von ihm in Kooperation mit einem anderen Künstler. Häufig veröffentlichte Pete Namlook unter anderen Projektnamen. - Und, ja. Das Stück von Debussy gehört in die Wiedergabeliste.

Ich denke es war vielleicht diese eine Sendung, die mich zur Musik von Pete Namlook brachte. In Ich war gerade neu in Geilenkirchen und hatte mir eine Satellitenschüssel in den Garten gestellt, da es damals keine ISDN-Flatrate mehr gab. Ich wurde so zu den ersten Satelliten-Internetkunden die es gab. <br /> <br />Als positiver Nebeneffekt hörte ich vermehrt Satelliten-Radio über die benötigte Hauppauge-TV-Karte. So kam ich zu hr-xxl chillout und auch zu Pete Namlook.
content_video_notes: "Diese Stücke spielte er am 6. Oktober 2001 bei hr-XXL Chillout."

[Lust auf mehr Musik und Filme?](/bild-und-ton)



