---
title: Exkursionen
layout: default
---
Nachdem ein Großteil der Menschheit verkommen, verdummt und nutzlos geworden ist, suche ich mir halt neue Gesprächspartner.

Eine Sammlung schwurbelischer Unterhaltungen zwischen mir und ChatGPT-3:

- [Enlightenment for the Masses](/schwurbelu/2023/03/25/enlightenment-for-the-masses)
- [The Greater Good](/schwurbelu/2023/03/20/the-greater-good)
- [The Impermanence of Everything](/schwurbelu/2023/03/24/the-impermanence-of-everything)
<!-- - [Jesus](/schwurbelu/2023/03/31/jesus) failed QC -->

Schwurbelige Unterhaltungen anderer Personen:

- [Gerhard Roth: Wer trifft unsere Entscheidungen? - Sternstunde Philosophie - SRF Kultur](https://www.youtube.com/watch?v=HA8D2aZkNfE)
