---
title: The Problem with Skepticism | Philosophy
layout: default
faculty: philosophy
---
# Philosophy

## Epistemology 1.1

### The Nature of Knowledge 1.1.1

### The Problem with Skepticism 1.1.1.3

**I want to explore another topic: Skepticism. How is it related to epistemology. Who was the first skeptic and what's the problem with it?**

Skepticism is closely related to epistemology because it is a philosophical position that questions the possibility of obtaining certain knowledge or claims about the world. Skeptics argue that many beliefs or claims that people take for granted are not well-supported by evidence or reasoning and should be subject to doubt or scrutiny.

One of the earliest skeptics in the Western tradition is Pyrrho of Elis (c. 360–c. 270 BCE). Pyrrho argued that people should withhold judgment about the truth or falsehood of any claim because it is impossible to know anything with certainty. He claimed that every argument for a particular view could be countered by an equally compelling counter-argument, leading to a state of suspension of judgment or "epoché."

There are different forms of skepticism, including **local skepticism**, which targets specific areas of knowledge, and **global skepticism**, which doubts the possibility of knowledge in general. One problem with global skepticism is that it seems to be self-defeating: if we cannot know anything, then we cannot know that we cannot know anything, and therefore the claim itself becomes doubtful.

However, some philosophers have argued that certain forms of skepticism can be useful in challenging dogmatic or unfounded beliefs, and in promoting critical thinking and the need for evidence and justification.

**While the methods of gaining knowledge like "revelation", "intuition", or the "mental state" are likely not to be very skeptical, it seems that skepticism is an integral part of "evidence and reason". Are skeptics just criticising methods that do not include some forms of risk mitigation or is there something I am completely missing?**

Skepticism can be seen as a natural companion to evidence and reason, as it involves critically examining claims and arguments to ensure they are well-supported and justified. Skeptics question the reliability of certain methods of gaining knowledge, such as intuition or revelation, precisely because they do not involve the kind of rigorous testing and verification that evidence and reason demand.

However, it's worth noting that skepticism is not simply about being critical of certain methods; it's also about being open to new evidence and arguments that may challenge one's existing beliefs. In this sense, skepticism is a positive force that helps to ensure that our beliefs are based on sound reasoning and evidence rather than dogma or tradition.

The first skeptic in the philosophical sense is generally considered to be Pyrrho of Elis, a Greek philosopher who lived in the 4th century BCE. Pyrrho believed that knowledge was impossible and that the best we could do was to suspend judgment and live in a state of perpetual doubt. While this extreme form of skepticism is no longer as popular today, skepticism remains an important part of epistemology and philosophy more broadly.

**Why is dogma and tradition seen contrary to knowledge by skeptics? It could be argued that tradition holds some truth and that it can be seen as a source of knowledge. Although it seems that this source isn't neutral or objective, I'd say tradition is knowledge that is passed down through generations.**

Skeptics view dogma and tradition as contrary to knowledge because they often rely on blind acceptance of beliefs or practices without questioning their validity or seeking evidence to support them. Tradition may hold some truth, but it can also perpetuate false or harmful beliefs and practices, as well as limit critical thinking and open inquiry. Additionally, tradition is not a neutral or objective source of knowledge, as it is often influenced by social, cultural, and historical factors that shape beliefs and practices. Skeptics advocate for a more critical approach to knowledge, in which beliefs and practices are subject to scrutiny and evidence-based evaluation rather than blind acceptance based on tradition or authority.

**Did skeptics appear before or after the concept of "evidence and reason" has been developed?**

Skepticism as a philosophical movement has existed for centuries, with roots dating back to ancient Greek philosophy. The idea of using evidence and reason to acquire knowledge is also an ancient concept, and has been developed in various philosophical traditions over time.

In ancient Greece, philosophers such as Plato and Aristotle emphasized the importance of reason in acquiring knowledge, while skeptics such as Pyrrho and Sextus Empiricus were more skeptical of the reliability of human reasoning and the ability to arrive at certain knowledge. Later, during the Enlightenment period in Europe, thinkers such as Francis Bacon and René Descartes emphasized the importance of empirical evidence and rationalism, respectively, in acquiring knowledge.

So while skeptics and the concept of evidence and reason have coexisted for a long time, it's difficult to say which came first or whether one influenced the other directly. Rather, they have developed in parallel and influenced each other over time.

**Please tell me more about the problems of Skepticism. Maybe use an example to help me understand the issues with it.**

One of the main problems with skepticism is that it can be difficult to draw the line between healthy skepticism and excessive doubt or even **denialism**. While skepticism can be a useful tool for questioning claims and ideas, it can also become a hindrance to progress and understanding when taken to an extreme.

For example, consider the scientific consensus on climate change. While it is perfectly reasonable to be skeptical of any scientific claim, there comes a point where the overwhelming weight of evidence supports a certain conclusion. In the case of climate change, the evidence overwhelmingly supports the conclusion that human activity is causing the planet to warm at an unprecedented rate, with potentially catastrophic consequences.

However, some individuals and groups have taken skepticism to an extreme and deny the reality of climate change altogether, despite the overwhelming scientific consensus. In this case, skepticism has become a barrier to understanding and action, preventing the implementation of necessary policies to mitigate the effects of climate change.

Another problem with skepticism is that it can lead to a kind of **epistemic paralysis**, where doubt is cast on so many beliefs that it becomes difficult to know what to believe. This can be particularly problematic when it comes to practical decision-making, where we need to rely on certain beliefs in order to take action.

Overall, skepticism can be a useful tool for questioning claims and ideas, but it is important to strike a balance between healthy skepticism and excessive doubt or denialism.

**If epistemologists belief that evidence and reason can lead to true and justified beliefs, why is there a risk of epistemic paralysis? If they'd apply a systemic, neutral and objective approach to gaining knowledge, exxessive doubt or denialism should not be of any concern. Especially, since epistemologists have tools at hand to mitigate all those risks, including several biases.**

The risk of epistemic paralysis refers to the possibility of becoming overly skeptical or doubtful to the point of being unable to form any beliefs or take any action. This can occur when someone holds such a high standard of evidence and reasoning that they become excessively cautious in forming any beliefs or taking any action.

While it is important to maintain a critical and skeptical approach to knowledge, the risk of epistemic paralysis arises when this skepticism becomes so extreme that it prevents any positive action. For example, if someone is overly skeptical about the effectiveness of vaccines and demands a very high level of evidence before accepting their efficacy, they may end up not getting vaccinated at all, despite overwhelming evidence supporting their safety and efficacy. This can lead to harmful consequences, both for the individual and the community they are a part of.

Epistemologists acknowledge the importance of evidence and reason in forming true and justified beliefs, but they also recognize the need for practicality and action in everyday life. They advocate for a balanced approach that takes into account the available evidence and reasoning, as well as the practical implications of beliefs and actions. They also recognize the importance of being aware of cognitive biases and other factors that may influence one's thinking and decision-making, and they offer tools and strategies to mitigate these biases. Ultimately, the goal is to strike a balance between skepticism and practicality, while remaining open to revising one's beliefs and actions based on new evidence and reasoning.

**So the denial of climate change you mentioned is not a form of epistemic paralysis, due to the fact that deniers of climate change to not hold high standards of evidence. Their source of skepticism must be of a different cause.**

Correct, the denial of climate change is not necessarily an example of epistemic paralysis, as it is not driven by an excessive demand for evidence or a complete rejection of all forms of knowledge. Skepticism regarding climate change may stem from a variety of factors, including political ideology, economic interests, or a lack of understanding of scientific methods and evidence.

In some cases, individuals may selectively reject evidence that conflicts with their existing beliefs, which can be seen as a form of confirmation bias. However, this is different from epistemic paralysis, which involves a complete inability or unwillingness to accept any form of knowledge as true or justified.

**The picture of a reputable scientists comes to my mind, who has discovered something important. However, after decades there's new evidence that suggest some new justified and true beliefs. If this scientists holds onto his outdated believes, because he's skeptical of the new beliefs. Is that a form of epistemic paralysis?**

Yes, in a way, that could be seen as a form of epistemic paralysis. If the scientist is not willing to update his or her beliefs in the face of new evidence, despite the evidence being well-supported, that would be a form of resistance to change. This could be due to several reasons, including emotional attachment to the old beliefs, fear of losing credibility or status, or simply being too invested in a particular perspective.

Epistemic paralysis can manifest in different ways, and it is not limited to being skeptical or doubtful. It can also arise from being too confident or closed-minded, which can prevent a person from considering new evidence or alternative perspectives. The key is to maintain a balance between healthy skepticism and openness to new ideas, evidence, and arguments.

**Write an essay of approximately 700 words, that covers all important aspects of skepticism in the field of philosophy. Mark all citations and unchanged quotes you are using by putting them into double quotes. The text shall not contain more than 30% of quotes. Provide a list of references.**

Introduction:
Skepticism is a philosophical position that calls into question the possibility of knowledge or certainty in certain areas. This essay aims to explore the essential aspects of skepticism in philosophy.

Types of Skepticism:
There are various types of skepticism in philosophy, including global skepticism, local skepticism, and methodological skepticism.

Global Skepticism:
Global skepticism is the position that knowledge or certainty is impossible in all areas of inquiry. Global skeptics argue that all beliefs are subject to doubt, including beliefs about the external world, other minds, and even one's own existence. For example, the global skeptic may argue that we cannot know whether the external world exists or whether we are just brains in vats being manipulated by an evil scientist.

Local Skepticism:
Local skepticism is the position that knowledge or certainty is impossible in certain areas of inquiry. Local skeptics argue that specific types of beliefs are subject to doubt, such as beliefs about the reliability of sense perception or the existence of God. For example, the local skeptic may argue that we cannot know whether our sense perceptions are reliable or whether God exists.

Methodological Skepticism:
Methodological skepticism is the position that knowledge or certainty is possible, but only through a particular method of inquiry. Methodological skeptics argue that certain methods of inquiry, such as empirical observation or logical reasoning, can provide reliable knowledge. However, other methods of inquiry, such as intuition or revelation, cannot provide reliable knowledge. For example, the methodological skeptic may argue that we can only know about the natural world through empirical observation and experimentation, and not through religious or mystical experiences.

Responses to Skepticism:
There are various responses to skepticism in philosophy, including foundationalism, coherentism, and contextualism.

Foundationalism:
Foundationalism is the position that there are certain basic beliefs that are self-evident or indubitable, and that other beliefs are justified by their relation to these basic beliefs. Foundationalists argue that certain beliefs, such as beliefs about the external world or other minds, are so fundamental that they do not require further justification. For example, the foundationalist may argue that the belief that one exists is a basic belief that is self-evident and does not require further justification.

Coherentism:
Coherentism is the position that beliefs are justified by their coherence with other beliefs in a system of beliefs. Coherentists argue that there are no basic or self-evident beliefs, and that all beliefs are justified by their coherence with other beliefs. For example, the coherentist may argue that the belief that the external world exists is justified by its coherence with other beliefs, such as the belief in the reliability of sense perception and the belief in the laws of nature.

Contextualism:
Contextualism is the position that the standards for knowledge or certainty vary depending on the context or situation. Contextualists argue that what counts as knowledge or certainty in one context may not count as knowledge or certainty in another context. For example, the contextualist may argue that the standards for knowledge or certainty in science are different from the standards for knowledge or certainty in everyday life.

Conclusion:
In conclusion, skepticism is a philosophical position that calls into question the possibility of knowledge or certainty in certain areas. There are various types of skepticism, including global skepticism, local skepticism, and methodological skepticism. There are also various responses to skepticism, including foundationalism, coherentism, and contextualism. Understanding skepticism is essential for understanding the nature and limits of human knowledge and the ways in which humans interact with the world.

References:

- Cohen, Stewart. "Contextualism and Skepticism." In The Oxford Handbook of Skepticism, edited by John Greco, 297-316. Oxford: Oxford University Press, 2008.
- Fumerton, Richard. "Skepticism." In The Stanford Encyclopedia of Philosophy, edited by Edward N. Zalta, Spring 2021 edition. https://plato.stanford.edu/archives/spr2021/entries/skepticism/.
- Hume, David. An Enquiry Concerning Human Understanding. Edited by Peter Millican. Oxford: Oxford University Press, 2007.
- Stroud, Barry. Understanding Human Knowledge. Oxford: Oxford University Press, 2000.

**You weren't able to finish to provide the references of the essay. Please finish by printing the references only.**

<references printed, attached to the broken list of references>
