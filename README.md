# hammeley.info

Just a Website hosted on Gitlab Pages.

Technologies in use:

- [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Jekyll SSG](https://jekyllrb.com/)
- [SASS](https://sass-lang.com/)
- [Bootstrap](https://getbootstrap.com/)
- [JQuery](https://jquery.com/)
- [hmmly.js](https://gitlab.com/aspera-non-spernit/hammeley.info/-/blob/dev/scripts/hmmly.js?ref_type=heads)

**WIP**

All features are implemented and work somewhat, but are likely to change.

## Features

- [x] Deploys [media.json](https://hammeley.info/data/media.json) to the live enrivonment.
- [ ] Simple jekyll supported layouts to handle different types of content.
- [ ] Separation from model, views and controller in a client-centric environment.

### Embedded videos

- [x] Super-quick on-demand-loading of embedded videos on a single page. This is extremly useful on an overview or index page.

### [Damerau-Levenshtein supported 404 error page](https://hammeley.info/404)

- [x] The 404 eorror page lists a number pages that are similar by url/path/name of the requested (non existent) page. 
- [ ] Optional redirect to best match

### One page search

- [x] Quick (client-side) filtering of posts or paragraphs by search terms in titles, textContent and frontmatter defined 'tags'.

### [Hmmly-Elements](https://gitlab.com/aspera-non-spernit/hammeley.info/-/blob/dev/scripts/lib/libhmmly/elements..js?ref_type=heads)

- [x] <hmmly-player> 
- [ ] <hmmly-playlist>

#### <hmmly-player>

Embed youtube videos with a single line of html.

- The <hmmly-player> allows to embed lots of youtube videos on a single page by reducing the load times siginicantly.
- Shows a thumbnail of the video instead.
- Prefetches on mousehover and loads the iframe embedded youtube player on lick.
- Supports three sources of media information: 

Examples:

    // A curated playlist in media.json:playlists
    <hmmly-player id="polygut" data-playlist-id="polygut">
    // A curated media source in media.json:[music, video, news, podcasts, ..]
    <hmmly-player id="tribalife" data-video-id="sYRE0IeYc_o">
    // A youtube video id (like in youtube.com/watch?v=_Jy6k0Vjw8A)
    <hmmly-player id="ablp" data-yt-vid="_Jy6k0Vjw8A">

### The media.json

    {
        "description": "Unified hmmly data source. ",
        "news": {
            "title": String
            "content": [
                {
                    "title": String,
                    "date": Date<String>,
                    "type": "yt",
                    "source": String,
                    "iframe": boolean
                }
            ]
        },
         "films": { .. },
         "music": { .. },
         "playlists: { .. },
         "recommendations": {
            ..
         }
    }


### [HmmlyModels](https://gitlab.com/aspera-non-spernit/hammeley.info/-/blob/dev/scripts/lib/libhmmly/models.js?ref_type=heads)

As the library grows, it was necessary to implement Javascript models, rather than to handle unnamed objects or raw json data.

- [ ] <Media>
- [ ] <Playlist>

### Markdown / HTML

A .hmmly-link controls media players, galleries and other collections of content (ie. the fiths track of an embedded youtube player playlist).

- [ ] [a link](#{local_ anchor}){: .hmmly_link}

### Contributions

- ChatGPT 3.5: Code improvements and debugging. A great companion and development partner you could wish for.

## For the developer

- The [pipeline quota](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab) because there's no visible tab on 'Usage quota'.


