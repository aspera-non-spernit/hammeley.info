---
title: "Ein Leben in Zeitabschnitten"
layout: zeitalter
permalink: /zeitalter/ueberblick
---
{% assign era = site.data.era | find: 'era-short', page.era-short %}
**Hinweis: Die folgenden Videos können Spuren von allem enthalten.**

Eine Übersicht über Zeitabschnitte, Epochen und Ären in einem Leben.
