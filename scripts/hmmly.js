"use strict";
import * as hmmly from  './lib/libhmmly/libhmmly.js';
import * as jQuery from './lib/jquery-3.6.3.min.js';

const LS_LAST_ACIVE_SLIDE = "activeSlide";
const WITH_AMBIENT = false;

// mv to animation
const slideToLastActiveSlide = () => {
    if (window.location.pathname === "/bild-und-ton") {
        var activeSlide = localStorage.getItem(LS_LAST_ACIVE_SLIDE);
        if (activeSlide !== null) {
            hmmly.dbg("hmmly.slideToLastActiveSlide(): last slide before reload " + parseInt(activeSlide));
            $('#hmmlyPlayerCarousel').carousel(parseInt(activeSlide));
        } else {
            hmmly.dbg("hmmly.slideToLastActiveSlide(): last slide before reload " + parseInt(activeSlide));
            activeSlide = 0;
        }
    }
}

// mv to animation
const storeActiveSlide = () => {
    $('#hmmlyPlayerCarousel').on('slid.bs.carousel', () => {
        var activeSlide = $('#hmmlyPlayerCarousel').find('.carousel-item.active').index();
        localStorage.setItem(LS_LAST_ACIVE_SLIDE, activeSlide);
        hmmly.dbg("hmmly.storeActiveSlide(): storing before reload " + activeSlide);
    });
}

const callThehacker = (elem, ms) => {
    let hacker = new hmmly.animation.Hacker(elem, ms);
    hacker.execute();
}

const getCredentials = () => {
    let urlParams = new URLSearchParams(window.location.search);
    if (! urlParams.get('username') || !urlParams.get('password') ) return;

    return {
        "username": urlParams.get('username'),
        "passphrase": urlParams.get('password')
    }
}
const authorize = async () => {
    let credentials = getCredentials();
    let te = new TextEncoder();
    let key = await hmmly.crypt.CreateKey.from_passphrase(te.encode(credentials.passphrase), te.encode(credentials.username));
    return key;
}
const decrypt = async () => {
    var encryptedElems = document.getElementsByClassName("decrypt");
    try {
        var key = await authorize();
        let cipher = new hmmly.crypt.Cipher();
        for (let elem of encryptedElems) {
            hmmly.dbg("Decrypting..");
            // Create an Encrypted object
            let encr = new hmmly.crypt.Encrypted(hmmly.crypt.Convert.fromHex(elem.dataset.iv), hmmly.crypt.Convert.fromHex(elem.innerHTML));
            let html = await cipher.decrypt(encr, key);
            elem.insertAdjacentHTML('afterend', html);
            elem.parentNode.removeChild(elem);
        }
    } catch (error) {
        hmmly.dbg("hmmly.decrypt(): ", error.message);
    }
}
const prep_content = () => {
    var plain = document.querySelector(".cookieEncr");
    if (!plain) {
        hmmly.dbg("hmmly.prep_content(): Element with class 'cookieEncr' not found.");
        return;
    }
    var te = new TextEncoder();
    return te.encode(plain.outerHTML);
}
// PROTECTION
const dev_protect_man = () => {
    console.log("ProtectMan");

    // create keys
    let creatorSecret = hmmly.crypt.CreateKey.random();
    let creatorSecretHex = hmmly.crypt.Convert.toHex(creatorSecret.derivedKey);

    let visitorSecret = hmmly.crypt.CreateKey.random();
    let dh = new hmmly.crypt.DiffieHellman(1777771, 7);
    let visitorPublic = dh.calculatePublicKey(visitorSecret);

    let symmetricKey = hmmly.crypt.CreateKey.random();

    // $("#randomPasskey").val(creatorSecret.derivedKey);
    $("#randomPasskeyHex").val(creatorSecretHex);

    $("#encryptContentBtn").on("click", async function() {
        let cipher = new hmmly.crypt.Cipher();
        let content = $("#plainTextArea").val();
        if (content && content !== "" && content.length !== 0) {
            let te = new TextEncoder();
            let encrypted = await cipher.encrypt(te.encode(content), symmetricKey); // returns hmmly.crypt.Encrypted
            console.log("Encrypted");
            console.log(encrypted.content);
            console.log(encrypted instanceof hmmly.crypt.Encrypted);
            console.log(symmetricKey instanceof hmmly.crypt.Key );
            console.log(visitorPublic instanceof hmmly.crypt.Key);
            console.log(symmetricKey.derivedKey instanceof Uint8Array);
            let encrSymmetricKey = await cipher.encrypt(symmetricKey.derivedKey, visitorPublic);
            let encrPackage = {
                "encrSymmetricKey": encrSymmetricKey,
                "package": encrypted
            };
            $("#encryptedTextArea").val(JSON.stringify(encrPackage));
        } 
    });
}
const isTracking = () => {
    var cookie = get_cookie("ppms_privacy_");
    if (cookie) {
        var cookieValue = cookie.split('=')[1]; // Extract the value part of the cookie
        var data = JSON.parse(decodeURIComponent(cookieValue)); // Parse the cookie value as JSON
        if (data.consents.analytics.status === 1) {
            $("#notification").remove();
            hmmly.dbg("hmmly.isTracking(): Access granted. Analytics cookies consent received.");
        } else {
            hmmly.dbg("hmmly.isTracking(): Access denied. Please agree to analytics cookies.");
            encryptElements("cookieEncr");
        }
    } else {
        hmmly.dbg("hmmly.isTracking(): Cookie not found.");
        encryptElements("cookieEncr");
    }
}

const encryptElements = async (className) => {
    var plain = Array.from(document.querySelectorAll(className)); // ".encrypt"

    if (plain.length > 0) { // Check if any elements were found
        try {
            var te = new TextEncoder();
            let cipher = new hmmly.crypt.Cipher();
            plain.forEach(async (e) => {
                try {
                    let arrayBuffer = te.encode(e.outerHTML);
                    let encrypted = await cipher.encrypt(arrayBuffer, hmmly.crypt.CreateKey.random());
                    let div = document.createElement("div");
                    div.classList.add("decrypt", "text-break"); // Separate multiple classes with commas
                    div.textContent = hmmly.crypt.Convert.toHex(encrypted.content);
                    div.dataset.iv = hmmly.crypt.Convert.toHex(encrypted.iv);
                    e.parentNode.replaceChild(div, e);
                } catch (error) {
                    console.error("Encryption Error:", error); // Debugging: Log any encryption errors
                }
            });

        } catch (error) {
            console.error("Main Error:", error); // Debugging: Log any main errors
        }
    }
}
const get_cookie = (name) => {
    var cookies = document.cookie.split(';');
    return cookies.find( cookie => cookie.trim().startsWith(name) );
}

// Check the referrer URL and load Libsodium if necessary
$( document ).ready( () => {
    console.log("hammeley.info (hmmly.js) app and its dependencies loaded.");
    // protect content + animation
    encryptElements(".encrypt")
        .then(() => {
            let encrypted = [...document.querySelectorAll(".hacker, .decrypt"), document.getElementsByTagName("code")[0] ];
            for (let e of encrypted) {
                let ms = Math.floor(Math.random() * 150000); // 2.5 minutes max
                callThehacker(e, ms);
            }
        });

    if (getCredentials()) { decrypt(); }

    // lazyload images
    hmmly.lazyload.lazyLoadImages();

    // search + filter
    let tf = document.getElementById("text-filter");
    let tracking = new hmmly.anal.Tracking();
    hmmly.anal.Analytics.listen(tf, hmmly.anal.Search.filterElements, tracking.addToPool );
    
    // header shadow on page scroll
    $(window).scroll(() => {
        if ( $(window).scrollTop() > 7) {
            $("#header").addClass("shadow");
        } else {
            $("#header").removeClass("shadow");
        }
    });
    // mv to error
    // 404 error handling
    if (document.title === "404 | hammeley.info") {
        document.getElementById("page_path").innerHTML = window.location.pathname;
        hmmly.utils.ErrorRedirect.notFound(10);
    }
    
    slideToLastActiveSlide();
    storeActiveSlide();

    // remove data-yt-prefetch after a while to make a second prefetch possible
    setInterval(() => {
        const headElement = document.querySelector('head');
        if (headElement.dataset.ytPrefetched) {
            delete headElement.dataset.ytPrefetched;
            hmmly.yt.PRECONNECTS.forEach(url => {
                const preconnectLinks = document.querySelectorAll(`link[rel="preconnect"][href="${url}"]`);
                preconnectLinks.forEach(link => link.remove());
            });
        }
    }, 3 * 60000);
    
    // Dispatch
    let conditions = [
        { "path": "/ueber", "func": isTracking },
        { "path": "/", "func": isTracking },
        { "path": "/dev/protect_man", "func": dev_protect_man }
    ];
    for (let c of conditions) { if (window.location.pathname.includes(c.path)) { c.func(); break; } }
});