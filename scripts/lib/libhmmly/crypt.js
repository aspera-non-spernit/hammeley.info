// import * as _sodium from './vendor/libsodium-wrappers.js';

// Encryption function
const CIPHER = "AES-CBC";
const HASH = "SHA-256";
const ITERATIONS = 1000;
const KDF = "PBKDF2";
const KEY_LENGTH = 256;

export class Cipher {
    /**
     * 
     * @param {*} encodedText
     * @param {*} key hmmly.crypt.Key
     * @returns 
     */
    async encrypt(encodedText, key) {
        if (!(encodedText instanceof Uint8Array)) { throw new Error('encodedText must be a Uint8Array'); }
        if (!( key instanceof Key)) { throw new Error('argument key must be instance of hmmly.crypt.Key'); }
        let iv = crypto.getRandomValues(new Uint8Array(16)); // Assuming IV size is 16 bytes (128 bits)
        let impKey = await crypto.subtle.importKey( "raw", key.derivedKey, { name: CIPHER }, false, ["encrypt"] );
        let encrypted = await crypto.subtle.encrypt( { name: CIPHER, iv: iv }, impKey, encodedText );
        return new Encrypted(iv, new Uint8Array(encrypted) );
    }

    async decrypt(encrypted, key) {
        try {
            let impKey = await crypto.subtle.importKey( "raw", key.derivedKey, { name: CIPHER }, false, ["decrypt"] );
            let decrypted = await crypto.subtle.decrypt( {  name: CIPHER, iv: encrypted.iv }, impKey, encrypted.content );
            return new TextDecoder().decode(decrypted);
        } catch (error) {
            if (error.message.includes("The operation failed for an operation-specific reason:")) {
                throw new Error("Error decrypting: Invalid key provided.");
            } else {
                throw new Error(error);
            }
        }
    }
}

export class Convert {
    static toHex(buffer) {
        return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
    }
    static fromHex(hexString) {
        let bytes = [];
        for (let i = 0; i < hexString.length; i += 2) {
            bytes.push(parseInt(hexString.substr(i, 2), 16));
        }
        return new Uint8Array(bytes).buffer;
    }
}

export class DiffieHellman {
    #prime;
    #generator;

    constructor(primeBigInt, generator) {
        this.#prime = primeBigInt;
        this.#generator = generator;
    }

    // Calculate public key from secret key
    calculatePublicKey(secretKey) {
        let publicKey = DiffieHellman.modPow(this.#generator, secretKey.derivedKey, this.#prime);
        return new Key(new Uint8Array([publicKey]), secretKey.salt, secretKey.iterations);
    }

    // Calculate shared secret from received public key and own secret key
    calculateSharedSecret(receivedPublicKey, ownSecretKey) {
        const sharedSecret = DiffieHellman.modPow(receivedPublicKey, ownSecretKey, this.#prime);
        return new Uint8Array([sharedSecret]);
    }

    // Helper function to calculate (base^exponent) % modulus efficiently
    static modPow(base, exponent, modulus) {
        let result = 1;
        base %= modulus;
        while (exponent > 0) {
            if (exponent & 1) {
                result = (result * base) % modulus;
            }
            exponent >>= 1;
            base = (base * base) % modulus;
        }
        return result;
    }
}

export class Encrypted {
    constructor(iv, content) {
        this.iv = iv;
        this.content = content;
    }
}

export class Key {
    constructor(derivedKey, salt, iterations) {
        if (!(derivedKey instanceof Uint8Array)) { throw new Error('derivedKey must be a Uint8Array'); }
        this.derivedKey = derivedKey;
        this.salt = salt;
        this.iterations = iterations;
    }
    hex() {
        return Convert.toHex(this.derivedKey);
    }
}

export class CreateKey {
    /**
     * 
     * @param {*} passphrase String
     * @param {*} salt String
     * @returns Key<Uint8Array, String, u8>
     */
    static async from_passphrase(passphrase, salt) {
        if (!salt) { throw new Error('Salt is required.'); }
        let enc = new TextEncoder();
        let passphraseBuffer = enc.encode(passphrase);
        let key = await crypto.subtle.importKey("raw", passphraseBuffer, {name: KDF}, false, ["deriveBits"]);
        let derivedBits = await crypto.subtle.deriveBits({ name: KDF, salt: enc.encode(salt), iterations: ITERATIONS, hash: HASH }, key, KEY_LENGTH);
        let derivedKey = new Uint8Array(derivedBits);
        return new Key(derivedKey, salt, ITERATIONS);
    }

    /**
     * 
     * @returns Uint8Array
     */
    static random() { return new Key (window.crypto.getRandomValues(new Uint8Array(32)), null, null) }

    static toBigInt(bytes) {
        let bigIntKey = 0n;
        for (let i = 0; i < bytes.length; i++) {
            bigIntKey = (bigIntKey << 8n) + BigInt(bytes[i]);
        }
        return bigIntKey;
    }
}


