"use strict";

import * as constants from './constants.js';
import * as dl from './vendor/damerau-levenshtein.js';


// unused const camelToDash = (str) => { return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase(); }
export const delay = (ms) => { return new Promise(resolve => { setTimeout( () => { resolve(); }, ms); }); };
export class Filter { static disabled(collection) { return collection.filter( (elem) => { return elem.disabled === undefined || elem.disabled !== true }); } }
export class Utils { static dbg(msg) { if (constants.DEBUG_MODE) { console.debug(msg); } } }

// Fisher-Yates shuffle algorithm
export const fisherYatesShuffle = (array) => {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
}

export class Args {
	static urlFragments() {
		var url = window.location.href;
		var fragments = url.split('#');
	
		// If there are fragments present
		if (fragments.length > 1) {
			var fragmentObject = {};
	
			// Extract key-value pairs from fragments
			var fragmentPairs = fragments[1].split('&');
			for (var i = 0; i < fragmentPairs.length; i++) {
				var pair = fragmentPairs[i].split('=');
				fragmentObject[pair[0]] = pair[1];
			}
	
			return fragmentObject;
		} else {
			return null;
		}
	}
}

export class DataProvider {
	static collection = null;

	constructor() {
		this.id = this.getAttribute(constants.HmmlyAttribute.ID);
		Utils.dbg("DataProvider() \"" + this.id + "\" created.");
	}

	static async fetch(path) { // TODO static coll
		var storedData = sessionStorage.getItem('DataProviderCollection');
		if (storedData) {
            Utils.dbg("DataProvider.fetch(" + path + "): Data found in sessionStorage.");
			DataProvider.collection = null;
            return JSON.parse(storedData); // Parse the JSON data and return it
        }  else if (DataProvider.collection !== null) {
            Utils.dbg("DataProvider.fetch(" + path + "): Data found in static DataProvider.collection.");
            return DataProvider.collection; // Return the cached collection
        } else {
			Utils.dbg("DataProvider.fetch(" + path + "): Fetching.");
			return fetch(path)
			.then( (response) => response.json() )
			.then( (json) => { 
				DataProvider.collection = json;
				Utils.dbg("DataProvider.fetch(" + path + "): Try storing data in session storage.");
				storedData = sessionStorage.setItem("DataProviderCollection", JSON.stringify(json) );
				return DataProvider.collection;
			});
		}
	}

	/**
	 * 
	 * @param {*} id 
	 * @param {*} notListed: bool // speeds up the search for media information that is known not to be in media.json
	 * @returns 
	 */
	static async findMediaById(id, notListed = false) {
		const UNKNOWN_MEDIA = { "id": id, "artist": "Unknown", "title": "Unknown", "date": new Date(), "date-comment": "Date unknown.", "yt_img": undefined };
		if (notListed) { return UNKNOWN_MEDIA; }
		else {
			var collection = await DataProvider.fetch(constants.MEDIA_PATH);
			for (let type of constants.MEDIA_TYPES) {
				Utils.dbg("DataProvider.findMediaById(" + id + "): Querying in " + type);
				var media = collection[type].content.find( (entry) => entry.id === id);
				if (media !== undefined) {
					Utils.dbg("DataProvider.findMediaById(" + id + ") Result: " + media.title);
					return media;
				}
			}
			Utils.dbg("DataProvider.findMediaById(" + id + "): Media not found");
			return UNKNOWN_MEDIA;
		}
	}
}

export class ErrorRedirect {
	static async notFound(num_results) {
		var data = await fetch(window.location.origin + "/sitemap.xml")
			.then( (response) => {
				if (!response.ok) {
					throw new Error(`ErrorRedirect.notFound(${num_results}): Failed to fetch sitemap. Status: ${response.status}`);
				}
			return response.text();
			});
		  
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString(data,"text/xml");
		
		var locs = xmlDoc.getElementsByTagName("loc");
		var distances = [];
		for (var loc of locs) {
			var url = new URL(decodeURIComponent(loc.childNodes[0].nodeValue));			
			var query = url.pathname.split("/").pop();
		
			distances.push(
				{
					distance: dl.distance(query, window.location.pathname),
					path: url.pathname
				 }
			);
			distances.sort((a,b) => a.distance - b.distance); 
		}

		var list = document.getElementById("similar");
		for (var i = 0; i < num_results; i++) {
			if (distances[i] !== undefined) {
				var h3 = document.createElement("h3");
				var a = document.createElement("a");
				a.setAttribute("href", distances[i].path);
				a.innerText = distances[i].path;
				h3.appendChild(a);
				list.appendChild(h3);
			}
		}
	}
}

export class Result {
	constructor(success, error) {
		if (success) {
			this.success = true;
			this.error = null;
		} else {
			this.success = false;
			this.error = error;
		}
	}
}

export class Validate {
	static links(links) {
		var validated_links = [];
		for (let link in links) {
			validated_links.push( Validate.link(link) );
		}
	}

	static link(link) {
		var err_msg = "";

		// mandatory attributes id and receiver 
		if (link.id === undefined) {
			Utils.dbg("Validate.link(): The current implementation requires an id for each hmmly-link referring to a selectable source.");
			err_msg = link + " has got no mandatory id that refers to a media id in media.json.";
		}
		
		if (link.receiver === undefined) {
			err_msg.concat(" The element is also missing the receiver=\"..\" attribute.");
		}

		// does the receiver element exist in the document?
		if (err_msg === "") {
			if ( !Validate.receiver_exists(link.receiver) ) {
				err_msg = "Validate.link(): The stated receiver: " + link.receiver + " couldn't be found in the document.";
			}
		}

		if (err_msg.length === 0) {
			return new Result(true, null);
		} else {
			return new Result(false, new Error(err_msg));
		}
	}

	/**
	 * 
	 * @param {*} id 
	 * @returns Result { success: true | false , error: Error | null }
	 */
	static playlistId(id) {
		if (id !== undefined && id !== null & id.length !== 0) {
			return new Result(true, null);
		} else if (id === undefined) {
			return new Result(false, new Error("Playlist id is undefined."));
		} else if (id === null ) {
			return new Result(false, new Error("Playlist id is null."));
		} else if (id.length === 0) {
			return new Result(false, new Error("Playlist id length is 0."));
		}
	}
	receiver_exists(id) {
		if (document.getElementById(constants.HmmlyAttribute.RECEIVER_ID) !== undefined) {
			return new Result(true, null);
		} else {
			return new Result(false, new Error("The receiver with id " + id + " couldn't be found in the document."));
		}
	}
}
