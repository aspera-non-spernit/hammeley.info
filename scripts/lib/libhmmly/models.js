"use strict";
import * as constants from './constants.js';
import * as utils from './utils.js';

/**
 * TODDO: each Hmmly-element that extends HTMLElement has a set of supported attributes:
 * 	static supportedAttributes = [constants.HmmlyAttribute.PLAYLIST_ID, constants.HmmlyAttribute.VIDEO_ID];
 *  This class validates the provided data-attributes and returns its values
 **/

export class AttributeValidator {
    static validate(element, supportedAttributes) {
        const attributes = Object.values(supportedAttributes);
        const unsupportedAttribute = attributes.find(attr => !element.dataset[attr]);
        if (unsupportedAttribute) {
            console.error(`Error: Unsupported attribute "${unsupportedAttribute}"`);
            return false;
        }
        return true;
    }
}

export class Validator {
	static validate(fields, object) {
        for (let i in fields) {
            console.log("Media.validate(): " + fields[i]);
            if (Object.prototype.hasOwnProperty.call(object, fields[i])) {
                if (this[fields[i]] === undefined) {
                    object.log(object.constructor.name + ".validate(" + object.id + ") has got an undefined " + fields[i] + ". Please fix.");
                    object[fields[i]] = object.constructor.name + ".validate(" + object.id + "): added value. Please fix!";
                }
            } else {
				// if this happens, class implementation is missing this field.
                throw new Error("Media.validate(): Object is missing mandaroty field " + fields[i] + ".");
            }
        }
    }
}

export class Playlist {
    static supportedAttritbutes = [constants.HmmlyAttribute.PLAYLIST_ID, constants.HmmlyAttribute.VIDEO_ID, constants.HmmlyAttribute.YOUTUBE_VID];

    constructor(fromObject, title, id, content) {
        if (fromObject !== null) {
            Object.assign(this, fromObject);
        } else {
            this.title = title;
            this.id = id;
            this.content = content;
        }
    }

    // either find playlist in media.playlists or a video-in supported collections constants.MEDIA_TYPES.
    static async by(hmmlyAttribute, value) {
		utils.Utils.dbg("Playlist.by(" + hmmlyAttribute + ", " + value + "): Searching..");
        // by playlistId
        if (hmmlyAttribute === constants.HmmlyAttribute.PLAYLIST_ID) {
            return utils.DataProvider.fetch(constants.MEDIA_PATH).then( (collection) => {
                var pl = collection.playlists.content.find( (playlist) => playlist.id === value );
                utils.Utils.dbg("Playlist.by(" + hmmlyAttribute + ", " + value + "): Playlist created by id.");
                return new Playlist(null, pl.title, pl.id, pl.content);
            });
        // by videoId
        } else if (hmmlyAttribute === constants.HmmlyAttribute.VIDEO_ID) {
            // returns a playlist with a single video, that is video.id
            return utils.DataProvider.fetch(constants.MEDIA_PATH).then( (collection) => {
                for (let collectionName of constants.MEDIA_TYPES) {
                    var video = collection[collectionName].content.find( (video) => video.id === value);
                    if (video) { break; }
                }
                var content = [
                    {
                        "title": video.artist + " - " + video.title,
                        "id": video.id
                    }
                ];
                utils.Utils.dbg("Playlist.by(" + hmmlyAttribute + ", " + value + "): Playlist created by video id.");
                return new Playlist(null, video.title, "playlist-vid-" + video.id, content);
            });
        // by youtubeVid
        } else if (hmmlyAttribute === constants.HmmlyAttribute.YOUTUBE_VID) {
            let pl = {
                "title": "A single Youtube Video",
                "id": "playlist-ytvid-" + value,
                "type": undefined, // is unknown
                "content": [
                    {
                        "title": "A Youtube Video",
                        "id": value
                    }
                ]
            };
            utils.Utils.dbg("Playlist.by(" + hmmlyAttribute + ", " + value + "): Playlist created by youtube video id.");
            return new Playlist(pl); 
        } else {
            utils.Utils.dbg("Playlist.by(" + hmmlyAttribute + ") unsupported. Supported: " + Playlist.supportedAttritbutes);
        }
	}
    // TODO: class Operation(s)
    static async op_rand(playlist_id) {
        var collection = await utils.DataProvider.fetch(constants.MEDIA_PATH);
        var playlist = collection.playlists.content.find( (playlist) => playlist.id === playlist_id);
        utils.Utils.dbg("Playlist.op_rand(" + playlist_id + "): Playlist found. Randomly selecting items.");

        var rand_content = [];
        for (let i = 0; i < 1; i++) { // Generating a playlist with 1 random item
            let index = Math.floor(Math.random() * playlist.content.length);
            let item = playlist.content[index];

            // Assuming the data source items have 'id' and 'title' properties
            rand_content.push({
                id: item.id,
                title: item.title
            });
        }
        return new Playlist(null, "Randomly selected item from " + playlist.id, "rand-" + playlist.id, rand_content);
    }

    //todo HmmlyAttributes
	static from(json) { // check if correct
        var pl = Object.assign(new Playlist, JSON.parse(json));
        return new Playlist(pl.id, pl.title, pl.content);
	}

    static toIdStrings(playlist) {
		utils.Utils.dbg("Playlist.toIdStrings(" + playlist.id + "): Converting Playlist content into string of ids.");
        utils.Utils.dbg("Playlist.toIdStrings(" + playlist.content + ") < Content.");
        var cs = playlist.content.map(a => a.id).toString(); 
		return cs;
    }
}
