import * as utils from './utils.js';

export class Analytics {
    static listen(elem, inputCallback, leaveCallback) {
       
        elem.addEventListener("input", (event) => {
            inputCallback( event.currentTarget.value.toLowerCase(), false ); // filterElements
        });
        utils.Utils.dbg("Analytics.listen(" + elem.id + ") to input.");

        elem.addEventListener("blur", (event) => {
            utils.Utils.dbg("Analytics.listen(" + elem.id + ") blur event detected.");
            leaveCallback(event.currentTarget.value.toLowerCase()); // addToPool
        });            
        utils.Utils.dbg("Analytics.listen(" + elem.id + ") to blur.");

        elem.addEventListener("focusout", (event) => {
            utils.Utils.dbg("Analytics.listen(" + elem.id + ") focusout event detected.");
            leaveCallback(event.currentTarget.value.toLowerCase()); // addToPool
        });
        utils.Utils.dbg("Analytics.listen(" + elem.id + ") to focusout.");

        elem.addEventListener("mouseleave", (event) => {
            utils.Utils.dbg("Analytics.listen(" + elem.id + ") mouseleave event detected.");
            leaveCallback(event.currentTarget.value.toLowerCase()); // addToPool
        });
        utils.Utils.dbg("Analytics.listen(" + elem.id + ") to mouseleave.");
    }
}
export class Search {
    constructor() {
        utils.Utils.dbg("Search(): No need for a Search instance. All functions are static in this class.");
    }
    
    static filterElements(queryString, found = false) { 
        document.querySelectorAll(".searchable").forEach( (element) => {
            let textContent = element.textContent.toLowerCase();
            let containsText = textContent.includes(queryString);
            let containsTags = element.dataset.tags && element.dataset.tags.toLowerCase().includes(queryString);
            if (found) {
                element.classList.toggle("d-none", containsText || containsTags);
            } else {
                element.classList.toggle("d-none", !(containsText || containsTags));
            }
        });
        window.scrollTo(0, 0);
    }
}
export class Tracking {
    constructor() {
        this.lastQuery = null;
        this.minQueryLength = 4;
        this.trackInterval = 1000; // 5000 milliseconds = 5 seconds
        this.pool = [];
        this.timer = null;
        this.addToPool = this.addToPool.bind(this);

        // Add beforeunload event listener
        window.addEventListener("beforeunload", () => {
            this.timer = null;
            // Check if lastQuery has not been added to the pool
            this.addToPool(this.lastQuery);
        });

        utils.Utils.dbg("Tracking(): created.");
    }

    async addToPool(queryString) {
        if (this.lastQuery !== queryString && queryString.length >= this.minQueryLength && queryString !== "") {
            utils.Utils.dbg("Tracking.addToPool(" + queryString +")");
            this.lastQuery = queryString;
            // Add queryString to pool
            let numResults = $(".searchable:visible").length; 
            let trackingItem = [queryString, window.location.pathname, numResults];
            this.pool.push(trackingItem);

            if (!this.timer && this.pool.length > 0) {
                // Start tracking at regular intervals
                this.timer = setInterval(() => {
                    this.#track();
                }, this.trackInterval);
            }
        } else {
            utils.Utils.dbg("Tracking.addToPool(" + queryString + "): hasn't changed from previous query: " + this.lastQuery + ". Not adding to tracking pool.");
        }
    }

    #track() {
        utils.Utils.dbg("Tracking().#track(): Tracking " + this.pool.length + " search terms.");
        if (this.pool.length > 0) {
            while (this.pool.length > 0) {
                // Get the first item in the pool
                let item = this.pool[0];
                // Track the item
                _paq.push(['trackSiteSearch', item[0], item[1], item[2]]);
                // Remove the tracked item from the pool
                this.pool.shift();
            }
            if (this.pool.length === 0) {
                clearInterval(this.timer);
                this.timer = null;
            }
        } else {
            // Clear the timer if the pool is empty
            clearInterval(this.timer);
            this.timer = null;
        }
    }
}