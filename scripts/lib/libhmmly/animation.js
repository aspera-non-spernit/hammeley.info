import * as utils from './utils.js';

class RingBuffer {
    constructor(array, randomIndex = true) {
        this.array = array;
        this.index = randomIndex ? Math.floor(Math.random() * array.length) : 0;
    }

    next() {
        if (this.index === this.array.length) {
            this.index = 0;
        }
        const element = this.array[this.index];
        this.index++;
        return element;
    }
}

const HACKER_TEXT = "iNFoRmAtIoNeN hAbEn HaT sEiNe ZeIt. KeInE iNfOrMaTiOnEn HaBeN hAt SeInE zEiT. ";
const CYRMAP = [
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ^1234567890!\"§$%&/()=?,.-;:_'ˇ",
    "ДВСDЁҒGНІЈКLМПОРQЯЅТЦЏШХЧZавсdёfgніјкlмпорqгѕтцѵшхчz ˇ1234567890!\"§$%&/()=?,.-;:_'^"
];

export class Hacker {
    constructor(elem, ms) {
       // console.log("Hacker(): Delaying execution by: " + ms + "ms.");
        this.elem = elem;
        this.delay = ms;
    }

    #pToSpan() {
        // Wrap each line of text in a <div> element
        let lines = this.elem.textContent.split('\n');
        this.elem.innerHTML = lines.map(line => `<div class="${this.elem.className}">${line}</div>`).join('');
        
        // Get all the character spans within each <div>
        this.spans = this.elem.querySelectorAll('div');
        this.spans.forEach(div => {
            let spans = [...div.textContent].map((c, index) => `<span data-index="${index}">${c}</span>`).join('');
            div.innerHTML = spans;
        });
    }

    #cyrconv_flex() {
        // additionally use cyrillic characters
        this.mapping = this.mapping.map((e) => {
            let index = CYRMAP[0].indexOf(e[1]);
            let newMappedCharacter = CYRMAP[1].charAt(index);
            let newMappedEntry = [e[0], newMappedCharacter, e[2], e[3]];
            return newMappedEntry;
        });
    }

    #generateMapping() {
        let rb = new RingBuffer(HACKER_TEXT);
        this.mapping = [];
        
        // Generate mapping for each line
        this.spans.forEach((div, divIndex) => {
            let spans = div.querySelectorAll('span');
            spans.forEach((span, index) => {
                this.mapping.push([span.textContent, rb.next(), index, divIndex]);
            });
        });
        utils.fisherYatesShuffle(this.mapping);
    }

    async execute() {
        await utils.delay(this.delay);
        this.#pToSpan();
        this.#generateMapping();
        this.#cyrconv_flex();
      
        for (let entry of this.mapping) {
            let index = entry[2];
            let newCharacter = entry[1];
            let divIndex = entry[3];
            if (index >= 0 && index < this.spans[divIndex].children.length) {
                try {
                    await utils.delay(13); // Delay the replacement of each span
                    this.spans[divIndex].children[index].textContent = newCharacter;
                } catch (error) {
                    utils.Utils.dbg("hmmly.animation.Hacker.execute(): Error replacing character:", error.message);
                    utils.Utils.dbg("hmmly.animation.Hacker.execute(): divIndex:", divIndex);
                    utils.Utils.dbg("hmmly.animation.Hacker.execute(): index:", index);
                    utils.Utils.dbg("hmmly.animation.Hacker.execute(): newCharacter:", newCharacter);
                    utils.Utils.dbg("hmmly.animation.Hacker.execute(): Error replacing character:", error.message);
                }
            } else {
                utils.Utils.dbg("hmmly.animation.Hacker.execute(): Invalid index:", index);
            }
        }
        this.#finalize();
    }

    #finalize() {
        // Combine spans back into the original lines
        this.spans.forEach(div => {
            let textContent = [...div.children].map(span => span.textContent).join('');
            div.innerHTML = textContent;
        });
    }
}