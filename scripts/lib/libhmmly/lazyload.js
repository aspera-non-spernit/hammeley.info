// lazyload.js

function lazyLoadImages() {
    var lazyloadImages = document.querySelectorAll(".lazyload");
  
    var observer = new IntersectionObserver( function(entries, observer) {
        entries.forEach( function(entry) {
            if (entry.isIntersecting) {
                var img = entry.target;
                img.src = img.dataset.src;
                img.classList.remove("lazyload");
                observer.unobserve(img);
            }
        });
    });
  
    lazyloadImages.forEach(function(img) {
        observer.observe(img);
    });
  
    window.addEventListener("scroll", function() {
        var offset = 100;
        lazyloadImages.forEach( function(img) {
            var bounding = img.getBoundingClientRect();
            if (  bounding.top >= -offset && bounding.left >= 0 && bounding.top <= (window.innerHeight || document.documentElement.clientHeight) ) {
                if ( !img.classList.contains("lazyload") ) {
                    return;
                }
                img.src = img.dataset.src;
                img.classList.remove("lazyload");
                img.removeAttribute("data-src");
                observer.unobserve(img);
            }
        });
    });
}
    
// Export the function for use in other files if needed
export { lazyLoadImages };
    