const LS_BOGDAN = "bogdan";

// Beamten-Detektor
function weekday() {
    var today = new Date().getDay();
    if (today >= 1 && today <= 5) { return true; } else { return false; }
}

async function checkIP() {
    let ip = await fetch( "https://jsonip.com", { mode: "cors"} )
        .then( resp => resp.json() )
        .then( json => { return json.ip; }
    );
    return ip;
}

function normalize(ip) {
    if (ip.includes(':')) {
        if (!ip.endsWith("::")) {
            ip = ip + '::';
        }
        let [left, right] = ip.split('::');
        let leftParts = left.split(':').map(part => part.padStart(4, '0'));
        let rightParts = right.split(':').map(part => part.padStart(4, '0'));
            while (leftParts.length + rightParts.length < 8) {
                leftParts.push('0000');
            }
            return [...leftParts, ...rightParts].join(':');
        }
    return ip;
}

function ipv4ToNumber(ip) {
    if (ip.includes(".")) { // only for ipv4
        var parts = ip.split('.').map(part => parseInt(part));
        return parts[0] * 256 ** 3 + parts[1] * 256 ** 2 + parts[2] * 256 + parts[3];
    }
    return ip;
}

function inRange(ip, range) {
    if ( ip.includes(".") ) {
        var ipNum = ipv4ToNumber(ip);
        var startIPNum = ipv4ToNumber(range.start);
        var endIPNum = ipv4ToNumber(range.end);
        return ipNum >= startIPNum && ipNum <= endIPNum;
    }
    // range comparison complicated
    if ( ip.includes(":") ) {
        let normalized = normalize(ip);
        if ( normalized >= normalize(range.start) &&  normalized <= normalize(range.end) ) {
            return true;
        }
    }
}

(async () => {
    // only check once
    if ( sessionStorage.getItem(LS_BOGDAN) ) { return; }

    let ip = await checkIP();
    let blocked = false;
    let href = "";

    // BLOCKING CONDITIONS

    if (!blocked) {
        // block unsecure referres
        if (document.referrer.startsWith("http:") || !document.referrer.startsWith("https:")) {
            blocked = true;
            href = document.referrer;
        }
    }
    
    if (!blocked) {
        // block headless browsers
        if (/HeadlessChrome/.test(navigator.userAgent)) { 
            // If the browser is headless Chrome OR window.chrome is undefined (headless Chrome)
            blocked = true; 
            href = "https://www.google.com/chrome"; 
        }
    }
    
    // cannot verify visitor ip
    if (!ip) {
        console.log("IP Check failed. Cannot trust visitor.");
        blocked = true;
        href = window.home ? window.home : "127.0.0.1";
    }

    // BLOCKING
    // ip ranges
    if (!blocked) {
        // blocked ip range
        let black_ranges = [
            // { "start": "194.15.138.11", "end": "194.15.138.11", "href": "https://www.zlb.de/" }, // test
            { "start": "77.87.224.0", "end": "77.87.231.255", "href": "https://bmi.bund.de/"},
            { "start": "195.88.116.0", "end": "195.88.117.255", "href": "https://www.arbeitsagentur.de/" },
            { "start": "2001:1004", "end": "2001:1004", "href": "https://www.arbeitsagentur.de/" },
            { "start": "51.79.0.0", "end": "51.79.255.255", "href": "https://ovhcloud.com" }
        ];
        blocked = black_ranges.some( range => {
            if ( inRange(ip, range) ) {
                href = range.href;
                return true;
            }
        });
    }

    // blocking referrers
    if ( !blocked && document.referrer && document.referrer !== "" ) {
        // blocked referrer
        let black_ref = ["website.informer.com", "jira.bka.extrapol.de"];
        var referrerParts = document.referrer.split("://");
        if (referrerParts.length > 1) {
            const referrerDomain = referrerParts[1];
            black_ref.forEach( (ref) => {
                if ( referrerDomain.includes(ref) ) {
                    href = document.referrer;
                    blocked = true;
                }
            });
        }
    }
    // execute block
    if (blocked) { window.location.href = href; }
    else { sessionStorage.setItem(LS_BOGDAN, "pass"); }
})();

var script = document.currentScript || document.scripts[ document.scripts.length - 1 ];
script.parentNode.removeChild(script);