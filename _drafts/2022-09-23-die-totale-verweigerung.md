---
title: "Die totale Verweigerung"
category: "phantasia"
layout: default
---

(temporärer Beitrag)

Die russische Regierung hat in den letzten Tagen die Teilmobilmachung erklärt und damit begonnen, Wehrpflichtige einzuziehen. Dabei ist es laut Medien derzeit nicht klar, wer genau eingezogen werden soll und in welchem Umfang damit bereits begonnen wurde.

Bei dem Begriff "Wehrpflichtige" denken viele bei uns in Deutschland vielleicht an den jungen 18-jährigen Mann. Die Wehrpflicht umfasst aber viel mehr deutsche Männer. Ungediente können in Friedenszeiten je nach Situation bis zum 32. Lebensjahr eingezogen werden. Im Spannungs- oder Verteidigungsfall gibt es auch für Ungediente keine Altersgrenzen.

Zur Erinnerung: Die Wehrpflicht wurde in Deutschland nur ausgesetzt, nicht abgeschafft. Sie ist seit Bestehen der Bundeswehr eine Kann-Bestimmung. Die Aussetzung der Wehrpflicht ging nicht mit einer Änderung der Wehrpflichtigkeit und einer Gesetzesänderung einher. An der Wehrpflicht an sich hat sich bis auf ein paar Änderungen bei den Altersgrenzen nichts getan.

Derzeit wird seitens der Politik diskutiert, ob den russischen Personen, die laut wahrheitsgemäßer Berichterstattung deutscher Medien wohl per Flugzeug in die Türkei geflogen oder mit dem Auto nach Finnland ausgereist sind, in der Europäischen Union und auch Deutschland Asyl gewehrt werden soll.

Grundsätzlich begrüße ich eine Regelung, die es Menschen ermöglicht, sich straffrei dem Dienstzwang zu entziehen, damit sie sich nicht an einem Krieg, einer militärischen Spezialoperation oder einem humanitären Einsatz zur Verteidigung der Freiheit beteiligen müssen. Auch nicht im Ersatzdienst.

Es stellt sich mir nun die interessante Frage, wie die deutsche Regierung handeln würde und wie sie insbesondere deutsche Männer behandeln würde, wenn ein hypothetischer Spannungs- oder Verteidigungsfall z. B. aufgrund einer Verpflichtung zur Bündnisverteidigung (NATO / EU) eintreten würde. Und zwar auf europäischem Boden.

Selbstverständlich passiert das nicht, aber was, wenn doch? Was wäre dann? Die Russen, die jetzt ihr Land verlassen sind, wären es Deutsche, sogenannte Totalverweigerer, wenn sie bereits eingezogen waren, sind sie auf Fahnenflucht. Durch ihre Flucht in das Ausland entgehen sie nicht nur einem möglichen Kriegseinsatz, sie stehen auch nicht für einen möglichen Ersatzdienst zur Verfügung.

Das deutsche Grundgesetz gewährt dem deutschen Wehrpflichtigen die Kriegsdienstverweigerung, nicht aber eine Totalverweigerung. Eine Totalverweigerung steht unter Strafe.

Ich gehe nicht davon aus, dass ein immer autoritärer werdendes Deutschland, indem seit fast drei Jahren fundamentale Grundrechte eingeschränkt sind, ein Land in dem Menschen das Gehalt einbehalten wird, weil sie auf die körperliche Unversehrtheit bestehen, besonders umsichtig mit seinen Wehrpflichtigen umgehen würde, wenn es die Situation "alternativlos" macht, Wehrpflichtige zum Kriegs- oder Ersatzdienst einzuziehen.

In so einem Szenario würde Deutschland russische (und ggf. auch ukrainische) Deserteure bewirten, während deutsche Totalverweigerer ihre Heimat verlassen müssten, um einer möglichen Strafe zu entgehen.

Das wäre ziemlich absurd. In einem Kriegsszenario, in dem Deutschland mit Russland im Krieg steht, müsste ich als gedienter Stabsunteroffizier der Reserve Kriegs- oder Ersatzdienst leisten, um den russischen Feind zu bekämpfen, während die russischen Totalverweigerer am Bahnhof stehen und mir bei der Abfahrt ins Einsatzgebiet zuwinken und viel Glück wünschen und dann mit ihren ukrainischen Nachbarn auf bessere Zeiten anstoßen - in Deutschland.

Wenn ich dem entgehen wollte, müsste ich wahrscheinlich nicht nur Deutschland, sondern den gesamten EU/NATO-Block verlassen. Soll ich dann nach Russland, um dort gegen EU-Truppen zu kämpfen?

Wie stellt sich die EU oder die Bundesregierung vor? Erhalten nun Russen, die wegen der Sanktionen kein Visum für die EU erhalten nun Bleiberecht und Asyl?

Im Jahr 2005 war man sich einig, dass Amerikanische Deserteure, die sich nicht an einem völkerrechtswidrigen Krieg beteiligen wollen, kein Asyl in Deutschland erhalten. [(Quelle)](https://www.spiegel.de/politik/deutschland/asyl-in-deutschland-fuer-us-deserteur-unwahrscheinlich-a-1020649.html)

Aber was ist von einer Bevölkerung zu erwarten, die zwar zu hunderttausenden für einen schwarzen Toten auf die Straße geht, nicht aber für getötete iranische Frauen?

Ich kann an dieser Stelle eindeutig sagen, dass ich für keinen Ersatz- oder Kriegsdienst jeglicher Art zur Verfügung stehen werde. Ich bin ja nun seit dem Beginn der "Corona-Pandemie" "Nazi", Corona-Schwurbler und ein mit "Menschenfeindlichkeit durchtränkter" Querdenker. Da gibt es bessere Menschen im Land, die sich solidarisch zeigen und ihren Beitrag für Deutschland leisten könnten.

Wie wärs mit Jan Böhmermann als Frontberichterstatter aus Rostow am Don? Sicherlich wären auch der gediente moderier:ende Markus Lanz, oder der Einzelkämpf:ende Anton Hofreit:ende ganzs besonders geeignet. Prinzipiell eigneten sich alle gendernden und geimpften Demokraten im Land, um unsere Freiheit gegen "Putin" zu verteidigen. Carla Reemtsma und Luisa Neubau:ende könnten sich auf die Straßen kleben. Die eignen sich ebenso wie die Geheimwaffe und Pommespanzer der Grünen ohne Ausbildung Ricarda Lang als Panzersprerr:ende.

Ebenso könnten, ja sollten alle Hobby-Militärstrategen, die seit Monaten davon ausgingen, dass Russland nicht nur die gesamte Ukraine, sondern auch Polen und Finnland überfallen würde und gleichzeitig von einem Sieg der Ukraine sprechen und lauthals nach Waffenlieferungen schreihen, zu allererst unsere Verbündeten in der Ukraine und Polen personell unterstützen. So viel Solidarität kann man ja wohl verlangen.

Das verlogene Dreckspack wird aber warscheinlich wieder als erstes unabkömmlich sein. So wie man es halt kennt.

Keine Panik. Ist alles nur verschwörungstheoretische Schwurbelei ohne Grundlage. Legt Euch wieder hin und schaut fern.
