---
title: Jeder stirbt für sich allein
layout: default
category: ego
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/voXkARKSm3I" title="Wir haben Menschen alleine sterben lassen“ – Kristina Schröder bei „Schuler! Fragen, was ist" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
