# JSON Defintions

--- wip ---

## entry

``` json
[
    { 
        "title": String,
        "evt-vid": String<(absolute url)> ,
        "iframe": bool
    }
]
```

- **iframe**: true if video can be embedded and played in an iframe, false if video cannot be played in an iframe

## era

``` json
[
    { 
        "name": String,
        "era_short": String,
        "start": String (DateFormat),
        "date" : String (DateFormat), 
        "end": String (DateFormat) or "now",
        "def_evt_vid": String (youtube id),
        "def_evt_title": String
    }
]
```
- **start**: RFC formatted date such as "1989-11-08" or the word "now" which renders the date of last compilation.
- **date**: Used temporarily to sort by "date" field. Same as start
- **end**: RFC formatted date such as "1977-12-31" or the word "now" which renders the date of last compilation.

## glossary

``` json
    {
        "word": String,
        "category" : String,
        "description_link": String (absolute Url)
    }
```

## music

``` json
[
    {
        "artist": String,
        "title": String,
        "disabled": Option<boolean>,
        "release_date": String (DateFormat),
        "date_comment": Option<String>,
        "ytid": String (youtube id),
        "ntk": Option<String>
    }
]
```

- **date_comment**: Optional additional short information such as single, album or unknown but estimated release date.
- **ntk**: Optional 'Nice-to-know'. Short additional description or explanation. May include `<a>`-tag with escaped quotation marks.  

## recommendations

``` json
[
    {  
        "title": String,  // section, topic, subject
        "recommendation": [
            {
                "author": String,
                "title": String,
                "date": String (DateFormat / %Y),
                "source": Option<String (absolute Url)>,
                "format": Option<String (file extension)>,
                "iframe": Option<bool>
            }
        ]
    },
]
```

## testimonials

``` json
[
    {
        "quote"    : String,
        "date"     : String (DateFormat),
        "author"   : String,
        "reference": String (absolute Url),
        "original" : String (absolute Url)
    }
]
```

## world events (todo)

``` json
[
    {
        "name": String,
        "evt-short": String,
        "start": String (DateFormat),
        "end": String (DateFormat) or "now",
        "evt-vid": String (youtube id),
        "evt-vid-title": String,
        "more": Option<Entry>
    }
]
```

- **end**: RFC formatted date such as "1977-12-31" or the word "now" which renders the date of last compilation.
