---
title: Bericht von der Wahlkampfstandfrond
layout: default
category: phantasia
---

In der letzten Legislaturperiode wurde vom Senat in Berlin entschieden, die Zivilgesellschaft zu stärken, die Partizipation an der funktionierenden Demokratie auszubauen und Anreize für mehr Bürgerbeteiligung zu schaffen. Daher werden die Wahlen zum Abgeordnetenhaus und der Bezirksverordnetenversammlung nicht mehr wie bisher alle fünf Jahre, sondern seit 2021 alle 1,5 Jahre durchgeführt.

Nach den Wahlen im September 2021 finden die nächsten Wahlen bereits am 12.2.2023 statt. Die demokratischen Parteien in Berlin beginnen ihren dreiwöchigen Wahlkampf, Sie informieren die Berliner an ihren Wahlschlachtsständen.

Durch ihre sehr gute Arbeit der regierenden Parteien, aber auch der Opposition ist mit einem hohen Andrang an den Informationsständen zu rechnen. Um den wissbegierigen Berlinern trotzdem die Möglichkeit zu geben, sich ausgiebig zu informieren, habe ich Informationen eingeholt, um im Sinne der gefilterten Wissensvermittlung, um sich den Ärger zu ersparen.

### Professionelle Wahlwerbung

Ein CDU-Kandidat wirbt dafür, dass er Berliner ist. Na immerhin, ein Fortschritt. Weiterhin will die CDU Kriminelle häufiger in Haft bringen.

Die SPD um Frau Ex-Doktor werben mit dem in Berlin bereits eingeführten 29 €-Ticket. Zumindest gibt mir das Plakat das Gefühl, dass ich nicht allein bin mit dem Gefühl, in einer Zeitschleife gefangen zu sein.

Die Grünen werben mit Inhaltsleerem wie "Grün und gerecht" und der mit Hitlerbart verzierten Sympathieträgerin und Spitzenkandidatin.

Die FDP hat den wohl den besten Photoshopper der Stadt engagiert und hat den Spitzenkandidaten mal so richtig durchgepimpt. Sie fordern auf, nicht alte Probleme bei Neuwahlen zu wählen. Das hat sicherlich mehr als 400.000 € gekostet.

Volt erkennt, dass [es wichtigere Dinge gäbe, als gut designte Wahlplakate](https://www.imago-images.de/st/0197877305). Bei welcher Partei "Gute Politik" zu erwarten ist, verschweigt Volt jedoch.

Die AfD leistet sich wieder einmal einen Fauxpas und möchte "Klima-Kleber" in den Knast schicken. Dabei sind das doch alles "Klima-Kleb:ende". Wissen die nicht, dass jede Nacht das Klima für den nächsten Tag am Firmament über der Erde durch aufopfernde Aktivkohle-Engel neu angeklebt wird?

### An die Frond

Ich hole mir wie üblich einen Kaffee im Einwegbecher, natürlich mit Plastikdeckel, um damit Arbeitsplätze bei der örtlichen Stadtreinigung zu sichern. Ich schlendere damit über den Boxhagener Platz und sehe als Erstes den Stand der SPD und den Herrn Heinemann. Die Partei von Franziska "die Plagiatorin" Giffey, "Kein Kontakt zu Ungeimpfte [sic]"-Dilek Kolat und "Ich verbiete Demonstrationen wann ich will"-Andreas Geisel werde ich aufgrund des zu erwartenden Brechreizes auf den leeren Magen meines vulnerablen Körpers meiden. Mir ist noch der Schullehrer am SPD-Stand beim letzten Wahlkrieg (2021, vor 2G) in Erinnerung geblieben. Ich versuchte damals den Bildungsbürg:enden mit schon vorhandenen Infektions- und Todeszahlen zu überzeugen, dass wir in der Corona-Plandemie auf einem Irrweg sind. Ohne Erfolg. Auch Kinder müssten Maske tragen und sich impfen lassen. Auch wollte er nichts von sich inzwischen bewahrheiteten "Verschwörungstheorien" wissen, die ich so eindringlich aufzwang. SPD. Nein Danke.

Da weder Vertreter der CDU, der AfD, Volt oder anderer Parteien in der Nähe waren, beschränkte sich mein Frondeinsatz auf Unterhaltungen mit Vertretern der Die Linke, der Die Grünen und der Die Freie Demokratische Partei.

Als Erstes fällt mir auf, dass keine der Parteien sichtbar Prospekte oder Handzettel mit "gendergerechter Sprache" anbietet. Möglicherweise waren diese versteckt. Weder auf Transparenten, Tischen oder sonstigen Flächen waren die noch vor anderthalb Jahren so deutlich mit stolz gezeigten Gender-Sternchen zu sehen. Einzig ein Plakat eines Grünen-Kandidaten benutzt noch das Gender-Sternchen für die Ansprache des Wahlvolk:Innen. Die restlichen Plakate und Werbematerialien machen auf mich den Eindruck eines äußerst vorsichtigen Sprachgebrauchs. Bei den Grünen habe ich das Gefühl, einmal tief in die Floskeltonne gegriffen wurde und alles beworben wird, was irgendwie gut klingt, aber schön unverbindlich bleibt.

"Wollen sie ein besseres Leben, dann wählen sie mich!".

Ich interessiere mich ja eh weniger für die Positionen der Parteien. Diese sind ja hinlänglich bekannt. Alle wollen nur das Beste für uns. Zudem haben die Floskeln kaum und nach den Wahlen gar keine Relevanz. Ich merke gerade: Es hört sich an wie etwas, was ein Politikverdrossener sagen könnte. Was ist mit mir los? Gerade bin ich noch knapp dem Corona-Tod entkommen und jetzt schon politikverdrossen?

Ich spare mir hier die detaillierte Beschreibung der einzelnen Gespräche. Thematisch wie auch argumentativ waren kaum Unterschiede zwischen den Parteien und deren Vertretern zu erkennen.

### Berlin

Die aktuellen Berlin-Themen sind u. a.:

- Volksentscheid "Deutsche Wohnen enteignen"
- Verkehr
- Bau (Wohnungsmangel)

Thematisch ist hier nicht viel zu sagen. Der Volksentscheid, der knapp von den Berlinern unterstützt wurde und die Enteignung von Immobilienkonzernen mit mehr als 3.000 Wohnung zum Ziel hat, wurde ohne Gesetzentwurf eingebracht, sodass dieser nicht bindend ist, den Parteien aber für die nächsten Jahre Differenzierungsmöglichkeiten bietet, ohne sie in irgendeinerweise wirklich etwas Wichtiges entscheiden müssten.

Beim Verkehr ist teils der Bund zuständig. Die Erweiterung der A100 (Abschnitt 17) steht an. Von seitens der Gegner der A100-Erweiterung wird wohl ein schonungsloser Kampf der Aktivier:enden gegen den Bau zu melden sein. Um das Jahr 2030 wird dann gebaut.

Obwohl in der genauen Ausgestaltung von "Verkehrsthemen" wie Fahrradspuren und Parkplätzen die Bezirke entscheiden, ist die [Verkehrs- und Mobilitätswende](https://de.wikipedia.org/wiki/Verkehrswende) natürlich bereits auf anderen Ebenen entschieden und natürlich alternativ los. Also wenn man nicht die gesamte Menschheit ermorden möchte. "How dare you?"

Der Wohnungsbau ist seit jeher ein Topthema der Stadt. Es ist sicherlich älter als ich selbst. An der begrenzten Fläche, dem Zuzug von Neuberlinern und den Mietpreissteigerungen wird keine Partei etwas entgegensetzen.

Die FDP setzt auf Dachausbau, andere auf Wohnungsbau oder Enteignungen. Wohnungen werden bereits gebaut, Dachgeschosses ausgebaut und Enteignungen wird es ganz sicher irgendwann in irgendeiner der Form wie der [Rekommunalisierung] geben. Es wird halt wahrscheinlich ein wenig teurer als ursprünglich veranschlagt, aber das hätte ja keiner ahnen können. Verantwortlich wird die Vorgängerregierung sein.

Bei allen Parteien jedoch gleich, dass für alles, was in Berlin nicht funktioniert, wahlweise der Koalitionspartner oder die Opposition verantwortlich ist. So ist bei den Linken die SPD für die Bildungsmisere schuld und die Grünen für das rumgeierere beim Enteignungsvolksentscheid. Auf den Plakaten der Grünen zwar wird mit "DWE umsetzen"(Deutsche Wohnen enteignen, umsetzen!) geworben. Am Informationsstand wird jedoch herumlamentiert: "Ja, wie das nun dann wirklich umgesetzt wird, muss man dann mal sehen. Also enteignen, so ohne Geld geht ja nicht."

Wichtig war in den letzten Jahren aber auf jeden Fall das ganz schnelle Enteignen, damit die Wohnungsmarktkrise umgehend entschärft werden kann.

Für so eine Information hätte ich echt nicht an den Stand gebraucht. Warum tun die das?

### Gendern

Die Vertreter der Die Linke frage ich, ob sie noch immer gendern, da ich nichts dergleichen sehe. Sie bestätigen: seit 10 Jahren gendere man bereits. Ich erwidere, dass Sprache durchaus als Repression benutzt werden könne. Zum Beispiel indem man sprachliche Subjekte zu Objekten oder substantivierten Adjektiven degradiert. Z. B., wenn Mitarbeiter und Nutzer zu Mitarbeitenden und Nutzenden marginalisiert werden.

Als linke Proletarierin und Arbeiterin müsste das doch eigentlich wissen? Ich erkläre ihr, dass ich mich nicht durch gendengerte Wörter angesprochen fühle, und frage sie, wo der Vorteil darin liege, wenn eine Minderheit sprachlich befriedigt wurde, die größere Mehrheit sich darin nicht wiederfindet. Ich bin nun mal keine Kund:in oder Studier:ender, sondern (ein) Kunde und (ein) Student. Ein Arbeiter ist etwas anderes als ein Arbeitender. Sie erwidert, dass das Ziel nicht sei, Personen auszuschließen, sondern einzuschließen. Daran hatte ich bisher gar nicht gedacht.
Auf das Argument, das für die sprachexludierten Menschen galt, nämlich dass sie sich von bestimmten Bezeichnungen (insb. generische Maskulinum) nicht angesprochen fühlten, gilt für mich anscheinend nicht.

Warum innerhalb kurzer Zeit neue, so einschneidende Sprachregelungen ohne breiten gesellschaftlichen Diskurs durchgesetzt werden, wird bei der Unterhaltung auch nicht geklärt werden. Chlorhühnchen, Aufrüstung, Wäldchenrodungen, Tempolimit, Abtreibung, Homo-Ehe, Migration, Strafrechtsverschärfung, Überwachungsstaat, Freihandelsabkommen usw. werden teils Jahre, wenn nicht Jahrzehnte diskutiert. Ich erwidere erneut, dass ich nun ein Beispiel vieler Personen sei, die sich nicht inkludiert fühlen. Etwas muss bei ihrer Betrachtung des Themas schiefgelaufen sein. Sie würde doch nicht wissentlich die Exklusion großer Bevölkerungsteile in Kauf nehmen? Ich erwarte eine Antwort, die mich mitnimmt. Stattdessen versteht sie gar nicht, was "da jetzt so schlimm dran sein soll", sie wolle doch nur das Beste für uns.

Psychologisch betrachtet müsste ich wohl behaupten, dass sie emotional-empathische Defizite aufweist. Narzissten und Psychopathen, wie sie angeblich in der Politik häufig anzutreffen sind, haben ein einseitiges Verständnis. Dementsprechend trifft man mit den eigenen Positionen auf großes Unverständnis.

Bei der Die Linke (aber auch bei der die Grünen) fällt mir auf, dass kein einziges Mal Zweifel an der eigenen Position aufkommt. Meine Bedenken, Begründungen und Wünsche werden nicht an- oder aufgenommen, geschweige denn mental verarbeitet, stattdessen setzt sofort der Verteidigungsmodus ein. 

Hört ihr denn nicht meine Signale?

Ich dachte immer, ich sei der Wähler, dem die Parteien zuhören, daraufhin ihre Wahlprogramme anpassen und mir dann ein Angebot machen, damit ich sie wähle? Vielleicht habe ich da auch einfach was falsch verstanden. Kurz vor der Wahl ist Die Linke inzwischen gleichauf mit der AfD. Wollt ihr meine Stimme nicht?

Auf meine Frage: "Wie es denn sein kann, dass in dem so linken Berlin, wo RGR nun seit Jahren tolle Arbeit abliefert, nun die CDU mit Kai Wegner die stärkste Kraft ist?", antwortet der junge Linke Mann: "Naja, Berlin verändert sich halt ständig."

Mit Ausnahme von Eberhard Diepgen und den Politgrößen Richard von Weizsäcker, Hans-Jochen-Vogel und Walter Schreiber hat ausschließlich die SPD den Regierenden Bürgermeister in der Stadt gestellt. Seit 2003 sogar überwiegend mit den Grünen und der Linken zusammen. Vor ca. einem Jahr erhielt die AfD 8,0 % und liegt nun drei Wochen vor der Wiederholungswahl bei 11 %. Die CDU steigerte sich von 18,0 % auf 23 % und ist derzeit 2 % vor den Grünen. SPD, FDP und Linke verlieren.

Ich teile den Vertretern der Linken und der Grünen mit, dass ich diesmal wie letztens die AfD wählen werde. Dank der neuen Partizipationsmöglichkeiten durch den kürzeren Wahlzyklus bin ich jetzt sogar AfD-Stammwähler. Wer hätte das gedacht?

Die Linke erkennt in meiner Wahlentscheidung richtigerweise eine Protestwählerstrategie. Was hat das bloß zu bedeuten? Am Wählerschwund sind doch die Wähl:enden verantwortlich.

Ich füge hinzu, dass ich, obwohl ich letztens noch die Erststimme an die Linken vergab, dies diesmal nicht tun werde, nachdem der Vorsitzende der Linken in Berlin wenige Wochen vor der Wahl schnell noch ein Böllerverbot fordern musste. Nein, ich bin ganz und gar nicht links. Ich will nur die Grünen aus dem Bezirk haben und die Frau am Stand letztes Jahr hat auf mich bei all der Meinungsverschiedenheiten den "normalsten" und kompetentesten Eindruck gemacht. Mit Parteiprogramm hatte meine Entscheidung rein gar nichts zu tun. Ich verschenke meine Erststimme regelrecht. Man muss sie eigentlich nur einsammeln.

Die Linken-Frau versicherte mir, das sei ja nicht Parteilinie. Für mich, dem an Silverster-geborenen, ist das wenig überzeugend. Von Verbotsorgien habe ich wirklich genug.

Der Grünenvertreter versteht gar nicht, warum ich Faschisten wählen will. Zugegeben, die Unterhaltung mit ihm begann damit, dass er mir Werbematerial in die Hand drücken wollte und ich darauf antwortete: "Ich wähle keine Faschisten." Ich korrigiere mich. "Ihr seit keine Faschisten, Eure Methoden sind jedoch faschistoid und denen in einem Faschistischen System sehr ähnlich."

Auszug aus https://de.wikipedia.org/wiki/Faschismustheorie:

- der Totalitätsanspruch
- eine kulturstiftende, auf Mythen, Riten und Symbolen basierende, irrationale weltliche Ersatzreligion,
- eine korporative, hierarchische Wirtschaftsorganisation,
- sowie ein totalitäres, in Funktionshierarchien gegliedertes Gesamtmodell der Gesellschaft.

Außerdem, wenn die AfD wird genauso eine Zeitenwende durchmacht wie die SPD und die Grünen, dann ist die Aufregung doch umsonst.

Das Ganze erinnert mich ein wenig an langjährig Angestellte in einem Betrieb, der aufgrund seiner Größe anders als ein riesen Konzern in der Lage sein müsste, Informationen, Einwendungen, Ideen und Verbesserungsvorschläge aufzunehmen, zu analysieren und möglicherweise umzusetzen. Angestellte wirft regelmäßig seine Ideen in den Briefkasten, denn der Betrieb gibt sich modern und verspricht Prämien bei Ideenumsetzung. Es gehen zahlreiche Ideen ein, jedoch wird nichts und von niemanden ernst genommen oder umgesetzt. Überrascht sind jedoch die Vorgesetzten, wenn Angestellte meistens die Besseren kündigen. "Das hätte keiner ahnen können. Die Kündigung kam so unerwartet. Der hat doch immer gute Arbeit geleistet."

### Corona

Während bei den "leichten" Themen also eher auf Schuldzuweisung und Unverständnis traf, wurde bei den heiklen Themen wie: Ukrainekrieg und Corona bei allen drei Parteienvertretern überwiegend [bagatellisiert](https://de.wikipedia.org/wiki/Bagatellisierung).

Auf meine teils unfair-suggestiv gestellten Fragen ("Wurde bei Corona alles richtig gemacht? "), antworten alle ungefähr so:

- "Na ja, es musste schnell entschieden werden, und da ist sicherlich nicht alles gut gelaufen."

Mit der Ausnahme einer Gesprächspartnerin der FDP die ich später auf Stralau antreffe, enthielten die Antworten keinerlei Anzeichen für Mitgefühl für die Geschädigten "zweiten Ranges" (wie ausgeschlossene Ungeimpfte, Impfgeschädigte, Zweifler, Kinder, Bewohner in Altenheimen, Küsntler, Pfleger mit Arbeitsverbot, Sicherheitsbehörden die Maßnahmen durchsetzen mussten, Unternehmer, die ihre Existenz verloren haben usw.). Ich konnte auch keine Hinweise auf Einsicht erkennen. Das sei halt so. Keine der Antworten enthielt Zweifel oder hinterfragte die Gründe der Maßnahmen.

Ich höre heraus, dass alle Befragten die Maßnahmen grundsätzlich unterstützten und dies auch bei einer neuen Pandemie, nach welcher Definition auch immer befürworten würden. Es ginge schließlich darum, Infektionen zu verhindern.

Beim Vertreter der Grünen bin ich mir nicht sicher, ob er verstanden hat, was die Merkmale faschistoider Systeme sind. Ich stelle suggestive Fragen wie:

- Es war ja meine freie Entscheidung mich nicht impfen zu lassen?

Er bestätigt das, und hat deshalb keine Bedenken wegen der Grundrechtseinschränkungen im Zuge der Corona-Plandemie. 

Ich sage ihm, dass ich in Folge der Corona-Maßnahmen mein Studium aufgeben musste, ich nicht mehr mit der BVG fahren durfte, nicht auf meine Arbeitsstelle durfte, mein Gehalt einbehalten wurde und ich nun vor der Räumung meiner Wohnung stehe. Seine Zwischenantworten waren: "Nein, musstest Du nicht.", "Wurde es nicht" und "Ist sie nicht."

Bei der FDP merke ich sofort, dass sich zwei zur Wahlsteh:enden von der Diskussion abwenden, sobald ich Corona ansprach. Der Hauch eines Schwurblers schwebt immer mit. Danke Deutschland.

Die eine blondhaarige, wieder in die Partei, kann mich in vielen Punkten verstehen und erzählt, wie sie die letzten Jahre erlebt hat. Sie sagt, dass sie das alles sehr fertig gemacht hat. Insbesondere in der Zeit ab November 2021.

Mir ist das schon fast unheimlich. Ich zweifle regelrecht an ihren Aussagen. "Ob, sie mir das alles nur erzählt, damit ich höre, was ich hören will und soll, oder meint sie das ernst?". Was ist mit mir passiert? Ich zweifle nun jede Aussage an. Sie kommt mir vor wie eine gut geschulte Kommunikatorin.

Nach den vielen Narrzisten und Psychopathen und eindeutig mental geschädigten Personen, die ich die letzten Jahre in den Medien bei Politik, Prominenz und im direkten Umfeld erleben musste, kommt sie mir vor wie eine Außerirdische.

Ich werde den Gedanken nicht los, dass die Ganze Sache um Corona nur eine Ouvertüre war. Nahezu alle Parteien unterstützten die rigorosen Maßnahmen. Niemand scheint beim nächsten Mal etwas anders machen zu wollen.

- Schwere Grundrechtseingriffe
- eine perfekt orchestrierte Hetz-Kampagne gegen ein viertel der Bevölkerung,
- das Sperren von Parkbänken
- Ausgangssperren
- Arbeitsverbote
- Verschuldung,
- nachbarschaftliche überwachen
- das Verbot sterbende in Krankenhäusern zu besuchen
- das abwiegeln jeglichen Zweifels
- die damit verbundene Cancel-Culture
- die Finanzierung von winzigen Biotech-Klitschen aus Mainz
- das Durchwinken bzw. Nichtverhandeln von Grundrechtseingriffen beim Verfassungsgericht
- das Bundeslandübergreifende Reiseverbot
- eine durch das Parlament ermächtigte Ministerpräsidentenkonferenz

Wenn Corona ein Notstandfall darstellt

Der [Verfassungsblog](https://verfassungsblog.de/covid-19-und-das-grundgesetz/) schreibt zwar, dass das Grundgesetzt nicht gut auf einen Notfall wie die Plandemie gewappnet ist. Anders als erhoff argumentiert der Blog ganz anders als ich es mir wünsche. Es wird bemängelt, dass eine in Notfällen notwendige Kompetenzkonzentration nur unzureichend möglich sei, und das Verfahrensvereinfachungen für die obersten Verfassungsorgane nur für den äußeren Notfall möglich sind. Es heißt also nicht, dass die Politik daran arbeitet, das Grundgesetz vor zukünnfigen Mißbrauch zu schützen. Vielmehr deutet es darauf hin, dass die Hindernisse für eine Machtkonzentration abgebaut werden sollen.

In einer alternden Gesellschaft, in der bis 2030 mindestens 5 Millionen Menschen mehr als heute über 60 Jahre sind, werden zwangsläufig auch mehr Menschen an Infektionskrankheiten sterben. Aber Fakten gelten nicht mehr. Wer am gesellschaftlichen Leben teilnehmen möchte, ist geimpft, trägt seinen digitalen Impfpass als Zugangskontrolle, demonstiert nicht, kritisiert nicht und fordert  Klimakrise?

Ich kann mir nicht vorstellen noch einmal in eine Kneipe oder in ein Restaurant zu gehen von dem ich weiß, dass der Eigentümer an der Türe digitale Impfpässe kontrolliert hat. Ich möchte auch nichts mehr mit Menschen zu tun haben, die nach mehreren Jahren keinerlei Zweifel an der Sinnhaftigkeit verschiedener Maßnahmen hatten.

Ich erzähle der Frau von der FDP, dass mich das Vorgehen der Regierung und breiter Teile der Gesellschaft geschockt hat und das Verhältnis zwischen mir und dem Staat nachhaltig geschädigt ist und ich gerne das Land verlassen würde und mir auch nichts mehr an meiner Staatsbürgerschaft liegt.

Sie meint, es sei sicherlich die falsche Entscheidung. Vielmehr müsse die Debattenkultur verbessert werden und mehrere Meinungen müssten zu Wort kommen. Aber liegt hier doch das Problem. Ist es nicht so, dass ein großer Teil der Bevölkerung zwar reden darf, aber nicht gehört wird? Laut Medien, Politik und NGOs werden alle Meinungen ja gehört und einbezogen.

Es ändert aber nichts am Durchziehen einer Agenda, die keinen Widerspruch duldet. Was ich heraushöre: "Ja, lass die mal quatschen, dann sind sie eine Weile beschäftigt."

Die (staatlichen) Medien haben ihren Auftrag natürlich erfüllt. Wie von ihnen erwartet wurde von Beginn an von oben nach unten kommuniziert. Die breit angelegte "Informationskampagne" hat Abweichler deutlich markiert und für den Rest der Plandemie außer Gefecht gesetzt. Zum Abschluss werden sie sich natürlich an einer Aufarbeitung beteiligen. Den doofen Michel zur Debatte einladen und und hier und da einmal eingestehen müssen, dass nicht alles gut lief. Aber man dürfe auch nicht die besondere Situation vergessen. Naja, beim nächsten Mal wird alles besser.

Ich halte das nicht für normal.

### Krieg gegen den Terror und russischer Angriffskrieg auf die Ukraine

Beim Ukrainekrieg besteht der Grünenvertreter auf Bewaffnung der Ukraine und auf Sanktionen Russlands, "bis, die Krim zurückerobert ist".

Beim Ukrainekrieg besteht der Grünenvertreter auf Bewaffnung der Ukraine und auf Sanktionen Russlands, "bis, die Krim zurückerobert ist".

Ich muss ihn natürlich auf die "Kriege gegen den Terror" der Amerikaner im Nahen Osten (Afghanistan / Irak) ansprechen. Hier fordert er keine Konsequenzen. Es sei damals sicherlich nicht alles richtig gelaufen. Die Amerikaner hätten aber bereits mit einem Ansehensverlust dafür bezahlt. Außerdem sei der Krieg ja schon 20 Jahre vorbei.

Der Grünenvertreter preist sogar stolz die Scholz'sche Zeitenwende an, als ich ihn auf den Slogan "Keine Waffenlieferungen in Krisengebiete" anspreche.

Ich wette, der junge Mann hat noch nie eine Pistole in der Hand gehalten und auch noch nie ungefilterte Bilder aus einem Kriegsgebiet gesehen. Er hat wahrscheinlich auch noch nie von Leuten gehört, die sobald sie eine Waffe in der Hand halten anfangen zu weinen und zu zittern oder sich umgebracht haben, weil sie niemanden töten wollen, nachdem sie an die Front geschickt wurden. Ihm ist sicherlich auch egal, wie die wahrscheinlich überwiegend männlichen Soldaten aus einem Einsatz zurückkehren. In den hochgelobten Vereinigten Staaten von Amerika sind über 10 % der fast 700.000 [Obdachlosen](https://en.wikipedia.org/wiki/Homeless_veterans_in_the_United_States), ehemalige Soldaten. Da ist halt einfach nicht alles richtig gelaufen. Naja, benachteiligte deutsche Frauen wird es ja nicht treffen.

Ich verstehe, dass nicht jeder ein Buddha oder ein Jesus ist und Krieg ablehnt. Ich verstehe aber nihct, wie Juristen, Starbucks-Barrista und all die anderen laktoseintoleranten Veganer, und ach so harten Jungs der Waffenliefernden so über fremdes Leben entscheiden können.

Warum melden sie sich nicht freiwillig?

Keiner konnte mir bisher erklären, welche Waffen und wieviel Soldaten wirklich benötigt werden, damit die Ukraine den Krieg, gegen die fünft größte Armee der Welt gewinnen kann und mit wieviel Toten man rechnet, bzw. wieviel Tote für den Endsieg eingerechnet sind. Bei einer Bevölkerung der Ukraine von ca. 40 Millionen, dürfte die Hälfte, also ca. 20 Millionen männlich sein. Das mittlere Alter dürfte so um die 40 liegen. Eventuell etwas jünger. Das heisst mindestens die Hälfte der Männer sind in einem wehrpflichtigen und einsatzfähigen Alter. Also ca. 10 Millionen.

Die FDP ist ein wenig differenzierter. Wohl wird auf die eindeutige Position von Marie-Agnes Strack-Zimmermann verwiesen, auf meine Aussage, dass ich eher mit der Position von Erich Vad (Brigadegeneral a. D.) übereinstimme und der Meinung bin, dass für die Waffenlieferungen an die Ukraine erst einmal ein Ziel definiert werden müsse, kommt ein wenig Meinungsaustausch zustande. Ich argumentiere, dass Russland für die nächsten 30 Jahre ausreichend Waffen besitze und ausreichend neue beschaffen könne, um den Krieg unendlich weiterzuführen. Solange kein Ziel definiert ist, das erklärt, was die Waffenlieferungen bewirken sollen, wird der Krieg nicht gelöst werden. Ist das Ziel die Rückeroberung der besetzten Gebiete nur der Krim, nur der Festlandgebiete? Wie viele Waffen und ukrainische Soldaten benötigt es, damit die besetzende Partei nachhaltig geschlagen werden kann und die Grenzen gesichert werden können. Soll die Ukraine nur das mindeste erhalten, damit der Krieg möglichst lange weiterläuft und damit [Blackrock](https://www.faz.net/aktuell/wirtschaft/milliardaere-wollen-wiederaufbau-der-ukraine-finanzieren-18474728.html) und die EU ein besseres Geschäft machen können, dann ist die derzeitige Mindestlieferungstaktik durchaus richtig.

Sicherlich ist es keine gute Idee der Ukraine keine Unterstützung anzubieten, nachdem man gerade noch ein Assiziierungsabkommen unterzeichnet hat, von dem Kritiker damals behaupten, es würde die Ukraine zerreissen und zu einem Krieg führen. Es kann aber auch nicht im Interesse sein, wissentlich Ukrainer in den Tod zu schicken, wenn doch absehbar ist, dass mit ein paar Panzern kein Land zurückerobert wird. An der Frontlinie hat sich seit April nicht mehr großartig viel getan.

Wie viele tote ukrainische Soldaten sind akzeptabel für einen Sieg?

Ich bemerke wie selbst die FDP medial indoktriniert ist. "Ein Diktatfrieden Russlands" wird es nicht geben wird mir gesagt. Es fehlt nur noch "der russische Angriffskrieg".

Ich erwidere, dass ich altmodisch zuerst auf eine Waffenruhe und dann auf Verhandlungen setzen würde. Diese Herangehensweise ist jedoch niemandem mehr bekannt.

Ich erzähle der Frau, dass ich bei einer unwahrscheinlichen Eskalation, als Reservist große Probleme hätte von ihnen in den Kriegsdienst geschicklt zu werden, während ukrainische und russische Flüchtlinge in Berlin mit Wodka auf bessere Zeiten anstoßen.

Trotzdem wünsche ich mir manchmal eine militärische Eskalation, nur um zu sehen wie die ganzen Weltverbesserer sich in die Hosen scheißen und schnell unabkömmlich werden.

Ich bedanke mich für den Zitronen-Tee der FDP und setze mich auf die nahegelegene Parkbank und warte auf meine fliegenden Freunde, dem Krähempärchen.

**Prognose**

Ich gehe davon aus, dass bis April die meisten Kampfhandlungen beendet sind. Zelensky wird wie alle vorherigen Partner der europäischen Länder und den Vereinigten Staaten von Amerika fallen gelassen, in dem große Waffenlieferungen ausblieben. Meine Annahme, dass keine Kampfpanzer geliefert werden hat sich ja bereits bestätigt. Weder die EU-Länder noch andere Staaten werden für die korrupte Ukraine, die sie ist in den Krieg mit einer Atommacht ziehen. Die Gründe für mögliche Konflikte waren bereits vor Jahrzehnten bekannt. Spätestens mit dem Zusammenfall der Sowietunion, der NATO-Erweiterung und der Isolierung Russlands war es nur eine Frage der Zeit.

Die EU und die Amerikaner werden der Ukraine den Frieden diktieren. Somit haben alle heutigen Kriegstreiber rechtbehalten, die heute sagen, dass es keienn russischen Diktatfrieden geben wird. Offiziell wird natürlich nicht von einem Diktat gesprochen, schließlich obliegt es ja der Ukraine das Angebot, dass sie nicht ablehnen können zuzustimmen oder weiterzukämpfen.

Die deutsche Wirtschaft freut sich auf die zukünftigen Geschäfte, die sie in der Ukraine machen kann. Die Europäer werden sich ein weniger einfacher tun, wenn sie einen verkleinerten Rumpfstaat Ukraine an die EU heranführen. Nach einer Zeit des Waffenstillstands werden EU-Länder Sicherheitstruppen in die Ukraine schicken. Ein Vertrag verhindert, dass die Amerikaner in der Ukraine stationiert werden.

Zwar ist die EU auch immer ein Vehikel für die Amerikaner und umgekehrt. Russland wird das aber sicherlich so akzeptieren. An der derzeitigen Grenzziehung wird sich kaum was ändern. Die russische Krim, die nach heutigen Maßstäben völkerrechtswidrig durch Kurstchow verschenkt wurde, bleibt natürlich russisch.

Der 70 jährige Putin dankt irgendwann als russicher Held ab. Grüne, die bis dahin nicht mehr regieren werden, werden weiterhin Sanktionen fordern. Friedrich Merz und Blackrock sagen nein.

Eine Wiederaufnahme der Wirtschaftsbeziehungen wird angesichts der zukünftigen schwierigen Aufgaben alternativlos bleiben. Selbstverständlich ist die Ukraine zu dem Zeitpunkt noch kein Mitglied der EU.
