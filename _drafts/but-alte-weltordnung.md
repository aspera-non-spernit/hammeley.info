---
title: Bild und Ton der Alten Weltordnung
layout: playlist
permalink: /bild-und-ton/alte-weltordnung
era-short: alte-weltordnung
filters: "date:1977-12-31:1989-11-09"
---
{% assign era = site.data.era | find: 'era-short', page.era-short %}

**Hinweis: Die folgenden Videos können Spuren von Punks, nach Bier stinkenden und heldenhaften Männern und Synthie-Pop enthalten.**

<!--

filters: comma separated list of filters. ie.: filters: "date:1977-12-31:1989-11-09, genre:pop, exlc_disabled:true"


-->