---
title: Schwurbelscience
layout: default
category: ego
---

Heute: Lebenslagen

Die Begriffe Inklusion und Exklusion werden in der Sozialforschung verwendet, um die Lebenslage von benachteiligten, psychisch kranken Menschen zu beschreiben, indem es die Perspektive an sozialer Teilhabe in Teilsystemen erfasst. Dabei werden unter Inklusion die Teilhabe und unter Exklusion der Ausschluss an Teilhabe verstanden. Eine Exklusion kann zum Beispiel auf dem Arbeitsmarkt erfolgen, z. B. indem Menschen, die auf Einhaltung ihrer Grundrechte bestehen, der Zugang zur Arbeitsstätte wegen in Kraft getretener 2G Regelungen verwehrt wird. Eine weitere Exklusion entsteht, wenn die betroffene Person, also der Arbeit verweigernde und damit psychisch kranke Delinquent aus dem sozialen Hilfesystem keine Hilfe erhält.

Die Exklusion aus den Teilsystemen wie Arbeitsmarkt, Sozialsystem und der Wohnung oder dem Umfeld als Grundbedürfnis des Menschen führt zu einer Verkleinerung des persönlichen Spielraums, welcher in verschiedenen Dimensionen beschrieben werden kann:

- der Versorgung- und Einkommensspielraum
- der Kontakt- und Kooperationsspielraum
- der Lern- und Erfahrungsspielraum
- der Muße- und Regenerationsspielraum und
- der Dispositionsspielraum

Je kleiner die Spielräume, desto weniger können die Lebensziele oder Grundanliegen erfüllt werden, die angestrebt würden, wenn externe Faktoren die Erreichung der Ziele nicht behindern würden.

Die Exklusion kann so umfassend sein, dass der Zugang zu Leistungen zur Lebensführung nicht mehr zugänglich ist.

Wegen entstandener Mittellosigkeit häufen sich Schulden an und Wohnungslosigkeit droht. Auf angespannten Wohnungsmärkten, wie zum Beispiel in Berlin, werden diese auffälligen und kooperationsunwillig gewordenen Personen obdachlos.

Diese Lebensumstände können lebensbedrohlich werden, zum Beispiel durch Verhungern, Erfrieren oder durch Suizid.

Sofern noch Zugang zu einem oder mehreren Teilsystemen besteht, zum Beispiel Kontakt zu Familie oder Kooperationspartnern, geht auch dieser wegen nicht vorhandener Ressourcen allmählich verloren. Sobald sich der Ausschluss aus allen Teilsystemen verfestigt hat, beginnt ein Teufelskreis, aus dem kaum noch herausgefunden werden kann. Die betroffenen Personen nehmen ihr Schicksal hin und führen damit die Exklusion selbst fort. Die häufig alleinstehenden Obdachlosen sind völlig isoliert und haben keinerlei sozialen Kontakte mehr.

Das weitere Leben ist durch ständige Mangelsituationen gekennzeichnet, indem alle Grundbedürfnisse täglich zur Disposition stehen.

Die Lebenslage führt in fast 10 % zum Suizid. In viel größerem Maße finden sich bei diesem Personenkreis Herz- und Gefäßkrankheiten und Intoxikationen. Bei fast einem Viertel kommt es zu einer Alkohol- und Drogen induzierten Fettleber.

Wohnungslose oder Obdachlose weisen eine deutlich höhere Prävalenz an psychischen Störungen und anderen Krankheiten auf.

In vielen Fällen, wie dem hier genannten krankhaften Bestehen auf Grundrechte sind diese erhebliche Delinquenz und andere psychische Vorerkrankungen und Charakterzüge die Ursache für die selbst verschuldete Situation, die in einem Kreislauf zu Arbeitslosigkeit, Armut, sozialer Ausgrenzung, Isolation, Verschlechterung der Symptomatik und neuer oder stärkerer psychischer Probleme führt.

So sollen auch von Wohnungslosigkeit bedrohte Delinquenten vom sozialpsychiatrischen Dienst unterstützt werden, anstatt die ursächlichen Probleme der Exklusion zu beheben.

Komorbiditäten sind bei besagtem Personenkreis üblich. Neben der Delinquenz, sich nicht impfen lassen zu wollen, sind auch andere soziale Defizite erkennbar. Andauernde Arbeitslosigkeit oder mangelnde Mietvertragsfähigkeit sind häufig Begleiter dieser Personen. Ist dies nicht der Fall, wird dafür gesorgt, dass es so kommt. Arbeitsverbote für delinquente Lauterbachverweigerer führen zum Ausbleiben des Gehalts, Aufstiegsbarrieren (nur bei Minderheiten und Frauen) und die freie Wahl prekärer Arbeitsverhältnisse mit geringem Einkommen können im Notfall wegen Mittellosigkeit und eingeschränktem Dispositionsspielraum zu Zahlungsausfällen und schließlich zur Mietvertragsbrüchigkeit und den ersten psychischen Auffälligkeiten durch andauernde Stresssituationen und Existenzängsten bei stetig fallender Stressresilienz führen.

War die betroffene Person bis zum Beginn der Obdachlosigkeit wenig psychisch auffällig, so werden psychische Krankheiten im Laufe des weiteren Lebensweges der Quertreiber zum chronischen Begleiter.

Zudem kommt die Stigmatisierung in interpersonellen Situationen dazu. Kaum jemand führt einen Small-Talk mit einem sichtbar Obdachlosen. Die Darstellung verarmter Personen im Fernsehen oder Diskussionen über asoziale Arbeitsverweigerer in der Politik führen zu einem stigmatisierten Umgang mit dem betroffenen Personenkreis.

Frauen haben es meist etwas einfacher als Männer, da diese traditionsbehaftet häufig noch als vulnerabel gelten, die durch den alten weißen Mann missbraucht wurden. Männer sind hingegen in der Regel immer selbst verschuldet in ihre Situation geraten und hatten nie Schicksalsschläge in ihrem Leben.

Unterwürfige Frauen finden auch dann häufiger eine neue Bleibe, wenn sie psychisch auffällig sind. Meistens bei irgendeinem Stecher, der sich solidarisch zeigen möchte. Deshalb finden sich mehr Männer als Frauen auf der Straße. Die unterwürfige Natur der Frauen hilft auch dabei, erst gar nicht in bedrohliche Lebenssituationen zu kommen. Frauen riskieren lieber eine Thrombose durch mittelbaren Impfzwang und tun, was die Führ:enden befehlen. Der selbstdenkende (neudeutsch: schwurbelnde) alte weiße Mann steht sich bei der allumfassenden Staatsbetreuung hingegen selbst im Weg.

Am Ende landen die Personen in einem Umfeld, aus dem sie nicht mehr herauskommen. Sei es das Wohnheim, die Psychiatrie oder das Justizsystem, aus keinem dieser Unterstützungseinrichtungen kommt die betroffene Person unbeschadet heraus. Der alte weiße Mann ist endlich die Person, die er schon immer sein sollte.

Ich plädiere deshalb dafür, Menschen bei ihrem Wunsch nach dem Tod aktiv zu unterstützen. Mehr Suizide unter Obdachlosen machen mehr Platz auf den Straßen, für Lastenfahrräder oder für Möbel, die zu verschenken sind. Weniger potenzielle Nutznießer des medizinischen Notfalls sparen Kosten im Krankensystem und bei den Hilfeeinrichtungen ein. Die frei gewordenen Mittel könnten besser in die Rettung der verbliebenen Menschheit durch Klimaschutz gesteckt werden.

Die immer wieder auftretende Wohnungsknappheit in deutschen Städten würde so rasch gelöst. Alle etablierten Parteien im demokratischen Spektrum könnten dies als Erfolg für sich verbuchen.

Vielleicht sollte sich die Bundesregierung das "Neue zukunftsweisende Verhältnis zwischen  Bürg:enden und Staat" das im [Kommunikationspapier](https://www.abgeordnetenwatch.de/sites/default/files/media/documents/2020-04/bmi-corona-strategiepapier.pdf) (Achtung: kann Brechreiz verursachen) zur Bewältigung der Corona-Plandemie auf Seite 17 angeregt wurde, zu Herzen nehmen und alle unliebsamen Personen einsperren oder gleich ganz liquidieren?

Aber was weis der alte weiße Delinquent denn schon?
