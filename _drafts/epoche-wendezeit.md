---
title: "Die Wendezeit"
layout: epoche
permalink: /wendezeit
era-short: wendezeit
---
{% assign era = site.data.era | find: 'era-short', page.era-short %}
**Hinweis: Folgende Videos können Spuren von Haut, athletischen Körpern, "weiße" Hautfarbe von Männern und Frauen nach traditioneller Definition enthalten.**

Fernsehsendungen wie [Baywatch](https://www.youtube.com/watch?v=wgnvGifrRoY) in der zahlreiche leichtbekleidete und gutaussehende Männer und Frauen (nach traditioneller Definition) gezeigt wurden, wurden damals auch von häßlichen Menschen, wie man sie damals nannte geschaut. Aber auch die Serie ["Eine schrecklich nette Familie 1987-1997](https://www.youtube.com/watch?v=wN3EP_-fAfT8) war ein großer Erfolg. Der Protagonist Al thematisierte hier des öfteren die ungesunde Ernährungsweise junger und alter weißer Frauen und war dahingehend den Weg für den Veganismus bereitet.

Musikalisch waren die 90er Jahre des eine Zeit der Experimente. Viele neue Musikrichtungen enstanden