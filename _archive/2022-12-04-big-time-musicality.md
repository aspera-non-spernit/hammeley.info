---
title: Big Time Musicality
category: music
layout : default
---

I can sense it  
Something important  
Is about to happen  
It's coming up  

It takes courage to enjoy it  
The hardcore and the gentle  
Big time sensuality  

We just met  
And I know i'm a bit too intimate  
But something huge is coming up  
And we're both included  

It takes courage to enjoy it  
The hardcore and the gentle  
Big time sensuality

I don't know my future after this weekend  
And I don't want to  

It takes courage to enjoy it  
The hardcore and the gentle  
Big time sensuality  

<div class="ratio ratio-16x9">
    <iframe src="https://www.youtube-nocookie.com/embed/IgmFmxnKTFc" title="Björk - Big Time Sensuality (The Fluke Minimix)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

Dieser Fluke-Remix erinnert stark an Fluke's <a href="https://www.youtube.com/watch?v=UiocUhpnBmk">Zion</a> aus dem Jahr 2003.
