---
title: "Das Dilemma der freiwilligen Unterwerfung"
category: "phantasia"
layout: default
---

"Dies ist der vierte Teil der Serie “Morgenröte der Freiheit”. Lesen Sie auch Teil 1, Teil 2 und Teil 3. In Teil 5 wird es darum gehen, wie man parallele Strukturen baut."  

**Was wäre, wenn der Mensch mal an sich selbst glaubte?**

> «Der Mensch ist frei geboren und überall liegt er in Ketten», notierte der Genfer Philosoph Rousseau am Anfang seines Traktats «Über den Gesellschaftsvertrag». Frei und zugleich in Ketten? Ein seltsamer Zustand, außer wohl für Rousseau, bei dem eher die Gesellschaft den Einzelnen gestaltete, statt umgekehrt. Oder kann man theoretisch frei und praktisch unfrei sein? ..

von Milosz Matuschek

Den gesamten Artikel und die anderen Teile der Serie findet man hier: [Was wäre, wenn der Mensch mal an sich selbst glaubte?](https://www.freischwebende-intelligenz.org/p/was-ware-wenn-der-mensch-mal-an-sich)

gefunden auf: [www.freischwebende-intelligenz.org](www.freischwebende-intelligenz.org)
