---
title: Mont Klamott
category: music
layout : default
---
Mitten in der City zwischen Staub und Straßenlärm  
Wächst ne grüne Beule aus dem Stadtgedärm  
Dort hängen wir zum Weekend die Lungen in den Wind  
Bis ihre schlappen Flügel so richtig durchgelüftet sind  

Neulich sitz ich mit 'ner alten Dame auf der Bank  
Wir reden über dies und das, da sag ich, Gott sei Dank  
Da ist ihnen mal was eingefallen, den Vätern dieser Stadt  
Dass unsereins 'n bissel frische Luft zum Atmen hat  

Mont Klamott auf'm Dach von Berlin  
Mont Klamott sind die Wiesen so grün  
Mont Klamott auf'm Dach von Berlin  
Mont Klamott sind die Wiesen so grün  

Die alte Dame lächelt matt  
Lass sie ruhn, die Väter dieser Stadt  
Die sind so tot seit Deutschlands Himmelfahrt  
Die Mütter dieser Stadt hab'n den Berg zusamm'gekarrt  

Mont Klamott auf'm Dach von Berlin  
Mont Klamott sind die Wiesen so grün  
Mont Klamott auf'm Dach von Berlin  
Mont Klamott sind die Wiesen so grün  

Mont Klamott auf'm Dach von Berlin  
Mont Klamott sind die Wiesen so grün  
Mont Klamott auf'm Dach von Berlin  
Mont Klamott sind die Wiesen so grün  

Mont Klamott auf'm Dach von Berlin  
Mont Klamott sind die Wiesen so grün

<div class="ratio ratio-16x9">
    <lite-youtube videoid="PbLFg2lIgho" playlabel="Silly - Mont Klamott" params="controls=1"> </lite-youtube>
</div>
