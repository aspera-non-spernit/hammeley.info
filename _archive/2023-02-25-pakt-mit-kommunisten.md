---
title: Teufelspakt mit Kommunisten
layout: default
category: phantasia
---

Der ukrainische Feldherr Wolodywihna Selenski hat entgegen der Anordnungen der freien Welt und ihrer Wahrheit verkündenden Medien einen Gipfel unter Führung der kommunistischen Chinesen in Betracht gezogen.

Während sich am Freitag Nachmittag ca. 170 friedensgeile Schwurbler in Münster getroffen haben, am Samstag weitere 100 Putinknechte in Berlin zusammenkommen sind, um sich symbolisch in Ketten zu legen, und die und von der Rechtsextremistin Sarah Putinknecht organisierten Veranstaltungen wenig besucht sind, bringt Feldherr Wolodywihna Selenski derweil die Planung des Westens und der ganzen freien Welt durcheinander.

"Es wird keinen Dikaktfrieden in diesem Angriffskrieg des imperialisitischen Russland geben", war bisher die veröffentlichte Meinung der Staats- und Parteimedien. "Warum soll die Ukraine denn an einen Verhandlungstisch mit dem nach einem ethnisch sauberen strebenden und imperialistischen Russland? Unsere tollen Panzer sind doch noch gar nicht geliefert, das macht doch gar keinen Sinn jetzt die verhandlungen zu beginnen. Wie sollen wir denn da behaupten können, dass unsere Waffen den Krieg beendeten", fragte vor ein paar Wochen noch der Witzeerzähler vom rbb, Faschissio Schröhter. Der SPD-Vizepropagandist und lobotomierte Olesxandr Lobotov erkannte ganz richtig: "Friedensdemos sind Einladungen an Mörder und Vergewaltiger so weiter zu machen wie bisher.

Nachdem die chinesische Regierung mit einem lächerlichen 12-Punkte-Plan an die Öffentlichkeit ging, um die wehrtechnischen Materialtests in der Ukraine zu beenden, denkt Selenski nun wirklich laut über einen Gipfel unter kommunistischer Führung nach. Es sei zwar kein echter Friedensplan und nicht alle Punkte können von uns akzptiert werden, aber die Einstellung der Kampfhandlungen bis April haben nun mal Priorität.

Während die Mehrheit der Deutschen das ähnlich sieht, macht sich Unverständnis bei den Guten breit: "Dass Selenski uns jetzt so in den Rücken fällt, ist mit nichts zu entschuldigen. Es ist unsere Freiheit, die in der Ukraine verteidigt wird, es sind unsere Waffen unserer Kriegsindustrie mit denen die Ukrainer den Krieg weiterzuführen haben und jetzt geht dieser friedensgeile Schauspieler mit den Kommunisten ins Bett. Diese Nazis. Ich distanziere mich hiermit ganz klar und deutlich von all meinen vorherigen Aussagen", sagt eine hochrangige grün-braun verwelkte Politikbetreib:ende, die nicht genannt werden möchte.

Bei den Meinungslenkern ist ebenso der Unmut zu spüren: "So schnell wie sich hier die Lage verändert gehen uns langsam die Floskeln aus. Was soll jetzt noch kommen? Pazifistische Kommunisten retten uns vor dem Dritten Weltkrieg?", hört man es aus den Volkserverhetzenden Redaktionen.

Frederic March, Atlantikbrückenbauer und ehemaliger Chef der Investorengruppe Schwarzfels hat einen Krisenstab eingerichtet: "Unsere Investoren gehen von einem ROI von über 9000 % aus. Wenn die Kriegshandlungen, ich meine die wehrtechnischen Materialtests jetzt schon enden, dann geht die Rechnung ja gar nicht mehr auf. Um die versprochenen Profite zu garantieren, ist eine Zerstörungsquote von 88,18% nötig, das ist derzeit noch nicht gegeben. Es ist uns so unmöglich allen Investoren den versprochenen Profit zu garantieren. Ich sehe keinen anderen Ausweg als dass der Staat für den uns in Aussicht gestellten, aber nicht realisierten Gewinn aufkommt. Es ist aber auch gut möglich, dass wir uns eine alternative Investitionsmöglichkeit suchen. Haben nicht die Polen Nordstream gesprengt? Ich werd' noch einmal mit Joe telefonieren, damit er das richtige Dossier veröffentlicht, damit unsere feministische Kriegsministerin den Anschluss legitimieren kann."

# "Regierungsbeteiligung ist auch Solzialmaßnahme"

Franziska Giffey ist außer sich und verwirrt, dass sie gerade ungekannten [Hass und Hetze](https://www.berliner-zeitung.de/news/franziska-giffey-erlebe-im-moment-ungekannten-hass-und-hetze-li.321709) erlebe: "Das ist für mich unbegreiflich, wie mir so der Hass entgegen kommt, ich bin doch geimpft und die Bürgermeisterin der Stadt. Außerdem dem dürfen nur die Guten hetzen, das war doch bisher Konsens."

Hauptfaschist und ehemaliger Senator für Stadtsicherheit Geisel soll Medienberichten nach unter keinen Umständen an einem neuen Senat beteiligt werden: "Der dumme Stasi-Ossi hat seine Schuldigkeit getan. Es ist den Berlinern nicht zu vermitteln, dass neben mir noch ein zweiter, korrupter Fascho im Sozenmantel in der Regierung bleibt. Also entweder er oder ich muss gehen. So oder so. Und ich muss ja bleiben, wegen der Pension und so, wissen sie?"
