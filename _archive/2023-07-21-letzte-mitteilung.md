---
title: Letzte Mitteilung
layout: default
cipher: hacker
---
23.7.2023 11:40

Eigentlich ist die Seite noch längst nicht fertig, ich habe aber keine Lust mehr und die Zeit läuft mir aber davon. Meine Möglichkeiten sind sehr eingeschränkt. Es ist aber auch bereits alles schon mehrmals von vielen gesagt worden, nur eben von den Falschen, zu denen ich auch gehöre. Dem habe ich nichts hinzuzufügen.

Ich halte es daher kurz:

Ich bin zu dumm für diese schöne, neue Welt. Nicht nur das. Ich bin eine Gefahr für die Menschheit, denn ich bin ungeimpft. Wenn alles gut läuft, habe ich es bereits heute oder die nächsten Tage hinter mir und ihr seid endlich frei und könnnt die Schöne Neue Welt genießen. Wahrscheinlich auf die Mädchenweise mit Vodka zum Mutantrinken und den aufgeschlitzten Pulsadern. An anderes kam ich nicht ran. Weder Pistole noch Heroin. Menschenwürdige Medikamente sind nicht verfügbar. Methoden, die meinen Körper verunstalten, wie der Sprung vor den Zug habe ich immer ausgeschlossen, obwohl ich ja jetzt auf dem Bahnhof wohne. Vodka habe ich gekauft. Messer habe ich dabei. 

Wenn es weniger gut läuft, dauert es noch ein paar Tage. Auch wenn man es später vielleicht behauptet, ich sei suizidal, drogenabhängig, depressiv oder psychisch krank. Nein, das ist es ganz sicher nicht. Es ist eine freie Entscheidungen. Durch die Erfahrungen, die ich wiederholt machen musste, ist der Wunsch lange gereift. Ich fühl mich derzeit ganz normal. Eigentlich komisch. Ich esse und trinke und putze mir die Zähne, als sei alles wie immer.

Ich bin jetzt 10 Tage auf der Straße. Es gibt keinerlei Hilfe ohne den Versuch der Erniedrigung. Manche Menschen lassen alles mit sich machen. Ich gehöre nicht dazu. Gott vergibt, ich nicht. Wir aehen uns in der Hölle wieder.

Vielleicht wäre es allgemein eine gute Idee, wenn mehr Menschen wie ich sich einfach umbringen würden. Dann wäre dieses schlechte Schauspiel über den funktionierenden Staat, der sozial und fortschrittlich sein will, nicht nötig. Ein Massensuizid für die Demokratie und den Weltfrieden. Nur noch gute Menschen auf der Welt. Ich habe schließlich geschworen, das Recht und die Freiheit tapfer zu verteidigen. Hätte nicht gedacht, dass es ao kommt.

An die Familie sei nur gerichtet, dass ich ebenso enttäuscht bin wie vom Rest der Menschheit. Zu einer Beisetzung braucht ihr nicht kommen. Spart das Geld. Euch gehts ja immer so schlecht.
