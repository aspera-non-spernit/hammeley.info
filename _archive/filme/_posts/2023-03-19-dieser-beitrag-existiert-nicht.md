---
title: Réalité
layout: default
---
<hmmly-player id="reality" data-video-id="ORD63Pc9wvs"></hmmly-player>
<!-- more -->
Als Filmenthusiast, der in seinem Leben sicherlich schon über 1000 Filme gesehen hat, möchte ich heute einen der besten surrealistischsten Filme der 2010er Jahre vorstellen. Reality von Quentin Dupieux (Skript und Regie).

Wer sich beeilt, kann sich den Film sogar auf Youtube anschauen, bis er dort wahrscheinlich gelöscht wird.

[Réalité auf Youtube, 1:26:19](https://www.youtube.com/watch?v=ORD63Pc9wvs&origin=https://hammeley.info/filme/2023/03/19/dieser-beitrag-existiert-nicht/){: #ORD63Pc9wvs}

Wer das Gerne mag, muss den Film gesehen haben. Die Wiederkehrende ["Music with changing Parts"](https://www.youtube.com/watch?v=3Uy_Ag7ETA4&origin=https://hammeley.info/filme/2023/03/19/dieser-beitrag-existiert-nicht/){: #3Uy_Ag7ETA4} von Philipp Glass ist aus dem Jahr 1971. 

Übrigens: Mir ist der Titel entfallen, und habe für die Suche GPT3 benutzt. Obwohl ich die Handlung nicht mehr ganz richtig in Erinnerung hatte, konnte mir GPT3 nach einigen Hinweisen den Namen des Films nennen.