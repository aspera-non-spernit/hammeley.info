---
title: Capital B . - Wem gehört Berlin?
layout: default
category: ego
---
Eine sehr empfehlenswerte fünfteilige Doku über die berliner Wendezeit bis heute. Arte (verfügbar bis 1.10.2025)

<div class="ratio ratio-16x9">
    <iframe title="Capital&#x20;B&#x20;-&#x20;Wem&#x20;geh&#x00F6;rt&#x20;Berlin&#x3F;&#x20;&#x28;1&#x2F;5&#x29;" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" frameborder="0" scrolling="no" width="560" height="315" src="https://www.arte.tv/embeds/de/087960-001-A?autoplay=false&mute=0"></iframe>
</div>
