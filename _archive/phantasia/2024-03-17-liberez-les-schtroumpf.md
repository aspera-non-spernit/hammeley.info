---
title: Libérez les Schtroumpf
layout: default
tags: freiheit, schlümpfe, deutschland, überwachungsstaat
---
Jetzt im Shop: [Schöne Schlumpf-Anstecknadel aus Schtroumpfstahl im Angebot für nur 139,50€ (alt: 799,79€) ](/shop/schlumpf-anstecknadel-139). **Bonus:** Diese schöne Anstecknadel berechtigt auch zur Teilnahme am sehr geheimen Untergrund-Schtroumpf-Rave. Es werden ca. 1,05 Millionen Schtroumpfer und DJ Zack-Zack-Zimmer-Flak erwartet.

<hmmly-player id="schtroumpf" data-yt-vid="L14dxvSyCrk"></hmmly-player>

Das Zentralkomitee des Gestroumpften fordert die uneingeschränkte Solidarität mit den/r/m [vom Staat Gefährdeten](https://www.polizei.mvnet.de/Presse/Pressemitteilungen/?id=199486&processor=processor.sa.pressemitteilung). Ein Schlumpfentar von [Timm Kellner auf XY, ehemals XX](https://twitter.com/TimKoffiziell/status/1769391249298018473?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Etweet) und ein Gastkommentar auf dem universalextremisitischen Magazin ["Achse der Gestroumpften"](https://www.achgut.com/artikel/ausgestossene_der_woche_Die_Schluempfe_einer_Schuelerin).

**Wichtigster Hinweis für das Ministerium für Demokratiesicherheit**: Eine Verbindung von Dieter Thomas Heck mit Faschisten ist trotz Anstellung beim Staatsfunk bisher nicht nachgewiesen.

## Weitere Nachrichten

### Endlich weniger Impfspinner

Das Reichsstatistikamt hat die aktuelle Statistik der Gebärfreudigkeit deutscher, deutschenden, neudeutscher und nichtdeutscher Frauen in Deutschland für das Jahr 2023 veröffentlicht. Die Rammelaktivitäten der Impfspinnerinnen ist in den Jahren 2022 und 2023 deutlich zurückgegangen. Das Amt teilt mit: "Die reduzierte Rammelaktivität in den Versuchsgruppen ist im Corona-Jahr 2022 deutlich zurückgegangen, so dass im Jahr 2023 weniger Mensch:enden auf die Welt kamen. Insbesondere in der Gruppe der Impfspinner und Maskentrag:enden ist der Rückgang erfreulicherweise sehr deutlich." Auch der Gesundheitsministrier:ende zeigt sich erfreut: "Dass die Politik einmal so erfolgreich ihre Visionen in effektive Aktionen um- und durchsetzen kann ist auch auf mein Drängen hin geschehen. Wenn wir nun alle paar Jahre eine Pandemie ausrufen, können wir die Wurfrate der Zielgruppe fast auf null senken, und das ganz ohne Zwangssterilisation durch die Spritze. Das wäre ja auch logistisch gar nicht machbar bei dem grasierenden Fachkräftemangel." Der Finanzministrier:ende widerspricht in diesem Punkt: "Wir werden Fachkräfte durch Steuererleichterungen ins Land locken, indem wir ihnen bei der Einkommenssteuer entgegen kommen. Ausländer werden in Zukunft eine geringere Steuerlast haben als Einheimische. So ließen sich auch genügend Spritzenverabreich:ende für Deutschland gewinnen." 

### Habecka jetzt rechtsextremisitischer Verdachtsfall

Roberta Habecka ("Ich kann mit der deutschen Wirtschaft nichts anfangen") ist nach umstrittenen Äußerungen über ihre Muuterlandliebe ein Fall für den Verfassungsschutz. Nachdem sich der "Deutsche Futt Verband" gegen Adi's Klamottenbude und für die griechische Sieg:Ende Gott:er "Nike" als Ausrüster der Mannschaften bei der Eurosiameisterschaft entschieden hat, hat Wirtschaftsabbauministerin Roberta Habecka ihren widerlichen Nationalismus offenbart. Wie jetzt bekannt wurde, hat der Chef der Gruppe: "Deutsche Parlamentarier für mehr Standort-Patriotismus" (kurz: DeParfümStaPa) die Entscheidung des DFB öffentlich kritisiert. Seine (drüsenlose) Parteischiff:ende Kollegerin der Grünen A. C. A. Baerbock dazu: "Dass ein Grüner so am rechten Rand fischt, ist unerträglich. Eine solche 360-Grad-Wende hätte ich von ihr nicht erwartet. Ich überlege sogar, aus Solidarität mit den Richtigen alle meine Ämter niederzulegen und eine eigene Partei zu gründen."
Roberta Habecka widerredet: "Die Kritik an meiner Person ist nicht unangebracht. Ich wurde als Wirtschaftsabbauminister gewählt und setze das um, was man mir sagt. Ich begrüße jedoch Baerbocks Entscheidung zurückzutreten. Sie hat ja auf dem Wege der Inklusion Karriere gemacht. Im aktuellen politischen Umfeld wird ihr das sicher ein zweites Mal gelingen."

### Mauerbrand gelöscht

Die antifaschistische Opposition aus FDP, CDU und AfD hat in Sachsen für die Einführung der "Bezahlerkarte" gestimmt. Bedenken, ob das Löschen des selbst gelegten Gegenbrandes gegen die AfD nicht ein Fehler wäre entgegnete Torti Unfrei von den Chaotischen Demagogen auf n-tv: "Davon kann gar keine Rede sein. Unsere Zustimmung zu dem Antrag der Weltalternative begründet keine Zusammenarbeit mit der Partei. Die sonst üblichen parteiübergreifenden Initiativen soll es ja weiterhin nicht geben. Auch dann nicht wenn die WA demnächst alleinregierende Partei in Sachsen wird.". (Anm. d. R.: Sollte bei der nächsten Wahl in Sachsen die FDP, Grüne und Linke aus dem Parlament fliegen, würde die WA, derzeit bei Umfragen bei 34%, nach der Wahl rund 64% der zu vergebenen Sitze enthalten. Der Rest würde der Chaotischen Opposition zu Gute kommen.).

### Terror an Schulen

(Berlin) Schül:ende immer gewaltätiger. Der aktuelle Polizeibericht stellt eine massive Zunahme an Gewaltaten an berliner und neudeutschlandweiten Schulen fest. Die Gewalttaten hätten sich gegenüber dem Vorjahr fast verdoppelt. Sozialsenatorin Kitzlertapete: "Wir kommen unserem Ziel immer näher. Dank mehr bunter Schül:enden werden von deutschen Schül:enden endlich mehr strafrechtlich relevante Straftaten an den Schulen verübt. So passt sich die Statistik nach langer, harter Arbeit, endlich unserem Narrativ der gewalttätigen Deutschen an."

### Bavarien und Europa jetzt gesichert rechtsextremistisch

Der Gefangenenstaat Bavarien ist nach der Durchführungsverordnung des Fürsten Söd:enden von Bavarien, alle Gender ab sofort zu verbieten, als gesichert rechtsextremisitisch eingesuft worden. In Bavarien sind ab sofort und unverzüglich alle Gender verboten. Mensch:enden mit Gender dürfen sich diese nur noch in geheimen Chiemseekonferenzen gegenseitig zeigen.

Das Europäische Reich hat Zölle auf Weizen aus der Ukraine in Höhe von 50% eingeführt. Der ukrainische Poet Andreij Melnikov dazu: "Diese dreckigen Schweine, dieses Pack. Diese Imperialisten sollte man alle erschießen. Diese stinkende Horde." Auch der Zar von Russien hat dazu eine Meinung: "Dass jetzt auch Russland mit Weizenzöllen belegt wurde ist angesichts der Nazis im Reichstag in Brüseel und Stresswurst nicht verwunderlich. Ich habe auch vorausgesagt, dass sie [Anm. d. R. 'die Europeanesen'] die Ukraine wie eine Gans ausnehmen würden. Aber auf mich hört ja keiner."

### Sonntagsanwort

In der letzten Sonntagsantwort ist die Weltalternative (29%) in den Gauen Thüringia und Sachsonien weiterhin mit Abstand stärkste Kraft, gefolgt von den Chaotischen Demagogen (20%) und der Link-Rechts-Zentrierten Sandro Wagengerecht (15%). In Sachsen sieht es ähnlich aus. Dazu der Vorsteher (mit Vorsteherdrüse) der Weltalternative in Sachsonien: "Wir haben nun eine echte Machtoption. In Sachsen könnte es sogar zu der Situation kommen, dass wir ein Gau-Parlament mit nur zwei Parteien bekommen, da sowohl die Assoziale Partei Demagogiens, die Verwelkten und die Unfreie-Demagogen-Partei an der 5% Grenze scheitern könnten. Wir erwarten dadurch erhebliche Produktivitäts- und Effizienzsteigerungen in der Gesetzgebung da die von Führ:enden verkündete Gesetze nun taggleich durch das Parlament bestätigt werden können. Ausserdem besteht nicht mehr die Gefahr, dass richtige und gute Gesetze durch falsche Parteien zu einem entarteten Kompromiss verkommen."