---
title: "Über Dinge die sich ändern und gleichbleiben"
layout: ueber
cipher: cookieEncr
deepl: 2023-02-21
---
## Über den Verantwortlichen im Sinne der Wahrheit

![V.i.S.d.W.](){:class="rounded lazyload float-end" data-src="/assets/images/ego/zenumi.jpg" style="max-width: 25%"}

- ist 1977 als freier Mensch im ["Freien Berlin" (West)](https://www.youtube.com/watch?v=e4hxGVgj2gY) geboren
- ist in 1000 Berlin 62 aufgewachsen und dort zur Schule gegangen
- hat an vier Wohnorten mehr als ein Jahr verbracht (2x Berlin, 1x Bayern, 1x NRW)
- verbrachte die Kindheit und Jugend in Berlin - Schöneberg
- wohnte 15 Jahre, bis 2023, in Berlin - Friedrichshain
- verfügt über ein abgeschlossenes Erststudium (BSc (Hons) Computing and IT & Business), ein abgebrochenes Zweitstudium (MSc Computational Science an der Uni Potsdam) und ist derzeit Student der [Schwurbeluniversität](/schwurbelu)
- ist seit 2008 Russlandversteher
- ist "ungeimpft", "ungetestet" und ungenießbar
- ist seit 2021 Querdenker, Nazi:in, rechts, Rassist, Schwurbler, Staatsfeind, Hassverbreiter, Mitglied der Corona-RAF, Mörder, Pandemietreiber, homophob, transfeindlich, AFD-Wähler, altdeutschschreiber, dumm, asozialer Trittbrettfahrer,
- seit 2022 Atom-(7,0%), Kohle-(16,7%), und (Fracking)-Gas- (18,5%)Stromnutzer, Fleischfresser (ich spare dafür bei der Umwelt- und Weltenrettung) und fährt dafür extra regelmäßig und umweltschädlich nach Polen
- und hat überhaupt kein Problem damit, wenn 2/3 der Drecksmenschheit der Klimakatasrophe zum Opfer fallen würde
- seit Juni 2023 durch wiederholt rechtswidrige Bescheide (Entscheidung schriftlich beweisbar) des Jobcenters FHK (752) obdachlos und leider noch am Leben. Nichtmal Corona hatte er. Schweine- und Vogelgrippe auch nicht. Waldsterben, sauren Regen, Ozon und Feinstaub auch überlebt.
- Heil Hammeley!
- [Referenzen](#referenzen)

<div class="clearfix"></div>

### Wirkungsstätten des V.i.S.d.W.

![Wohnorte](){:class="clearfix rounded lazyload" data-src="/assets/images/wohn.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%"}

#### Über Berlin-Schöneberg

Die Heimat des jungen V.i.S.d.W. war Schöneberg, ein Bezirk Berlins mit ca. 120.000 Einwohnern + die 28.000 aus Friedenau.

Viele Persönlichkeiten lebten und leben in dem Bezirk:

![Schöneberg](){:class="rounded lazyload" data-src="/assets/images/schoeneberg.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%"}

aber auch:

- Edgar Fröse (Tangerine Dream), der in der Schwäbischen Straße seine musikalische Karriere begann,
- Graciano Rocchigiani, der berliner Boxweltmeister, der in der Hauptstraße augewachsen ist,
- Pierre Littbarski, der Fußballweltmeister von 1990, im Bezirk aufgwachsen ist und beim VfL Schöneberg seine Karriere begann,
- Hugo Egon Balder, der in der Semperstraße aufgewachsen ist,
- aber auch korrupte Politiker und Bundespräsidenten leben oder lebten in Schöneberg oder besitzen dort Eigentum

und sind mit dem Bezirk verbunden.

Auf dem Friedhof Großgörschenstraße sind beerdigt:

- Carl Bolle
- Die Brüder Grimm
- Rudolf Virchow
- Rio Reiser
- Graciano Rocchigiani
- eine Mitschülerin aus der Grundschule
- uvm.

Weltbewegende Ergeignisse fanden in Schöneberg statt:

- John F. Kennedy, hat am 26. Juni 1963 vor dem Rathaus Schöneberg den berühmten Satz "Ich bin ein Berliner" ausgesprochen,
- Terroranschlag auf die Diskothek La Belle am 5. April 1986

#### Über Berlin-Friedrichshain

Von 2008 an bis 2023 residierte der V.i.S.d.W. im Bezirk Friedrichshain, in der Nähe des Boxhagener Platzes. Im Gegensatz zu seiner ersten Heimat Schöneberg hat Friedrichshain nur wenige bekannte Persönlichkeiten, dafür aber eine umso bewegtere Geschichte. Auf dem [Friedhof der Märzgefallenen](https://de.wikipedia.org/wiki/Friedhof_der_M%C3%A4rzgefallenen) im Volkspark-Friedrichshain findet sich eine Gedenk- und Informationsstätte über die Märzrevolution von 1848.

Nach der Eingemeindung entwickelte sich der Bezirk schnell zu einer Arbeiter- und Kommunistenhochburg. [Karl Marx](https://de.wikipedia.org/wiki/Karl-Marx-Erinnerungsst%C3%A4tte_in_Berlin-Stralau) verbrachte während seines Studiums in Berlin auf Stralau seine Freizeit. Bereits 1895 fand in der Frankfurter Allee 102 eine Arbeiterversammlung statt, zu der auch [Wladimir Iljitsch Lenin](https://de.wikipedia.org/wiki/Datei:Gedenktafel_Frankfurter_Allee_102_(Frhai)_Wladimir_Iljitsch_Uljanow_Lenin.jpg) kam. In Lichtenberg (fast noch Friedrichshain) kam es 1918 zu Aufständen, bei der [marxistische Sozialisten](https://de.wikipedia.org/wiki/Rathauspark_(Berlin-Lichtenberg)) erschossen wurden.

Als die Nationalsozialisten im Kampf um die Macht im Reich immer stärker wurden, formierte sich im Viertel die Widerstandsbewegung. Da die Kommunisten den Machtkampf gegen die Nationalsozialisten verloren und viele Widerstandskämpfer ermordet wurden, konnten diese in ihrem weiteren Leben nicht weiter wirken.

Am [17. Juni 1953](https://youtu.be/uceY4ugAsjM?t=3570&origin=https://hammeley.info/ueber) fand der erste und mit dem von 1989 wohl bekannteste Aufstand in der DDR statt.

Nach der Wende kam es zu einer zweiten Hausbesetzer-Welle (nach der in den 70-80 Jahren in West-Berlin). Zahlreiche [Häuser im Bezirk wurden besetzt](https://www.berlin-besetzt.de/ "Berlin besetzt"). Einige wenige illegale Hausbesetzungen konnten sich bis zur Räumung der [Liebigstraße 34](https://de.wikipedia.org/wiki/Liebigstra%C3%9Fe_34_(Berlin)) im Oktober 2020 halten.

In der [Mainzer Straße (1990)](https://de.wikipedia.org/wiki/R%C3%A4umung_der_Mainzer_Stra%C3%9Fe) entstand das zweite ["Tuntenhaus"](https://de.wikipedia.org/wiki/Tuntenhaus_(Berlin)) der Stadt, nachdem das erste in der Bülowstraße 55 in Schöneberg 1983 geräumt und abgerissen worden war.

Im Gegensatz zum eher beschaulichen Schöneberg ist Friedrichshain seit Ende der 1990er Jahre für sein Nachtleben und seine Kneipen und Restaurants rund um den Boxhagener Platz bekannt. Auch der. V.i.S.d.W. besuchte bereits in den 1990ern den Bezirk.

Der V.i.S.d.W. verbringt seine Freizeit häufig auf der [Halbinsel Stralau](https://www.youtube.com/watch?v=auEf04Vg5ZA&origin=https://hammeley.info/ueber "Kampf um die Rummelsburger Bucht"), am Spreeufer an der Mühlenstraße (East Side Gallery), in Rummelsburg (im angrenzenden Lichtenberg) oder auch im benachbarten Treptow, wo sich der [Treptower Park](https://www.youtube.com/watch?v=-56OLa_iau4&origin=https://hammeley.info/ueber) und der Plänterwald befinden. Gelegentlich fährt er auch mit der Tram nach Karlshorst und schaut sich auf der [Trabrennbahn Karlshorst](https://www.youtube.com/watch?v=GwDtFOakD9s&origin=https://hammeley.info/ueber) Pferde oder spaziert durch die Wuhlheide.

![Friedrichshain](){:class="rounded lazyload" data-src="/assets/images/friedrichshain.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%"}

#### Obdachlosigkeit 

Das Jobcenter Friedrichshain hat mehrmals (vorsätzlich) rechtswidrige Bescheide erlassen, "Leistungen" verweigert und monatelang nicht überwiesen und mein Konto gepfändet, was mehrmals zu Obdachlosigkeit geführt hat.
Seit Juni 2023 lebe ich auf der Straße. Die eingereichten Widersprüche und Klagen blieben seit 2022 unbearbeitet und erfolglos.Im April 2024 wurde die Rechtswidrigkeit der Bescheide anerkannt. Mir wurde alles genommen. Nicht nur meine Wohnung, sondern auch meine Möbel, Musikintrumente, technische Geräte (Rechner), alle Dokumente (Zeugnisse, Abschlüsse), Bücher, Fotoalben aus der Kindheit, Kleidung, alles.

Nach 46 Jahren im besten, demokratischsten Rechtsstaat der Welt besitze ich derzeit zwei kurze Hosen, zwei Jeans, ein paar T-Shirts, Unterwäsche, ein Paar Schuhe. Ich wurde mehrmals auf der Straße und im Zug (von 'Fachkräften') beklaut, so dass ich teilweise nur noch das hatte, was ich an mir trug. HEIL H.

Täterinnen sind die Sachbearbeiterinnen des Jobcenters Friedrichshain-Kreuzberg Hahn, Mielke, Deutrich (752) und die faschistische und korrupte Regierung. Ich wünsche Ihnen alle einen langen qualvollen Tod auf den ich ein Bier, vielleicht sogar auch eine Flasche Champagner trinke. Weiter zu nennen sind D.R.K. (Dallas), A.L. (Charité) und weitere gute Menschen. Alle ihire Taten sind schriftlich für die Ewigkeit festgehalten. Selbst erarbeitet. Glückwunsch.

Ich bin weiterhin ohne festen Wohnsitz und 'lebe' seit mehreren Monaten in einem Land ohne Millionen von 'Fachkräften'. Hier gibt es wenige Barbershops und Spielcasinos, wenig Müll und weniger Asoziale (neudeutsch: gute Menschen).

Entgegen dem Wunsch in meinem letzten Testament möchte ich unter keinen Umständen in Berlin und wenn möglich auch nicht in Deutschland beigesetzt werden. Der Rest des Testaments bleibt unverändert.

#### Besuchte Länder

![Besuchte Länder](){:class="rounded lazyload" data-src="/assets/images/besuchte-laender.png" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%"}

### Genetik

#### Genetische Ausprägungen

Seit etwa zehn Jahren ist die Gendiagnostik auch für Normalsterbliche erschwinglich. Anfälligkeit für bestimmte Krankheiten, Lebenserwartung, körperliche Statur und Aussehen, Charakter, beruflicher, sportlicher und akademischer Erfolg lassen sich heute mit hoher Wahrscheinlichkeit durch die Gene vorhersagen.

Im Folgenden sind einige Eigenschaften des V.i.S.d.W. (Übermensch) aufgeführt, die durch seine Gene bestimmt werden:

Haplogruppe : U106 > U198 R1b1b2a1a1

![Gene](){:class="rounded lazyload" data-src="/assets/images/gene.png" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%"}

#### Genetische Herkunft des V.i.S.d.W

Die genetische Herkunft hängt, wie der Name schon sagt, sehr stark vom getesteten Genom, aber auch vom vorhandenen Datensatz aller getesteten Personen ab und kann sich daher in Zukunft um einige Prozentpunkte ändern.

Die dargestellten Chromosomen zeigen die Position der herkunftsbestimmenden Gene für die Regionen Schottland, Ägäische Inseln, Baltische Staaten und Frankreich. Für die Regionen Schweden/Dänemark, Osteuropa und Deutschland befinden sich die Gene auf vier, sechs und 22 Chromosomen. Die Chromosomen 2, 4, 5, 8, 10 und 20 sind vollständig und ausschließlich (beide Elternteile) deutscher Herkunft.

Im Vergleich zu anderen getesteten mitteleuropäischen Personen ist dies eine "normale" Zusammensetzung. Eine Ausnahme bildet vielleicht der 2%ige Anteil von den Ägäischen Inseln. Dieser lässt sich auf die [minoische Kultur](https://www.youtube.com/watch?v=nJUpw_wd-7I&origin=https://hammeley.info/ueber "Die untergegangene antike Kultur der Minoer auf der Insel des Minotaurus") zurückführen, die als sogenannte Inselhüpfer über das neolithische Westanatolien nach Südeuropa eingewandert sind.

Der V.i.S.d.W. ist auch mit den [Skythen](https://www.youtube.com/watch?v=Kbu7Q3FMJ5Q&origin=https://hammeley.info/ueber "History of the Scythians: an Ancient Nomadic Culture") verwandt, die zur Zeit des antiken Griechenlands das Gebiet östlich des Schwarzen Meeres bis zur heutigen Ukraine besiedelten.

Der V.i.S.d.W. ist also nicht nur Germane, sondern auch freiheitsschreiender Ossi und Kümmmeltürke.

![Genetik](){:class="rounded lazyload" data-src="/assets/images/genetik_2502.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%"}

## Über diese Seite

- historischer Abriss über Geschehnisse in einem Leben
- Aufklärung
- öffentliches Tagebuch
- Digitaler Nachlass
- Hobby IT-Projekt

Kleispenden zur Unterstützung der Seite werden gerne angenommen unter:

- Monero Chain (XMR)

![Monero XMR](){:class="rounded lazyload" data-src="/assets/images/wallet_48c3jD6UC1g3WxgeBjaAGZRadGgBNduNxKw4QCjujck3C79g7dbSU137fguv5ZrLTWdMbdYAzsz5ybzQvnYqF7iMU8sjLgi.png" style="display:block; margin-left:auto; margin-right:auto; max-width: 77%"}

### Kontakt

Probleme, Wünsche und Anregungen können an diese [eMail-Adresse](&#109;&#97;&#73;&#108;&#84;&#79;&#58;&#99;&#111;&#110;&#116;&#97;&#99;&#116;&#45;&#112;&#114;&#111;&#106;&#101;&#99;&#116;&#43;&#97;&#115;&#112;&#101;&#114;&#97;&#45;&#110;&#111;&#110;&#45;&#115;&#112;&#101;&#114;&#110;&#105;&#116;&#45;&#104;&#97;&#109;&#109;&#101;&#108;&#101;&#121;&#45;&#105;&#110;&#102;&#111;&#45;&#51;&#57;&#48;&#51;&#54;&#56;&#56;&#48;&#45;&#105;&#115;&#115;&#117;&#101;&#45;&#64;&#105;&#110;&#99;&#111;&#109;&#105;&#110;&#103;&#46;&#103;&#105;&#116;&#108;&#97;&#98;&#46;&#99;&#111;&#109;) geschickt werden.

### Datenerhebung und -verarbeitung

Seit dem 2.7.2023 wird auf hammeley.info piwik pro genutzt, um Informationen über die Nutzung der Seite zu sammeln. Dazu gehört die Interaktionen von Besuchern mit der Webseite: z. B. wie häufig das Menü aufgerufen wird un welche Seiten darüber geladen werden oder welche Suchbegriffe in das Suchfeld eingegeben werden und wie viele Suchergebnisse gefunden werden.

Gesammelte Daten werden nicht an Dritte weiter gegeben und nicht für Werbezwecke verwendet.